mkdir build
cd build

cmake -G "Ninja" ^
    -D CMAKE_INSTALL_PREFIX=%PREFIX% ^
    -D BUILD_SHARED_LIBS:BOOL=ON ^
    -D BUILD_EXAMPLES:BOOL=OFF ^
    -D BUILD_TESTING:BOOL=OFF ^
    -D ENABLE_HDF5:BOOL=OFF ^
    -D ENABLE_CBLAS:BOOL=ON ^
    -D ENABLE_OPENMP:BOOL=ON ^
    -DCMAKE_BUILD_TYPE=Release ^
    ..

ninja install
