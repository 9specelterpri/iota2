#!/usr/bin/env bash

set +x
set -e # Abort on error

[[ -d build ]] || mkdir build
cd build

export CMAKE_LIBRARY_PATH=${PREFIX}/lib:${CMAKE_LIBRARY_PATH}
export CMAKE_INCLUDE_PATH=${PREFIX}/include:${CMAKE_INCLUDE_PATH}

if [[ $target_platform =~ linux.* ]]; then
    export LDFLAGS="$LDFLAGS -Wl,-rpath-link,${PREFIX}/lib"
fi

cmake -G "Ninja" \
      -D CMAKE_INSTALL_PREFIX=$PREFIX \
      -D CMAKE_MACOSX_RPATH:BOOL=ON \
      -D CMAKE_OSX_SYSROOT=${CONDA_BUILD_SYSROOT:-OFF} \
      -D BUILD_SHARED_LIBS:BOOL=ON \
      -D BUILD_EXAMPLES:BOOL=OFF \
      -D BUILD_TESTING:BOOL=OFF \
      -D ENABLE_HDF5:BOOL=OFF \
      -D ENABLE_CBLAS:BOOL=ON \
      -D ENABLE_OPENMP:BOOL=ON \
      ..

ninja install
