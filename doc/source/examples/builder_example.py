#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
This script provides all function necessary to create a new builder, steps
and functions.
This is an example to introduce these basics to a new developer.
It does not allow to produce a product related to land use or remote sensing.
"""
import os
import random
import shutil
from collections import OrderedDict
from functools import partial
from typing import Optional

from iota2.common.file_utils import file_search_and as FSA
from iota2.configuration_files import read_config_file as rcf
from iota2.sequence_builders.i2_sequence_builder import i2_builder
from iota2.steps import iota2_step
from iota2.steps.iota2_step import Step, StepContainer

# =============================================================================
# This block regroups both function for creating directories and the
# corresponding step.
# In iota2 source code, the create_directories function should be placed in
# iota2.common modules and step_create_directories in iota2.steps
# =============================================================================


def create_directories(root: str) -> None:
    """
    Function for creating folders
    The tree structure never changes, because the folder names are used
     in the different steps.
    Parameters
    ----------
    root:
        the high level output path
    """
    if os.path.exists(root):
        shutil.rmtree(root)
    os.mkdir(root)
    folders = ["segmentation", "statistics", "model", "classif", "merge_tiles", "final"]
    for folder in folders:
        if not os.path.exists(os.path.join(root, folder)):
            os.mkdir(os.path.join(root, folder))


class step_create_directories(iota2_step.Step):
    """
    This step will be the workflow first step.
    The step create all directories asked if not exists

    """

    # Define which ressources block is readed for launching this step
    resources_block_name = "create_directory"

    def __init__(self, cfg, cfg_resources_file):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        output_path = rcf.read_config_file(self.cfg).getParam("chain", "output_path")

        task = self.i2_task(
            task_name="create_directories",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={"f": create_directories, "root": output_path},
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(task, "first_task")

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Construct output directories"
        return description


# =============================================================================
#
# =============================================================================


def write_segments_by_tiles(output_path: str, tile: str, seed: int = 0) -> None:
    """
    Write a file containing a list of segments.
    The number of segment is random.

    Parameters
    ----------
    tile :
        A tile name
    seed :
        Fix the random seed
    """
    folder = os.path.join(output_path, "segmentation")
    # fix the random seed for reproducibility,
    if seed != 0:
        random.seed(seed)

    # pick a random number between 0 and 50
    rand = random.randint(1, 11)
    with open(os.path.join(folder, f"{tile}.txt"), "w") as tile_file:
        chain = [f"Segment_{tile}_{i}" for i in range(int(rand))]
        tile_file.write("\n".join(chain))


class step_write_segment(iota2_step.Step):
    """
    This step will be the workflow second step.
    The step simulate a segmentation by tiles

    """

    # Define which ressources block is readed for launching this step
    resources_block_name = "segmentation"

    def __init__(self, cfg, cfg_resources_file):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        output_path = rcf.read_config_file(self.cfg).getParam("chain", "output_path")
        tiles = rcf.read_config_file(self.cfg).getParam("MyParams", "tiles").split(" ")
        seed = rcf.read_config_file(self.cfg).getParam("MyParams", "seed")

        for tile in tiles:
            task = self.i2_task(
                task_name=f"writing_segment_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": write_segments_by_tiles,
                    "output_path": output_path,
                    "tile": tile,
                    "seed": seed,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="segment writing",
                task_sub_group=f"compute {tile}",
                task_dep_dico={"first_task": []},
            )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Compute the segmentation by tiles"
        return description


# =============================================================================
#
# =============================================================================


def compute_zonal_stats(output_path: str, tile: str) -> None:
    """
    For a given tile, simulate the zonal stats computation.
    """
    in_folder = os.path.join(output_path, "segmentation")
    out_folder = os.path.join(output_path, "statistics")
    with open(os.path.join(out_folder, f"Stats_{tile}.txt"), "w") as new_file:
        with open(os.path.join(in_folder, f"{tile}.txt")) as file_tile:
            contents = file_tile.read().split("\n")
            for line in contents:
                chain = line.replace("Segment", "Stats") + "\n"
                new_file.write(chain)


class step_compute_zonal_stats(iota2_step.Step):
    """
    This step will be the third in the workflow.
    It is not possible to predict the number of file produced before
    the execution.
    """

    resources_block_name = "statistics"

    def __init__(self, cfg, cfg_resources_file):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        output_path = rcf.read_config_file(self.cfg).getParam("chain", "output_path")
        tiles = rcf.read_config_file(self.cfg).getParam("MyParams", "tiles").split(" ")

        for tile in tiles:

            task = self.i2_task(
                task_name=f"zonal_stats_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": compute_zonal_stats,
                    "output_path": output_path,
                    "tile": tile,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="zonal_stats",
                task_sub_group=f"compute {tile}",
                task_dep_dico={"segment writing": [f"compute {tile}"]},
            )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Compute zonal stats"
        return description


# =============================================================================
#
# =============================================================================


def training_model(output_path: str) -> None:
    """
    Simulate the training of a classifier
    """
    model_file = os.path.join(output_path, "model", "Model_global.txt")
    # get all stats files
    stats_files = FSA(os.path.join(output_path, "statistics"), True, "Stats", ".txt")
    with open(model_file, "w") as model:
        model.write(str(len(stats_files)))


class step_training(iota2_step.Step):
    """
    This step will be the third in the workflow.
    It is not possible to predict the number of file produced before
    the execution.
    """

    resources_block_name = "training"

    def __init__(self, cfg, cfg_resources_file):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        output_path = rcf.read_config_file(self.cfg).getParam("chain", "output_path")
        tiles = rcf.read_config_file(self.cfg).getParam("MyParams", "tiles").split(" ")
        # Waiting for all tile files and launch an unique task
        # We need to compute the dependency outside the task definition
        dep = []
        for tile in tiles:
            dep.append(f"compute {tile}")
        task = self.i2_task(
            task_name="training",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={"f": training_model, "output_path": output_path},
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(
            task,
            task_group="training",
            task_sub_group="training",
            task_dep_dico={"zonal_stats": dep},
        )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Training"
        return description


# =============================================================================
#
# =============================================================================


def classify(output_path: str, tile: str, segment_number: int) -> None:
    """
    For a given tile, simulate the classification of each segment.
    This classifier algorithm is able to classify only one segment at the
    same time and need to write the output in a temporary file.
    """
    in_folder = os.path.join(output_path, "statistics")
    out_folder = os.path.join(output_path, "classif")
    model = os.path.join(output_path, "model", "Model_global.txt")
    with open(os.path.join(in_folder, f"Stats_{tile}.txt")) as file_tile:
        contents = file_tile.read().split("\n")
        for i, line in enumerate(contents):
            if i == segment_number:
                with open(
                    os.path.join(out_folder, f"classif_{tile}_{i}.txt"), "w"
                ) as sub_file:
                    chain = f"{model} " + line.replace("Stats", "Classif") + "\n"
                    sub_file.write(chain)


class step_classification(iota2_step.Step):
    """
    This step will be the third in the workflow.
    It is not possible to predict the number of file produced before
    the execution.
    """

    resources_block_name = "split_segment"

    def __init__(self, cfg, cfg_resources_file):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # initialised by the builder when calling the second container
        dict_nb_segment = Step.dico_message_interface
        print(dict_nb_segment)
        output_path = rcf.read_config_file(self.cfg).getParam("chain", "output_path")
        tiles = rcf.read_config_file(self.cfg).getParam("MyParams", "tiles").split(" ")

        for tile in tiles:
            count = dict_nb_segment[tile]
            for i in range(count):
                task = self.i2_task(
                    task_name=f"classification_{tile}_{i}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": classify,
                        "output_path": output_path,
                        "tile": tile,
                        "segment_number": i,
                    },
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="classif",
                    task_sub_group=f"classif {tile} {i}",
                    task_dep_dico={},
                )
                # task_dep_dico is empty as it is the first step of new container

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Classification"
        return description


# #############################################################################
#
# #############################################################################


def merge_classification_by_tile(output_path: str, tile: str) -> None:
    # get all files created for this tile
    in_folder = os.path.join(output_path, "classif")
    out_folder = os.path.join(output_path, "merge_tiles")
    files = FSA(in_folder, True, f"classif_{tile}_", ".txt")

    files.sort()
    with open(os.path.join(out_folder, f"result_{tile}.txt"), "w") as outfile:
        for fil in files:
            with open(fil) as infile:
                outfile.write(infile.read())


class step_merge_classification_by_tile(iota2_step.Step):
    """
    The last step of the workflow
    For each file found merge the content in a unique file
    The focus point is that it is not possible to know how many previous
    we are waiting for.
    """

    resources_block_name = "merge_files"

    def __init__(self, cfg, cfg_resources_file):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        output_path = rcf.read_config_file(self.cfg).getParam("chain", "output_path")
        tiles = rcf.read_config_file(self.cfg).getParam("MyParams", "tiles").split(" ")
        # initialised and created by the builder when calling the container
        dict_nb_segment = Step.dico_message_interface

        for tile in tiles:
            dep = [f"classif {tile} {i}" for i in range(dict_nb_segment[tile])]
            task = self.i2_task(
                task_name=f"merge_subset_segment_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": merge_classification_by_tile,
                    "output_path": output_path,
                    "tile": tile,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="merge files",
                task_sub_group=f"merge files {tile}",
                task_dep_dico={"classif": dep},
            )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge"
        return description


# =============================================================================
#
# =============================================================================


def mosaic(output_path: str) -> None:

    in_folder = os.path.join(output_path, "merge_tiles")
    out_folder = os.path.join(output_path, "final")
    files = FSA(in_folder, True, ".txt")
    files.sort()
    with open(os.path.join(out_folder, "final.txt"), "w") as outfile:
        for fil in files:
            with open(fil) as infile:
                outfile.write(infile.read())


class step_mosaic(iota2_step.Step):
    """
    Merge all classification into a final product
    """

    resources_block_name = "mosaic"

    def __init__(self, cfg, cfg_resources_file):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        output_path = rcf.read_config_file(self.cfg).getParam("chain", "output_path")
        tiles = rcf.read_config_file(self.cfg).getParam("MyParams", "tiles").split(" ")

        dep = [f"merge files {tile}" for tile in tiles]
        task = self.i2_task(
            task_name="mosaic",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": mosaic,
                "output_path": output_path,
            },
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(
            task,
            task_group="mosaic",
            task_sub_group="mosaic",
            task_dep_dico={"merge files": dep},
        )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Mosaic"
        return description


# =============================================================================
# Builder
# =============================================================================


class i2_example_builder(i2_builder):
    """ """

    def __init__(
        self,
        cfg: str,
        config_resources: str,
        schduler_type: str,
        restart: bool = False,
        tasks_states_file: Optional[str] = None,
        hpc_working_directory: str = "TMPDIR",
    ):
        super().__init__(
            cfg,
            config_resources,
            schduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )

        self.config_resources = config_resources
        # create dict for control variables
        self.control_var = {}
        # steps group definitions
        self.steps_group["global"] = OrderedDict()

        self.sort_step()

    def get_dir(self):
        """
        usage : return iota2_directories
        """
        directories = [
            "segmentation",
            "statistics",
            "model",
            "classif",
            "merge_tiles",
            "final",
        ]

        iota2_outputs_dir = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )

        return [os.path.join(iota2_outputs_dir, d) for d in directories]

    def get_number_of_segment_by_tiles(self):
        """Function to determine the number of segment by tile"""
        segmentation_directory = os.path.join(self.output_i2_directory, "segmentation")

        files = FSA(segmentation_directory, True, ".txt")
        dic_count = {}
        for fil in files:
            tile = os.path.basename(fil).split(".")[0]
            num_lines = sum(1 for line in open(fil))
            dic_count[tile] = num_lines
        Step.dico_message_interface = dic_count

    def build_steps(
        self, cfg: str, config_ressources: Optional[str] = None
    ) -> list[StepContainer]:
        """
        build steps
        """
        s_container_init = StepContainer(name="segmentation_init")
        s_container_segment = StepContainer(name="classification_segment")

        s_container_segment.prelaunch_function = partial(
            self.get_number_of_segment_by_tiles
        )

        s_container_init.append(
            partial(step_create_directories, self.cfg, self.config_resources), "global"
        )
        s_container_init.append(
            partial(step_write_segment, self.cfg, self.config_resources), "global"
        )
        s_container_init.append(
            partial(step_compute_zonal_stats, self.cfg, self.config_resources), "global"
        )
        s_container_init.append(
            partial(step_training, self.cfg, self.config_resources), "global"
        )

        s_container_segment.append(
            partial(step_classification, self.cfg, self.config_resources), "global"
        )
        s_container_segment.append(
            partial(step_merge_classification_by_tile, self.cfg, self.config_resources),
            "global",
        )
        s_container_segment.append(
            partial(step_mosaic, self.cfg, self.config_resources), "global"
        )
        return [s_container_init, s_container_segment]
