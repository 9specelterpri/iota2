i2_obia advanced uses
=====================

Input segmentation
******************

The OBIA workflow accept three kind of input segmentation.
All of them requires to use the :ref:`obia_segmentation_path <i2_obia.obia.obia_segmentation_path>` parameter.

This parameter accept three values:

- ``None`` or parameter not in configuration file: the workflow use :ref:`SLIC <autocontext_pixels>` to produce the segmentation

- ``path/segmentation_name.tif``: a tif image covering all tiles requested. In this case, the segmentation will be vectorized and clipped over each tiles.

- ``path/segmentation_name.shp`` : a vector file (shp or sqlite).

**In both last cases**, the segmentation can be sparse. For tif input, the no-data must be set to 0. For vector data, holes are simply not considered. There is no filtering done in OBIA, then the segmentation must be ready to use before launching the chain.

Eco-climatic regions
********************

The use of eco-climatic regions is available with OBIA.
As for pixel classification, samples are associated with an unique region.
More :ref:`information <obia_eco_climatic_region>` about how samples and segments are associated to regions.

A additionnal parameter is available: :ref:`region_priority <i2_obia.obia.region_priority>`, to fix the order of regions are viewed. Otherwise, the numerical order is used.

Zonal Statistics
****************

The parameter :ref:`stats_used <i2_obia.obia.stats_used>` allow the user to choose which zonal statistics are used for learning and classification steps. Obviously, the statistics used for learning must be the same for classification.

There are five statistics always computed by OTB application, but by default only the ``mean`` is used. This parameter is limited by default as the user must be careful with the dimension of data generated. Look at :ref:`this section <compute_zonal_stats>` for more information.

To enable the choose of statistics, simply add in the configuration file the parameter :ref:`stats_used <i2_obia.obia.stats_used>` in the ``obia`` section.

.. code-block:: console

    obia:{

        buffer_size:2000
        stats_used: ["mean", "std"]
	}


