Two data-set are available, containing minimal data required to run Iota2 builders:

* A entire Sentinel 2 tile, with two dates (`8.8 Go <https://docs.iota2.net/data/IOTA2_TEST_S2.tar.bz2>`_)
* An extraction of Sentinel 2 data, with three dates over different eco-climatic region (Soon)
 
The archive contains:

.. raw:: html
   :file: interactive-tree-root.html

.. container:: interactive-tree-source

	* /XXXX/IOTA2_TEST_S2
		archive content
			content of the tutorial archive after content extraction
		* ! external_code
			python code folder
				user custom python code used for external features / feature maps
			* external_code.py
				python code file
					contains user python code used to produce the spectral indices. Rules for creating user code are explained :doc:`in external features page <external_features>`.
		* ! IOTA2_Outputs
			output folder
				folder used for iota2 output folders
			* (empty)
		* sensor_data
			input raster data
				the directory which contains Sentinel-2 data. These data **must** be stored by tiles as in the archive.
			* T31TCJ
				* ! SENTINEL2A_20180511-105804-037_L2A_T31TCJ_D_V1-7
					* MASKS
						* SENTINEL2A_20180511-105804-037_L2A_T31TCJ_D_V1-7_*.tif
					* SENTINEL2A_20180511-105804-037_L2A_T31TCJ_D_V1-7_FRE_B*.tif
					* SENTINEL2A_20180511-105804-037_L2A_T31TCJ_D_V1-7_FRE_STACK.tif
				* ! SENTINEL2A_20180521-105702-711_L2A_T31TCJ_D_V1-7
					* MASKS
						* SENTINEL2A_20180521-105702-711_L2A_T31TCJ_D_V1-7_*.tif
					* SENTINEL2A_20180521-105702-711_L2A_T31TCJ_D_V1-7_FRE_B*.tif
					* SENTINEL2A_20180521-105702-711_L2A_T31TCJ_D_V1-7_FRE_STACK.tif
			* T31TDJ
				* ! SENTINEL2A_20180511-105804-037_L2A_T31TDJ_D_V1-7
					* MASKS
						* SENTINEL2A_20180511-105804-037_L2A_T31TDJ_D_V1-7_*.tif
					* SENTINEL2A_20180511-105804-037_L2A_T31TDJ_D_V1-7_ATB_R1.tif
					* SENTINEL2A_20180511-105804-037_L2A_T31TDJ_D_V1-7_FRE_B*.tif
					* SENTINEL2A_20180511-105804-037_L2A_T31TDJ_D_V1-7_FRE_STACK.tif
					* SENTINEL2A_20180511-105804-037_L2A_T31TDJ_D_V1-7_MTD_ALL.xml
					* SENTINEL2A_20180511-105804-037_L2A_T31TDJ_D_V1-7_QKL_ALL.jpg
				* ! SENTINEL2A_20180521-105702-711_L2A_T31TDJ_D_V1-7
					* MASKS
						* SENTINEL2A_20180521-105702-711_L2A_T31TDJ_D_V1-7_*.tif
					* SENTINEL2A_20180521-105702-711_L2A_T31TDJ_D_V1-7_ATB_R1.tif
					* SENTINEL2A_20180521-105702-711_L2A_T31TDJ_D_V1-7_FRE_B*.tif
					* SENTINEL2A_20180521-105702-711_L2A_T31TDJ_D_V1-7_FRE_STACK.tif
					* SENTINEL2A_20180521-105702-711_L2A_T31TDJ_D_V1-7_MTD_ALL.xml
					* SENTINEL2A_20180521-105702-711_L2A_T31TDJ_D_V1-7_QKL_ALL.jpg
		* vector_data
			input vector data
				directory containing input vector data
			* reference_data.cpg
			* reference_data.dbf
			* reference_data.prj
			* reference_data.shp
				shapefile
					the shapeFile containing geo-referenced and labelled polygons (no multi-polygons, no overlapping) used to train a classifier.
			* reference_data.shx
			* EcoRegion.dbf
			* EcoRegion.prj
			* EcoRegion.qpj
			* EcoRegion.shp
				shapefile
					shapeFile containing two geo-referenced polygons representing a spatial stratification (eco-climatic areas, for instance).
			* EcoRegion.shx
		* colorFile.txt
			color table
				.. container:: desc

					colors used in classification map

					.. code-block:: console
					 
						$ cat colorFile.txt
						...
						211 255 85 0
						...

					Here the class **211** has the **RGB** color **255 85 0**.
		* IOTA2_Example.cfg
			example config file
				the file used to set iota2's parameters such as inputs/outputs paths, classifier parameters etc.
		* i2_tutorial_classification.cfg
			sample config file
				sample config file for classification builder
		* i2_tutorial_features_map.cfg
			sample config file
				sample config file for feature map builder
		* i2_tutorial_obia.cfg
			sample config file
				sample config file for object base image analysis
		* nomenclature23.txt
			nomenclature file
				.. container:: desc
		
					label's name. The purpose of the file is to get a pretty results report at the end of the chain by relabeling integers labels by a more verbose type.

					.. code-block:: console

						& cat nomenclature.txt
						...
						prairie:211
						...

					Here the class **211** corresponds to the class **prairie**
		* vecteur_23.qml
	