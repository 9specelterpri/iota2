# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module containing all natively usable pytorch neural networks in iota2"""

from typing import Optional

import torch
import torch.nn as nn
import torch.nn.functional as F


class ANNFailInheritance(nn.Module):
    """Dummy class for tests purposes."""

    def __init__(
        self,
        nb_features: int,
        nb_class: int,
        layer: int = 1,
        doy_sensors_dic: Optional[dict] = None,
    ):
        super().__init__()
        self.nb_class = nb_class
        self.layer = layer
        self.sensors_doy = doy_sensors_dic
        self.fc1 = nn.Linear(in_features=nb_features, out_features=16)
        self.fc2 = nn.Linear(in_features=16, out_features=12)
        self.output = nn.Linear(in_features=12, out_features=nb_class)

    def forward(self, s2_theia):  # pylint: disable=C0116
        # flat values due to flat input nn first layer
        values = s2_theia.reshape(-1, s2_theia.shape[1] * s2_theia.shape[-1])
        values = F.relu(self.fc1(values))
        values = F.relu(self.fc2(values))
        values = torch.softmax(self.output(values), dim=1)
        return values
