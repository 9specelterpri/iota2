<img src="assets/iota2-logo.png" width="100">

Infrastructure pour l'Occupation des sols par Traitement Automatique Incorporant les Orfeo Toolbox Applications - iota<sup>2</sup> 

Getting started with iota2? Look at the online documentation:

- https://docs.iota2.net/

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
