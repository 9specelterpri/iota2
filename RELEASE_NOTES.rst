iota2-v20220610 - Changes since version v20210602
------------------------------------------------

Features added
**************

*      Upgrade to OTB 8.0.0
            OTB version 8.0.0 (https://www.orfeo-toolbox.org/CookBook-8.0/) is used in iota2
			issue: https://framagit.org/iota2-project/iota2/-/issues/430
*      Deep Learning workflow
            iota2 is able to train neural networks and use them for inference using Pytorch.
            issue: https://framagit.org/iota2-project/iota2/-/issues/194
            doc: https://docs.iota2.net/develop/deep_learning.html
*      Python 3.9
            OTB's upgrade allows us to use a recent Python: from 3.6 to 3.9. 
            Most of our libraries are also upgraded to their most recent version.
            issue: https://framagit.org/iota2-project/iota2/-/issues/430
*      CONDA entry points
            CONDA entry points allow us to clean the PATH environement variable
            issue: https://framagit.org/iota2-project/iota2/-/issues/429
*      Sampling rates
            iota2 generates one sampling rates csv file per model.
            issue https://framagit.org/iota2-project/iota2/-/issues/388
            doc: https://docs.iota2.net/develop/iota2_samples_management.html#tracing-back-the-actual-number-of-samples
*      iota2 keeps the user label column
            The column containing the user labels is now tracked in each temporary iota2 database
            issue: https://framagit.org/iota2-project/iota2/-/issues/366
*      External features with padding
            External features come with a padding option, chunks can have an overlap.
            issue: https://framagit.org/iota2-project/iota2/-/issues/466
            doc: https://docs.iota2.net/develop/external_features.html
*      Early check of user-provided external feature function
            The function provided by the user to feed the iota2 external features is checked before iota2's launch
            issue: https://framagit.org/iota2-project/iota2/-/issues/438
*      External features with parameters
            The function provided by the user can receive parameters defined in the config file
            issue: https://framagit.org/iota2-project/iota2/-/issues/393
            doc: https://docs.iota2.net/develop/external_features.html#examples
*      About co-registration
            Co-registration workflow has been removed from iota2 waiting for an OTB fix
            issue: https://framagit.org/iota2-project/iota2/-/issues/463
*      Classify irregular time series
            issue: https://framagit.org/iota2-project/iota2/-/issues/317
*      Release of the duplicated dates constraint
            If the gapfilling is used, duplicate dates are automatically merged.
            issue: https://framagit.org/iota2-project/iota2/-/issues/431
*       Tiled exogenous data for external features
            Using exogenous data on multiple tiles was broken.
            There is now a tool to retile the data so it can be used on multiple tiles
            issue: https://framagit.org/iota2-project/iota2/-/issues/472
            doc: https://docs.iota2.net/develop/i2_features_map_builder.html#external-features
*       Scheduler
            Rename "local" scheduler_type to "debug"
*      Vectorization workflow
            Vectorization of large areas (test on French territory)
	    Simplifying the vectorization tutorial
	    Use geopandas for geometric operations (simplification and buffer)	    
*      Nodata value in external features
            The value reprensenting the absence of data can now be set in the config file.
            issue: https://framagit.org/iota2-project/iota2/-/issues/467
            doc: https://docs.iota2.net/develop/i2_features_map_builder.html#external-features
*      Documentation
	   - Hosting the doc at https://docs.iota2.net/master/ instead of  http://osr-cesbio.ups-tlse.fr/oso/donneeswww_TheiaOSO/iota2_documentation/master
	   - Documentation improvement: Sentinel-1 missing doc, interactive tree, etc.
       - Using sphinx-multiversion instead of sphinx-versionning
       - Output tree structure cleanup and interactive version
	   

Bugs fixed
**********

*      Misplaced logs
            https://framagit.org/iota2-project/iota2/-/issues/514
*      Lower columns name
            https://framagit.org/iota2-project/iota2/-/issues/444
*      Artefact in maps
            https://framagit.org/iota2-project/iota2/-/issues/458
*      Hardcoded pixel type
            https://framagit.org/iota2-project/iota2/-/issues/375
*      Feature map without S2
            https://framagit.org/iota2-project/iota2/-/merge_requests/67



iota2-v20210203 - Changes since version v20200309
-------------------------------------------------

Features added
**************

*      Be able to change raster's resolution (issue 212)
            Using the parameter "spatial_resolution" users are able to set iota2 working resolution
*      Enable custom features, defined by a python function
            Allow users to provide a python function to create new features (which will be used for learning and classification stages)
*      Code style
            Use yapf to standardize python scripts
*      Remove configuration file from every iota2 function (API).
            Most iota2 functions are usable without using a configuration file
*      New scheduling workflow and builder design
            Use Dask to schedule tasks
*      New documentation architecture and host
            Migration from readthedocs.io to http://osr-cesbio.ups-tlse.fr
*      Refactoring of the configuration file
            Standardization and reorganization of parameters between sections. Old configuration files are automatically detected and parsed to the new shape.
*      Re-encoding of labels
            Iota2 is able to deal with string labels
*      Check if enough dates are available before running iota2
            New parameter "minimum_required_dates"
*      Launch iota2 only on sensor data already processed by iota2 (i.e: s2_output_path becomes s2_path)


Bugs fixed
**********

*     BUG FIX: alter user database (convert the datafield to lowercase)
*     BUG FIX: issue #213 random segfault when ExportImage
*     BUG FIX: data augmentation step gets wrong files (issue #215)
*     BUG FIX: CSV file separator (issue 216)
*     BUG FIX: Sentinel L3A usage (issue 259)


iota2-v20200309 - Changes since version v0.5.1
----------------------------------------------

Features added
**************

* check iota2 inputs
* iota2 can use autoContext workflow
* iota2 can use scikit-learn machine-learning algorithms
* iota2 can use cross-validation algorithm on scikit-learn models
* installation thanks to conda
* generate probability map
* fusion post classification of optical and SAR data
* fusion of classifications
* available sensors:
    - Sentinel-1
    - Sentinel-2
        L2A format, THEIA + PEPS
        L3A format
    - Landsat-5 old format
    - Landsat-5 new format 
    - Landsat-8 old format
    - Landsat-8 new format
    - User features  
* post classification tools
    - regularisation
        - majority
        - adaptive
    - vectorisation / simplification /smooth
    - zonal statistics

Bugs fixed
**********
