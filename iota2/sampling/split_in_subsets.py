# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import argparse
import logging
import random

from osgeo import ogr

from iota2.vector_tools import vector_functions as vf

logger = logging.getLogger("distributed.worker")


def get_random_poly(
    layer, field, classes, ratio, region_field, regions, random_seed=None
):
    """Use to randomly split samples in learning and validation considering classes in regions.

    Parameters
    ----------
    layer :
    field : string
        data field
    classes : list
    ratio : float
        ratio number of validation polygons / number of training polygons
    region_field : string
        region field
    regions : list
        list of regions to consider
    random_seed : int
        random seed (default = None)
    Returns
    -------
    tuple(sample_id_learn, sample_id_valid)
        where sample_id_learn is a set of polygon's ID dedicated to learn models
        where sample_id_valid is a set of polygon's ID dedicated to validate models
    """
    sample_id_learn = []
    sample_id_valid = []
    layer.ResetReading()
    for region in regions:
        for cl in classes:
            listid = []
            layer.SetAttributeFilter(None)
            attrib_filter = f"{field}={cl} AND {region_field}='{region}'"
            layer.SetAttributeFilter(attrib_filter)
            featureCount = float(layer.GetFeatureCount())
            if featureCount == 1:
                for feat in layer:
                    _id = feat.GetFID()
                    sample_id_learn.append(_id)
                    feature_clone = feat.Clone()
                    layer.CreateFeature(feature_clone)
                    sample_id_valid.append(feature_clone.GetFID())
                    break

            elif featureCount > 1:
                polbysel = round(featureCount * float(ratio))
                polbysel = max(polbysel, 1)
                for feat in layer:
                    _id = feat.GetFID()
                    listid.append(_id)
                    listid.sort()

                random.seed(random_seed)
                random_id_learn = random.sample(listid, int(polbysel))
                sample_id_learn += [fid for fid in random_id_learn]
                sample_id_valid += [
                    currentFid
                    for currentFid in listid
                    if currentFid not in sample_id_learn
                ]

    sample_id_learn.sort()
    sample_id_valid.sort()

    layer.SetAttributeFilter(None)
    return set(sample_id_learn), set(sample_id_valid)


def split_in_subsets(
    vecto_file,
    data_field,
    region_field,
    ratio=0.5,
    seeds=1,
    driver_name="SQLite",
    learning_flag="learn",
    validation_flag="validation",
    split_groundtruth=True,
    random_seed=None,
):
    """
    This function is dedicated to split a shape into N subsets
    of training and validations samples by adding a new field
    by subsets (seed_X) containing 'learn', 'validation'

    Parameters
    ----------

    vecto_file : string
        input vector file
    data_field : string
        field which discriminate class
    region_field : string
        field which discriminate region
    ratio : int
        ratio between learn and validation features
    seeds : int
        number of random splits
    driver_name : string
        OGR layer name
    learning_flag : string
        learning flag
    validation_flag : string
        validation flag
    crossValidation : bool
        enable cross validation split
    split_groundtruth
        enable the ground truth split
    random_seed : int
        random seed
    """
    driver = ogr.GetDriverByName(driver_name)
    source = driver.Open(vecto_file, 1)
    layer = source.GetLayer(0)

    class_avail = vf.get_field_element(
        vecto_file,
        driver_name=driver_name,
        field=data_field,
        mode="unique",
        elem_type="int",
    )
    region_avail = vf.get_field_element(
        vecto_file,
        driver_name=driver_name,
        field=region_field,
        mode="unique",
        elem_type="str",
    )
    all_fields = vf.get_all_fields_in_shape(vecto_file, driver=driver_name)

    fid_area = [(f.GetFID(), f.GetGeometryRef().GetArea()) for f in layer]
    fid = [fid_ for fid_, area in fid_area]

    id_learn = []
    id_val = []

    for seed in range(seeds):
        source = driver.Open(vecto_file, 1)
        layer = source.GetLayer(0)

        seed_field_name = "seed_" + str(seed)
        seed_field = ogr.FieldDefn(seed_field_name, ogr.OFTString)

        if seed_field_name not in all_fields:
            layer.CreateField(seed_field)

        random_seed_number = None
        if random_seed is not None:
            random_seed_number = random_seed + seed
        id_learn, id_val = get_random_poly(
            layer,
            data_field,
            class_avail,
            ratio,
            region_field,
            region_avail,
            random_seed_number,
        )

        if split_groundtruth is False:
            id_learn = id_learn.union(id_val)
        for i in fid:
            flag = None
            if i in id_learn:
                flag = learning_flag
            elif i in id_val:
                flag = validation_flag

            feat = layer.GetFeature(i)
            feat.SetField(seed_field_name, flag)
            layer.SetFeature(feat)
        i = layer = None


if __name__ == "__main__":
    DESCRIPTION = (
        "This function is dedicated to split a shape into N subsets"
        "of training and validations samples by adding a new field"
        "by subsets (seed_X) containing 'learn' or 'validation'"
    )
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(
        "-vector", dest="vecto_file", help="path to the vector file", required=True
    )
    parser.add_argument(
        "-data_field",
        dest="data_field",
        help="field in vector file to consider",
        required=True,
    )
    parser.add_argument(
        "-ratio",
        dest="ratio",
        help="ratio = learning/validation (0 < ratio < 1)",
        required=False,
        type=float,
        default=0.5,
    )
    parser.add_argument(
        "-seeds",
        dest="seeds",
        help="number of subsets",
        required=False,
        type=int,
        default=1,
    )

    args = parser.parse_args()
    split_in_subsets(args.vecto_file, args.data_field, args.ratio, args.seeds)
