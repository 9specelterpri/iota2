# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import logging
import os

from osgeo import ogr

from iota2.common import file_utils as fu
from iota2.common.utils import run
from iota2.vector_tools import merge_files as mf
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def add_field_model(shp_in, mod_num, field_out, logger=LOGGER):
    """
    add a field to a shapeFile and for every feature, add an ID
    IN :
        - shp_in : a shapeFile
        - mod_num : the number to associate to features
        - field_out : the new field name
    OUT :
        - an update of the shape file in
    """
    source = ogr.Open(shp_in, 1)
    layer = source.GetLayer()
    new_field = ogr.FieldDefn(field_out, ogr.OFTInteger)
    layer.CreateField(new_field)

    for feat in layer:
        if feat.GetGeometryRef():
            layer.SetFeature(feat)
            feat.SetField(field_out, mod_num)
            layer.SetFeature(feat)
        else:
            logger.debug(f"feature {feat.GetFID()} has not geometry")


def create_model_shape_from_tiles(
    tiles_model,
    path_tiles,
    output_path,
    out_shp_name,
    field_out,
    working_directory,
    logger=LOGGER,
):
    """
    create one shapeFile where all features belong to a model number according to the model description

    IN :
        - tiles_model : a list of list which describe which tile belong to which model
            ex : for 3 models
                tile model 1 : D0H0,D0H1
                tile model 2 : D0H2,D0H3
                tile model 3 : D0H4,D0H5,D0H6

                tiles_model = [["D0H0","D0H1"],["D0H2","D0H3"],["D0H4","D0H5","D0H6"]]
        - path_tiles : path to the tile's envelope with priority consideration
            ex : /xx/x/xxx/x
            /!\ the folder which contain the envelopes must contain only the envelopes   <========
        - output_path : path to store the resulting shapeFile
            ex : x/x/x/xxx
        - out_shp_name : the name of the resulting shapeFile
            ex : "model"
        - field_out : the name of the field which will contain the model number
            ex : "Mod"
        - working_directory : path to working directory (not mandatory, due to cluster's architecture default = None)

    OUT :
        a shapeFile which contains for all feature the model number which it belong to
    """
    if working_directory is None:
        path_to_tmp = output_path + "/AllTMP"
    else:
        # HPC case
        path_to_tmp = working_directory
    if not os.path.exists(path_to_tmp):
        run("mkdir " + path_to_tmp)

    to_remove = []
    for i in range(len(tiles_model)):
        for j in range(len(tiles_model[i])):
            to_remove.append(
                vf.rename_shapefile(path_tiles, tiles_model[i][j], "", "", path_to_tmp)
            )

    all_tile_path = []
    all_tile_path_er = []

    for i in range(len(tiles_model)):
        for j in range(len(tiles_model[i])):
            try:
                _ = all_tile_path.index(path_tiles + "/" + tiles_model[i][j] + ".shp")
            except ValueError:
                all_tile_path.append(path_to_tmp + "/" + tiles_model[i][j] + ".shp")
                all_tile_path_er.append(
                    path_to_tmp + "/" + tiles_model[i][j] + "_ERODE.shp"
                )

    for i in range(len(tiles_model)):
        for j in range(len(tiles_model[i])):
            current_tile = path_to_tmp + "/" + tiles_model[i][j] + ".shp"
            add_field_model(current_tile, i + 1, field_out, logger)

    for path in all_tile_path:
        vf.erode_shapefile(path, path.replace(".shp", "_ERODE.shp"), 0.1)

    mf.mergeVectors(out_shp_name, output_path, all_tile_path_er)
    if not working_directory:
        run("rm -r " + path_to_tmp)
    else:
        for rm_shp in to_remove:
            vf.remove_shape(
                rm_shp.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"]
            )


def generate_region_shape(
    envelope_directory: str,
    output_region_file: str,
    out_field_name: str,
    i2_output_path: str,
    working_directory: str,
    logger=LOGGER,
) -> None:
    """Generate regions shape.

    Parameters
    ----------
    envelope_directory: str
        directory containing all iota2 tile's envelope
    output_region_file: str
        output file
    out_field_name: str
        output field containing region
    i2_output_path: str
        iota2 output path
    working_directory: str
        path to a working directory
    """
    region = []
    all_tiles = fu.file_search_and(envelope_directory, False, ".shp")
    region.append(all_tiles)

    if not output_region_file:
        output_region_file = os.path.join(i2_output_path, "MyRegion.shp")

    p_f = output_region_file.replace(" ", "").split("/")
    out_name = p_f[-1].split(".")[0]

    path_mod = ""
    for i in range(1, len(p_f) - 1):
        path_mod = path_mod + "/" + p_f[i]

    create_model_shape_from_tiles(
        region,
        envelope_directory,
        path_mod,
        out_name,
        out_field_name,
        working_directory,
        logger,
    )
