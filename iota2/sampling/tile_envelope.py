"""Module which handle tile's priority."""


# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import logging
import os
import shutil
from typing import List

from osgeo import gdal, ogr, osr
from osgeo.gdalconst import *

from iota2.common import file_utils as fu
from iota2.common.file_utils import get_raster_resolution
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


class Tile:
    """Represent a tile."""

    def __init__(self, path, name, test_mode=False):
        gtif = gdal.Open(path)
        self.path = path
        if not test_mode:
            self.x = float(gtif.GetGeoTransform()[0])
        else:
            self.x = 0.0
        if not test_mode:
            self.y = float(gtif.GetGeoTransform()[3])
        else:
            self.y = 0.0
        self.name = name
        self.envelope = "None"
        self.priority_env = "None"

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_path(self):
        return self.path

    def get_name(self):
        return self.name

    def set_envelope(self, env):
        self.envelope = env

    def get_envelope(self):
        return self.envelope

    def get_origin(self):
        return self.x, self.y

    def get_priority_env(self):
        return self.priority_env

    def set_priority_env(self, priority):
        self.priority_env = priority

    def set_x(self, x_val):
        self.x = x_val

    def set_y(self, y_val):
        self.y = y_val


def create_shape(min_x, min_y, max_x, max_y, out, name, proj=2154):
    """Create a shape with only one geometry (a rectangle)."""
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(min_x, min_y)
    ring.AddPoint(max_x, min_y)
    ring.AddPoint(max_x, max_y)
    ring.AddPoint(min_x, max_y)
    ring.AddPoint(min_x, min_y)

    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)

    driver = ogr.GetDriverByName("ESRI Shapefile")
    try:
        output = driver.CreateDataSource(out)
    except ValueError:
        raise Exception("Could not create output datasource " + out)

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(proj)
    new_layer = output.CreateLayer(name, geom_type=ogr.wkbPolygon, srs=srs)
    if new_layer is None:
        raise Exception("Could not create output layer")

    new_layer.CreateField(ogr.FieldDefn("FID", ogr.OFTInteger))
    new_layer_def = new_layer.GetLayerDefn()
    feature = ogr.Feature(new_layer_def)
    feature.SetGeometry(poly)
    ring.Destroy()
    poly.Destroy()
    new_layer.CreateFeature(feature)

    output.Destroy()


def substract_shape(shape1, shape2, shapeout, name_shp, proj):
    """
    shape 1 - shape 2 in shapeout/nameshp.shp
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_source_1 = driver.Open(shape1, 0)
    data_source_2 = driver.Open(shape2, 0)

    layer1 = data_source_1.GetLayer()
    for feature1 in layer1:
        geom_1 = feature1.GetGeometryRef()

    layer2 = data_source_2.GetLayer()
    for feature2 in layer2:
        geom_2 = feature2.GetGeometryRef()

    newgeom = geom_1.Difference(geom_2)
    poly = ogr.Geometry(ogr.wkbPolygon)

    # -----------------
    # -- Create output file
    try:
        output = driver.CreateDataSource(shapeout)
    except ValueError:
        raise Exception("Could not create output datasource " + str(shapeout))

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(proj)

    new_layer = output.CreateLayer(name_shp, geom_type=ogr.wkbPolygon, srs=srs)
    if new_layer is None:
        raise Exception("Could not create output layer")

    new_layer.CreateField(ogr.FieldDefn("FID", ogr.OFTInteger))
    new_layer_def = new_layer.GetLayerDefn()
    feature = ogr.Feature(new_layer_def)
    feature.SetGeometry(newgeom)
    newgeom.Destroy()
    poly.Destroy()
    new_layer.CreateFeature(feature)

    output.Destroy()


def create_raster_footprint(
    tile_path: str, common_vec_mask: str, padding_size: int = 0, erode_unit: int = 4
):

    common_vec_mask_tmp = common_vec_mask.replace(".shp", "_tmp.shp")
    vf.keep_biggest_area(tile_path.replace(".tif", ".shp"), common_vec_mask_tmp)

    x_res, _ = get_raster_resolution(tile_path)
    vf.erode_or_dilate_shapefile(
        infile=common_vec_mask_tmp,
        outfile=common_vec_mask,
        buffdist=-(erode_unit + padding_size) * x_res,
    )
    driver = ogr.GetDriverByName("ESRI Shapefile")
    driver.DeleteDataSource(common_vec_mask_tmp)
    return common_vec_mask


def is_intersect(shp1, shp2):
    """
    IN :
        shp1,shp2 : 2 tile's envelope (only one polygon by shapes)
    OUT :
        intersect : true or false
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_source_1 = driver.Open(shp1, 0)
    data_source_2 = driver.Open(shp2, 0)

    layer1 = data_source_1.GetLayer()
    for feature1 in layer1:
        geom1 = feature1.GetGeometryRef()
    layer2 = data_source_2.GetLayer()
    for feature2 in layer2:
        geom2 = feature2.GetGeometryRef()

    intersection = geom1.Intersection(geom2)
    if intersection.GetArea() != 0:
        return True
    return False


def get_shape_extent(shape: str):
    """Get vector file extent and return the envelope geometry object
    Parameters
    ----------
    shape:
        the input shapefile
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_source = driver.Open(shape, 0)
    layer = data_source.GetLayer()
    for feature in layer:
        geom = feature.GetGeometryRef()
    return geom.GetEnvelope()  # [min_x, max_x, min_y, max_y]


def erode_inter(current_tile, next_tile, intersection, buff, proj):

    xo, yo = current_tile.get_origin()
    xn, yn = next_tile.get_origin()
    extent = get_shape_extent(intersection)

    if yo == yn and xo != xn:  # left priority
        min_x = extent[0]
        max_x = extent[1] - buff
        min_y = extent[2]
        max_y = extent[3]

    elif yo != yn and xo == xn:  # upper priority
        min_x = extent[0]
        max_x = extent[1]
        min_y = extent[2] + buff
        max_y = extent[3]

    else:
        return False

    vf.remove_shape(intersection.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"])
    path_folder = "/".join(
        intersection.split("/")[0 : len(intersection.split("/")) - 1]
    )
    create_shape(
        min_x,
        min_y,
        max_x,
        max_y,
        path_folder,
        intersection.split("/")[-1].replace(".shp", ""),
        proj,
    )
    return True


def diag(current_tile, next_tile) -> bool:
    xo, yo = current_tile.get_origin()
    xn, yn = next_tile.get_origin()
    if not (yo == yn and xo != xn) and not (yo != yn and xo == xn):
        return True
    return False


def priority_key(item):
    """
    IN :
        item [list of Tile object]
    OUT :
        return tile origin (upper left corner) in order to manage tile's priority
    """
    return (-item.get_y(), item.get_x())  # upper left priority


def erode_diag(current_tile, next_tile, intersection, buff, tmp, proj):

    xo, yo = current_tile.get_origin()  # tuile la plus prio
    xn, yn = next_tile.get_origin()
    extent = get_shape_extent(intersection)  # [min_x, max_x, min_y, max_y]

    if yo > yn and xo > xn:
        min_x = extent[1] - buff
        max_x = extent[1]
        min_y = extent[2]
        max_y = extent[3]

        vf.remove_shape(
            intersection.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"]
        )
        path_folder = "/".join(
            intersection.split("/")[0 : len(intersection.split("/")) - 1]
        )
        create_shape(
            min_x,
            min_y,
            max_x,
            max_y,
            path_folder,
            intersection.split("/")[-1].replace(".shp", ""),
            proj,
        )

        tmp_name = next_tile.get_name() + "_TMP"
        substract_shape(next_tile.get_priority_env(), intersection, tmp, tmp_name, proj)

        vf.remove_shape(
            next_tile.get_priority_env().replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        vf.cp_shape_file(
            tmp + "/" + tmp_name.replace(".shp", ""),
            next_tile.get_priority_env().replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        vf.remove_shape(
            tmp + "/" + tmp_name.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"]
        )

        tmp_name = current_tile.get_name() + "_TMP"
        substract_shape(
            current_tile.get_priority_env(),
            next_tile.get_priority_env(),
            tmp,
            tmp_name,
            proj,
        )

        vf.remove_shape(
            current_tile.get_priority_env().replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        vf.cp_shape_file(
            tmp + "/" + tmp_name.replace(".shp", ""),
            current_tile.get_priority_env().replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        vf.remove_shape(
            tmp + "/" + tmp_name.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"]
        )

    if yo > yn and xo < xn:

        tmp_name = next_tile.get_name() + "_TMP"
        substract_shape(
            next_tile.get_priority_env(),
            current_tile.get_priority_env(),
            tmp,
            tmp_name,
            proj,
        )

        vf.remove_shape(
            next_tile.get_priority_env().replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        vf.cp_shape_file(
            tmp + "/" + tmp_name.replace(".shp", ""),
            next_tile.get_priority_env().replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        vf.remove_shape(
            tmp + "/" + tmp_name.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"]
        )


def gen_tile_env_prio(obj_list_tile, tmp_file, proj, padding_size):

    buff = 600  # offset in order to manage nodata in image's border

    obj_list_tile.reverse()
    list_shp = [
        create_raster_footprint(
            c_obj_list_tile.get_path(),
            tmp_file + "/" + c_obj_list_tile.get_name() + ".shp",
            padding_size,
        )
        for c_obj_list_tile in obj_list_tile
    ]

    for env, current_tile in zip(list_shp, obj_list_tile):
        current_tile.set_envelope(env)
        current_tile.set_priority_env(env.replace(".shp", "_PRIO.shp"))
        vf.cp_shape_file(
            env.replace(".shp", ""),
            env.replace(".shp", "") + "_PRIO",
            [".prj", ".shp", ".dbf", ".shx"],
        )

    for i in range(len(obj_list_tile)):
        current_tile_env = obj_list_tile[i].get_envelope()
        for j in range(1 + i, len(obj_list_tile)):
            next_tile_env = obj_list_tile[j].get_envelope()
            if is_intersect(current_tile_env, next_tile_env):

                inter_name = (
                    obj_list_tile[i].get_name()
                    + "_inter_"
                    + obj_list_tile[j].get_name()
                )
                intersection = vf.clip_vector_data(
                    obj_list_tile[i].get_envelope(),
                    obj_list_tile[j].get_envelope(),
                    tmp_file,
                    inter_name,
                )
                not_diag = erode_inter(
                    obj_list_tile[i], obj_list_tile[j], intersection, buff, proj
                )
                if not_diag:
                    tmp_name = obj_list_tile[i].get_name() + "_TMP"
                    substract_shape(
                        obj_list_tile[i].get_priority_env(),
                        intersection,
                        tmp_file,
                        tmp_name,
                        proj,
                    )

                    vf.remove_shape(
                        obj_list_tile[i].get_priority_env().replace(".shp", ""),
                        [".prj", ".shp", ".dbf", ".shx"],
                    )
                    vf.cp_shape_file(
                        tmp_file + "/" + tmp_name.replace(".shp", ""),
                        obj_list_tile[i].get_priority_env().replace(".shp", ""),
                        [".prj", ".shp", ".dbf", ".shx"],
                    )
                    vf.remove_shape(
                        tmp_file + "/" + tmp_name.replace(".shp", ""),
                        [".prj", ".shp", ".dbf", ".shx"],
                    )

    obj_list_tile.reverse()
    for i in range(len(obj_list_tile)):
        current_tile_env = obj_list_tile[i].get_envelope()
        for j in range(1 + i, len(obj_list_tile)):
            next_tile_env = obj_list_tile[j].get_envelope()
            if is_intersect(current_tile_env, next_tile_env):
                if diag(obj_list_tile[i], obj_list_tile[j]):
                    inter_name = (
                        obj_list_tile[i].get_name()
                        + "_inter_"
                        + obj_list_tile[j].get_name()
                    )
                    intersection = vf.clip_vector_data(
                        obj_list_tile[i].get_envelope(),
                        obj_list_tile[j].get_envelope(),
                        tmp_file,
                        inter_name,
                    )
                    erode_diag(
                        obj_list_tile[i],
                        obj_list_tile[j],
                        intersection,
                        buff,
                        tmp_file,
                        proj,
                    )
                else:
                    tmp_name = obj_list_tile[i].get_name() + "_TMP"
                    substract_shape(
                        obj_list_tile[i].get_priority_env(),
                        obj_list_tile[j].get_priority_env(),
                        tmp_file,
                        tmp_name,
                        proj,
                    )

                    vf.remove_shape(
                        obj_list_tile[i].get_priority_env().replace(".shp", ""),
                        [".prj", ".shp", ".dbf", ".shx"],
                    )
                    vf.cp_shape_file(
                        tmp_file + "/" + tmp_name.replace(".shp", ""),
                        obj_list_tile[i].get_priority_env().replace(".shp", ""),
                        [".prj", ".shp", ".dbf", ".shx"],
                    )
                    vf.remove_shape(
                        tmp_file + "/" + tmp_name.replace(".shp", ""),
                        [".prj", ".shp", ".dbf", ".shx"],
                    )


def generate_shape_tile(
    tiles: List[str], path_wd: str, output_path: str, proj: int, padding_size: int
) -> None:
    """generate tile's envelope with priority management

    Parameters
    ----------
    tiles : list
        list of tiles envelopes to generate
    path_out : str
        output directory
    path_wd : str
        working directory
    output_path : str
        iota2 output directory
    proj : int
        epsg code of target projection
    """
    path_out = os.path.join(output_path, "envelope")
    if not os.path.exists(path_out):
        os.mkdir(path_out)
    features_path = os.path.join(output_path, "features")

    cmask_name = "MaskCommunSL"
    for tile in tiles:
        if not os.path.exists(features_path + "/" + tile):
            os.mkdir(features_path + "/" + tile)
            os.mkdir(features_path + "/" + tile + "/tmp")
    common_directory = path_out + "/commonMasks/"
    if not os.path.exists(common_directory):
        os.mkdir(common_directory)

    common = [
        features_path + "/" + Ctile + "/tmp/" + cmask_name + ".tif" for Ctile in tiles
    ]

    obj_list_tile = [
        Tile(current_tile, name) for current_tile, name in zip(common, tiles)
    ]
    obj_list_tile_sort = sorted(obj_list_tile, key=priority_key)

    tmp_file = path_out + "/TMP"

    if path_wd:
        tmp_file = path_wd + "/TMP"
    if not os.path.exists(tmp_file):
        os.mkdir(tmp_file)
    gen_tile_env_prio(obj_list_tile_sort, tmp_file, proj, padding_size)
    all_prio = fu.file_search_and(tmp_file, True, "_PRIO.shp")
    for prio_tile in all_prio:
        tile_name = prio_tile.split("/")[-1].split("_")[0]
        vf.cp_shape_file(
            prio_tile.replace(".shp", ""),
            path_out + "/" + tile_name,
            [".prj", ".shp", ".dbf", ".shx"],
        )

    shutil.rmtree(tmp_file)
    shutil.rmtree(common_directory)
