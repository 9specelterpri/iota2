#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from iota2.vector_tools import vector_functions as vf

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("usage: <shapefile>")
        sys.exit(1)
    else:
        print(vf.get_nb_feat(sys.argv[1]))
        sys.exit(0)
