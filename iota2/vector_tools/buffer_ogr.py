"""Apply a buffer of a defined distance on features of an input shapefile and create a new shapefile with same attributes."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys

from osgeo import ogr


def buffer_poly(
    inputfn, output_buffer_fn="", buffer_dist="0", outformat="ESRI Shapefile"
):

    buffer_dist = float(buffer_dist)
    inputds = ogr.Open(inputfn)
    inputlyr = inputds.GetLayer()

    shpdriver = ogr.GetDriverByName(outformat)
    if os.path.exists(output_buffer_fn):
        shpdriver.DeleteDataSource(output_buffer_fn)
    output_bufferds = shpdriver.CreateDataSource(output_buffer_fn)
    bufferlyr = output_bufferds.CreateLayer(
        output_buffer_fn, srs=inputlyr.GetSpatialRef(), geom_type=ogr.wkbPolygon
    )
    feature_defn = bufferlyr.GetLayerDefn()

    in_layer_defn = inputlyr.GetLayerDefn()
    for i in range(0, in_layer_defn.GetFieldCount()):
        field_defn = in_layer_defn.GetFieldDefn(i)
        bufferlyr.CreateField(field_defn)

    for feature in inputlyr:
        ingeom = feature.GetGeometryRef()
        geom_buffer = ingeom.Buffer(buffer_dist)
        if geom_buffer.GetArea() != 0:
            out_feature = ogr.Feature(feature_defn)
            out_feature.SetGeometry(geom_buffer)
            # copy input value
            for i in range(0, feature_defn.GetFieldCount()):
                out_feature.SetField(
                    feature_defn.GetFieldDefn(i).GetNameRef(), feature.GetField(i)
                )

            bufferlyr.CreateFeature(out_feature)

    return bufferlyr


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        parser = argparse.ArgumentParser(
            description="Apply a buffer of a defined distance"
            "on features of an input shapefile and create a new shapefile with same attributes"
        )
        parser.add_argument(
            "-s",
            dest="inshapefile",
            action="store",
            help="Input shapefile",
            required=True,
        )
        parser.add_argument(
            "-o",
            dest="outshapefile",
            action="store",
            help="Ouput shapefile",
            required=True,
        )
        parser.add_argument(
            "-b", dest="buff", action="store", help="Buffer size (m)", default="0"
        )
        parser.add_argument(
            "-format",
            dest="outformat",
            action="store",
            default="ESRI Shapefile",
            help="output format (default : ESRI Shapefile)",
        )
        args = parser.parse_args()
        buffer_poly(args.inshapefile, args.outshapefile, args.buff, args.outformat)
