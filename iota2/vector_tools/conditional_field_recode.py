"""Add a field of a shapefile and populate it with a user defined value based on another field value condition."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys

from osgeo import ogr

from iota2.vector_tools import change_name_field as cnf
from iota2.vector_tools import delete_field as df
from iota2.vector_tools import vector_functions as vf


def con_field_recode(shapefile, fieldin, fieldout, valin, valout):
    """Use only in nomenclature_harmonisation in prepare_samples.py which is not used."""
    data_src = ogr.Open(shapefile, 1)
    lyr = data_src.GetLayer()

    fieldList = vf.get_fields(lyr)
    try:
        indfield = fieldList.index(fieldin)
    except Exception:
        raise Exception(
            (
                f"The field {fieldin} does not exist in the input shapefile",
                f"You must choose one of these existing fields : {' / '.join(fieldList)}",
            )
        )

    # Field type
    in_layer_defn = lyr.GetLayerDefn()
    field_type_code = in_layer_defn.GetFieldDefn(indfield).GetType()
    field_type = in_layer_defn.GetFieldDefn(indfield).GetFieldTypeName(field_type_code)

    changefield = False
    if fieldout.lower() in [x.lower() for x in fieldList]:
        print(
            (
                f"Field '{fieldout}' already exists. Existing value "
                f"of {fieldout} field will be changed !!!"
            )
        )
    else:
        if fieldout.lower() in [x.lower() for x in fieldList]:
            changefield = True

            # find index and field name
            fieldlisttmp = [x.lower() for x in fieldList]
            i = fieldlisttmp.index(fieldout.lower())
            fieldtodel = fieldList[i]

            fieldoutoriginal = fieldout
            fieldout = "_" + fieldout
        try:
            new_field = ogr.FieldDefn(fieldout, ogr.OFTInteger)
            lyr.CreateField(new_field)
            print(f"Field '{fieldout}' created")
        except Exception:
            print(f"Error while creating field '{fieldout}'")
            sys.exit(-1)

    if field_type != "String":
        lyr.SetAttributeFilter(fieldin + "=" + str(valin))
        if lyr.GetFeatureCount() != 0:
            try:
                change_value_field(lyr, fieldout, valout)
                print(f"Field '{fieldout}' populated with {valout} value")
            except Exception:
                print(f"Error while populate field '{fieldout}'")
                sys.exit(-1)
        else:
            print(f"The value '{valin}' does not exist for the field '{fieldin}'")
    else:
        lyr.SetAttributeFilter(fieldin + "='" + str(valin) + "'")
        if lyr.GetFeatureCount() != 0:
            try:
                change_value_field(lyr, fieldout, valout)
                print(f"Field '{fieldout}' populated with {valout} value")
            except Exception:
                print(f"Error while populate field '{fieldout}'")
                sys.exit(-1)
        else:
            print(f"The value '{valin}' does not exist for the field '{fieldin}'")

    if changefield:
        df.deleteField(shapefile, fieldtodel)
        cnf.change_name(shapefile, fieldout, fieldoutoriginal)

    data_src.Destroy()


def change_value_field(layer, field, value):
    for feat in layer:
        layer.SetFeature(feat)
        feat.SetField(field, value)
        layer.SetFeature(feat)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        parser = argparse.ArgumentParser(
            description="Add a field of a shapefile "
            "and populate it with a user defined value based on another field value condition"
        )
        parser.add_argument(
            "-s",
            dest="shapefile",
            action="store",
            help="Input shapefile",
            required=True,
        )
        parser.add_argument(
            "-if",
            dest="ifield",
            action="store",
            help="Existing field for value condition",
            required=True,
        )
        parser.add_argument(
            "-of",
            dest="ofield",
            action="store",
            help="Field to create and populate",
            required=True,
        )
        parser.add_argument(
            "-vc",
            dest="cvalue",
            action="store",
            help="Value condition on the input field",
            required=True,
        )
        parser.add_argument(
            "-vp",
            dest="pvalue",
            action="store",
            help="Value to populate in the new field",
            required=True,
        )
        args = parser.parse_args()
        con_field_recode(
            args.shapefile, args.ifield, args.ofield, args.cvalue, args.pvalue
        )
