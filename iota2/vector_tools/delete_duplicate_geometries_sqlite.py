"""Find geometries duplicates based on sqlite method."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sqlite3 as lite
import sys

from iota2.common.utils import run
from iota2.vector_tools import vector_functions as vf


def delete_duplicate_geometries_sqlite(
    shapefile,
    outformat="ESRI Shapefile",
    do_corrections=True,
    output_file=None,
    quiet_mode=False,
):
    """Check if a features is duplicates, then if it does it will not be copied in output shapeFile.

    Parameters
    ----------
    input_shape : string
        input shapeFile
    do_correction : bool
        flag to remove duplicates
    output_shape : string
        output shapeFile, if set to None output_shape = input_shape
    quiet_mode : bool
        flag to print information
    Return
    ------
    tuple
        (output_shape, duplicates_features_number) where duplicates_features_number
        is the number of duplicated features
    """
    if outformat != "SQLite":
        tmpnamelyr = "tmp" + os.path.splitext(os.path.basename(shapefile))[0]
        tmpname = f"{tmpnamelyr}.sqlite"
        outsqlite = os.path.join(os.path.dirname(shapefile), tmpname)
        run(f"ogr2ogr -f SQLite {outsqlite} {shapefile} -nln tmp")
        lyrin = "tmp"
    else:
        lyrin = vf.get_layer_name(shapefile, "SQLite")
        outsqlite = shapefile

    conn = lite.connect(outsqlite)
    cursor = conn.cursor()
    cursor.execute(f"select count(*) from {lyrin}")
    nbfeat0 = cursor.fetchall()

    cursor.execute("create temporary table to_del (ogc_fid int, geom blob);")
    cursor.execute(
        f"insert into to_del(ogc_fid, geom) select min(ogc_fid), "
        f"GEOMETRY from {lyrin} group by GEOMETRY having count(*) > 1;"
    )
    cursor.execute(
        f"delete from {lyrin} where exists(select * from to_del where "
        f"to_del.geom = {lyrin}.GEOMETRY and to_del.ogc_fid <> {lyrin}.ogc_fid);"
    )
    cursor.execute(f"select count(*) from {lyrin}")
    nbfeat1 = cursor.fetchall()
    nb_dupplicates = int(nbfeat0[0][0]) - int(nbfeat1[0][0])

    if do_corrections:
        conn.commit()

        if output_file is None and outformat != "SQLite":
            run(f"rm {shapefile}")

        shapefile = output_file if output_file is not None else shapefile

        if output_file is None and outformat != "SQLite":
            run(f"ogr2ogr -f 'ESRI Shapefile' {shapefile} {outsqlite}")

        if nb_dupplicates != 0:
            if quiet_mode is False:
                print(
                    "Analyse of duplicated features done. "
                    f"{nb_dupplicates} duplicates found and deleted"
                )
        else:
            if quiet_mode is False:
                print("Analyse of duplicated features done. No duplicates found")

        cursor = conn = None

    if outformat != "SQLite":
        os.remove(outsqlite)
    return shapefile, nb_dupplicates


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        parser = argparse.ArgumentParser(
            description="Find geometries duplicates based on sqlite method"
        )

        parser.add_argument(
            "-in",
            dest="inshape",
            action="store",
            help="Input shapefile to analyse",
            required=True,
        )

        args = parser.parse_args()

        delete_duplicate_geometries_sqlite(args.inshape)
