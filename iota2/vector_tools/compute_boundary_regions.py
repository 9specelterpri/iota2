#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module for boundary fusion step."""
import logging
import os
import shutil
from typing import Optional, Tuple, Union

import geopandas as gpd
import itk
import numpy as np

from iota2.common import file_utils as fut
from iota2.common import otb_app_bank as otb

LOGGER = logging.getLogger("distributed.worker")


def get_sorted_regions_from_df(dataframe, field):
    """Return the numerically sorted regions."""
    all_values = dataframe[field].unique()
    # ensure that regions are sorted in numerical order
    all_values = [int(reg) for reg in all_values]
    all_values.sort()
    all_values = [str(reg) for reg in all_values]
    return all_values


def shape_to_otb(shape: str,
                 ref: str,
                 background: int = 0,
                 foreground: int = 1):
    """Convert a vector file to a otb application.

    Parameters
    ----------
    shape:
        the vector file to rasterize
    ref:
        the raster reference for rasterization
    background:
        set a value for no data (no geometry)
    foreground:
        set a value for valid data
    """
    rasterize = otb.CreateRasterizationApplication({
        "in":
        shape,
        "out":
        shape.replace("shp", "tif"),
        "im":
        ref,
        "background":
        background,
        "mode":
        "binary",
        "mode.binary.foreground":
        foreground,
        "pixType":
        "uint8"
    })
    rasterize.Execute()

    return rasterize


def compute_distance_map(region_file: str,
                         common_mask: str,
                         exterior_buffer_size: int,
                         output_path: str,
                         masks_region_path: str,
                         tile: str,
                         region_field: str,
                         epsilon: float,
                         interior_buffer_size: int,
                         spatial_res: Tuple[Union[float, int], Union[float,
                                                                     int]],
                         working_directory: Optional[str] = None,
                         logger=LOGGER) -> None:
    """Compute the distance map from regions.

    Parameters
    ----------
    region_file:
        region vector file
    common_mask:
        common mask of the tile
    exterior_buffer_size:
        the size in meters of the exterior buffer (outside the region)
    output_path:
        output folder to store results
    masks_region_path:
        path where classification masks are stored
    tile:
        the tile name
    region_field:
        the region column name in vector file
    epsilon:
        the lower limit (0 can create holes in map)
        to be stored it must be valid once multiplied by 1000
    interior_buffer_size:
        the size in meters for the interior buffer (inside the region)
    spatial_res:
        the spatial resolution
    working_directory:
        folder to store intermediate results
    """
    fut.ensure_dir(output_path)
    fut.ensure_dir(masks_region_path)
    wdir = working_directory if working_directory else output_path
    # create_classification_mask(tile, envelope, region_file, common_mask,
    #                            masks_region_path, wdir)

    output_file = os.path.join(output_path, f"{tile}_buffer_area.shp")
    # determine all the regions on the working area
    ecoregion_gdf = gpd.read_file(region_file)
    global_regions = get_sorted_regions_from_df(ecoregion_gdf, region_field)
    # create a mask to merge classif after fusion
    # Use a buffer to englobe the entire tile and use this to rasterise shapes
    shape_mask = common_mask.replace(".tif", ".shp")
    mask_gdf = gpd.read_file(shape_mask)
    mask_gdf["geometry"] = mask_gdf.geometry.buffer(2 * interior_buffer_size)
    mask_gdf.to_file(os.path.join(wdir, f"{tile}_buffered_mask.shp"))
    common_mask_buffer = os.path.join(wdir, f"{tile}_buffered_mask.tif")
    raster_buffer = otb.CreateRasterizationApplication({
        "in":
        os.path.join(wdir, f"{tile}_buffered_mask.shp"),
        "out":
        common_mask_buffer,
        "background":
        0,
        "mode":
        "binary",
        "pixType":
        "uint8",
        "spx":
        spatial_res[0],
        "spy":
        -spatial_res[1] if spatial_res[1] > 0 else spatial_res[1]
    })
    raster_buffer.ExecuteAndWriteOutput()
    # remove all regions outside the buffered area
    ecoregion_buffer = gpd.overlay(ecoregion_gdf, mask_gdf, how="intersection")

    regions = ecoregion_buffer[region_field].unique()

    # Compute the positive buffer to generate mask
    ecoregion_buffer_pos = ecoregion_buffer.copy()
    ecoregion_buffer_pos["geometry"] = ecoregion_buffer_pos.geometry.buffer(
        exterior_buffer_size)

    # Wrote classification masks with the positive buffer to ensure that
    # boundaries regions have classified pixels
    ecoregion_diff = gpd.overlay(ecoregion_buffer_pos,
                                 ecoregion_gdf,
                                 how="intersection")

    for region in ecoregion_diff[region_field + "_1"].unique():

        mask_region = f"Boundary_MASK_region_{region}_{tile}.shp"
        ss_region = ecoregion_diff[ecoregion_diff[region_field +
                                                  "_1"] == region]
        ss_region.to_file(os.path.join(wdir, mask_region))
        rasterize_mask_region = otb.CreateRasterizationApplication({
            "in":
            os.path.join(wdir, mask_region),
            "out":
            os.path.join(masks_region_path,
                         mask_region.replace(".shp", ".tif")),
            "im":
            common_mask,
            "background":
            0,
            "mode":
            "attribute",
            "mode.attribute.field":
            region_field + "_1",
            "pixType":
            "uint8"
        })
        rasterize_mask_region.ExecuteAndWriteOutput()
    # Compute the negative buffer to compute the distance map
    ecoregion_buffer_neg = ecoregion_buffer.copy()
    ecoregion_buffer_neg["geometry"] = ecoregion_buffer_neg.geometry.buffer(
        -interior_buffer_size)
    ecoregion_buffer_neg = ecoregion_buffer_neg[ecoregion_buffer_neg.area > 0]

    ecoregion_buffer_neg.to_file(output_file)

    # create empty map for create the final R dimension distance map
    bandmath = otb.CreateBandMathApplication({
        "il": [common_mask],
        "exp":
        "0",
        "out":
        os.path.join(wdir, "no_distance.tif"),
        "pixType":
        "uint8"
    })
    bandmath.ExecuteAndWriteOutput()
    liste_input = []
    # Compute each distance map for all regions
    for region in global_regions:
        dep_app = []
        # handle the case when the tile is inside an unique region
        if len(regions) == 1:
            bandmath = otb.CreateBandMathApplication({
                "il": [common_mask],
                "exp":
                "1000",
                "out":
                os.path.join(wdir, "uni_distance.tif"),
                "pixType":
                "uint16"
            })
            bandmath.ExecuteAndWriteOutput()
            liste_input.append(os.path.join(wdir, "uni_distance.tif"))
        elif region in regions:
            tmp_pos = os.path.join(
                wdir, f"Boundary_MASK_region_{region}_{tile}.shp")

            tmp_reg = os.path.join(wdir, f"tile_{tile}_region_{region}.shp")

            ss_region = ecoregion_buffer_neg[ecoregion_buffer_neg[region_field]
                                             == region]

            if ss_region.empty:
                print(region, "skipped (no feature in)")
                logger.info(f"For tile {tile} the region {region} is skipped "
                            "as there is no feature in")
                liste_input.append(os.path.join(wdir, "no_distance.tif"))
                continue
            ss_region.to_file(tmp_reg)
            # rasterize over the extended area
            region_raster = shape_to_otb(tmp_reg, common_mask_buffer)
            dep_app.append(region_raster)

            region_raster = region_raster.ExportImage("out")

            # Distance is in pixel units
            daniel = itk.DanielssonDistanceMapImageFilter.New(
                itk.GetImageViewFromArray(region_raster["array"]))
            np_view = itk.GetArrayViewFromImage(daniel.GetOutput())
            region_raster["array"] = np_view
            boundary_raster = shape_to_otb(tmp_pos, common_mask_buffer)
            dep_app.append(boundary_raster)

            boundary_raster = boundary_raster.ExportImage("out")
            dmax = (interior_buffer_size + exterior_buffer_size) / max(
                abs(spatial_res[0]), abs(spatial_res[1]))
            masked_daniel = np.where(boundary_raster["array"] > 0, np_view,
                                     dmax)

            dmid = interior_buffer_size / spatial_res[0]
            weights = np.where(masked_daniel < dmax,
                               (-0.5 / dmid) * masked_daniel + 1, epsilon)
            weights = np.where(weights < epsilon, epsilon, weights)
            region_raster["array"] = weights * 1000  # save as int16

            # Write image and apply mask
            # crop the weights for the tile
            roi = otb.CreateExtractROIApplication({
                "in":
                region_raster,
                "out":
                tmp_reg.replace(".shp", "_distance.tif"),
                "mode":
                "fit",
                "mode.fit.im":
                common_mask,
                "pixType":
                "uint16"
            })
            roi.ExecuteAndWriteOutput()
            liste_input.append(tmp_reg.replace(".shp", "_distance.tif"))
        else:
            # if regions not in tile insert empty map
            liste_input.append(os.path.join(wdir, "no_distance.tif"))

    # Concatenate all distance maps
    concat = otb.CreateConcatenateImagesApplication({
        "il":
        liste_input,
        "out":
        os.path.join(wdir, f"{tile}_distance_map.tif"),
        "pixType":
        "uint16"
    })
    concat.ExecuteAndWriteOutput()

    if working_directory:
        shutil.copy(os.path.join(wdir, f"{tile}_distance_map.tif"),
                    os.path.join(output_path, f"{tile}_distance_map.tif"))
