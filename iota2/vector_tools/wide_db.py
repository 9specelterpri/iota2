#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import re
import sqlite3
from collections import ChainMap
from typing import Union

import numpy as np
import pandas as pd
from pandas.io import sql

from iota2.common.file_utils import sort_by_first_elem
from iota2.vector_tools.vector_functions import get_fields, get_layer_name

# may inherit from pd.DataFrame ?


class WideDataBase:
    """class allowing to build a database containing more than 2000 cols

    This class is able to managed
    - if number of columns > 2000
    - if columns are different across input db
    - nans values

    usage :
    merged_db = '/path/my_db.h5'
    sqlite_inputs = ['sqlite_1.sqlite', 'sqlite_2.sqlite']
    cols = ["col_1", "col_2", "mask_1", "mask_2"]
    wide_db = WideDataBase(sql_files=sqlite_inputs,
                           cols_order=cols,
                           queryable_cols=["i2label"],
                           specific_nans_rules={"(mask_)": 2.0})
    wide_db.write(merged_db)
    every nans in ["col_1", "col_2"] will be replaced by 0.0
    every nans in ["mask_1", "mask_2"] will be replaced by 2.0

    Warnings
    --------
    - if there is geometies, their will be lost in output db
    - nan values type must be the same than the column's type

    """

    def __init__(
        self,
        sql_files: Union[None, list[str]] = None,
        dataframes: Union[None, list[pd.DataFrame]] = None,
        cols_order: Union[None, list[str]] = None,
        fid_col: str = "fid",
        queryable_cols: Union[None, list[str]] = None,
        nan_values: float = 0.0,
        specific_nans_rules: Union[dict, None] = None,
        reading_chunk_size: int = 100000,
    ):
        """
        Parameters
        ----------
        sql_files
            list of input sqlite files to merge
        dataframes
            not implemented
        cols_order
            specify output columns and their order
            it must be build considering every inputs columns
        fid_col
            output column name containing an unique integer
        queryable_cols
            queryable columns (hdf.select where...)
        nan_values
            replace nan values in dataframe (into columns not in specific_nans_rules)
        specific_nans_rules
            {regular expression pattern:nan_value}
            ie : merge_wide_sqlite(..., nan_values=-1, specific_nans_rules={('mask'): 2})
            then every columns containing 'mask' in their name, nans
            will be replaced by 2. Every other nan values will be replaced by -1
        reading_chunk_size
            read input sqlite max size
        """
        assert sql_files or dataframes, (
            "one of these parameters : " "'sql_files', 'dataframes' must be provided"
        )
        self.specific_nans_rules = specific_nans_rules
        self.sql_files = sql_files
        self.dataframes = dataframes
        self.reading_chunk_size = reading_chunk_size
        self.nan_values = nan_values
        self.fid_col = fid_col
        self.queryable_cols = queryable_cols

        if self.queryable_cols is None:
            self.queryable_cols = []
        if self.fid_col not in self.queryable_cols:
            self.queryable_cols += [self.fid_col]

        if sql_files:
            self.__cols_per_db = [
                get_fields(sql_file, "SQLite") for sql_file in sql_files
            ]
        if cols_order:
            self.__cols = cols_order
        else:
            db_list = sql_files if sql_files else dataframes
            self.__cols = self.guess_output_cols(db_list)

    @property
    def columns(self):
        return self.__cols

    @columns.setter
    def columns(self, cols_list: list[str]):
        """ """
        if not isinstance(cols_list, list):
            raise TypeError(f"'{self.__class__.__name__}.columns' must be a list")
        missing_cols = []
        for val in cols_list:
            if val not in self.__cols:
                missing_cols.append(val)
        if missing_cols:
            raise ValueError(
                f"columns : {', '.join([col for col in missing_cols])} "
                "does not exists in input cols : "
                f"{', '.join([col for col in self.__cols])}"
            )
        self.__cols = cols_list

    def add_columns(self):
        """TODO ?"""
        pass

    def nans_cols_rules(self) -> list[tuple[float, str]]:
        """ """
        specific_nans_rules_cols = []
        for col in self.__cols:
            for re_pattern, nan_value in self.specific_nans_rules.items():
                if re.search(re_pattern, col):
                    specific_nans_rules_cols.append((nan_value, col))
        return specific_nans_rules_cols

    def guess_output_cols(self, db_list) -> list[str]:
        expected_columns = []
        for input_db in db_list:
            if isinstance(input_db, str):
                expected_columns += get_fields(input_db, driver="SQLite")
            elif isinstance(input_db, pd.DataFrame):
                expected_columns += list(input_db.columns)
        return list(set(expected_columns))

    def write(self, output_file):
        if os.path.exists(output_file):
            os.remove(output_file)
        spec_nans_rules = []
        if self.specific_nans_rules:
            spec_nans_rules = self.nans_cols_rules()
        spec_nans_rules_cols = [col_name for _, col_name in spec_nans_rules]
        general_nans_columns_rule = list(set(self.__cols) - set(spec_nans_rules_cols))
        if self.sql_files:
            self.write_from_sql(
                output_file, self.sql_files, general_nans_columns_rule, spec_nans_rules
            )
        elif self.dataframes:
            self.write_from_df()

    def write_from_sql(
        self,
        output_file,
        sql_files: list[str],
        general_nans_columns_rule,
        spec_nans_rules,
    ):
        """ """
        for input_sql, in_cols in zip(sql_files, self.__cols_per_db):
            tbl_name = get_layer_name(input_sql, "SQLite")
            conn = sqlite3.connect(input_sql)
            cols_to_selec = [ccol for ccol in self.__cols if ccol in in_cols]
            in_df = sql.read_sql(
                f"SELECT {', '.join(cols_to_selec)} FROM {tbl_name};",
                conn,
                chunksize=self.reading_chunk_size,
            )
            start_fid = 0
            for i2_df in in_df:
                cols = [col for col in list(i2_df.columns) if col != "geometry"]
                cols.append(self.fid_col)
                dummy_df = pd.DataFrame(columns=self.__cols)
                i2_df[self.fid_col] = np.arange(start_fid, start_fid + len(i2_df))
                dummy_df = dummy_df.append(i2_df[cols])

                for nan_value, spec_cols in spec_nans_rules:
                    dummy_df[spec_cols] = dummy_df[spec_cols].fillna(nan_value)

                dummy_df[general_nans_columns_rule] = dummy_df[
                    general_nans_columns_rule
                ].fillna(self.nan_values)

                dummy_df[self.fid_col] = dummy_df[self.fid_col].astype(int)
                WideDataBase.wide_df_to_hdf(
                    output_file,
                    dummy_df,
                    queryable_cols=self.queryable_cols,
                    complevel=9,
                    complib="zlib",
                )
                start_fid = len(dummy_df)

    def write_from_df(self):
        print("write from df")
        pass

    def __dir__(self):
        excluded_functions = [
            "write_from_sql",
            "guess_output_cols",
            "nans_cols_rules",
            "read_hdf_wide_df",
            "wide_df_to_hdf",
            "write_from_df",
        ]
        return list(set(dir(WideDataBase)) - set(excluded_functions))

    def __str__(self):
        return "TODO : __str__"

    def __repr__(self):
        return "TODO : __repr__"

    @classmethod
    def get_columns(hdf_db_file: str):
        """TODO"""
        pass

    @classmethod
    def wide_df_to_hdf(
        cls,
        filename,
        data,
        columns=None,
        max_cols=2000,
        queryable_cols=["fid"],
        **kwargs,
    ):
        """Write a pandas.DataFrame with a large number of columns
        to one HDFStore.

        Parameters
        -----------
        filename : str
            name of the HDFStore
        data : pandas.DataFrame
            data to save in the HDFStore
        columns: list
            a list of columns for storing. If set to `None`, all
            columns are saved.
        maxColSize : int (default=2000)
            this number defines the maximum possible column size of
            a table in the HDFStore.

        tribute to : https://stackoverflow.com/questions/16639503/
        unable-to-save-dataframe-to-hdf5-object-header-message-is-too-large
        """
        max_cols = max_cols - len(queryable_cols)
        store = pd.HDFStore(filename, **kwargs)
        if columns is None:
            columns = data.columns
        if (col_size := columns.shape[0]) > max_cols:
            num_of_splits = np.ceil(col_size / max_cols).astype(int)
            cols_split = []
            for i in range(num_of_splits):
                col_subset = list(columns[i * max_cols : (i + 1) * max_cols])
                for queryable_col in queryable_cols:
                    if queryable_col not in col_subset:
                        col_subset.insert(0, queryable_col)
                cols_split.append(col_subset)
            _cols_tab_num = ChainMap(
                *[
                    dict(zip(columns, [f"data{num}"] * col_size))
                    for num, columns in enumerate(cols_split)
                ]
            )
            cols_tab_num = pd.Series(dict(_cols_tab_num))  # .sort_index()
            for num, cols in enumerate(cols_split):
                store.append(
                    f"data{num}",
                    data[cols],
                    format="table",
                    data_columns=queryable_cols,
                )
            store.append(
                "colsTabNum", cols_tab_num, format="table", data_columns=queryable_cols
            )
        else:
            store.append(
                "data", data[columns], format="table", data_columns=queryable_cols
            )
        store.close()

    @classmethod
    def read_hdf_wide_df(
        cls, filename, columns=None, queryable_cols=None, chunksize=50000, **kwargs
    ):
        """Read a `pandas.DataFrame` from a HDFStore.

        Parameter
        ---------
        filename : str
            name of the HDFStore
        columns : list
            the columns in this list are loaded. Load all columns,
            if set to `None`.

        Returns
        -------
        data : pandas.DataFrame
            loaded data.

        tribute to https://stackoverflow.com/questions/16639503/
        unable-to-save-dataframe-to-hdf5-object-header-message-is-too-large
        """

        if queryable_cols is None:
            queryable_cols = []
        force_start = force_stop = None
        if "start" in kwargs:
            if "stop" not in kwargs:
                raise ValueError("'start' and 'stop' must be set together")
            force_start = kwargs.pop("start", None)
            force_stop = kwargs.pop("stop", None)
        store = pd.HDFStore(filename, mode="r")
        data = []
        tables = store.keys()
        if "/colsTabNum" in tables:
            nrows = store.get_storer("data0").nrows
            colsTabNum = store.select("colsTabNum")
            if columns is not None:
                tabNums = colsTabNum[columns]
                couples = []
                for col_name, table_name in zip(
                    list(tabNums.index), list(tabNums.values)
                ):
                    if not (col_name, table_name) in couples:
                        couples.append((table_name, col_name))
                        data_row = []
                        cols_to_get = [col_name]
                sorted_couples = sort_by_first_elem(couples)
                reduced_couples = []
                for table_name_m, cols_name_m in sorted_couples:
                    cols_tmp = []
                    for elem in cols_name_m:
                        if elem not in cols_tmp:
                            cols_tmp.append(elem)
                    reduced_couples.append((table_name_m, cols_tmp))
                for cpt in range(nrows // chunksize + 1):
                    data_cols = []
                    for table_name_m, col_name_m in reduced_couples:
                        cols_to_get = [elem for elem in col_name_m if elem in columns]
                        if queryable_cols:
                            cols_to_get = cols_to_get + queryable_cols
                        data_to_df = store.select(
                            table_name_m,
                            columns=cols_to_get,
                            start=force_start if force_start else cpt * chunksize,
                            stop=force_stop if force_stop else (cpt + 1) * chunksize,
                            **kwargs,
                        )
                        data_cols.append(data_to_df)
                    data_row = pd.concat(data_cols, axis=1)
                    if force_start:
                        break
                    # data_row.reset_index(drop=True, inplace=True)
                    data_row = data_row.loc[:, ~data_row.columns.duplicated()]
                    data.append(data_row)
                data = pd.concat(data, axis=0)

            else:
                sorted_tbl = sorted(
                    [(val, int(val.split("data")[-1])) for val in colsTabNum.unique()],
                    key=lambda x: x[1],
                )
                sorted_tbl = [val for val, _ in sorted_tbl]
                for table in sorted_tbl:
                    data.append(store.select(table, **kwargs))

                data = pd.concat(data, axis=1)
                # drop duplicated columns (because of 'fid_col' in
                # function wide_df_to_hdf function)
                data = data.loc[:, ~data.columns.duplicated()]

        else:
            nrows = store.get_storer("data").nrows
            data = []
            for cpt in range(nrows // chunksize + 1):
                data.append(
                    store.select(
                        "data",
                        columns=columns,
                        start=force_start if force_stop else cpt * chunksize,
                        stop=force_stop if force_stop else (cpt + 1) * chunksize,
                        **kwargs,
                    )
                )
                if force_start:
                    break
            data = pd.concat(data, axis=0)
            # data = store.select('data', columns=columns, **kwargs)
        cols_to_drop = [elem for elem in queryable_cols if elem not in columns]
        if cols_to_drop:
            data.drop(cols_to_drop, axis=1, inplace=True)
        store.close()
        return data
