"""Change exsiting field name " "of an input shapefile."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys

from osgeo import ogr

from iota2.vector_tools import delete_field
from iota2.vector_tools import vector_functions as vf


def change_name(filein, fieldin, fieldout):

    field_list = vf.get_fields(filein)
    if fieldout in field_list:
        print(f"Field name {fieldout} already exists")
        sys.exit(1)

    # Get input file and field characteritics
    source = ogr.Open(filein, 1)
    layer = source.GetLayer()
    layer_defn = layer.GetLayerDefn()
    i = layer_defn.GetFieldIndex(fieldin)

    # Create the out field with in field characteristics
    try:
        field_type_code = layer_defn.GetFieldDefn(i).GetType()
        field_width = layer_defn.GetFieldDefn(i).GetWidth()
        field_precision = layer_defn.GetFieldDefn(i).GetPrecision()
    except Exception:
        print("Field {fieldin} not exists in the input shapefile")
        sys.exit(0)

    new_field = ogr.FieldDefn(fieldout, field_type_code)
    new_field.SetWidth(field_width)
    new_field.SetPrecision(field_precision)
    layer.CreateField(new_field)

    for feat in layer:
        val = feat.GetField(fieldin)
        layer.SetFeature(feat)
        feat.SetField(fieldout, val)
        layer.SetFeature(feat)

    layer = feat = new_field = source = None

    delete_field.deleteField(filein, fieldin)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        parser = argparse.ArgumentParser(
            description="Change exsiting field name " "of an input shapefile"
        )
        parser.add_argument(
            "-s",
            dest="shapefile",
            action="store",
            help="Input shapefile",
            required=True,
        )
        parser.add_argument(
            "-fi", dest="fieldi", action="store", help="Field to rename", required=True
        )
        parser.add_argument(
            "-fo", dest="fieldo", action="store", help="New field name", required=True
        )
        args = parser.parse_args()
        try:
            change_name(args.shapefile, args.fieldi, args.fieldo)
            print("Field has been changed successfully")
        except Exception as err:
            print("Problem occured in the process")
            sys.stderr.write(f"ERROR: {str(err)}n")
            sys.exit(0)
