#!/usr/bin/env python3

import argparse
import glob
import logging
import os
import os.path
import re
import sqlite3
import sys
import time
from typing import Optional, Union

import numpy as np
import pandas as pd
import xarray as xr
from osgeo import ogr
from pandas.io import sql

import iota2.common.i2_constants as i2_const
from iota2.common.file_utils import sort_by_first_elem
from iota2.common.utils import run
from iota2.vector_tools.vector_functions import get_fields, get_layer_name

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


def merge_wide_sqlite(
    input_sqlites: list[str],
    output_hdf: str,
    cols_order: Union[list[str], None],
    fid_col: str = "fid",
    nan_values: Optional[float] = None,
    specific_nans_rules: Union[dict, None] = None,
    chunk_size: int = 100000,
    logger=LOGGER,
) -> None:
    """merge sqlite db into a hdf db.

    This function is able to managed
    - if number of columns > 2000
    - if columns are different across input db
    - nans values

    the results hdf db will contains a new col, parameter 'fid_col'
    which represent the row number

    Parameters
    ----------
    input_sqlites
        list of input sqlite files to merge
    output_hdf
        output hdf database file
    cols_order
        specify output columns and their order
        it must be build considering every inputs columns
    fid_col
        output column name containing an unique integer
    nan_values
        replace nan values in dataframe (into columns not in specific_nans_rules)
    specific_nans_rules
        {regular expression pattern:nan_value}
        ie : merge_wide_sqlite(..., nan_values=-1, specific_nans_rules={('mask'): 2})
        then every columns containing 'mask' in their name, nans
        will be replaced by 2. Every other nan values will be replaced by -1
    chunk_size
        read input sqlite max size
    """
    if os.path.exists(output_hdf):
        logger.warning(f"output file '{output_hdf}' already exists" ", overwriting")
        os.remove(output_hdf)
    if cols_order:
        expected_columns = cols_order
    else:
        expected_columns = []
        for input_sql in input_sqlites:
            expected_columns += get_fields(input_sql, driver="SQLite")
        expected_columns = list(set(expected_columns))

    specific_nans_rules_cols = []
    specific_nans_rules_cols_flat = []
    if specific_nans_rules:
        for col in expected_columns:
            for re_pattern, nan_value in specific_nans_rules.items():
                if re.search(re_pattern, col):
                    specific_nans_rules_cols.append((nan_value, col))
                    specific_nans_rules_cols_flat.append(col)
    basic_nans_columns_rule = list(
        set(expected_columns) - set(specific_nans_rules_cols_flat)
    )
    specific_nans_rules_cols = sort_by_first_elem(specific_nans_rules_cols)
    expected_columns.insert(0, fid_col)
    start_fid = 0
    netcdf_chunks_files = []
    for input_sql in input_sqlites:
        logger.info(input_sql)
        print(input_sql)
        tbl_name = get_layer_name(input_sql, "SQLite")
        conn = sqlite3.connect(input_sql)

        in_df = sql.read_sql(f"SELECT * FROM {tbl_name};", conn, chunksize=chunk_size)
        for chk_num, i2_df in enumerate(in_df):
            i2_df[fid_col] = np.arange(start_fid, start_fid + len(i2_df))
            dummy_df = pd.DataFrame(columns=expected_columns)
            # TODO find a way to store str cols
            i2_df = i2_df.select_dtypes(exclude=["object"])
            # i2_df[expected_columns] allow us to exclude regions columns
            cols_of_interest = [
                col_name
                for col_name in list(i2_df.columns)
                if col_name in expected_columns
            ]
            dummy_df = dummy_df.append(i2_df[cols_of_interest])
            dummy_df[cols_of_interest] = dummy_df[cols_of_interest].astype(float)
            dummy_df[fid_col] = dummy_df[fid_col].astype(int)

            for nan_value, spec_cols in specific_nans_rules_cols:
                dummy_df[spec_cols] = dummy_df[spec_cols].fillna(nan_value)

            if nan_values is not None:
                dummy_df[basic_nans_columns_rule] = dummy_df[
                    basic_nans_columns_rule
                ].fillna(nan_values)

            dummy_df = dummy_df.set_index(fid_col)
            # xr_ds = dummy_df.to_xarray()
            da_xr = xr.DataArray(
                dummy_df.values,
                dims=("fid", "bands"),
                coords={
                    "fid": np.arange(start_fid, start_fid + len(i2_df)),
                    "bands": list(dummy_df.columns),
                },
            )
            output_parquet = input_sql.replace(".sqlite", f"_chk_{chk_num}.nc")
            netcdf_chunks_files.append(output_parquet)
            da_xr.to_netcdf(output_parquet, mode="w")
            start_fid += len(dummy_df)
    start = time.time()
    netcdf_data = xr.open_mfdataset(netcdf_chunks_files)
    end = time.time()
    logger.info(
        f"xr.open_mfdataset nb files : {len(netcdf_chunks_files)} timing : {end - start} sec"
    )
    start = time.time()
    netcdf_data.to_netcdf(output_hdf)
    end = time.time()
    logger.info(f"to_netcdf {output_hdf} timing : {end - start} sec")

    for netcdf_chunks_file in netcdf_chunks_files:
        if os.path.exists(netcdf_chunks_file):
            os.remove(netcdf_chunks_file)


def mergeVectors(
    outname: str,
    opath: str,
    files: list[str],
    ext: str = "shp",
    out_tbl_name: Optional[str] = None,
):
    """Merge a list of vector files in one.

    Parameters
    ----------

    outname
        output filename (without extension)
    opath
        path to the directory which will contains the resulting file
    files
        list of files to merge
    ext
        output file extension (shp or sqlite)
    out_tbl_name
        force output layer/table name
    """
    done = []

    outType = ""
    if ext == "sqlite":
        outType = " -f SQLite "
    if files:
        goodfiles = []
        for filein in files:
            if os.path.exists(filein):
                goodfiles.append(filein)
            else:
                print(f"{filein} not exist !")

        if len(goodfiles) != 0:
            file1 = files[0]
            nbfiles = len(files)
            filefusion = opath + "/" + outname + "." + ext
            if not os.path.exists(filefusion):
                table_name = outname
                if out_tbl_name:
                    table_name = out_tbl_name
                fusion = (
                    "ogr2ogr "
                    + filefusion
                    + " "
                    + file1
                    + " "
                    + outType
                    + " -nln "
                    + table_name
                )
                run(fusion)

                done.append(file1)
                for f in range(1, nbfiles):
                    fusion = (
                        "ogr2ogr -update -append "
                        + filefusion
                        + " "
                        + files[f]
                        + " -nln "
                        + table_name
                        + " "
                        + outType
                    )
                    run(fusion)
                    done.append(files[f])

        return filefusion


def mergeVectors_layer(infiles, outfile, outformat="ESRI Shapefile"):
    """
    Merge a list of vector files in one, by adding a new  layer for each vector
    """
    """
   if(not outfile.lower().endswith('.shp')):
      print(basename(outfile) + " is not a valid name for shapefile output." \
      "It will be replaced by " + basename(outfile)[:-4] + ".shp")
      outfile = basename(outfile)[:-4] + '.shp'
   """

    driver = ogr.GetDriverByName(outformat)

    if os.path.exists(outfile):
        driver.DeleteDataSource(outfile)

    if not isinstance(infiles, list):
        print(infiles)
        files = glob.glob(infiles + "/" + "*.shp")
        files.append(glob.glob(infiles + "/" + "*.sqlite"))
        if not files:
            print("Folder " + infiles + "does not contain vector files")
            sys.exit(-1)
    else:
        files = infiles

    # Append first file to the output file
    file1 = files[0]
    fusion = f"ogr2ogr -f '{outformat}' " + outfile + " " + file1
    run(fusion)

    layername = os.path.splitext(os.path.basename(outfile))[0]
    # Append other files to the output file
    nbfiles = len(files)
    progress = 0
    for f in range(1, nbfiles):
        if not isinstance(files[f], list):
            fusion2 = (
                f"ogr2ogr -f '{outformat}' -update -append "
                + outfile
                + " "
                + files[f]
                + " -nln "
                + layername
            )
            run(fusion2)
        progress += 1
        print(f"Progress : {(float(progress) / float(nbfiles) * 100.0)}")

    return outfile


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        usage = "usage: %prog [options] "
        parser = argparse.ArgumentParser(description="Merge shapefiles")
        parser.add_argument(
            "-list",
            nargs="+",
            dest="shapefiles",
            action="store",
            help="List of input shapefiles",
        )
        parser.add_argument(
            "-p", dest="opath", action="store", help="Folder of input shapefiles"
        )
        parser.add_argument(
            "-o",
            dest="outshapefile",
            action="store",
            help="ESRI Shapefile output filename and path",
            required=True,
        )
        parser.add_argument(
            "-format",
            dest="outformat",
            action="store",
            help="output format (default : ESRI Shapefile)",
            default="ESRI Shapefile",
        )
        args = parser.parse_args()
        if args.opath is None:
            if args.shapefiles is None:
                print(
                    "Either folder of input shapefiles or "
                    "list of input shapefiles have to be given"
                )
                sys.exit(-1)
            else:
                mergeVectors(
                    args.outshapefile, args.opath, args.shapefiles, args.outformat
                )
        else:
            mergeVectors_layer(args.opath, args.outshapefile, args.outformat)
