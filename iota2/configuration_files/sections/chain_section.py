# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

from typing import ClassVar, Literal, Optional

from pydantic import Field
from pydantic.fields import ModelField

from iota2.configuration_files.sections.cfg_utils import (
    ConfigError,
    FileParameter,
    Iota2ParamSection,
    PathParameter,
)


class Iota2ProjParam(str):
    """Class dedicated to the 'proj' parameter."""

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value, field: ModelField):
        """Validate projection field."""
        if not value.startswith("EPSG:"):
            raise ConfigError(f"Parameter chain.{field.name} must starts with 'EPSG:'")
        try:
            _ = int(value.split(":")[-1])
        except ValueError as exc:
            raise ConfigError(
                f"EPSG code '{value.split(':')[-1]}' can not be casted to an integer"
            ) from exc
        return value


class ListTileParameter(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value, field: ModelField):
        delimiter = " "
        forbiden_chars = [",", "|", ":", ";"]

        if not isinstance(value, str):
            raise ConfigError(
                f"The parameter '{field.name}' must be a string,"
                f" type detected : {type(value)}"
            )
        value = delimiter.join(value.split())
        for forbiden_char in forbiden_chars:
            if forbiden_char in value:
                raise ConfigError(
                    "'list_tile' configuration file parameter"
                    f" delimiter must be a whitespace. "
                    f"forbiden characters :'{forbiden_chars}'"
                )
        return value


class SpatialResolution(list):
    """https://field-idempotency--pydantic-docs.netlify.app/usage/types/#custom-data-types"""

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value, field: ModelField):
        if isinstance(value, str):
            raise ConfigError(
                f"{field.name} must be build as [spx, spy] or spx (square pixel)"
            )
        init_spatiale_res = []
        if isinstance(value, (float, int)):
            init_spatiale_res = [float(value), float(value)]
        elif isinstance(value, (list, tuple)):
            init_spatiale_res = [float(val) for val in value]
        if init_spatiale_res and len(init_spatiale_res) > 2:
            raise ConfigError(
                f"{field.name} must contains at most " "2 values : [spx, spy]"
            )
        return init_spatiale_res

    @classmethod
    def __modify_schema__(cls, field_schema, field: Optional[ModelField]):
        if field:
            field_schema["available_on_builders"] = [
                "i2_classification",
                "i2_obia",
                "i2_features_map",
            ]
            field_schema["doc_type"] = "list or scalar"
            field_schema["short_desc"] = "Output spatial resolution"
            field_schema["long_desc"] = (
                "The spatial resolution expected."
                "It can be provided as integer or float,"
                "or as a list containing two values"
                " for non squared resolution"
            )


class ChainSection(Iota2ParamSection):
    """Definition of the parameters that belong to the 'chain' section."""

    section_name: ClassVar[str] = "chain"

    output_statistics: bool = Field(
        True,
        short_desc="output_statistics",
        doc_type="bool",
        available_on_builders=["i2_classification"],
    )

    # sensors paths
    l5_path_old: PathParameter = Field(
        None,
        doc_type="str",
        short_desc="Absolute path to Landsat-5 images coming"
        " from old THEIA format (D*H*)",
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    l8_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc="Absolute path to Landsat-8 images coming"
        "from new tiled THEIA data",
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    l8_path_old: PathParameter = Field(
        None,
        doc_type="str",
        short_desc="Absolute path to Landsat-8 images coming"
        " from old THEIA format (D*H*)",
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    s2_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to Sentinel-2" " images (THEIA format)"),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    s2_output_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to store preprocessed data " "in a dedicated directory"
        ),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    s2_s2c_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to Sentinel-2" " images (Sen2Cor format)"),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    s2_s2c_output_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to store preprocessed data " "in a dedicated directory"
        ),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    s2_l3a_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to Sentinel-2" " L3A images (THEIA format)"),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    s2_l3a_output_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to store preprocessed data " "in a dedicated directory"
        ),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    s1_path: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to Sentinel-1 configuration file"),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    user_feat_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to the user's features path"),
        long_desc=(
            "Absolute path to the user's features path. " "They must be stored by tiles"
        ),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    # other params
    minimum_required_dates: int = Field(
        2,
        doc_type="int",
        short_desc=("required minimum number of available dates for each sensor"),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    cloud_threshold: int = Field(
        0,
        doc_type="int",
        short_desc="Threshold to consider that a pixel is valid",
        long_desc=(
            "Indicates the threshold for a polygon to"
            " be used for learning. It use the validity count,"
            " which is incremented if a cloud, "
            "a cloud shadow or a saturated pixel is detected"
        ),
        available_on_builders=["i2_classification", "i2_obia"],
    )
    first_step: str = Field(
        doc_type="str",
        short_desc="The step group name indicating where the chain start",
        available_on_builders=[
            "i2_classification",
            "i2_obia",
            "i2_vectorization",
            "i2_features_map",
        ],
    )
    last_step: str = Field(
        doc_type="str",
        short_desc="The step group name indicating where the chain ends",
        available_on_builders=[
            "i2_classification",
            "i2_obia",
            "i2_vectorization",
            "i2_features_map",
        ],
    )
    logger_level: Literal[
        "NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"
    ] = Field(
        "INFO",
        doc_type="str",
        short_desc=(
            "Set the logger level: NOTSET, DEBUG," " INFO, WARNING, ERROR, CRITICAL"
        ),
        available_on_builders=[
            "i2_classification",
            "i2_obia",
            "i2_vectorization",
            "i2_features_map",
        ],
    )
    region_path: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to a " "region vector file"),
        available_on_builders=["i2_classification", "i2_obia"],
    )
    region_field: str = Field(
        "region",
        doc_type="str",
        short_desc=("The column name for region indicator in" "`region_path` file"),
        long_desc=(
            "This column in the database must contains string which"
            " can be converted into integers. For instance '1_2' "
            "does not match this condition"
        ),
        available_on_builders=["i2_classification", "i2_obia"],
    )
    check_inputs: bool = Field(
        True,
        doc_type="bool",
        short_desc="Enable the inputs verification",
        long_desc=(
            "Enable the inputs verification."
            " It can take a lot of time for large dataset. "
            "Check if region intersect reference data for instance"
        ),
        available_on_builders=["i2_classification", "i2_obia"],
    )
    spatial_resolution: SpatialResolution = []
    remove_output_path: bool = Field(
        True,
        doc_type="bool",
        short_desc=(
            "Before the launch of iota2, " "remove the content of `output_path`"
        ),
        long_desc=(
            "Before the launch of iota2, remove the content of `output_path`."
            " Only if the `first_step` is `init` and the folder name is valid "
        ),
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    output_path: str = Field(
        None,
        doc_type="str",
        short_desc="Absolute path to the output directory",
        long_desc=(
            "Absolute path to the output directory."
            "It is recommended to have one directory"
            " per run of the chain"
        ),
        mandatory_on_builders=[
            "i2_classification",
            "i2_obia",
            "i2_vectorization",
            "i2_features_map",
        ],
        available_on_builders=[
            "i2_classification",
            "i2_obia",
            "i2_vectorization",
            "i2_features_map",
        ],
    )
    nomenclature_path: FileParameter = Field(
        doc_type="str",
        short_desc="Absolute path to the nomenclature description file",
        mandatory_on_builders=["i2_classification", "i2_obia"],
        available_on_builders=["i2_classification", "i2_obia"],
    )
    color_table: FileParameter = Field(
        doc_type="str",
        short_desc=(
            "Absolute path to the file that links " "the classes and their colours"
        ),
        mandatory_on_builders=["i2_classification", "i2_obia"],
        available_on_builders=["i2_classification", "i2_obia"],
    )
    list_tile: ListTileParameter = Field(
        doc_type="str",
        short_desc="List of tile to process, separated by space",
        mandatory_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
        available_on_builders=["i2_classification", "i2_obia", "i2_features_map"],
    )
    ground_truth: FileParameter = Field(
        doc_type="str",
        short_desc="Absolute path to reference data",
        mandatory_on_builders=["i2_classification", "i2_obia"],
        available_on_builders=["i2_classification", "i2_obia"],
    )
    data_field: str = Field(
        doc_type="str",
        short_desc=("Field name indicating classes " "labels in `ground_thruth`"),
        mandatory_on_builders=["i2_classification", "i2_obia"],
        available_on_builders=["i2_classification", "i2_obia"],
    )
    proj: Iota2ProjParam = Field(
        doc_type="str",
        short_desc="The projection wanted. Format EPSG:XXXX is mandatory",
        mandatory_on_builders=[
            "i2_classification",
            "i2_obia",
            "i2_vectorization",
            "i2_features_map",
        ],
        available_on_builders=[
            "i2_classification",
            "i2_obia",
            "i2_vectorization",
            "i2_features_map",
        ],
    )
