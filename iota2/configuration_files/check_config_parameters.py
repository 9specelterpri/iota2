#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
This module contains all functions for testing known configuration parameters

These test must concern only typing

They can be overloaded by providing custom check.
"""
import logging
import os
from inspect import signature
from typing import Any, Callable

from osgeo import ogr

from iota2.common import service_error as sErr
from iota2.common.file_utils import get_iota2_project_dir, verify_import

LOGGER = logging.getLogger("distributed.worker")
FunctionNameWithParams = tuple[str, dict[str, Any]]

# #########################################################################
# Tools
# #########################################################################


def test_var_config_file(cfg, section, variable, varType, valeurs="", valDefaut=""):
    """
    This function check if variable is in obj
    and if it has varType type.
    Optionnaly it can check if variable has values in valeurs
    Exit the code if any error are detected
    :param section: section name of the obj where to find
    :param variable: string name of the variable
    :param varType: type type of the variable for verification
    :param valeurs: string list of the possible value of variable
    :param valDefaut: value by default if variable is not in the configuration file
    """

    if not hasattr(cfg, section):
        raise sErr.configFileError(
            "Section '" + str(section) + "' is not in the configuration file"
        )

    objSection = getattr(cfg, section)

    if not hasattr(objSection, variable):
        if valDefaut != "":
            setattr(objSection, variable, valDefaut)
        else:
            raise sErr.parameterError(
                section,
                "mandatory variable '"
                + str(variable)
                + "' is missing in the configuration file",
            )
    else:
        tmpVar = getattr(objSection, variable)

        if not isinstance(tmpVar, varType):
            message = (
                "variable '"
                + str(variable)
                + "' has a wrong type\nActual: "
                + str(type(tmpVar))
                + " expected: "
                + str(varType)
            )
            raise sErr.parameterError(section, message)

        if valeurs != "":
            ok = 0
            for index in range(len(valeurs)):
                if tmpVar == valeurs[index]:
                    ok = 1
            if ok == 0:
                message = (
                    "bad value for '"
                    + variable
                    + "' variable. Value accepted: "
                    + str(valeurs)
                    + " Value read: "
                    + str(tmpVar)
                )
                raise sErr.parameterError(section, message)


def get_param(cfg, section, variable):
    """
    Return the value of variable in the section from config
    file define in the init phase of the class.
    :param section: string name of the section
    :param variable: string name of the variable
    :return: the value of variable
    """

    if not hasattr(cfg, section):
        # not an osoError class because it should NEVER happened
        raise Exception("Section is not in the configuration file: " + str(section))

    objSection = getattr(cfg, section)
    if not hasattr(objSection, variable):
        # not an osoError class because it should NEVER happened
        raise Exception("Variable is not in the configuration file: " + str(variable))

    tmpVar = getattr(objSection, variable)

    return tmpVar


def test_ifexists(path):
    if not os.path.exists(path):
        raise sErr.dirError(path)


def test_shape_name(input_vector):
    """ """
    import string

    avail_characters = string.ascii_letters
    if (first_character := os.path.basename(input_vector)[0]) not in avail_characters:
        raise sErr.configError(
            f"the file '{input_vector}' is containing a non-ascii letter at first "
            f"position in it's name : {first_character}"
        )

    ext = input_vector.split(".")[-1]
    # If required test if gdal can open this file
    if ext not in (allowed_format := ["shp", "sqlite"]):
        raise sErr.configError(
            f"{input_vector} is not in right format."
            f"\nAllowed format are {allowed_format}"
        )


def check_sample_selection(cfg, path_conf):
    """ """

    def check_parameters(sampleSel):

        not_allowed_p = [
            "outrates",
            "in",
            "mask",
            "vec",
            "out",
            "instats",
            "field",
            "layer",
            "rand",
            "inxml",
        ]
        strats = ["byclass", "constant", "percent", "total", "smallest", "all"]
        for p in not_allowed_p:
            if p in sampleSel:
                raise sErr.configError(
                    f"'{p}' parameter must not be set in arg_train.sample_selection"
                )

        if "sampler" in sampleSel:
            sampler = sampleSel["sampler"]
            if sampler not in ["periodic", "random"]:
                raise sErr.configError("sampler must be 'periodic' or 'random'")
        if "sampler.periodic.jitter" in sampleSel:
            jitter = sampleSel["sampler.periodic.jitter"]
            if not isinstance(jitter, int):
                raise sErr.configError("jitter must an integer")
        if "strategy" in sampleSel:
            strategy = sampleSel["strategy"]
            if strategy not in strats:
                strats_list = " or ".join([f"'{elem}'" for elem in strats])
                raise sErr.configError(f"strategy must be {strats_list}")
        if "strategy.byclass.in" in sampleSel:
            byclass = sampleSel["strategy.byclass.in"]
            if not isinstance(byclass, str):
                raise sErr.configError("strategy.byclass.in must a string")
        if "strategy.constant.nb" in sampleSel:
            constant = sampleSel["strategy.constant.nb"]
            if not isinstance(constant, int):
                raise sErr.configError("strategy.constant.nb must an integer")
        if "strategy.percent.p" in sampleSel:
            percent = sampleSel["strategy.percent.p"]
            if not isinstance(percent, float):
                raise sErr.configError("strategy.percent.p must a float")
        if "strategy.total.v" in sampleSel:
            total = sampleSel["strategy.total.v"]
            if not isinstance(total, int):
                raise sErr.configError("strategy.total.v must an integer")
        if "elev.dem" in sampleSel:
            dem = sampleSel["elev.dem"]
            if not isinstance(dem, str):
                raise sErr.configError("elev.dem must a string")
        if "elev.geoid" in sampleSel:
            geoid = sampleSel["elev.geoid"]
            if not isinstance(geoid, str):
                raise sErr.configError("elev.geoid must a string")
        if "elev.default" in sampleSel:
            default = sampleSel["elev.default"]
            if not isinstance(default, float):
                raise sErr.configError("elev.default must a float")
        if "ram" in sampleSel:
            ram = sampleSel["ram"]
            if not isinstance(ram, int):
                raise sErr.configError("ram must a int")
        if "target_model" in sampleSel:
            target_model = sampleSel["target_model"]
            if not isinstance(target_model, int):
                raise sErr.configError("target_model must an integer")

    try:
        sampleSel = dict(cfg.arg_train.sample_selection)
        check_parameters(sampleSel)
        if "per_model" in sampleSel:
            for model in sampleSel["per_model"]:
                check_parameters(dict(model))
    # Error managed
    except sErr.configFileError:
        print("Error in the configuration file " + path_conf)
        raise
    # Warning error not managed !
    except Exception:
        print("Something wrong happened in serviceConfigFile !")
        raise


def all_sameBands(items):
    return all(bands == items[0][1] for path, bands in items)


def check_chain_parameters(cfg, path_conf):
    try:
        # test of variable
        if get_param(cfg, "chain", "region_path"):
            test_shape_name(get_param(cfg, "chain", "region_path"))
            # TODO ensure that check region vector is done in builder
            # check_region_vector(cfg)

    # Error managed
    except sErr.configFileError:
        print("Error in the configuration file " + path_conf)
        raise
    # Warning error not managed !
    except Exception:
        print("Something wrong happened in serviceConfigFile !")
        raise


def verify_code_path(code_path):
    """checks the code path
    returns False if code path refers to default external_code
    raises an error if code path refers to non existing custom code file
    returns True if the code path refers to an existing custom code file"""

    if code_path is None:
        return False
    if code_path.lower() == "none":
        return False
    if len(code_path) < 1:
        return False
    if not os.path.isfile(code_path):
        raise ValueError(f"Error: {code_path} is not a correct path")

    return True


def get_external_features_functions(
    module_path: str,
    function_names: list[str],
) -> tuple[dict[str, Callable], list[str]]:
    """search for functions given by name
    returns found_functions and not_found_functions
    module_path: path of custom external features
    function_names: names of the searched functions"""

    found_functions = {}  # dict mapping name -> function
    not_found_functions = []
    # tests validity of module path and if it contains external functions
    if verify_code_path(module_path):
        module = verify_import(module_path)
        for function in function_names:
            try:
                fn = getattr(module, function)
                found_functions[function] = fn
            except AttributeError:
                not_found_functions.append(function)
    else:
        # all function names are not found
        not_found_functions = function_names
    return found_functions, not_found_functions


def search_external_features_function(
    user_defined_module_path: str,
    list_functions: list[FunctionNameWithParams],
) -> tuple[dict[str, Callable], list[str]]:
    """searches function first in user defined module
    and then fallbacks to iota2 module path"""

    # location of iota2 external_code
    in_source_module = os.path.join(
        get_iota2_project_dir(), "iota2", "common", "external_code.py"
    )

    # extracts only function names as string
    function_names = [x[0] for x in list_functions]

    # looks for function in user defined module path
    found_functions, not_found_functions = get_external_features_functions(
        user_defined_module_path, function_names
    )

    # if some functions has not been found, search functions in source
    if not_found_functions:
        in_source_functions, not_in_source_functions = get_external_features_functions(
            in_source_module, not_found_functions
        )

        # all remaining function must be found in source
        if not_in_source_functions:
            raise AttributeError(
                f"functions {not_in_source_functions} not"
                "found in external code source\n"
                f"user_code_source {user_defined_module_path}\n"
                f"iota2 code source {in_source_module}\n"
            )

        # add in_source_functions to found_functions
        found_functions.update(in_source_functions)

    return found_functions


def type_check_functions_kwargs(
    functions: dict[str, Callable], flist: list[FunctionNameWithParams]
):
    """checks keword arguments typing
    functions: a dictionnary mapping function name to associated callable
    flist: a list of tuples with function names / arguments"""
    for (fname, fkwargs) in flist:
        type_check_callable_kwargs(functions[fname], fkwargs)


def type_check_callable_kwargs(callable: Callable, kwargs: dict[str, Any]):
    """checks if kwargs type match callable
    callable: the calloable to check against
    kwargs: keyword arguments to check against callable"""
    sig = signature(callable)
    for (k, v) in kwargs.items():
        param = sig.parameters.get(k)
        if not param:
            raise (KeyError(f"function with signature {sig} has no {k} argument"))
        if param.annotation == param.empty:
            pass  # no indication given, could log a warning?
        elif param.annotation != type(v):
            raise (
                TypeError(
                    f"argument '{k}' has type {type(v)} but {param.annotation} expected"
                )
            )


# #############################################################################
# Functions usable by builders for input checking
# #############################################################################
def region_vector_field_as_string(region_path: str, region_field: str):
    """
    This function raise an error if region field is not a string
    """
    if region_path is None:
        return True
    test_shape_name(region_path)
    if not region_path:
        raise sErr.configError("chain.region_path must be set")

    if not region_path:
        raise sErr.configError("chain.region_field must be set")

    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(region_path, 0)
    if dataSource is None:
        raise Exception("Could not open " + region_path)
    layer = dataSource.GetLayer()
    field_index = layer.FindFieldIndex(region_field, False)
    layerDefinition = layer.GetLayerDefn()
    field_type_code = layerDefinition.GetFieldDefn(field_index).GetType()
    fieldType = layerDefinition.GetFieldDefn(field_index).GetFieldTypeName(
        field_type_code
    )
    if fieldType != "String":
        raise sErr.configError("the region field must be a string")
