#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Module for parse config file"""
import logging
import os
import warnings
from collections import Counter, OrderedDict
from pathlib import Path
from typing import Any, Optional, Union

from pydantic import BaseModel

from config import Config
from iota2.common.file_utils import file_search_and, get_iota2_project_dir
from iota2.configuration_files.sections.chain_section import ChainSection
from iota2.configuration_files.sections.classif_section import ClassifSection
from iota2.configuration_files.sections.other_sections import (
    BuilderSection,
    CoregistrationSection,
    DimRedSection,
    ExternalFeaturesSection,
    I2FeatureExtractionSection,
    OBIASection,
    PythonDataManagingSection,
    SciKitSection,
    SensorsDataInterpSection,
    TaskRetry,
)
from iota2.configuration_files.sections.sensors_sections import (
    Landsat5OldSection,
    Landsat8OldSection,
    Landsat8Section,
    Sentinel2L3ASection,
    Sentinel2S2CSection,
    Sentinel2TheiaSection,
    UserFeatSection,
)
from iota2.configuration_files.sections.simplification_section import (
    SimplificationSection,
)
from iota2.configuration_files.sections.train_section import TrainSection

LOGGER = logging.getLogger("distributed.worker")


class read_internal_config_file:
    """Class to read/write configuration file without any validations."""

    def __init__(self, i2_cfg_file: str):
        """Init read_internal_config_file"""
        self.i2_cfg_file = i2_cfg_file
        with open(self.i2_cfg_file, encoding="UTF-8") as i2_conf_file:
            cfg = Config(i2_conf_file)
        self.cfg_as_dict = cfg.as_dict()

    def getParam(self, section: str, name: str):
        """to mimic the currents getParam() behavior"""
        if section not in self.cfg_as_dict:
            raise ValueError(
                f"section '{section}' does not exists "
                f"in the configuration file '{self.i2_cfg_file}'"
            )
        if name not in self.cfg_as_dict[section]:
            raise ValueError(
                f"parameter '{name}' do not exists in"
                f" section '{section}' configuration file '{self.i2_cfg_file}'"
            )
        return self.cfg_as_dict[section][name]

    def save(self, output_file: str, overwrite: bool = True) -> None:
        """save current parameter status to a file as a dictionary

        implemented mainly for unit test puposes

        Note
        ----
        save every field's values (even default ones)
        """
        output_file = Path(output_file)
        if output_file.exists() and not overwrite:
            warnings.warn(
                f"file : '{output_file}' already exists, skipping save", Warning
            )
            return
        with open(output_file, "w", encoding="UTF-8") as file_cfg:
            file_cfg.write(str(self.cfg_as_dict))


# class read_config_file(metaclass=Singleton):
class read_config_file:
    """I2 configuration file reader."""

    i2_available_sections = [
        BuilderSection,
        ChainSection,
        ClassifSection,
        TaskRetry,
        TrainSection,
        CoregistrationSection,
        SensorsDataInterpSection,
        I2FeatureExtractionSection,
        DimRedSection,
        ExternalFeaturesSection,
        PythonDataManagingSection,
        SciKitSection,
        SimplificationSection,
        Landsat8Section,
        Landsat8OldSection,
        Sentinel2TheiaSection,
        Sentinel2S2CSection,
        Sentinel2L3ASection,
        Landsat5OldSection,
        UserFeatSection,
        OBIASection,
    ]

    # @classmethod
    # def destroy(cls):
    #     """Remove the class instance"""
    #     cls._instances = None
    #     cls._instances = WeakValueDictionary()

    def __init__(self, i2_cfg_file: str):
        """Read_config_file constructor.

        Parameters
        ----------
        i2_cfg_file
            configuration file
        validate_field
            remove pydantic field validators
        """
        self.i2_cfg_file = i2_cfg_file
        with open(self.i2_cfg_file, encoding="UTF-8") as i2_conf_file:
            cfg = Config(i2_conf_file)
        cfg_as_dict = cfg.as_dict()
        if BuilderSection.section_name in cfg_as_dict:
            builder_model = BuilderSection.parse_obj(
                cfg_as_dict[BuilderSection.section_name]
            )
        else:
            builder_model = BuilderSection()

        self.i2_cfg_dict = self.broadcast_builders(builder_model, cfg_as_dict)

    def __str__(self) -> str:
        return str(self.i2_cfg_dict)

    def save(self, output_file: str, overwrite: bool = True) -> None:
        """save current parameter status to a file as a dictionary

        implemented mainly for unit test puposes

        Note
        ----
        save every field's values (even default ones)
        """
        output_file = Path(output_file)
        if output_file.exists() and not overwrite:
            warnings.warn(
                f"file : '{output_file}' already exists, skipping save", Warning
            )
            return
        with open(output_file, "w", encoding="UTF-8") as file_cfg:
            file_cfg.write(str(self.i2_cfg_dict))

    def set_param(
        self, section: str, param_name: str, value: str, validate_value: bool = True
    ) -> None:
        """Update parameter value in configuration file.

        Parameters
        ----------
        section
            section's name
        param_name
            parameter's name
        value
            new value
        validate_value
            flag to check input value type (according to field's definition)
        """
        # check if the section exists
        section_values = self.getSection(section).copy()
        if validate_value:
            section_basemodel = None
            for section_class in self.i2_available_sections:
                if section_class.section_name == section:
                    section_basemodel = section_class
            if section_basemodel is None:
                raise ValueError(f"section '{section}' does not exists")

            section_values[param_name] = value
            updated_section = section_basemodel.parse_obj(section_values).dict()
            self.i2_cfg_dict[section] = updated_section
        else:
            self.i2_cfg_dict[section][param_name] = value

    @classmethod
    def get_params_descriptions(cls) -> dict[str, Any]:
        """I2 parameters documentation."""

        def exclude_fields_from_schema(
            schema: dict[str, dict], excluded_fields: list[str]
        ) -> dict:
            """Exclude some pydantic fields from a pydantic schema, useful to doc."""
            new_schema = schema.copy()
            for section in new_schema.keys():
                for excluded_field in excluded_fields:
                    new_schema[section]["properties"].pop(excluded_field, None)
            return new_schema

        i2_cfg_schema = {}
        for section_class in cls.i2_available_sections:
            section_name = section_class.section_name
            i2_cfg_schema.update({section_name: section_class.schema()})

        i2_cfg_schema = exclude_fields_from_schema(
            i2_cfg_schema, [BuilderSection.section_name]
        )

        descriptions = {}
        field_num = 0
        for section_name, schema in i2_cfg_schema.items():
            fields_meta = schema["properties"]
            for field_name, field_meta in fields_meta.items():
                mandatory_on_builders = field_meta.get("mandatory_on_builders", ())
                available_on_builders = field_meta.get(
                    "available_on_builders", BuilderSection.avail_builders
                )
                for builder_name in mandatory_on_builders:
                    if builder_name not in available_on_builders:
                        raise ValueError(
                            "inconsistency detected between "
                            "'mandatory_on_builders' and "
                            "'available_on_builders' at field '{field_name}'"
                            " definition"
                        )
                if "short_desc" not in field_meta:
                    raise ValueError(
                        "missing attribute 'short_desc' "
                        f"for the parameter '{field_name}'"
                    )
                if "\n" in field_meta["short_desc"]:
                    raise ValueError(
                        f"the field '{field_name}' get the "
                        "character '\n' which forbidden"
                    )
                descriptions[field_num] = {
                    "field_name": field_name,
                    "section": section_name,
                    "short_desc": field_meta["short_desc"],
                    "long_desc": field_meta.get("long_desc", None),
                    "default": field_meta.get("default"),
                    "type": field_meta["doc_type"],
                    "available_on_builders": available_on_builders,
                    "mandatory_on_builders": mandatory_on_builders,
                }
                field_num += 1
        return descriptions

    def broadcast_builders(
        self, builder_model: BaseModel, cfg_as_dict: dict
    ) -> tuple[dict, dict]:
        """Add a new field to every BaseModel and validate BaseModel sections."""
        i2_cfg_dict = {BuilderSection.section_name: builder_model.dict()}
        for section_class in self.i2_available_sections:
            section_name = section_class.section_name
            if section_name == BuilderSection.section_name:
                continue
            section_class.add_fields(builders=(list, builder_model.builders_class_name))
            if section_name in cfg_as_dict:
                section_values = section_class.parse_obj(
                    cfg_as_dict[section_name]
                ).dict()
            else:
                section_values = section_class().dict()
            i2_cfg_dict.update({section_name: section_values})
        return i2_cfg_dict

    def getSection(self, section: str) -> dict:
        """Get an entire cfg section."""
        if section not in self.i2_cfg_dict:
            raise ValueError(
                f"section '{section}' does not exists "
                f"in the configuration file '{self.i2_cfg_file}'"
            )
        return self.i2_cfg_dict[section]

    def getParam(self, section: str, name: str) -> Any:
        """Get a iota2 cfg parameter."""
        if section not in self.i2_cfg_dict:
            raise ValueError(
                f"section '{section}' does not exists "
                f"in the configuration file '{self.i2_cfg_file}'"
            )
        if name not in self.i2_cfg_dict[section]:
            raise ValueError(
                f"parameter '{name}' do not exists in"
                f" section '{section}' configuration file '{self.i2_cfg_file}'"
            )
        return self.i2_cfg_dict[section][name]

    def get_builders(self) -> list:
        """Get list all builders involved."""

        def check_code_path(code_path):

            if code_path is None:
                return False
            if code_path.lower() == "none":
                return False
            if len(code_path) < 1:
                return False
            if not os.path.isfile(code_path):
                raise ValueError(f"Error: {code_path} is not a correct path")
            return True

        def check_import(module_path):
            import importlib

            spec = importlib.util.spec_from_file_location(
                module_path.split(os.sep)[-1].split(".")[0], module_path
            )
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            return module

        def check_function_in_module(module, builder_name):

            try:
                builder_init = getattr(module, builder_name)
                return builder_init
            except AttributeError:
                raise AttributeError(f"{module.__name__} has no class {builder_name}")

        # will contains iota2 builder module then user builder modules
        modules_path = []

        i2_mod_path = os.path.join(
            get_iota2_project_dir(), "iota2", "sequence_builders"
        )
        i2_modules = file_search_and(i2_mod_path, True, ".py")
        modules_path = list(
            filter(lambda x: ".pyc" not in x and "__init__" not in x, i2_modules)
        )

        if user_mod_paths := self.getParam("builders", "builders_paths"):
            user_modules = []
            for user_mod_path in user_mod_paths:
                user_modules += file_search_and(user_mod_path, True, ".py")
            modules_path += list(
                filter(
                    lambda x: ".pyc" not in x
                    and "__init__" not in x
                    and ".py~" not in x,
                    user_modules,
                )
            )

        class_names = self.getParam("builders", "builders_class_name")
        builders = []
        for class_name in class_names:
            class_found = []
            for module_path in modules_path:
                module_path_valid = check_code_path(module_path)
                module = None
                init_builder = None
                if module_path_valid:
                    module = check_import(module_path)
                    try:
                        init_builder = check_function_in_module(module, class_name)
                        class_found.append(True)
                    except AttributeError:
                        class_found.append(False)
                        continue
                builders.append(init_builder)
                break
            if not any(class_found):
                raise ValueError(
                    f"builder class '{class_name}' can't be found in "
                    f"modules '{' '.join(modules_path)}'"
                )
        return builders


class iota2_parameters:
    """
    describe iota2 parameters as usual python dictionary
    """

    def __init__(self, config):
        """ """
        self.__config = config
        self.projection = int(self.__config.getParam("chain", "proj").split(":")[-1])
        self.all_tiles = self.__config.getParam("chain", "list_tile")
        self.i2_output_path = self.__config.getParam("chain", "output_path")
        self.extract_bands_flag = self.__config.getParam(
            "iota2_feature_extraction", "extract_bands"
        )
        self.auto_date = self.__config.getParam(
            "sensors_data_interpolation", "auto_date"
        )
        self.write_outputs_flag = self.__config.getParam(
            "sensors_data_interpolation", "write_outputs"
        )
        self.features_list = self.__config.getParam("arg_train", "features")
        self.enable_gapfilling = self.__config.getParam(
            "sensors_data_interpolation", "use_gapfilling"
        )
        self.hand_features_flag = self.__config.getParam(
            "sensors_data_interpolation", "use_additional_features"
        )
        self.copy_input = self.__config.getParam(
            "iota2_feature_extraction", "copy_input"
        )
        self.rel_refl = self.__config.getParam("iota2_feature_extraction", "rel_refl")
        self.keep_dupl = self.__config.getParam(
            "iota2_feature_extraction", "keep_duplicates"
        )
        self.vhr_path = self.__config.getParam("coregistration", "vhr_path")
        self.acor_feat = self.__config.getParam("iota2_feature_extraction", "acor_feat")
        self.user_patterns = self.__config.getParam("userFeat", "patterns").split(",")

        self.available_sensors_section = [
            "Sentinel_2",
            "Sentinel_2_S2C",
            "Sentinel_2_L3A",
            "Sentinel_1",
            "Landsat8",
            "Landsat8_old",
            "Landsat5_old",
            "userFeat",
        ]
        self.working_resolution = self.__config.getParam("chain", "spatial_resolution")

    def get_sensors_parameters(
        self, tile_name: str
    ) -> dict[str, dict[str, Union[str, list[str], int]]]:
        """get enabled sensors parameters"""
        sensors_parameters = {}
        for sensor_section_name in self.available_sensors_section:
            sensor_parameter = self.build_sensor_dict(tile_name, sensor_section_name)
            if sensor_parameter:
                sensors_parameters[sensor_section_name] = sensor_parameter
        return sensors_parameters

    def build_sensor_dict(
        self, tile_name: str, sensor_section_name: str
    ) -> dict[str, Union[str, list[str], int]]:
        """get sensor parameters"""
        sensor_dict = {}
        sensor_output_target_dir = None
        sensor_data_param_name = ""
        keep_bands = None
        if sensor_section_name == "Sentinel_2":
            sensor_data_param_name = "s2_path"
            sensor_output_target_dir = self.__config.getParam("chain", "s2_output_path")
            keep_bands = self.__config.getParam("Sentinel_2", "keep_bands")
        elif sensor_section_name == "Sentinel_1":
            sensor_data_param_name = "s1_path"
        elif sensor_section_name == "Landsat8":
            sensor_data_param_name = "l8_path"
            keep_bands = self.__config.getParam("Landsat8", "keep_bands")

        elif sensor_section_name == "Landsat8_old":
            sensor_data_param_name = "l8_path_old"
            keep_bands = self.__config.getParam("Landsat8_old", "keep_bands")

        elif sensor_section_name == "Landsat5_old":
            sensor_data_param_name = "l5_path_old"
            keep_bands = self.__config.getParam("Landsat5_old", "keep_bands")

        elif sensor_section_name == "Sentinel_2_S2C":
            sensor_data_param_name = "s2_s2c_path"
            keep_bands = self.__config.getParam("Sentinel_2_S2C", "keep_bands")

            sensor_output_target_dir = self.__config.getParam(
                "chain", "s2_s2c_output_path"
            )
        elif sensor_section_name == "Sentinel_2_L3A":
            sensor_data_param_name = "s2_l3a_path"
            sensor_output_target_dir = self.__config.getParam(
                "chain", "s2_l3a_output_path"
            )
            keep_bands = self.__config.getParam("Sentinel_2_L3A", "keep_bands")

        elif sensor_section_name == "userFeat":
            sensor_data_param_name = "user_feat_path"
        else:
            raise ValueError(f"unknown section : {sensor_section_name}")
        sensor_data_path = self.__config.getParam("chain", sensor_data_param_name)
        if not sensor_data_path:
            return sensor_dict
        if sensor_section_name == "Sentinel_1":
            sensor_section = {}
        else:
            sensor_section = self.__config.getSection(sensor_section_name)

        sensor_dict["tile_name"] = tile_name
        sensor_dict["target_proj"] = self.projection
        sensor_dict["all_tiles"] = self.all_tiles
        sensor_dict["image_directory"] = sensor_data_path
        sensor_dict["write_dates_stack"] = sensor_section.get(
            "write_reproject_resampled_input_dates_stack", None
        )
        sensor_dict["extract_bands_flag"] = self.extract_bands_flag
        sensor_dict["output_target_dir"] = sensor_output_target_dir
        sensor_dict["keep_bands"] = keep_bands
        sensor_dict["i2_output_path"] = self.i2_output_path
        sensor_dict["temporal_res"] = sensor_section.get("temporal_resolution", None)
        sensor_dict["auto_date_flag"] = self.auto_date
        sensor_dict["date_interp_min_user"] = sensor_section.get("start_date", None)
        sensor_dict["date_interp_max_user"] = sensor_section.get("end_date", None)
        sensor_dict["write_outputs_flag"] = self.write_outputs_flag
        sensor_dict["features"] = self.features_list
        sensor_dict["enable_gapfilling"] = self.enable_gapfilling
        sensor_dict["hand_features_flag"] = self.hand_features_flag
        sensor_dict["hand_features"] = sensor_section.get("additional_features", None)
        sensor_dict["copy_input"] = self.copy_input
        sensor_dict["rel_refl"] = self.rel_refl
        sensor_dict["keep_dupl"] = self.keep_dupl
        sensor_dict["vhr_path"] = self.vhr_path
        sensor_dict["acor_feat"] = self.acor_feat
        sensor_dict["patterns"] = self.user_patterns
        sensor_dict["working_resolution"] = self.working_resolution
        return sensor_dict

    def get_sentinel1_input_dates(
        self, targeted_tiles: Optional[list[str]] = None
    ) -> dict:
        """ """

        tiles = self.all_tiles.split(" ")
        if targeted_tiles is not None:
            tiles = targeted_tiles

        check_orbit_order = ["DES", "ASC"]
        sensors_dates = OrderedDict()
        for orbit in check_orbit_order:
            for tile in tiles:
                i2_input_date_dir = os.path.join(
                    self.i2_output_path, "features", tile, "tmp"
                )

                files_tile_dates = file_search_and(
                    i2_input_date_dir, True, f"Sentinel1_{orbit}_{tile}_input_dates.txt"
                )
                if files_tile_dates:
                    sensors_dates[orbit] = {}
                    tile_dates = []
                    for file_tile_dates in files_tile_dates:
                        with open(file_tile_dates) as date_file:
                            for line in date_file:
                                try:
                                    new_date = int(line)
                                    tile_dates.append(new_date)
                                except ValueError:
                                    pass
                    sensors_dates[orbit][tile] = Counter(tile_dates)

        sensors_dates_buff = OrderedDict()
        for s1_orbit, sensor_tiles_dates in sensors_dates.items():
            sensor_dates = []
            for _, tile_dates_counter in sensor_tiles_dates.items():
                # counter iterations
                for date, count_in_tile in tile_dates_counter.items():
                    if date not in sensor_dates:
                        sensor_dates += [date] * count_in_tile
                    else:
                        counter_all_dates = Counter(sensor_dates)
                        if count_in_tile == counter_all_dates[date] == 1:
                            pass
                        else:
                            sensor_dates += [date] * count_in_tile
            sensors_dates_buff[s1_orbit] = list(sensor_dates)

        sorted_sensors_dates = OrderedDict()
        for orbit, sensor_dates in sensors_dates_buff.items():
            sorted_sensors_dates[f"Sentinel1_{orbit}_vv"] = list(
                map(str, sorted(sensor_dates))
            )
            sorted_sensors_dates[f"Sentinel1_{orbit}_vh"] = list(
                map(str, sorted(sensor_dates))
            )
        return sorted_sensors_dates

    def get_available_sensors_dates(
        self, targeted_tiles: Optional[list[str]] = None
    ) -> OrderedDict:
        """ """
        from iota2.sensors.sensors_container import sensors_container

        tiles = self.all_tiles.split(" ")
        if targeted_tiles is not None:
            tiles = targeted_tiles

        for tile in tiles:
            sensors_parameters = self.get_sensors_parameters(tile)
            sensor_tile_container = sensors_container(
                tile, None, self.i2_output_path, **sensors_parameters
            )
            sensors = sensor_tile_container.get_enabled_sensors()
            sensors_order = [sensor.__class__.name for sensor in sensors]
            # write every sensors dates
            for sensor in sensors:
                if (
                    sensor.name.lower() == "userfeatures"
                    or sensor.name.lower() == "sentinel1"
                ):
                    continue
                sensor.write_dates_file()

        sensors_dates = OrderedDict()
        for sensor_name in sensors_order:
            sensors_dates[sensor_name] = {}
            if (
                sensor_name.lower() == "userfeatures"
                or sensor_name.lower() == "sentinel1"
            ):
                continue
            for tile in tiles:
                i2_input_date_dir = os.path.join(
                    self.i2_output_path, "features", tile, "tmp"
                )
                if "sentinel1" in sensor_name.lower():
                    patterns = [f"{sensor_name}", f"{tile}_input_dates.txt"]
                else:
                    patterns = [f"{sensor_name}_{tile}_input_dates.txt"]
                tile_dates = []
                files_tile_dates = file_search_and(i2_input_date_dir, True, *patterns)
                if not files_tile_dates:
                    raise ValueError(
                        f"{tile} inputs dates cannot be find : {sensor_name}"
                    )
                for file_tile_dates in files_tile_dates:
                    with open(file_tile_dates) as date_file:
                        for line in date_file:
                            try:
                                new_date = int(line)
                                tile_dates.append(new_date)
                            except ValueError:
                                pass
                sensors_dates[sensor_name][tile] = Counter(tile_dates)

        sensors_dates_buff = OrderedDict()
        for sensor_name, sensor_tiles_dates in sensors_dates.items():
            sensor_dates = []
            for _, tile_dates_counter in sensor_tiles_dates.items():
                # counter iterations
                for date, count_in_tile in tile_dates_counter.items():
                    if date not in sensor_dates:
                        sensor_dates += [date] * count_in_tile
                    else:
                        counter_all_dates = Counter(sensor_dates)
                        if count_in_tile == counter_all_dates[date] == 1:
                            pass
                        else:
                            sensor_dates += [date] * count_in_tile
            sensors_dates_buff[sensor_name] = list(sensor_dates)

        # print(sensors_dates_buff)
        sorted_sensors_dates = OrderedDict()
        for sensor_name, sensor_dates in sensors_dates_buff.items():
            sorted_sensors_dates[sensor_name] = list(map(str, sorted(sensor_dates)))
        return sorted_sensors_dates
