#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""sentinel_1 class definition"""

import logging
import os
from collections import OrderedDict

import iota2.common.i2_constants as i2_const
from iota2.common.file_utils import file_search_and, get_nb_date_in_tile
from iota2.common.otb_app_bank import (
    CreateBandMathApplication,
    CreateConcatenateImagesApplication,
    CreateImageTimeSeriesGapFillingApplication,
    generateSARFeat_dates,
    getInputParameterOutput,
    getSARstack,
)

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")

# in order to avoid issue 'No handlers could be found for logger...'
LOGGER.addHandler(logging.NullHandler())


class sentinel_1:
    """Sentinel_1 class definition."""

    name = "Sentinel1"
    nodata_value = -10000

    def __init__(
        self,
        tile_name: str,
        image_directory: str,
        all_tiles: str,
        i2_output_path: str,
        enable_gapfilling: bool,
        write_outputs_flag: bool,
        **kwargs,  # pylint: disable=unused-argument
    ):
        """ """
        import configparser

        self.tile_name = tile_name

        config_parser = configparser.ConfigParser()

        # running attributes
        self.s1_cfg = image_directory
        self.write_outputs_flag = write_outputs_flag
        config_parser.read(self.s1_cfg)
        s1_output_processing = config_parser.get("Paths", "output")
        self.all_tiles = all_tiles
        self.output_processing = os.path.join(s1_output_processing, tile_name[1:])
        self.i2_output_path = i2_output_path
        self.features_dir = os.path.join(self.i2_output_path, "features", tile_name)
        self.use_gapfilling = enable_gapfilling
        # sensors attributes
        self.mask_pattern = "BorderMask.tif"
        # output's names
        self.mask_orbit_pol_name = f"{self.__class__.name}_{tile_name}_MASK"
        self.gapfilling_orbit_pol_name = f"{self.__class__.name}_{tile_name}_TSG"
        self.gapfilling_orbit_pol_name_mask = f"{self.__class__.name}_{tile_name}_MASK"
        self.sar_features_name = f"{self.__class__.name}_{tile_name}_Features.tif"
        self.user_sar_features_name = (
            f"{self.__class__.name}_{tile_name}_USER_Features.tif"
        )
        self.footprint_name = f"{self.__class__.name}_{tile_name}_footprint.tif"

        self.stack_band_position = ["vv", "vh"]
        self.features_names_list = ["DES", "ASC"]

    def _get_dates(self, date_file_prefix, date_file_suffix) -> dict[str, list[str]]:
        """Return sorted available dates per obits."""
        i2_input_date_dir = os.path.join(
            self.i2_output_path, "features", self.tile_name, "tmp"
        )
        orbits = ["DES", "ASC"]
        sensors_dates: dict[str, list[str]] = OrderedDict()
        for orbit in orbits:
            files_tile_dates = file_search_and(
                i2_input_date_dir,
                True,
                f"{date_file_prefix}_{orbit}_{date_file_suffix}.txt",
            )
            if files_tile_dates:
                sensors_dates[orbit + "_vv"] = []
                with open(files_tile_dates[0]) as date_file:
                    for line in date_file:
                        try:
                            new_date = str(int(line))
                            sensors_dates[orbit + "_vv"].append(new_date)
                        except ValueError:
                            pass
                sensors_dates[orbit + "_vh"] = sensors_dates[orbit + "_vv"]
        return sensors_dates

    def get_available_dates(self) -> dict[str, list[str]]:
        """Return sorted available input dates per obits."""
        return self._get_dates("Sentinel1", f"{self.tile_name}_input_dates")

    def get_interpolated_dates(self) -> dict[str, list[str]]:
        """Return sorted available interpolated dates per obits."""
        return self._get_dates("S1_vv", "dates_interpolation")

    def get_available_dates_masks(self):
        """Return sorted available masks."""

    def preprocess(
        self, working_dir=None, ram=128, logger=LOGGER
    ):  # pylint: disable=unused-argument
        """sentinel_1 preprocessing"""
        # to generate dates txt file per dates
        _ = getSARstack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            os.path.join(self.i2_output_path, "features"),
            workingDirectory=working_dir,
        )

    def footprint(self, ram=128):
        """get sentinel_1 footprint"""

        s1_border_masks = file_search_and(
            self.output_processing, True, self.mask_pattern
        )

        sum_mask = "+".join([f"im{i + 1}b1" for i in range(len(s1_border_masks))])
        expression = f"{sum_mask}=={len(s1_border_masks)}?0:1"
        raster_footprint = os.path.join(self.features_dir, "tmp", self.footprint_name)
        footprint_app = CreateBandMathApplication(
            {
                "il": s1_border_masks,
                "out": raster_footprint,
                "exp": expression,
                "ram": str(ram),
            }
        )
        footprint_app_dep = []
        return footprint_app, footprint_app_dep

    def get_time_series(self, ram=128):  # pylint: disable=unused-argument
        """
        Due to the SAR data, time series must be split by polarisation
        and orbit (ascending / descending)
        """
        (all_filtered, all_masks, interp_date_files, input_date_files) = getSARstack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            os.path.join(self.i2_output_path, "features"),
            workingDirectory=None,
        )
        # to be clearer
        s1_data = OrderedDict()
        s1_labels = OrderedDict()

        for filtered, _, _, in_dates in zip(
            all_filtered, all_masks, interp_date_files, input_date_files
        ):
            sar_mode = os.path.basename(filtered.GetParameterValue("outputstack"))
            sar_mode = "_".join(os.path.splitext(sar_mode)[0].split("_")[0:-1])
            polarisation = sar_mode.split("_")[1]
            orbit = sar_mode.split("_")[2]

            s1_data[sar_mode] = filtered
            sar_dates = sorted(
                get_nb_date_in_tile(in_dates, display=False, raw_dates=True),
                key=lambda x: int(x),
            )
            labels = [
                f"{self.__class__.name}_{orbit}{polarisation}_{date}".lower()
                for date in sar_dates
            ]
            s1_labels[sar_mode] = labels
        dependancies = []
        return (s1_data, dependancies), s1_labels

    def get_time_series_raw(self, ram=128):  # pylint: disable=unused-argument
        """."""
        (s1_data, dependancies), s1_labels = self.get_time_series()

        for _, app in s1_data.items():
            app.Execute()
            dependancies.append(app)
        data_to_stack = [app for _, app in s1_data.items()]

        stack_time_series = CreateConcatenateImagesApplication({"il": data_to_stack})
        stack_time_series.Execute()
        stacked_features_labels = []
        for _, labels in s1_labels.items():
            stacked_features_labels += labels
        return (stack_time_series, dependancies), stacked_features_labels

    def get_time_series_masks(
        self, ram=128, logger=LOGGER
    ):  # pylint: disable=unused-argument
        """
        Due to the SAR data, masks series must be split by polarisation
        and orbit (ascending / descending)
        """
        (all_filtered, all_masks, interp_date_files, input_date_files) = getSARstack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            os.path.join(self.i2_output_path, "features"),
            workingDirectory=None,
        )
        # to be clearer
        s1_masks = OrderedDict()
        nb_avail_masks = 0
        for filtered, masks, _, _ in zip(
            all_filtered, all_masks, interp_date_files, input_date_files
        ):
            sar_mode = os.path.basename(filtered.GetParameterValue("outputstack"))
            sar_mode = "_".join(os.path.splitext(sar_mode)[0].split("_")[0:-1])
            polarisation = sar_mode.split("_")[1]
            orbit = sar_mode.split("_")[2]
            mask_orbit_pol_name = (
                f"{self.mask_orbit_pol_name}_{orbit}_{polarisation}.tif"
            )

            mask_orbit_pol = os.path.join(self.features_dir, "tmp", mask_orbit_pol_name)
            masks_app = CreateConcatenateImagesApplication(
                {
                    "il": masks,
                    "out": mask_orbit_pol,
                    "pixType": "uint8" if len(masks) > 255 else "uint16",
                    "ram": str(ram),
                }
            )
            s1_masks[sar_mode] = masks_app
            nb_avail_masks += len(masks)

        dependancies = []
        return s1_masks, dependancies, nb_avail_masks

    def get_time_series_masks_raw(
        self, ram=128, logger=LOGGER
    ):  # pylint: disable=unused-argument
        """."""
        s1_masks, dependancies, nb_avail_masks = self.get_time_series_masks()
        data_to_stack = []
        for _, app in s1_masks.items():
            app.Execute()
            dependancies.append(app)

        # vv masks are the same than vh masks
        if "S1_vv_DES" in s1_masks:
            data_to_stack.append(s1_masks["S1_vv_DES"])
        if "S1_vv_ASC" in s1_masks:
            data_to_stack.append(s1_masks["S1_vv_ASC"])

        mask_time_series = CreateConcatenateImagesApplication({"il": data_to_stack})
        nb_avail_masks = nb_avail_masks / 2
        return mask_time_series, dependancies, nb_avail_masks

    def write_dates_file(self):
        """Actually this method do not write the file containing intput tile dates
        but return
        """
        _ = getSARstack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            os.path.join(self.i2_output_path, "features"),
            workingDirectory=None,
        )

    def get_time_series_gapFilling(self, ram=128):
        """
        Due to the SAR data, time series must be split by polarisation
        and orbit (ascending / descending)
        """
        import configparser

        (all_filtered, all_masks, interp_date_files, input_date_files) = getSARstack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            os.path.join(self.i2_output_path, "features"),
            workingDirectory=None,
        )
        # to be clearer
        s1_data = OrderedDict()
        s1_labels = OrderedDict()

        config = configparser.ConfigParser()
        config.read(self.s1_cfg)

        interpolation_method = config.get(
            "Processing", "gapFilling_interpolation", fallback=I2_CONST.s1_interp_method
        )
        dependancies = []

        for filtered, masks, interp_dates, in_dates in zip(
            all_filtered, all_masks, interp_date_files, input_date_files
        ):
            sar_mode = os.path.basename(filtered.GetParameterValue("outputstack"))
            sar_mode = "_".join(os.path.splitext(sar_mode)[0].split("_")[0:-1])
            polarisation = sar_mode.split("_")[1]
            orbit = sar_mode.split("_")[2]

            gapfilling_orbit_pol_name_masks = (
                f"{self.gapfilling_orbit_pol_name_mask}_{orbit}_{polarisation}.tif"
            )
            gapfilling_raster_mask = os.path.join(
                self.features_dir, "tmp", gapfilling_orbit_pol_name_masks
            )

            masks_stack = CreateConcatenateImagesApplication(
                {"il": masks, "out": gapfilling_raster_mask, "ram": str(ram)}
            )

            if self.write_outputs_flag is False:
                filtered.Execute()
                masks_stack.Execute()
            else:
                filtered_raster = filtered.GetParameterValue(
                    getInputParameterOutput(filtered)
                )
                masks_stack_raster = masks_stack.GetParameterValue(
                    getInputParameterOutput(masks_stack)
                )
                if not os.path.exists(masks_stack_raster):
                    masks_stack.ExecuteAndWriteOutput()
                if not os.path.exists(filtered_raster):
                    filtered.ExecuteAndWriteOutput()
                if os.path.exists(masks_stack_raster):
                    masks_stack = masks_stack_raster
                if os.path.exists(filtered_raster):
                    filtered = filtered_raster
            dependancies.append((filtered, masks_stack))
            gapfilling_orbit_pol_name = (
                f"{self.gapfilling_orbit_pol_name}_{orbit}_{polarisation}.tif"
            )
            gapfilling_raster = os.path.join(
                self.features_dir, "tmp", gapfilling_orbit_pol_name
            )

            gap_app = CreateImageTimeSeriesGapFillingApplication(
                {
                    "in": filtered,
                    "mask": masks_stack,
                    "it": interpolation_method,
                    "id": in_dates,
                    "od": interp_dates,
                    "comp": str(1),
                    "out": gapfilling_raster,
                }
            )
            s1_data[sar_mode] = gap_app

            sar_dates = sorted(
                get_nb_date_in_tile(interp_dates, display=False, raw_dates=True),
                key=lambda x: int(x),
            )
            labels = [
                f"{self.__class__.name}_{orbit}{polarisation}_{date}".lower()
                for date in sar_dates
            ]
            s1_labels[sar_mode] = labels
        return (s1_data, dependancies), s1_labels

    def get_features(self, ram=128, logger=LOGGER):  # pylint: disable=unused-argument
        """get sar features"""
        import configparser

        if self.use_gapfilling:
            (s1_data, dependancies), s1_labels = self.get_time_series_gapFilling(ram)
        else:
            (s1_data, dependancies), s1_labels = self.get_time_series(ram)
        config = configparser.ConfigParser()
        config.read(self.s1_cfg)

        sar_features_expr = None
        if config.has_option("Features", "expression"):
            sar_features_expr_cfg = config.get("Features", "expression")
            if sar_features_expr_cfg.lower() != "none":
                sar_features_expr = sar_features_expr_cfg.split(",")

        dependancies = [dependancies]
        s1_features = []
        sar_time_series = {
            "asc": {
                "vv": {"App": None, "availDates": None},
                "vh": {"App": None, "availDates": None},
            },
            "des": {
                "vv": {"App": None, "availDates": None},
                "vh": {"App": None, "availDates": None},
            },
        }
        for sensor_mode, time_series_app in list(s1_data.items()):
            _, polarisation, orbit = sensor_mode.split("_")
            # inputs
            if self.write_outputs_flag is False:
                time_series_app.Execute()
            else:
                time_series_raster = time_series_app.GetParameterValue(
                    getInputParameterOutput(time_series_app)
                )
                if not os.path.exists(time_series_raster):
                    time_series_app.ExecuteAndWriteOutput()
                if os.path.exists(time_series_raster):
                    time_series_app = time_series_raster

            sar_time_series[orbit.lower()][polarisation.lower()][
                "App"
            ] = time_series_app

            s1_features.append(time_series_app)
            dependancies.append(time_series_app)
            if self.use_gapfilling:
                date_file = file_search_and(
                    self.features_dir,
                    True,
                    f"{polarisation.lower()}_{orbit.upper()}_dates_interpolation.txt",
                )[0]
            else:
                tar_dir = os.path.join(
                    config.get("Paths", "output"), self.tile_name[1:]
                )
                date_file = file_search_and(
                    tar_dir,
                    True,
                    f"{polarisation.lower()}_{orbit.upper()}_dates_input.txt",
                )[0]
            sar_time_series[orbit.lower()][polarisation.lower()][
                "availDates"
            ] = get_nb_date_in_tile(date_file, display=False, raw_dates=True)
        features_labels = []
        for sensor_mode, features in list(s1_labels.items()):
            features_labels += features
        if sar_features_expr:
            sar_user_features_raster = os.path.join(
                self.features_dir, "tmp", self.user_sar_features_name
            )
            user_sar_features, user_sar_features_lab = generateSARFeat_dates(
                sar_features_expr, sar_time_series, sar_user_features_raster
            )
            if self.write_outputs_flag is False:
                user_sar_features.Execute()
            else:
                if not os.path.exists(sar_user_features_raster):
                    user_sar_features.ExecuteAndWriteOutput()
                if os.path.exists(sar_user_features_raster):
                    user_sar_features = sar_user_features_raster
            dependancies.append(user_sar_features)
            s1_features.append(user_sar_features)
            features_labels += user_sar_features_lab
        sar_features_raster = os.path.join(
            self.features_dir, "tmp", self.sar_features_name
        )
        sar_features = CreateConcatenateImagesApplication(
            {"il": s1_features, "out": sar_features_raster, "ram": str(ram)}
        )
        return (sar_features, dependancies), features_labels
