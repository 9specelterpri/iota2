# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging
import os
import shutil

import iota2.configuration_files.read_config_file as rcf
from iota2.common.file_utils import ensure_dir
from iota2.common.otb_app_bank import (
    CreateBandMathApplication,
    CreateConcatenateImagesApplication,
)
from iota2.common.utils import run
from iota2.sensors.sensors_container import sensors_container
from iota2.vector_tools.vector_functions import erode_shapefile, remove_shape

LOGGER = logging.getLogger("distributed.worker")


def preprocess(
    tile_name, config_path, output_path, working_directory=None, ram=128, logger=LOGGER
):
    """Preprocessing input rasters data by tile.

    Parameters
    ----------
    tile_name [string]
        tile's name
    config_path [string]
        absolute path to the configuration file
    output_path : str
        iota2 output path
    working_directory [string]
        absolute path to a working directory
    ram [int]
        pipeline's size (Mo)
    """
    logger.debug(f"pre-processing {tile_name}")
    running_parameters = rcf.iota2_parameters(rcf.read_config_file(config_path))
    sensors_parameters = running_parameters.get_sensors_parameters(tile_name)

    remote_sensor_container = sensors_container(
        tile_name, working_directory, output_path, **sensors_parameters
    )
    remote_sensor_container.sensors_preprocess(available_ram=ram, logger=logger)


def common_mask(
    tile_name,
    output_path,
    sensors_parameters,
    working_directory=None,
    ram=128,
    logger=LOGGER,
):
    """Compute common mask considering all sensors by tile.

    Parameters
    ----------
    tile_name [string]
        tile's name
    config_path [string]
        absolute path to the configuration file
    output_path : str
        iota2 output path
    working_directory [string]
        absolute path to a working directory
    ram [int]
        pipeline's size (Mo)
    """
    logger.info(f"{tile_name} common masks generation")
    remote_sensor_container = sensors_container(
        tile_name, working_directory, output_path, **sensors_parameters, logger=logger
    )
    common_mask_app, _ = remote_sensor_container.get_common_sensors_footprint(
        available_ram=ram
    )
    common_mask_raster = common_mask_app.GetParameterValue("out")
    common_mask_vector = common_mask_raster.replace(".tif", ".shp")
    ensure_dir(os.path.split(common_mask_raster)[0], raise_exe=False)
    common_mask_app.ExecuteAndWriteOutput()

    common_mask_vector_cmd = (
        'gdal_polygonize.py -f "ESRI Shapefile" -mask '
        f"{common_mask_raster} {common_mask_raster} {common_mask_vector}"
    )
    run(common_mask_vector_cmd, logger=logger)


def validity(
    tile_name,
    config_path,
    output_path,
    maskout_name,
    view_threshold,
    working_directory=None,
    ram=128,
    logger=LOGGER,
):
    """Compute validity raster/vector by tile.

    Parameters
    ----------
    tile_name [string]
        tile's name
    config_path [string]
        absolute path to the configuration file
    maskout_name [string]
        output vector mask's name
    view_threshold [int]
        threshold
    working_directory [string]
        absolute path to a working directory
    ram [int]
        pipeline's size (Mo)
    """
    features_dir = os.path.join(output_path, "features", tile_name)
    validity_name = "nbView.tif"

    validity_out = os.path.join(features_dir, validity_name)
    validity_processing = validity_out
    if working_directory:
        ensure_dir(os.path.join(working_directory, tile_name))
        validity_processing = os.path.join(working_directory, tile_name, validity_name)

    running_parameters = rcf.iota2_parameters(rcf.read_config_file(config_path))
    sensors_parameters = running_parameters.get_sensors_parameters(tile_name)
    remote_sensor_container = sensors_container(
        tile_name, working_directory, output_path, **sensors_parameters
    )

    sensors_time_series_masks = remote_sensor_container.get_sensors_time_series_masks(
        available_ram=ram
    )
    sensors_masks_size = []
    sensors_masks = []
    for sensor_name, (
        time_series_masks,
        _,
        nb_bands,
    ) in sensors_time_series_masks:
        if sensor_name.lower() == "sentinel1":
            for _, time_series_masks_app in list(time_series_masks.items()):
                time_series_masks_app.Execute()
                sensors_masks.append(time_series_masks_app)
        else:
            time_series_masks.Execute()
            sensors_masks.append(time_series_masks)
        sensors_masks_size.append(nb_bands)

    total_dates = sum(sensors_masks_size)
    merge_masks = CreateConcatenateImagesApplication(
        {"il": sensors_masks, "ram": str(ram)}
    )
    merge_masks.Execute()

    validity_app = CreateBandMathApplication(
        {
            "il": merge_masks,
            "exp": f"{total_dates}-({'+'.join([f'im1b{i + 1}' for i in range(total_dates)])})",
            "ram": str(0.7 * ram),
            "pixType": "uint8" if total_dates < 255 else "uint16",
            "out": validity_processing,
        }
    )
    validity_app.ExecuteAndWriteOutput()
    if working_directory:
        shutil.copy(validity_processing, os.path.join(features_dir, validity_name))
    threshold_raster_out = os.path.join(
        features_dir, maskout_name.replace(".shp", ".tif")
    )
    threshold_vector_out_tmp = os.path.join(
        features_dir, maskout_name.replace(".shp", "_TMP.shp")
    )
    threshold_vector_out = os.path.join(features_dir, maskout_name)

    input_threshold = (
        validity_processing if os.path.exists(validity_processing) else validity_out
    )

    threshold_raster = CreateBandMathApplication(
        {
            "il": input_threshold,
            "exp": f"im1b1>={view_threshold}?1:0",
            "ram": str(0.7 * ram),
            "pixType": "uint8",
            "out": threshold_raster_out,
        }
    )
    threshold_raster.ExecuteAndWriteOutput()
    cmd_poly = (
        f"gdal_polygonize.py -mask {threshold_raster_out} {threshold_raster_out} "
        f'-f "ESRI Shapefile" {threshold_vector_out_tmp} '
        f"{os.path.splitext(os.path.basename(threshold_vector_out_tmp))[0]} cloud"
    )
    run(cmd_poly, logger=logger)

    erode_shapefile(threshold_vector_out_tmp, threshold_vector_out, 0.1)
    os.remove(threshold_raster_out)
    remove_shape(
        threshold_vector_out_tmp.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"]
    )
