"""Tool which apply pylint to every iota2's python file."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

from io import StringIO

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
from pathlib import Path
from typing import Tuple

import pandas as pd
from dask.distributed import Client, as_completed
from pylint.lint import Run
from pylint.reporters.text import TextReporter
from tabulate import tabulate

from iota2.common.file_utils import file_search_and, get_iota2_project_dir


def pylint_score(py_file: str) -> Tuple[float, str]:
    """Get pylint score for a python file."""
    pylint_output = StringIO()  # Custom open stream
    reporter = TextReporter(pylint_output)

    pylintrc = Path(get_iota2_project_dir()) / "iota2" / ".pylintrc"
    res = Run(["--rcfile", str(pylintrc), py_file], reporter=reporter, exit=False)
    return res.linter.stats.global_note, py_file


if __name__ == "__main__":

    i2_directory = get_iota2_project_dir()
    print(i2_directory)
    i2_py_files = list(
        filter(
            lambda x: x.endswith(".py") and "__init__" not in x,
            file_search_and(i2_directory, True, ".py"),
        )
    )
    NB_WORKERS = 10
    client = Client(n_workers=NB_WORKERS, threads_per_worker=1)
    print(
        (
            "Number of python files detected "
            f"in {i2_directory} : "
            f"{len(i2_py_files)}"
        )
    )
    futures = client.map(pylint_score, i2_py_files)
    pylint_res = [future.result() for future in as_completed(futures)]
    pylint_res = sorted(pylint_res, key=lambda x: x[0])
    pylint_df = pd.DataFrame(pylint_res, columns=["pylint score", "file"])
    pylint_df.index += 1
    print(tabulate(pylint_df, headers="keys", tablefmt="grid"))
