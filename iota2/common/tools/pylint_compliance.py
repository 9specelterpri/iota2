#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

"""
Perform pylint on all iota2 project.
Print a table containing all occurence of pylint report.
"""
import argparse
import json
from collections import Counter
from io import StringIO
from pathlib import Path

import pandas as pd
from dask.distributed import Client, as_completed
from pylint.lint import Run
from pylint.reporters import JSONReporter
from tabulate import tabulate

from iota2.common.file_utils import file_search_and, get_iota2_project_dir


def collect_pylint_msg(
    file_to_pylint: Path, pylintrc_file: Path
) -> list[tuple[str, str, str]]:
    """Perform pylint to a file and return a list of labels."""
    pylint_output = StringIO()
    reporter = JSONReporter(pylint_output)
    Run(
        ["--rcfile", str(pylintrc_file), str(file_to_pylint)],
        reporter=reporter,
        do_exit=False,
    )
    report_as_json = json.loads(pylint_output.getvalue())
    return [
        (
            f"{msg['message-id']}, {msg['symbol']}",
            f"{msg['path']}",
            f"line {msg['line']}, col {msg['column']}",
        )
        for msg in report_as_json
    ]


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Perform pylint on all iota2 project.")
    parser.add_argument(
        "-target_msgs",
        dest="target_msgs",
        nargs="+",
        default=None,
        help="target pylint messages, ie : C0303 R1732",
        required=False,
    )
    args = parser.parse_args()
    target_msgs = args.target_msgs

    i2_directory = get_iota2_project_dir()
    pylintrc = Path(get_iota2_project_dir()) / "iota2" / ".pylintrc"
    i2_py_files = list(
        filter(
            lambda x: x.endswith(".py") and "__init__" not in x,
            file_search_and(i2_directory, True, ".py"),
        )
    )
    NB_WORKERS = 10
    client = Client(n_workers=NB_WORKERS, threads_per_worker=1)
    print(
        "Number of python files detected " f"in {i2_directory} : " f"{len(i2_py_files)}"
    )

    futures = client.map(collect_pylint_msg, i2_py_files, [pylintrc] * len(i2_py_files))
    pylint_res = [future.result() for future in as_completed(futures)]
    flaten_res = [elem for msg_list in pylint_res for elem in msg_list]
    msgs_id, paths, positions = zip(*flaten_res)
    pylint_df = pd.DataFrame(
        {"pylint message": msgs_id, "file": paths, "position": positions}
    )

    pylint_df = pylint_df.groupby(
        "pylint message",
        group_keys=True,
    ).apply(lambda x: x)
    if target_msgs is not None:
        filtered_df = pylint_df.filter(regex="|".join(target_msgs), axis=0)
        print(tabulate(filtered_df, headers="keys", tablefmt="github", showindex=False))
    else:
        labels_count = dict(Counter(pylint_df["pylint message"].values))

        pylint_df = pd.DataFrame(
            {
                "pylint message": list(labels_count.keys()),
                "occurence": list(labels_count.values()),
            }
        )
        pylint_df = pylint_df.sort_values(by=["occurence"], ascending=False)
        pylint_df["done : :white_check_mark: not done : :white_large_square:"] = [
            ":white_large_square:"
        ] * len(pylint_df)
        print(tabulate(pylint_df, headers="keys", tablefmt="github", showindex=False))
