"""
Functions used to write features maps
"""
import logging
import os
from logging import Logger
from typing import Any, Optional, Union

import rasterio as rio
from osgeo import gdal

import iota2.common.file_utils as fu
import iota2.common.raster_utils as ru

sensors_params_type = dict[str, Union[str, list[str], int]]

FunctionNameWithParams = tuple[str, dict[str, Any]]

LOGGER = logging.getLogger("distributed.worker")


def add_label_on_raster_band(raster, list_bands, feat_labels) -> None:
    """
    Use : Add labels as description to bands of a raster
    IN:
        raster [str]:
            Name of the raster file
        list_bands [list(str)]:
            List of bands to label
        feat_labels [list(str)]:
            List of labels to add as descriptions of the bands of the raster.
    OUT:
        None
    """
    ds = gdal.Open(raster, gdal.GA_Update)
    for band, label in zip(list_bands, feat_labels):
        rb = ds.GetRasterBand(band)
        rb.SetDescription(label)
    del ds


def write_features_by_chunk(
    tile: str,
    working_directory: str,
    output_path: str,
    sar_optical_post_fusion: bool,
    sensors_parameters: sensors_params_type,
    chunk_config: ru.ChunkConfig,
    mode: str = "usually",
    module_path: Optional[str] = None,
    list_functions: Optional[list[FunctionNameWithParams]] = None,
    force_standard_labels: bool = False,
    targeted_chunk: Optional[int] = None,
    concat_mode: bool = False,
    multi_param_cust=None,
    logger: Logger = LOGGER,
) -> None:
    """
    usage : compute from a stack of data -> gapFilling -> features computation
            -> sampleExtractions
    thanks to OTB's applications'

    IN:
        tile [string]:
            name of the current tile to compute (ex: T31TCJ)
        working_directory [str]:
            path of the working directory
        output_path [str]:
            path where the features by chunks are to be written
        sar_optical_post_fusion [bool]:
            flag use to remove SAR data from features
        sensors_parameters [see type alias]:
            sensor parameters as a dict yielded by get_sensors_parameters()
        chunk_config [ChunkConfig]:
            kind of "dataclass" with chunk size / padding
        mode [str]:
            'usually' / 'SAR' used to get only sar features
        module_path [str]:
            optional custom module path, if not set, searches iota2 'external_code.py'
        list_functions [see type alias]:
            list of function names with their keyword arguments as a dict
            as outputed by the dcp.convert_functions
        force_standard_labels [bool]:
            (de)activate standardize labels for feature extraction
        targeted_chunk [int]:
            number of the current chunk to compute
        concat_mode [bool]:
            (de)activate concatenation with input data
        multi_param_cust [?]:
            # DOCME
        logger [Logger]:
            logger

    OUT:
        None
    """
    # const
    # seed_position = -1
    from iota2.common import generate_features as genFeatures
    from iota2.common.custom_numpy_features import compute_custom_features

    # tile = train_shape.split("/")[-1].split(".")[0].split("_")[0]
    if not working_directory:
        working_directory = output_path
    working_directory_features = os.path.join(working_directory, tile)

    if not os.path.exists(working_directory_features):
        try:
            os.mkdir(working_directory_features)
        except OSError:
            logger.warning(f"{working_directory_features} already exists")

    (
        apps_dict,
        labs_dict,
        dep_features,  # pylint: disable=W0612
    ) = genFeatures.generate_features(
        working_directory_features,
        tile,
        sar_optical_post_fusion,
        output_path,
        sensors_parameters,
        mode=mode,
        force_standard_labels=force_standard_labels,
        logger=logger,
    )
    if multi_param_cust:
        apps_dict["enable_interp"] = multi_param_cust["enable_gap"]
        apps_dict["enable_raw"] = multi_param_cust["enable_raw"]
        apps_dict["enable_masks"] = multi_param_cust["enable_raw"]
    else:
        apps_dict["enable_interp"] = True
        apps_dict["enable_raw"] = False
        apps_dict["enable_masks"] = False
    print(" start compute features")
    opath = os.path.join(output_path, "customF")
    if not os.path.exists(opath):
        os.mkdir(opath)
    output_name = os.path.join(opath, f"{tile}_chunk_{targeted_chunk}.tif")
    (
        otbimage,  # pylint: disable=W0612
        feat_labels,
        _,
        _,
        _,
        deps,  # pylint: disable=W0612
    ) = compute_custom_features(
        tile=tile,
        output_path=output_path,
        sensors_parameters=sensors_parameters,
        module_path=module_path,
        list_functions=list_functions,
        otb_pipelines=apps_dict,
        feat_labels=labs_dict["interp"],  # remove this parameter ?
        path_wd=working_directory_features,
        chunk_config=chunk_config,
        targeted_chunk=targeted_chunk,
        enabled_raw=multi_param_cust["enable_raw"],
        enabled_gap=multi_param_cust["enable_gap"],
        exogeneous_data=multi_param_cust["exogeneous_data"],
        fill_missing_dates=multi_param_cust["fill_missing_dates"],
        all_dates_dict=multi_param_cust["all_dates_dict"],
        mask_valid_data=multi_param_cust["mask_valid_data"],
        mask_value=multi_param_cust["mask_value"],
        concat_mode=concat_mode,
        output_name=output_name,
    )

    add_label_on_raster_band(output_name, range(1, len(feat_labels) + 1), feat_labels)


def merge_features_by_tiles(iota2_directory, tile, res, compress=None):

    """merge all chunks of a tile"""
    list_tile = fu.file_search_and(
        os.path.join(iota2_directory, "customF"), True, tile, ".tif"
    )
    features_map = os.path.join(iota2_directory, "by_tiles", f"{tile}.tif")
    raster = rio.open(list_tile[0])
    output_type = raster.meta["dtype"]
    if compress is None:
        compress_options = {"COMPRESS": "LZW", "BIGTIFF": "YES"}
    else:
        compress_options = compress
    fu.assembleTile_Merge(
        list_tile,
        res,
        features_map,
        output_pixel_type=output_type,
        creation_options=compress_options,
    )


def merge_features_maps(
    iota2_directory,
    output_name=None,
    res=10,
    output_type=None,
    compress=None,
    no_data_value=-10000,
):
    """merge tiles to a final mosaic"""
    list_tile = fu.file_search_and(
        os.path.join(iota2_directory, "by_tiles"), True, ".tif"
    )
    if output_type is None:
        raster = rio.open(list_tile[0])
        output_type = raster.meta["dtype"]
    if output_name is None:
        feature_map = os.path.join(iota2_directory, "final", "features_map.tif")
        feature_map_tmp = os.path.join(iota2_directory, "final", "features_map_tmp.tif")
    else:
        feature_map = os.path.join(iota2_directory, "final", output_name)
        feature_map_tmp = os.path.join(
            iota2_directory, "final", output_name.replace(".tif", "_tmp.tif")
        )
    if compress is None:
        compress_options = {"COMPRESS": "LZW", "BIGTIFF": "YES"}
    else:
        compress_options = compress
    fu.assembleTile_Merge(
        list_tile,
        res,
        feature_map_tmp,
        output_pixel_type=output_type,
        creation_options=compress_options,
        input_no_data=no_data_value,
        output_no_data=no_data_value,
    )
    ru.compress_raster(feature_map_tmp, feature_map)
    if os.path.exists(feature_map_tmp):
        os.remove(feature_map_tmp)
