#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================


class i2Error(Exception):
    """Base class for exceptions in iota2 chain"""

    pass


class directoryError(i2Error):
    """Base subclass for exception in the configuration file
    IN :
        msg [string] : explanation of the error
    """

    def __init__(self, directory_path):
        msg = f"directory : {directory_path} cannot be created"
        i2Error.__init__(self, msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class intersectionError(i2Error):
    """Base subclass for exception in the configuration file
    IN :
        msg [string] : explanation of the error
    """

    def __init__(self):
        msg = "no intersection between georeferenced inputs"
        i2Error.__init__(self, msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class configFileError(i2Error):
    """Base subclass for exception in the configuration file
    IN :
        msg [string] : explanation of the error
    """

    def __init__(self, msg):
        i2Error.__init__(self, msg)
        self.msg = msg

    def __str__(self):
        return repr(f"iota2 ERROR : {self.msg}")


class dataBaseError(i2Error):
    """ """

    def __init__(self, msg):
        super().__init__(self, msg)
        self.msg = msg

    def __str__(self):
        return repr(f"dataBaseError : {self.msg}")


class sqliteCorrupted(dataBaseError):
    """ """

    def __init__(self, journalsqlite_path):
        sqlite_path = journalsqlite_path.replace("-journal", "")
        msg = (
            f"'.sqlite-journal' file detetected, please remove "
            f"{journalsqlite_path} and {sqlite_path}"
        )
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class invalidGeometry(dataBaseError):
    """ """

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class invalidProjection(dataBaseError):
    """ """

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class emptyFeatures(dataBaseError):
    """ """

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class emptyGeometry(dataBaseError):
    """ """

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class duplicatedFeatures(dataBaseError):
    """ """

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class containsMultipolygon(dataBaseError):
    """ """

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class namingConvention(dataBaseError):
    """ """

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class missingField(dataBaseError):
    """ """

    def __init__(self, database_path, missing_field):
        msg = f"{database_path} does not contains the field '{missing_field}'"
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class fieldType(dataBaseError):
    """ """

    def __init__(self, input_vector, data_field, expected_type):
        msg = f"the field '{data_field}' in {input_vector} must be {expected_type} type"
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class tooSmallRegion(dataBaseError):
    """ """

    def __init__(self, input_vector, area_threshold, nb_too_small_geoms):
        msg = (
            f"the region shape '{input_vector}' contains {nb_too_small_geoms} "
            f"regions or sub-regions inferior than {area_threshold}, please remove them."
        )
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


####################################################################
# List of error class definition for the configuration file,
# inherits the configFileError class
####################################################################
class parameterError(configFileError):
    """Exception raised for errors in a parameter in the configuration file
    (like absence of a mandatory variable)
    IN :
        msg [string] : explanation of the error
    """

    def __init__(self, section, msg):
        self.section = section
        self.msg = msg

    def __str__(self):
        return "Error: In section " + repr(self.section) + ", " + self.msg


class dirError(configFileError):
    """Exception raised for errors in mandatory directory
    IN :
        directory [string] : name of the directory
    """

    def __init__(self, directory):
        self.directory = directory

    def __str__(self):
        self.msg = "Error: " + repr(self.directory) + " doesn't exist"
        return self.msg


class configError(configFileError):
    """Exception raised for configuration errors in the configuration file
    (like incompatible parameters)
    IN :
        msg [string] : explanation of the error
    """

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "Error: " + repr(self.msg)


class fileError(configFileError):
    """Exception raised for errors inside an input file
    (like a bad format or absence of a variable)
    IN :
        msg [string] : explanation of the error
    """

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "Error: " + repr(self.msg)


####################################################################
####################################################################
