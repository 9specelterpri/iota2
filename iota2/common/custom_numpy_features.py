#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" This module allow to use python function in an OTB pipeline"""
import logging
from typing import Any, Optional, Union

import numpy as np
import rasterio as rio
from rasterio.plot import reshape_as_image

from iota2.common import file_utils as fu
from iota2.common import raster_utils as ru
from iota2.common.i2_constants import Iota2Constants
from iota2.configuration_files.check_config_parameters import (
    search_external_features_function,
)

FunctionNameWithParams = tuple[str, dict[str, Any]]
I2_CONST = Iota2Constants()

LOGGER = logging.getLogger("distributed.worker")

sensors_params_type = dict[str, Union[str, list[str], int]]


def exogenous_data_tile(exogenous_data_tif: str, tilename: str):
    """interpolates tile in exogenous data path if possible"""
    if not exogenous_data_tif:
        # allow null value
        return exogenous_data_tif
    else:
        # if string exists, replace $TILE by actual tile name
        return exogenous_data_tif.replace("$TILE", tilename)


class data_container:
    """This class contains all methods to access data in image"""

    def __init__(
        self,
        tile_name: str,
        output_path: str,
        sensors_parameters: sensors_params_type,
        enabled_raw=False,
        enabled_gap=True,
        fill_missing_dates=False,
        all_dates_dict=None,
        exogeneous_data=None,
        missing_refl_values=I2_CONST.i2_missing_dates_no_data,
        missing_masks_values=I2_CONST.i2_missing_dates_no_data_mask,
        working_dir: Optional[str] = None,
    ):
        from iota2.sensors.sensors_container import sensors_container

        self.interpolated_data = None  # empty attribute to address data
        self.raw_data = None
        self.binary_masks = None
        self.exogeneous_data_array = None
        self.exogeneous_data_name = exogeneous_data
        self.out_data = None
        self.raw_dates = {}
        self.interpolated_dates = {}
        self.all_dates = all_dates_dict
        self.fill_missing_dates = fill_missing_dates
        self.missing_refl_values = missing_refl_values
        self.missing_masks_values = missing_masks_values
        self.dim_ts = {}  # dictionnary containing time indices for each sensor

        def manage_s1_raw_data(s1_orbits, all_orbits_dates, shift):
            """
            all_orbits_dates
                contain every dates for every orbit of the current tile
            """
            s1_raw_dates = []
            s1_all_dates = []

            ordered_s1_orbits = [
                f"Sentinel1_{orbit}" for orbit in list(all_orbits_dates.keys())
            ]
            for ordered_s1_orbit in ordered_s1_orbits:
                for date in self.raw_dates[ordered_s1_orbit]:
                    s1_raw_dates.append(f"{ordered_s1_orbit}_{date}")
                for date in self.all_dates[ordered_s1_orbit]:
                    s1_all_dates.append(f"{ordered_s1_orbit}_{date}")

            if not self.fill_missing_dates:
                all_dates = s1_raw_dates
            else:
                all_dates = s1_all_dates

            labels = []
            for date in all_dates:
                sensor_name, orbit, pol, date = date.split("_")
                labels.append(f"{sensor_name}_{orbit}{pol}_{date}".lower())

            existing_indices_refl = []
            for orbit in s1_orbits:
                raw_dates = self.raw_dates[f"Sentinel1_{orbit}"]
                for raw_date in raw_dates:
                    expected_date = f"Sentinel1_{orbit}_{raw_date}"
                    indices = [
                        index
                        for index, element in enumerate(all_dates)
                        if element == expected_date
                    ]
                    if len(indices) != 1:
                        raise ValueError(
                            "Something goes wrong with the Sentinel-1"
                            " date management in custom features, "
                            "a dates is duplicated"
                        )
                    existing_indices_refl.append(indices[0])
            row, col, _ = self.raw_data.shape
            filled_data = np.full((row, col, len(all_dates)), self.missing_refl_values)
            nb_raw_sensor_bands = len(s1_raw_dates)
            filled_data[:, :, existing_indices_refl] = self.raw_data[
                :, :, shift : nb_raw_sensor_bands + shift
            ]
            return filled_data, labels, nb_raw_sensor_bands

        def manage_userfeatures_date(shift, nb_features, from_raw=True):
            """ """
            if from_raw:
                return self.raw_data[:, :, shift : shift + nb_features]
            else:
                return self.interpolated_data[:, :, shift : shift + nb_features]

        def manage_raw_data(sensor_name, sensor_bands, shift):
            if not self.fill_missing_dates:
                # return self.raw_data
                all_dates = self.raw_dates[sensor_name]
            else:
                all_dates = self.all_dates[sensor_name]
            raw_dates = self.raw_dates[sensor_name]
            existing_indices_refl = []
            labels = []
            for i, date in enumerate(all_dates):
                for ind_band, band in enumerate(sensor_bands):
                    if date in raw_dates:
                        existing_indices_refl.append(i * len(sensor_bands) + ind_band)
                    labels.append(f"{sensor_name}_{band}_{date}")
            if len(existing_indices_refl) == 0:
                raise ValueError(
                    "Something goes wrong with the date management"
                    "Check the dates provided for gapfilling"
                    "or the type provided to compute_custom_features"
                )
            row, col, _ = self.raw_data.shape
            filled_data = np.full(
                (row, col, len(all_dates) * len(sensor_bands)), self.missing_refl_values
            )
            nb_raw_sensor_bands = len(self.raw_dates[sensor_name]) * len(sensor_bands)
            filled_data[:, :, existing_indices_refl] = self.raw_data[
                :, :, shift : nb_raw_sensor_bands + shift
            ]
            return filled_data, labels, nb_raw_sensor_bands

        def manage_s1_raw_mask(s1_orbits, all_orbits_dates, shift):
            """
            all_orbits_dates
                contain every dates for every orbit of the current tile
            """
            s1_raw_dates = []
            s1_all_dates = []

            ordered_s1_orbits = [
                f"Sentinel1_{orbit}"
                for orbit in list(all_orbits_dates.keys())
                if "vh" not in orbit
            ]

            for ordered_s1_orbit in ordered_s1_orbits:
                for date in self.raw_dates[ordered_s1_orbit]:
                    s1_raw_dates.append(f"{ordered_s1_orbit}_{date}")
                for date in self.all_dates[ordered_s1_orbit]:
                    s1_all_dates.append(f"{ordered_s1_orbit}_{date}")
            if not self.fill_missing_dates:
                all_dates = s1_raw_dates
            else:
                all_dates = s1_all_dates
            existing_indices_mask = []
            labels = []
            s1_orbits_vv = [s1_orbit for s1_orbit in s1_orbits if "vh" not in s1_orbit]
            for date in all_dates:
                sensor_name, orbit, _, date = date.split("_")
                labels.append(f"{sensor_name}_{orbit}MASK_{date}")

            for orbit in s1_orbits_vv:
                raw_dates = self.raw_dates[f"Sentinel1_{orbit}"]
                for raw_date in raw_dates:
                    expected_date = f"Sentinel1_{orbit}_{raw_date}"
                    indices = [
                        index
                        for index, element in enumerate(all_dates)
                        if element == expected_date
                    ]
                    if len(indices) != 1:
                        raise ValueError(
                            "Something goes wrong with the Sentinel-1"
                            " date management in custom features, "
                            "a dates is duplicated"
                        )
                    existing_indices_mask.append(indices[0])
            row, col, _ = self.raw_data.shape
            filled_mask = np.full((row, col, len(all_dates)), self.missing_masks_values)
            nb_raw_sensor_bands = len(s1_raw_dates)
            filled_mask[:, :, existing_indices_mask] = self.binary_masks[
                :, :, shift : nb_raw_sensor_bands + shift
            ]
            return filled_mask, labels, nb_raw_sensor_bands

        def manage_raw_mask(sensor_name, shift):
            if not self.fill_missing_dates:
                # return self.raw_data
                all_dates = self.raw_dates[sensor_name]
            else:
                all_dates = self.all_dates[sensor_name]

            raw_dates = self.raw_dates[sensor_name]
            existing_indices_mask = []
            labels = []
            for i, date in enumerate(all_dates):
                labels.append(f"{sensor_name}_MASK_{date}")
                if date in raw_dates:
                    existing_indices_mask.append(i)
            row, col, _ = self.raw_data.shape
            filled_mask = np.full((row, col, len(all_dates)), self.missing_masks_values)
            nb_raw_sensor_bands = len(self.raw_dates[sensor_name])
            filled_mask[:, :, existing_indices_mask] = self.binary_masks[
                :, :, shift : nb_raw_sensor_bands + shift
            ]
            return filled_mask, labels, nb_raw_sensor_bands

        def compute_s1_indices(dates, shift=0):
            """."""
            indices = list(range(shift, shift + len(dates)))
            new_shift = len(indices) + shift
            return indices, new_shift

        def compute_userfeatures_indices(
            userfeatures_list: list[str], shift: int
        ) -> tuple[list[str], list[str], list[int], int]:
            """userfeatures must be formatted as FEATURESNAME_band_INDEX"""
            feat_list = []
            for index, userfeature in enumerate(userfeatures_list):
                feat_name, _, __ = userfeature.split("_")
                feat_list.append((feat_name, shift + int(index)))
            feat_ind = [elem[1] for elem in fu.sort_by_first_elem(feat_list)]
            feat_names = [elem[0] for elem in fu.sort_by_first_elem(feat_list)]
            sensors_list = ["userFeatures"] * len(feat_ind)
            return sensors_list, feat_names, feat_ind, shift + len(userfeatures_list)

        def compute_indices(
            spectral_bands, spectral_indices, dates, shift=0
        ) -> tuple[list, int]:

            indices = []
            for i, _ in enumerate(spectral_bands):
                ind = [shift + i + len(spectral_bands) * x for x, _ in enumerate(dates)]
                indices.append(ind)

            len_spectral_band = len(dates) * len(spectral_bands) + shift
            for i, _ in enumerate(spectral_indices):
                spect_begin = len_spectral_band + i * len(dates)
                ind = range(spect_begin, spect_begin + len(dates))
                indices.append(ind)

            new_shift = (
                len(dates) * len(spectral_bands)
                + len(dates) * len(spectral_indices)
                + shift
            )
            return indices, new_shift

        # From this tile get enabled sensors
        sensor_tile_container = sensors_container(
            tile_name, working_dir, output_path, **sensors_parameters
        )

        gap_list_of_bands = []
        gap_time_series_indices = []
        gap_list_sensors = []
        gap_shift = 0

        raw_list_of_bands = []
        raw_time_series_indices = []
        raw_masks_indices = []
        raw_list_sensors = []
        raw_shift = 0
        raw_masks_shift = 0

        for sensor in sensor_tile_container.get_enabled_sensors():
            if sensor.name == "Sentinel1":
                s1_orbits = []
                s1_masks_indices = []
                if enabled_gap:
                    orbit_dates = sensor.get_interpolated_dates()
                    if "DES_vv" in orbit_dates:
                        s1_orbits = ["DES_vv", "DES_vh"]

                    if "ASC_vv" in orbit_dates:
                        s1_orbits += ["ASC_vv", "ASC_vh"]

                    for sensor_type in s1_orbits:
                        gap_tmp_indices, gap_shift = compute_s1_indices(
                            orbit_dates[sensor_type], gap_shift
                        )
                        sub_s1_sensor = f"{sensor.name}_{sensor_type}"
                        gap_list_of_bands.append("")
                        gap_time_series_indices.append(gap_tmp_indices)
                        gap_list_sensors.append(sub_s1_sensor)

                if enabled_raw:
                    orbit_dates = sensor.get_available_dates()
                    if "DES_vv" in orbit_dates:
                        s1_orbits = ["DES_vv", "DES_vh"]
                        s1_masks_indices = [
                            (
                                raw_masks_shift,
                                raw_masks_shift + len(orbit_dates["DES_vv"]),
                            ),
                            (
                                raw_masks_shift,
                                raw_masks_shift + len(orbit_dates["DES_vv"]),
                            ),
                        ]
                        raw_masks_shift += len(orbit_dates["DES_vv"])
                    if "ASC_vv" in orbit_dates:
                        s1_orbits += ["ASC_vv", "ASC_vh"]
                        s1_masks_indices += [
                            (
                                raw_masks_shift,
                                raw_masks_shift + len(orbit_dates["ASC_vv"]),
                            ),
                            (
                                raw_masks_shift,
                                raw_masks_shift + len(orbit_dates["ASC_vv"]),
                            ),
                        ]
                        raw_masks_shift += len(orbit_dates["ASC_vv"])
                    for sensor_type, masks_indices in zip(s1_orbits, s1_masks_indices):
                        raw_tmp_indices, raw_shift = compute_s1_indices(
                            orbit_dates[sensor_type], raw_shift
                        )
                        sub_s1_sensor = f"{sensor.name}_{sensor_type}"
                        self.raw_dates[sub_s1_sensor] = orbit_dates[sensor_type]
                        raw_list_of_bands.append("")
                        raw_time_series_indices.append(raw_tmp_indices)
                        raw_list_sensors.append(sub_s1_sensor)

                        raw_masks_indices.append(
                            range(masks_indices[0], masks_indices[1])
                        )

            elif sensor.name != "userFeatures":
                spectral_bands = sensor.stack_band_position
                spectral_indices = sensor.features_names_list
                if enabled_gap:
                    (
                        _,
                        self.interpolated_dates[sensor.name],
                    ) = sensor.write_interpolation_dates_file(write=False)
                    # tmp_indices = compute_indices_after_gapfilling(sensor, bands)
                    tmp_indices, gap_shift = compute_indices(
                        spectral_bands,
                        spectral_indices,
                        self.interpolated_dates[sensor.name],
                        gap_shift,
                    )

                    gap_list_of_bands += spectral_bands
                    gap_list_of_bands += spectral_indices
                    gap_time_series_indices += tmp_indices
                    gap_list_sensors += [sensor.name] * (
                        len(spectral_bands) + len(spectral_indices)
                    )
                if enabled_raw:
                    _, self.raw_dates[sensor.name] = sensor.write_dates_file()
                    raw_tmp_indices, raw_shift = compute_indices(
                        spectral_bands,
                        [],
                        self.raw_dates[sensor.name],
                        raw_shift,
                    )
                    raw_list_of_bands += spectral_bands
                    # raw data has no spectral features
                    raw_time_series_indices += raw_tmp_indices
                    raw_list_sensors += [sensor.name] * (len(spectral_bands))
                    raw_masks_indices.append(
                        range(
                            raw_masks_shift,
                            raw_masks_shift + len(self.raw_dates[sensor.name]),
                        )
                    )
                    raw_masks_shift += len(self.raw_dates[sensor.name])
            else:
                _, labels = sensor.get_features()
                if enabled_gap:
                    (
                        tmp_sensors,
                        tmp_featnames,
                        tmp_indices,
                        raw_shift,
                    ) = compute_userfeatures_indices(labels, gap_shift)
                    gap_list_of_bands += tmp_featnames
                    gap_list_sensors += tmp_sensors
                    gap_time_series_indices += tmp_indices
                if enabled_raw:
                    (
                        tmp_sensors,
                        tmp_featnames,
                        tmp_indices,
                        raw_shift,
                    ) = compute_userfeatures_indices(labels, raw_shift)
                    raw_list_of_bands += tmp_featnames
                    raw_list_sensors += tmp_sensors
                    raw_time_series_indices += tmp_indices

        # store time indices for later use
        self.dim_ts["interp"] = gap_shift
        self.dim_ts["raw"] = raw_shift
        self.dim_ts["mask"] = raw_masks_shift
        if self.exogeneous_data_name is not None:
            self.dim_ts["exogeneous"] = rio.open(self.exogeneous_data_name).count

        # TODO: handle user feature selection
        def get_filled_stack():
            """
            Function to access data as numpy array

            Returns a nd-array containing all sensors
            """
            filled_full_stack = None
            all_labels = []
            shift = 0
            for sensor in sensor_tile_container.get_enabled_sensors():
                if sensor.name == "Sentinel1":
                    filled_stack, labels, new_shift = manage_s1_raw_data(
                        s1_orbits, orbit_dates, shift
                    )
                elif sensor.name == "userFeatures":
                    _, labels = sensor.get_features()
                    filled_stack = manage_userfeatures_date(
                        shift, nb_features=len(labels)
                    )
                    shift += len(labels)
                else:
                    filled_stack, labels, new_shift = manage_raw_data(
                        sensor.name, sensor.stack_band_position, shift
                    )
                shift += new_shift
                all_labels += labels
                if filled_full_stack is None:
                    filled_full_stack = filled_stack
                else:
                    filled_full_stack = np.concatenate(
                        (filled_full_stack, filled_stack), axis=2
                    )
            return filled_full_stack, all_labels

        def get_filled_masks():
            filled_full_mask = None
            all_labels = []
            shift = 0
            for sensor in sensor_tile_container.get_enabled_sensors():
                if sensor.name == "Sentinel1":
                    filled_mask, labels, new_shift = manage_s1_raw_mask(
                        s1_orbits, orbit_dates, shift
                    )
                elif sensor.name == "userFeatures":
                    continue
                else:
                    filled_mask, labels, new_shift = manage_raw_mask(sensor.name, shift)
                all_labels += labels
                if filled_full_mask is None:
                    filled_full_mask = filled_mask
                else:
                    filled_full_mask = np.concatenate(
                        (filled_full_mask, filled_mask), axis=2
                    )
                shift += new_shift

            return filled_full_mask, all_labels

        if enabled_raw:
            setattr(self, "get_filled_stack", get_filled_stack)
            setattr(self, "get_filled_masks", get_filled_masks)

        for band, indices, sensor in zip(
            raw_list_of_bands, raw_time_series_indices, raw_list_sensors
        ):

            def get_band_raw(indices=indices):
                # current_self = self
                data = np.take(self.raw_data, indices, axis=2)
                return data

            function_name = f"get_raw_{sensor}_{band}"
            if not band:
                function_name = f"get_raw_{sensor}"
            setattr(self, function_name, get_band_raw)

        for band, indices, sensor in zip(
            gap_list_of_bands, gap_time_series_indices, gap_list_sensors
        ):

            def get_band(indices=indices):
                # current_self = self
                data = np.take(self.interpolated_data, indices, axis=2)
                return data

            function_name = f"get_interpolated_{sensor}_{band}"
            if not band:
                function_name = f"get_interpolated_{sensor}"
            setattr(self, function_name, get_band)

        def get_exogeneous_data():
            if len(self.exogeneous_data_array.shape) == 3:
                # if multiband change the shape to [row, cols, bands]
                return reshape_as_image(self.exogeneous_data_array)
            else:
                return self.exogeneous_data_array[:, :, np.newaxis]

        if self.exogeneous_data_name is not None:
            setattr(self, "get_exogeneous_data", get_exogeneous_data)

        for indices, sensor in zip(raw_masks_indices, raw_list_sensors):

            def get_binary_masks(indices=indices):
                return np.take(self.binary_masks, indices, axis=2)

            function_name = f"get_{sensor}_binary_masks"
            if enabled_raw:
                setattr(self, function_name, get_binary_masks)

        def get_raw_dates():
            return self.raw_dates

        if enabled_raw:
            # setattr(self, "get_binary_masks", get_binary_masks)
            setattr(self, "get_raw_dates", get_raw_dates)

        def get_interpolated_dates():
            return self.interpolated_dates

        if enabled_gap:
            setattr(self, "get_interpolated_dates", get_interpolated_dates)


class custom_numpy_features(data_container):
    """This class add functions provided by an user.
    and concatenate the results to the original feature stack"""

    def __init__(
        self,
        tile_name: str,
        output_path: str,
        sensors_params: sensors_params_type,
        module_name: str,
        list_functions: list[FunctionNameWithParams],
        concat_mode: bool = True,
        enabled_raw: bool = False,
        enabled_gap: bool = True,
        fill_missing_dates: bool = False,
        all_dates_dict: Optional[dict[str, list[str]]] = None,
        exogeneous_data: Optional[str] = None,
        missing_refl_values=I2_CONST.i2_missing_dates_no_data,
        missing_masks_values=I2_CONST.i2_missing_dates_no_data_mask,
        allow_nans: bool = False,
        working_dir: Optional[str] = None,
    ):
        """
        Parameters
        ----------
        tile_name: str
            The tile name, mandatory to get the sensor container
        output_path: str
        sensors_param:
            A dictionary containing all requiered information to instanciate
            the enabled sensors
        module_name: str
            The user provided python code. The full path to a file is requiered
        list_functions: List[FunctionNameWithParams]
            A list of function name and parameters that will be applied
        working_dir: optional (str)
            Use to store temporary data
        """

        # import types  # solves issues about type and inheritance (?)

        self.allow_nans = allow_nans
        self.concat_mode = concat_mode
        self.func_param = list_functions
        self.enabled_gap = enabled_gap

        if fill_missing_dates and not enabled_raw:
            raise ValueError(
                "Ask for fill missing dates but not for access raw data."
                "Check your configuration"
            )
        super().__init__(
            tile_name,
            output_path,
            sensors_params,
            enabled_raw,
            enabled_gap,
            fill_missing_dates,
            all_dates_dict,
            exogeneous_data,
            missing_refl_values,
            missing_masks_values,
            working_dir,
        )
        # load external functions
        self.external_functions = search_external_features_function(
            module_name, list_functions
        )

    def process(
        self, interpolated_data, binary_masks, raw_data, exogeneous_data_array=None
    ):
        """
        This function apply a set of functions to data
        Parameters
        ----------
        data: numpy array
            the numpy array to process
        Return
        ------
        data: numpy array
            the original numpy array concatenated with the custom features processed
        binary_masks: numpy array
            a binary array indicating if a pixel is masked
        raw_data: numpy array
            a numpy array of not interpolated sensors data
        exogeneous_data: numpy array
            a numpy array containing data which must be cropped at chunk size
        """
        self.interpolated_data = interpolated_data
        self.binary_masks = binary_masks

        self.raw_data = raw_data
        self.exogeneous_data_array = exogeneous_data_array
        new_labels = []
        try:
            for i, function in enumerate(self.func_param):
                fun_name = function[0]
                kwargs = function[1]
                func = self.external_functions[fun_name]
                # call function with it's arguments
                feat, labels = func(self, **kwargs)
                if isinstance(feat, np.ndarray) and len(feat.shape) in [2, 3]:
                    # ensure that feat is fill with number
                    if not self.allow_nans and not np.isfinite(feat).all():
                        raise ValueError(
                            "Error during custom_features computation"
                            " np.nan or infinite founded."
                            " Check if there is no division by zero"
                        )
                    # feat is a numpy array with shape [row, cols, bands]

                    # handle the case when return a 1d feature
                    if len(feat.shape) == 2:
                        feat = feat[:, :, None]
                    elif feat.any() and len(feat.shape) != 3:
                        raise ValueError("The return feature must be a 2d or 3d array")
                    # check if user defined well labels
                    if len(labels) != feat.shape[2]:
                        labels = [
                            f"custFeat_{i+1}_b{j+1}" for j in range(feat.shape[2])
                        ]
                    new_labels += labels
                    if i == 0:
                        # Initialize the out feature stack
                        self.out_data = feat[:]
                    else:
                        self.out_data = np.concatenate((self.out_data, feat), axis=2)
            if self.concat_mode:
                # If concat_mode join feature to the spectral data
                features_stack = self.interpolated_data
                if not self.enabled_gap:
                    features_stack = self.raw_data
                if isinstance(self.out_data, np.ndarray):
                    self.out_data = np.concatenate(
                        (features_stack, self.out_data), axis=2
                    )
                else:
                    self.out_data = features_stack

            return self.out_data, new_labels

        except AttributeError as err:
            print(
                "You try to access an non existing function" "Check your configuration"
            )
            print("You probably trying to access raw data without enable it")
            raise err
        except Exception as err:
            print("Error during custom_features computation")
            raise err

    def test_user_feature_with_fake_data(self) -> tuple[bool, Optional[Exception]]:
        """tests the external feature on fake data:
        1. creates fake data
        2. run the custom features
        3. intercept an error if there is
        returns:
          - success: (False, None)
          - failure: (True, Exception)
        """

        # creates fake data
        interpolated_data = np.ones((4, 3, self.dim_ts["interp"]), dtype=np.int16)
        raw_data = np.ones((4, 3, self.dim_ts["raw"]), dtype=np.int16)
        binary_masks = np.ones((4, 3, self.dim_ts["mask"]), dtype=bool)
        exogeneous_data_array = None
        if "exogeneous" in self.dim_ts:
            if self.dim_ts["exogeneous"] == 1:
                exogeneous_data_array = np.ones(
                    (4, 3),
                    dtype=np.int16,
                )
            else:
                exogeneous_data_array = np.ones(
                    (self.dim_ts["exogeneous"], 4, 3),
                    dtype=np.int16,  # Change order to match reshape_as_image
                )

        # try to exec functions using fake data
        try:
            self.process(
                interpolated_data,
                binary_masks,
                raw_data,
                exogeneous_data_array=exogeneous_data_array,
            )
        except Exception as e:  # pylint: disable=W0703
            return (True, e)
        return (False, None)


def compute_custom_features(
    tile: str,
    output_path: str,
    sensors_parameters: sensors_params_type,
    module_path: str,
    list_functions: list[FunctionNameWithParams],
    otb_pipelines,
    feat_labels: list[str],
    path_wd: str,
    chunk_config: ru.ChunkConfig,
    targeted_chunk: int,
    enabled_raw: bool,
    enabled_gap: bool,
    fill_missing_dates: bool,
    missing_refl_values: float = I2_CONST.i2_missing_dates_no_data,
    missing_masks_values: float = I2_CONST.i2_missing_dates_no_data_mask,
    all_dates_dict=None,
    mask_valid_data: Optional[str] = None,
    mask_value: int = 0,
    exogeneous_data: Optional[str] = None,
    concat_mode: bool = True,
    output_name: Optional[str] = None,
    expected_output_bands: Optional[int] = None,
    allow_nans: bool = False,
    logger=LOGGER,
    remove_padding: bool = True,
):
    """
    This function applies a list of functions to an otb pipeline data and
    returns an otbimage object.
    Parameters
    ----------

    Return
    ------

    """
    from functools import partial

    from iota2.common.raster_utils import insert_external_function_to_pipeline

    exogeneous_data = exogenous_data_tile(exogeneous_data, tile)

    expected_keys = [
        "interp",
        "raw",
        "masks",
        "enable_interp",
        "enable_raw",
        "enable_masks",
    ]
    if not all(k in otb_pipelines for k in expected_keys):
        raise ValueError(
            "Keys are missing in otb_pipelines parameters."
            f"{otb_pipelines.keys()} should be {expected_keys}"
        )
    if not otb_pipelines["enable_raw"] == enabled_raw:
        raise ValueError(
            f"otb_pipeline enable_raw ({otb_pipelines['enable_raw']}) not "
            f"match with param enabled_raw {enabled_raw}"
        )
    cust = custom_numpy_features(
        tile_name=tile,
        output_path=output_path,
        sensors_params=sensors_parameters,
        module_name=module_path,
        list_functions=list_functions,
        concat_mode=concat_mode,
        enabled_raw=enabled_raw,
        enabled_gap=enabled_gap,
        fill_missing_dates=fill_missing_dates,
        all_dates_dict=all_dates_dict,
        exogeneous_data=exogeneous_data,
        missing_refl_values=missing_refl_values,
        missing_masks_values=missing_masks_values,
        allow_nans=allow_nans,
        working_dir=path_wd,
    )
    function_partial = partial(cust.process)
    # TODO : how to fill labels_features_name ?
    # The output path is empty to ensure the image was not writed
    (
        feat_array,
        new_labels,
        out_transform,
        _,
        mask_array,
        otbimage,
        projection,
        deps_pipeline,
    ) = insert_external_function_to_pipeline(
        otb_pipelines=otb_pipelines,
        working_dir=path_wd,
        function=function_partial,
        chunk_config=chunk_config,
        output_path=output_name,
        mask_valid_data=mask_valid_data,
        mask_value=mask_value,
        targeted_chunk=targeted_chunk,
        exogeneous_data=exogeneous_data,
        is_custom_feature=True,
        output_number_of_bands=expected_output_bands,
        logger=logger,
        remove_padding=remove_padding,
    )

    # feat_array is an raster array with shape
    # [band, rows, cols] but otb requires [rows, cols, bands]
    crop_otbimage = convert_numpy_array_to_otb_image(
        otbimage, feat_array, out_transform
    )
    if concat_mode:
        feat_labels += new_labels
    else:
        feat_labels = new_labels
    return (
        crop_otbimage,
        feat_labels,
        out_transform,
        mask_array,
        projection,
        deps_pipeline,
    )


def convert_numpy_array_to_otb_image(otbimage, array, out_transform, logger=LOGGER):
    """
        This function allow to convert a numpy array to an otb image
    Parameters
    ----------
    otbimage :
        an otb image dictionary
    array :
        a rasterio array (shape is [bands, row, cols])
    out_transform :
        the geotransform corresponding to array
    """
    otbimage["array"] = np.ascontiguousarray(reshape_as_image(array))
    # the next line cause zeros in the output array
    # otbimage["array"] = np.array(reshape_as_image(array), order="C")
    logger.debug(
        "Numpy image inserted in otb pipeline: " f"{fu.memory_usage_psutil()} MB"
    )
    # get the chunk size
    size = otbimage["array"].shape
    # get the chunk origin
    origin = out_transform * (0, 0)
    # add a demi pixel size to origin
    # offset between rasterio (gdal) and OTB
    otbimage["origin"].SetElement(0, origin[0] + (otbimage["spacing"][0] / 2))
    otbimage["origin"].SetElement(1, origin[1] + (otbimage["spacing"][1] / 2))
    # Set buffer image region
    # Mandatory to keep the projection in OTB pipeline
    otbimage["region"].SetIndex(0, 0)
    otbimage["region"].SetIndex(1, 0)
    otbimage["region"].SetSize(0, size[1])
    otbimage["region"].SetSize(1, size[0])
    return otbimage
