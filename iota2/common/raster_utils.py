#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provides some functions to manipulate rasters."""
import logging
import os
from functools import partial
from typing import Optional, Union

import numpy as np
import otbApplication
import rasterio
import torch
import torch.nn.functional as F
from osgeo import gdal
from rasterio import mask, warp
from rasterio.io import MemoryFile
from rasterio.merge import merge
from rasterio.transform import Affine
from sklearn.preprocessing import LabelEncoder

from iota2.common.file_utils import get_raster_n_bands
from iota2.common.otb_app_bank import (
    CreateBandMathApplication,
    CreateBandMathXApplication,
)
from iota2.common.utils import run

LOGGER = logging.getLogger("distributed.worker")


def traduct_array(in_array: np.ndarray, enc: LabelEncoder) -> np.ndarray:
    """Traduct 2D array according to a LabelEncoder."""
    dic_converter = {
        torch_label: i2label for torch_label, i2label in enumerate(enc.classes_)
    }
    # u, inv = np.unique(in_array, return_inverse=True)
    # traducted_array = np.array([dic_converter[x] for x in u])[inv].reshape(
    #     in_array.shape
    # )
    print("dic_converter ", dic_converter)
    traducted_array = np.copy(in_array)
    for torch_label, i2_label in dic_converter.items():
        traducted_array[in_array == torch_label] = i2_label
    return traducted_array


def img_as_patches(in_img: Union[torch.Tensor, np.ndarray], radius) -> torch.Tensor:
    """
    img
        data shaped as (nb_bands, size_x, size_y)

    Note
    ----
    return a tensors shaped as (1, nb_bands, size_x, size_y, kernel, kernel)
    where kernel = 2 * radius + 1
    """
    kernel = 2 * radius + 1
    stride = 1
    pad = (radius, radius, radius, radius)
    if isinstance(in_img, torch.Tensor):
        img = in_img.unsqueeze(0)
        img = F.pad(input=img, pad=pad, mode="constant", value=0)
        img = (
            img.unfold(3, kernel, stride)
            .unfold(2, kernel, stride)
            .permute(0, 1, 2, 3, 5, 4)
        )

    elif isinstance(in_img, np.ndarray):
        img = torch.from_numpy(in_img).unsqueeze(0)
        img = F.pad(input=img, pad=pad, mode="constant", value=0)
        img = np.array(
            img.unfold(3, kernel, stride)
            .unfold(2, kernel, stride)
            .permute(0, 1, 2, 3, 5, 4)
        )
    else:
        raise ValueError(f"type : {type(in_img)} not supported")
    return img


def reorder_proba_map(
    proba_map_path_in: str,
    proba_map_path_out: str,
    class_model: list[int],
    all_class: list[int],
    pix_type: str,
    ram_mb: float = 1000.0,
) -> None:
    """Reorder the probability map.

    in order to merge proability raster containing a different number of
    effective class it is needed to reorder them according to a reference

    Parameters
    ----------
    proba_map_path_in :
        input probability map
    proba_map_path_out :
        output probability map
    class_model :
        list containing labels in the model used to classify
    all_class :
        list containing all possible labels
    """

    class_model_copy = class_model[:]

    if len(class_model) != len(all_class):
        in_nb_bands = get_raster_n_bands(proba_map_path_in)
        expected_nb_bands = len(all_class)
        nodata_label_idx = len(all_class)
        in_reorder = proba_map_path_in
        if in_nb_bands != expected_nb_bands:
            proba_new_band = CreateBandMathXApplication(
                {
                    "il": proba_map_path_in,
                    "ram": str(ram_mb),
                    "pixType": pix_type,
                    "exp": "im1; 0 * im1b1",
                    "out": proba_map_path_out,
                }
            )
            proba_new_band.ExecuteAndWriteOutput()
            # last band contains only 0 values now
            nodata_label_idx = len(class_model) + 1
            in_reorder = proba_map_path_out

        nodata_label_buff = "NODATALABEL"
        index_vector = []

        for index_label, expected_label in enumerate(all_class):
            if expected_label not in class_model:
                class_model_copy.insert(index_label, nodata_label_buff)
        for class_model_label in class_model_copy:
            if class_model_label in class_model:
                idx = class_model.index(class_model_label) + 1
            else:
                idx = nodata_label_idx
            index_vector.append(idx)
        exp = f"bands(im1, {'{' + ','.join(map(str, index_vector)) + '}'})"
        reorder_app = CreateBandMathXApplication(
            {
                "il": in_reorder,
                "ram": str(ram_mb),
                "exp": exp,
                "out": proba_map_path_out,
                "pixType": pix_type,
            }
        )
        reorder_app.ExecuteAndWriteOutput()


def roi_raster(
    in_raster: str, out_raster: str, roi_coord: list[tuple[float, float]]
) -> None:
    """use rasterio to extract raster region of interest

    The ROI does not have to be square

    Parameters
    ----------

    in_raster:
        raster to crop
    out_raster:
        raster cropped
    roi_coord:
        list of points which define a polygon [[x1, y1], ..., [xn, yn]]
    """
    src = rasterio.open(in_raster)
    roi_polygon_src_coords = warp.transform_geom(
        {"init": str(src.crs).lower()},
        src.crs,
        {"type": "Polygon", "coordinates": [roi_coord]},
    )
    out_image, out_transform = mask.mask(src, [roi_polygon_src_coords], crop=True)
    with rasterio.open(
        out_raster,
        "w",
        driver="GTiff",
        height=out_image.shape[1],
        width=out_image.shape[2],
        count=out_image.shape[0],
        crs=src.crs,
        dtype=out_image.dtype,
        transform=out_transform,
    ) as dst:
        dst.write(out_image)


def re_encode_raster(
    in_raster: str,
    out_raster: str,
    conversion_table: dict,
    no_data_values: int = 0,
    ram: int = 1024,
    logger=LOGGER,
) -> tuple[bool, str]:
    """according to a conversion table, re_encode the monoband raster values

    in_raster
        input monoband raster
    out_raster
        output raster
    ram
        pipeline's size
    conversion_table
        conversion_table[old_pixel_value] = new_pixel_value

    Notes
    -----
    return True if success.
    """
    conv_list = []
    new_labels = []
    all_castable = []
    for old_label, new_label in conversion_table.items():
        conv_list.append(f"im1b1=={new_label}?{old_label}")
        try:
            casted = int(old_label)
            new_labels.append(casted)
            all_castable.append(True)
        except ValueError:
            new_labels.append(int(new_label))
            all_castable.append(False)

    out_format = "uint8" if max(new_labels) < 255 else "uint32"
    join_conv_list = ":".join(conv_list)
    join_conv_list = f"{join_conv_list}:{no_data_values}"
    re_encoder = CreateBandMathApplication(
        {
            "il": [in_raster],
            "exp": join_conv_list,
            "out": out_raster,
            "ram": str(ram),
            "pixType": out_format,
        }
    )

    if all(all_castable):
        re_encoder.ExecuteAndWriteOutput()
        success = True
    else:
        logger.warning(
            f"the raster {in_raster} can not be encode with the rules "
            f"{conversion_table}, values to encode are strings"
        )
        success = False
        out_format = "uint8"
    return success, out_format


def extract_raster_bands(
    in_raster: str, out_raster: str, bands_of_interest: list[int]
) -> None:
    """use gdal_translate to extract bands of interest, band's index
    start at 1.

    Parameters
    ----------

    in_raster:
        raster to crop
    out_raster:
        raster cropped
    bands_of_interest:
        list of bands to copy in output raster
    """
    ds = gdal.Open(in_raster)
    gdal.Translate(out_raster, ds, bandList=bands_of_interest)


def compress_raster(
    raster_in: str, raster_out: str, compress_mode: str = "LZW"
) -> bool:
    """compress a raster thanks to gdal_translate"""
    success = True
    command = (
        f"gdal_translate -co 'COMPRESS={compress_mode}' "
        f"-co 'BIGTIFF=YES' {raster_in} {raster_out}"
    )
    try:
        run(command)
    except Exception:
        success = False
    return success


def raster_to_array(InRaster):
    """
    convert a raster to an array
    """
    array_out = None
    dataset = gdal.Open(InRaster)
    array_out = dataset.ReadAsArray()
    return array_out


class ChunkConfig:
    def __init__(
        self,
        chunk_size_mode: str,
        number_of_chunks: int,
        chunk_size_x: int,
        chunk_size_y: int,
        padding_size_x: int = 0,
        padding_size_y: int = 0,
    ):
        self.chunk_size_mode = chunk_size_mode  # "user_fixed" or "split_number"
        self.number_of_chunks = number_of_chunks
        self.chunk_size_x = chunk_size_x
        self.chunk_size_y = chunk_size_y
        self.padding_size_x = padding_size_x
        self.padding_size_y = padding_size_y


def insert_external_function_to_pipeline(
    otb_pipelines,
    working_dir: str,
    function: partial,
    chunk_config: ChunkConfig,
    output_path: Optional[str] = None,
    mask_valid_data: Optional[str] = None,
    mask_value: int = 0,
    targeted_chunk: Optional[int] = None,
    output_number_of_bands: int = 1,
    is_custom_feature=False,
    exogeneous_data: Optional[str] = None,
    logger=LOGGER,
    remove_padding: bool = True,
) -> tuple[np.ndarray, list[str], Affine, int]:
    """Apply a python function to an otb pipeline

    If a mask is provided (values not to be taken into account are 'mask_value'),
    then the resulting output could be define as the following :
    output = output * mask

    Parameters
    ----------
    otb_pipeline: otbApplication
        otb application ready to be Execute()
    working_dir: str
        working directory path
    function: partial
        function to apply
    output_path: str
        output raster path (optional)
    mask_valid_data: str
        input mask path (optional)
    mask_value: int
        input mask value to consider (optional)
    chunk_size_mode : str
        "user_fixed" / "auto" / "split_number"
    chunk_size_x: int
        chunk x size (optional)
    chunk_size_y: int
        chunk y size (optional)
    targeted_chunk : int
        process only the targeted chunk
    output_number_of_bands : int
        used only if targeted_chunk and mask are set
    ram: int
        available ram
    remove_padding
        flag to remove padding

    Return
    ------
    tuple
        (np.array, new_labels, affine transform, epsg code)
    """

    # if mask_valid_data and output_number_of_bands is None:
    #     raise ValueError(
    #         "if the parameter 'mask_valid_data' is enable,"
    #         "then the parameter 'output_number_of_bands' become mandatory")

    mosaic = new_labels = None

    (
        roi_rasters,
        epsg_code,
        dep,  # pylint: disable=unused-variable
        paddings,
    ) = split_raster(
        otb_pipelines=otb_pipelines, chunk_config=chunk_config, working_dir=working_dir
    )
    if targeted_chunk is not None:
        roi_rasters = {
            "interp": [roi_rasters["interp"][targeted_chunk]],
            "raw": [roi_rasters["raw"][targeted_chunk]],
            "masks": [roi_rasters["masks"][targeted_chunk]],
        }  # roi_rasters[targeted_chunk]]
        paddings = [paddings[targeted_chunk]]

    mask_array = None
    if mask_valid_data:
        mask_array = raster_to_array(mask_valid_data)

    exogeneous_data_array = None
    if exogeneous_data:
        exogeneous_data_array = raster_to_array(exogeneous_data)
    new_arrays = []
    chunks_mask = []
    for interp_raster, raw_raster, masks_raster, padding in zip(
        roi_rasters["interp"], roi_rasters["raw"], roi_rasters["masks"], paddings
    ):
        start_x = int(interp_raster.GetParameterString("startx"))
        size_x = int(interp_raster.GetParameterString("sizex"))
        start_y = int(interp_raster.GetParameterString("starty"))
        size_y = int(interp_raster.GetParameterString("sizey"))

        region_info = (
            f"processing region start_x : {start_x} size_x :"
            f" {size_x} start_y : {start_y} size_y : {size_y}"
        )
        logger.info(region_info)
        # print("memory usage : {}".format(memory_usage_psutil()))
        if not otb_pipelines["enable_interp"]:
            interp_raster = None
        if not otb_pipelines["enable_raw"]:
            raw_raster = None
        if not otb_pipelines["enable_masks"]:
            masks_raster = None
        (
            (roi_array, proj_geotransform),
            mask,
            new_labels,
            otbimage,
            projection,
            deps,
        ) = process_function(
            function=function,
            interpolated_pipeline=interp_raster,
            raw_pipeline=raw_raster,
            binary_mask_pipeline=masks_raster,
            mask_arr=mask_array,
            mask_value=mask_value,
            stream_bbox=(start_x, size_x, start_y, size_y),
            is_custom_feature=is_custom_feature,
            exogeneous_data_array=exogeneous_data_array,
            padding=padding,
            remove_padding=remove_padding,
        )

        new_arrays.append((roi_array, proj_geotransform))
        chunks_mask.append(mask)
    all_data_sets = get_rasterio_datasets(
        new_arrays,
        mask_value,
        force_output_shape=(size_y, size_x, output_number_of_bands),
    )

    if len(all_data_sets) > 1:
        mosaic, out_trans = merge(all_data_sets)
    else:
        if isinstance(new_arrays[0][0], int):
            mosaic = np.repeat(mask[np.newaxis, :, :], output_number_of_bands, axis=0)
        else:
            mosaic = np.moveaxis(new_arrays[0][0], -1, 0)
        out_trans = all_data_sets[0].transform
    if output_path:
        with rasterio.open(
            output_path,
            "w",
            driver="GTiff",
            height=mosaic.shape[1],
            width=mosaic.shape[2],
            count=mosaic.shape[0],
            crs=f"EPSG:{epsg_code}",
            transform=out_trans,
            dtype=mosaic.dtype,
        ) as dest:
            dest.write(mosaic)
    # the returned otbimage is a dictionary
    # we don't need a list as only the object (swig) is relevant the
    # final dictionary is overwritted by insert_external_function_to_pipeline
    return (
        mosaic,
        new_labels,
        out_trans,
        epsg_code,
        chunks_mask,
        otbimage,
        projection,
        deps,
    )


def get_rasterio_datasets(
    array_proj: list[tuple[Union[np.ndarray, int], dict]],
    mask_value: int = 0,
    force_output_shape: tuple[int, int, int] = (1, 1, 1),
    force_output_type: str = "int32",
) -> list[rasterio.io.DatasetReader]:
    """transform numpy arrays (containing projection data) to rasterio datasets
    it works only with 3D arrays
    """
    all_data_sets = []
    expected_arr_shapes = {
        arr[0].shape for arr in array_proj if isinstance(arr[0], np.ndarray)
    }
    if not expected_arr_shapes:
        expected_arr_shapes = [force_output_shape]
    expected_arr_shape = list(expected_arr_shapes)[0]
    expected_arr_types = {
        arr[0].dtype for arr in array_proj if isinstance(arr[0], np.ndarray)
    }
    if not expected_arr_types:
        expected_arr_types = [force_output_type]
    expected_arr_type = list(expected_arr_types)[0]

    for index, new_array in enumerate(array_proj):
        array = new_array[0]
        if isinstance(array, int):
            array = np.full(expected_arr_shape, mask_value, dtype=expected_arr_type)

        proj = new_array[-1]["projection"]
        geo_transform = new_array[-1]["geo_transform"]

        if index == 0:
            epsg_code = proj.GetAttrValue("AUTHORITY", 1)

        transform = Affine.from_gdal(*geo_transform)
        array_ordered = np.moveaxis(array, -1, 0)
        array_ordered_shape = array_ordered.shape

        if len(array_ordered_shape) != 3:
            raise ValueError("array's must be 3D")
        else:
            height = array_ordered_shape[1]
            width = array_ordered_shape[2]
            count = array_ordered_shape[0]
        with MemoryFile() as memfile:
            with memfile.open(
                driver="GTiff",
                height=height,
                width=width,
                count=count,
                dtype=array.dtype,
                crs=f"EPSG:{epsg_code}",
                transform=transform,
            ) as dataset:
                dataset.write(array_ordered)
            all_data_sets.append(memfile.open())
    return all_data_sets


def process_function(
    function: partial,
    padding: dict[str, int],
    interpolated_pipeline: otbApplication = None,
    raw_pipeline: otbApplication = None,
    binary_mask_pipeline: otbApplication = None,
    mask_arr: Optional[np.ndarray] = None,
    mask_value: int = 0,
    stream_bbox: Optional[tuple[int, int, int, int]] = None,
    is_custom_feature=False,
    exogeneous_data_array=None,
    remove_padding: bool = True,
) -> tuple[tuple, np.ndarray, dict, list[str]]:
    """apply python function to the output of an otbApplication

    Parameters
    ----------
    otb_pipeline : otbApplication
        otb application ready to be Execute()
    function : itertools.partial
        function manipulating numpy array
    mask_arr: np.ndarray
        mask raster array
    mask_value: int
        every pixels under 'mask_value' will be ignored
    stream_bbox : tuple
        stream bounding box
    Return
    ------
    tuple
        (np.ndarray, dict)
    """
    from osgeo import osr
    from sklearn.preprocessing import binarize

    if (
        interpolated_pipeline is None
        and raw_pipeline is None
        and binary_mask_pipeline is None
    ):
        raise ("Error: no input data are used in process function")
    interpolated_image = {"array": None}
    raw_image = {"array": None}
    binary_mask_image = {"array": None}
    otbimage = None
    roi_to_ignore = False
    roi_contains_mask_part = False
    mask_roi = None
    start_x, size_x, start_y, size_y = stream_bbox

    if mask_arr is not None:
        mask_roi = mask_arr[start_y : start_y + size_y, start_x : start_x + size_x]
        mask_roi = binarize(mask_roi)
        unique_mask_values = np.unique(mask_roi)
        if len(unique_mask_values) == 1 and unique_mask_values[0] == mask_value:
            roi_to_ignore = True
        elif len(unique_mask_values) > 1 and mask_value in unique_mask_values:
            roi_contains_mask_part = True

    exo_roi = None
    if exogeneous_data_array is not None:
        if len(exogeneous_data_array.shape) == 3:
            exo_roi = exogeneous_data_array[
                :, start_y : start_y + size_y, start_x : start_x + size_x
            ]
        else:
            exo_roi = exogeneous_data_array[
                start_y : start_y + size_y, start_x : start_x + size_x
            ]
    deps = []
    if interpolated_pipeline is not None:
        interpolated_pipeline.Execute()
        deps.append(interpolated_pipeline)
        proj = interpolated_pipeline.GetImageProjection("out")
        projection = osr.SpatialReference()
        projection.ImportFromWkt(proj)
        origin_x, origin_y = interpolated_pipeline.GetImageOrigin("out")
        xres, yres = interpolated_pipeline.GetImageSpacing("out")

        # remove this call to GetVectorImageAsNumpyArray
        # allow a gain of few seconds requiered to initialise otbimage object
        # This step is very time consumming as the whole pipeline is computed here
        # print("Export Array")
        # array = otb_pipeline.GetVectorImageAsNumpyArray("out")
        interpolated_image = interpolated_pipeline.ExportImage("out")
        if otbimage is None:
            otbimage = interpolated_image
    if raw_pipeline is not None:
        raw_pipeline.Execute()
        deps.append(raw_pipeline)
        proj = raw_pipeline.GetImageProjection("out")
        projection = osr.SpatialReference()
        projection.ImportFromWkt(proj)
        origin_x, origin_y = raw_pipeline.GetImageOrigin("out")
        xres, yres = raw_pipeline.GetImageSpacing("out")
        raw_image = raw_pipeline.ExportImage("out")
        if otbimage is None:
            otbimage = raw_image
    if binary_mask_pipeline is not None:
        binary_mask_pipeline.Execute()
        deps.append(binary_mask_pipeline)
        proj = binary_mask_pipeline.GetImageProjection("out")
        projection = osr.SpatialReference()
        projection.ImportFromWkt(proj)
        origin_x, origin_y = binary_mask_pipeline.GetImageOrigin("out")
        xres, yres = binary_mask_pipeline.GetImageSpacing("out")
        binary_mask_image = binary_mask_pipeline.ExportImage("out")
        if otbimage is None:
            otbimage = binary_mask_image
    new_labels = []
    output_arr = mask_value
    if roi_to_ignore is False:
        if not is_custom_feature:
            output_arr, new_labels = function(interpolated_image["array"])
        else:
            output_arr, new_labels = function(
                interpolated_data=interpolated_image["array"],
                raw_data=raw_image["array"],
                binary_masks=binary_mask_image["array"],
                exogeneous_data_array=exo_roi,
            )
        if roi_contains_mask_part:
            output_arr = output_arr * mask_roi[:, :, np.newaxis]

    # gdal offset
    offset_x = 0
    offset_y = 0
    if remove_padding:
        offset_x = (padding["startx"] - 0.5) * xres
        offset_y = (padding["starty"] - 0.5) * yres
        output_arr = output_arr[
            padding["starty"] : (size_y - padding["endy"]),
            padding["startx"] : (size_x - padding["endx"]),
            :,
        ]
    geo_transform = [origin_x + offset_x, xres, 0, origin_y + offset_y, 0, yres]

    output = (
        output_arr,
        {"projection": projection, "geo_transform": geo_transform},
    )
    return output, mask_roi, new_labels, otbimage, projection, deps


def get_padding(chunk_config, boundary, size_x, size_y):
    padding_start_x = min(chunk_config.padding_size_x, boundary["startx"])
    padding_start_y = min(chunk_config.padding_size_y, boundary["starty"])
    padding_end_x = min(
        chunk_config.padding_size_x, size_x - boundary["startx"] - boundary["sizex"]
    )
    padding_end_y = min(
        chunk_config.padding_size_y, size_y - boundary["starty"] - boundary["sizey"]
    )
    return {
        "startx": padding_start_x,
        "endx": padding_end_x,
        "starty": padding_start_y,
        "endy": padding_end_y,
    }


def get_boundary_with_padding(boundary, padding):
    return {
        "startx": boundary["startx"] - padding["startx"],
        "sizex": boundary["sizex"] + padding["startx"] + padding["endx"],
        "starty": boundary["starty"] - padding["starty"],
        "sizey": boundary["sizey"] + padding["starty"] + padding["endy"],
    }


def get_chunks_boundaries_carto(
    shape: tuple[int, int], transform: rasterio.transform, chunk_config: ChunkConfig
) -> list[dict[str, int]]:
    """ """
    boundaries, _ = get_chunks_boundaries(shape, chunk_config)
    boundaries_carto = []
    for boundary in boundaries:

        llx = boundary["startx"]
        lrx = llx + boundary["sizex"]

        # lly = boundary["starty"]
        # uly = lly + boundary["sizey"]
        uly = boundary["starty"]
        lly = uly + boundary["sizey"]

        llx_carto, lly_carto = rasterio.transform.xy(transform, lly, llx)
        lrx_carto, uly_carto = rasterio.transform.xy(transform, uly, lrx)
        boundaries_carto.append(
            {
                "lowerleft_x": llx_carto,
                "lowerleft_y": lly_carto,
                "upperright_x": lrx_carto,
                "upperright_y": uly_carto,
            }
        )
    return boundaries_carto


def get_chunks_boundaries(
    shape: tuple[int, int], chunk_config: ChunkConfig
) -> tuple[list[dict[str, int]], list[dict[str, int]]]:
    import math

    import numpy as np

    boundaries = []
    paddings = []
    size_x, size_y = shape[0], shape[1]

    if (
        2 * chunk_config.padding_size_x >= size_x
        or 2 * chunk_config.padding_size_y >= size_y
    ):
        raise ValueError(
            f"Error: padding size ({chunk_config.padding_size_x},{chunk_config.padding_size_y}) "
            f"exceeds the image size ({size_x}, {size_y})"
        )

    if chunk_config.chunk_size_mode == "user_fixed":
        for start_y in np.arange(0, size_y, chunk_config.chunk_size_y):
            for start_x in np.arange(0, size_x, chunk_config.chunk_size_x):
                boundary = {
                    "startx": start_x,
                    "sizex": min(chunk_config.chunk_size_x, size_x - start_x),
                    "starty": start_y,
                    "sizey": min(chunk_config.chunk_size_y, size_y - start_y),
                }
                padding = get_padding(chunk_config, boundary, size_x, size_y)
                paddings.append(padding)
                boundaries.append(get_boundary_with_padding(boundary, padding))

    elif chunk_config.chunk_size_mode == "split_number":

        if chunk_config.number_of_chunks > size_x + size_y:
            raise ValueError(
                f"Error: number of chunks ({chunk_config.number_of_chunks})"
                f"vastly exceeds the image size ({size_x * size_y} pixels)"
            )
        if chunk_config.number_of_chunks > size_y:
            unused_chunks = chunk_config.number_of_chunks - size_y
            split_x = np.linspace(0, size_x, unused_chunks + 1)
            split_y = np.linspace(0, size_y, size_y + 1)
        else:
            split_x = np.linspace(0, size_x, 2)
            split_y = np.linspace(0, size_y, chunk_config.number_of_chunks + 1)

        split_y = [math.floor(x) for x in split_y]
        split_x = [math.floor(x) for x in split_x]

        for i, start_y in enumerate(split_y[:-1]):
            for j, start_x in enumerate(split_x[:-1]):
                boundary = {
                    "startx": start_x,
                    "sizex": split_x[j + 1] - start_x,
                    "starty": start_y,
                    "sizey": split_y[i + 1] - start_y,
                }
                padding = get_padding(chunk_config, boundary, size_x, size_y)
                paddings.append(padding)
                boundaries.append(get_boundary_with_padding(boundary, padding))
    else:
        raise ValueError(
            f"Unknow split method {chunk_config.chunk_size_mode}, only split_number"
            " and user_fixed are handled"
        )
    return boundaries, paddings


def split_raster(
    otb_pipelines,  #: Dict[str, otbApplication],
    chunk_config: ChunkConfig,
    working_dir: str,
):  # -> Tuple[Dict[str, List[otbApplication]], int, List[otbApplication]]:
    """extract regions of interest over the otbApplication

    Parameters
    ----------
    otb_pipelines :
        Dictionnary containing otb applications
    chunk_size : tuple
        tuple(chunk_size_x, chunk_size_y)
    ram_per_chunk : int
        otb's pipeline size (Mo)
    working_dir : str
        working directory
    """
    from osgeo import osr

    from iota2.common.otb_app_bank import CreateExtractROIApplication

    otb_pipeline = otb_pipelines["interp"]
    otb_raw = otb_pipelines["raw"]
    otb_masks = otb_pipelines["masks"]

    otb_pipeline.Execute()
    otb_raw.Execute()
    otb_masks.Execute()
    proj = otb_pipeline.GetImageProjection("out")
    projection = osr.SpatialReference()
    projection.ImportFromWkt(proj)
    # origin_x, origin_y = otb_pipeline.GetImageOrigin("out")
    # xres, yres = otb_pipeline.GetImageSpacing("out")
    x_size, y_size = otb_pipeline.GetImageSize("out")

    boundaries, paddings = get_chunks_boundaries(
        shape=(x_size, y_size), chunk_config=chunk_config
    )
    independant_raster = {
        "interp": [],
        "raw": [],
        "masks": [],
    }  # type: Dict[str, List[otbApplication]]
    for index, boundary in enumerate(boundaries):
        output_roi = ""
        output_roi_raw = ""
        output_roi_masks = ""
        if working_dir:
            output_roi = os.path.join(working_dir, f"ROI_{index}.tif")
            output_roi_raw = os.path.join(working_dir, f"ROI_{index}_raw.tif")
            output_roi_masks = os.path.join(working_dir, f"ROI_{index}_masks.tif")

        roi_raw = CreateExtractROIApplication(
            {
                "in": otb_raw,
                "startx": boundary["startx"],
                "sizex": boundary["sizex"],
                "starty": boundary["starty"],
                "sizey": boundary["sizey"],
                "out": output_roi_raw,
            }
        )
        independant_raster["raw"].append(roi_raw)
        roi = CreateExtractROIApplication(
            {
                "in": otb_pipeline,
                "startx": boundary["startx"],
                "sizex": boundary["sizex"],
                "starty": boundary["starty"],
                "sizey": boundary["sizey"],
                "out": output_roi,
            }
        )
        independant_raster["interp"].append(roi)

        roi_masks = CreateExtractROIApplication(
            {
                "in": otb_masks,
                "startx": boundary["startx"],
                "sizex": boundary["sizex"],
                "starty": boundary["starty"],
                "sizey": boundary["sizey"],
                "out": output_roi_masks,
            }
        )
        independant_raster["masks"].append(roi_masks)

    return (
        independant_raster,
        projection.GetAttrValue("AUTHORITY", 1),
        [otb_pipeline, otb_raw, otb_masks],
        paddings,
    )


def merge_rasters(
    rasters: list[str],
    output_path: str,
    output_type: str = "Int16",
) -> tuple[np.ndarray, Affine]:
    """merge geo-referenced rasters thanks to rasterio.merge

    Parameters
    ----------
    rasters : list
        list of raster's path
    output_path : str
        output path

    Return
    ------
    tuple
        merged array, rasterio output transform
    """
    from iota2.common.file_utils import assembleTile_Merge, get_raster_resolution

    res_x, res_y = get_raster_resolution(rasters[0])
    assembleTile_Merge(
        rasters, (res_x, -res_y), output_path, output_pixel_type=output_type
    )
    # ~ rasters_datasets = [rasterio.open(raster) for raster in rasters]
    # ~ out_arr, out_trans = merge(rasters_datasets)
    # ~ if output_path:
    # ~ with rasterio.open(output_path,
    # ~ "w",
    # ~ driver='GTiff',
    # ~ height=out_arr.shape[1],
    # ~ width=out_arr.shape[2],
    # ~ count=out_arr.shape[0],
    # ~ crs="EPSG:{}".format(epsg_code),
    # ~ transform=out_trans,
    # ~ dtype=out_arr.dtype) as dest:
    # ~ dest.write(out_arr)
    # ~ return out_arr, out_trans
