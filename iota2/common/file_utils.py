#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" This module contains a lot of useful file manipulation functions"""

import csv
import datetime
import errno
import glob
import hashlib
import importlib
import logging
import os
import random
import resource
import shutil
import sys
from collections import defaultdict
from datetime import timedelta
from typing import Optional, Union

import numpy as np
import osgeo
import otbApplication as otb
from osgeo import gdal, osr

from config import Config
from iota2.common.utils import RemoveInStringList, run

LOGGER = logging.getLogger("distributed.worker")


def verify_import(module_path: str):
    """Check if a module can be imported

    Parameters
    ----------
    module_path:
        the path to module

    """
    spec = importlib.util.spec_from_file_location(
        module_path.split(os.sep)[-1].split(".")[0], module_path
    )
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module


def md5(file_name: str):
    """
    return the md5 of a file
    Parameters
    ----------
    file_name:
        the file name
    """
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as fil:
        for chunk in iter(lambda: fil.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def detect_csv_delimiter(csv_file: str) -> str:
    """detect csv delimiter"""
    sniffer = csv.Sniffer()
    with open(csv_file) as csvfile:
        first_line = next(csvfile).rstrip()
    dialect = sniffer.sniff(first_line)
    delimiter = dialect.delimiter
    return delimiter


def find_and_replace_file(
    input_file: str, output_file: str, str_to_find: str, new_str: str
):
    """search string in file and replace by the new one into an output file"""
    with open(input_file) as in_file:
        with open(output_file, "w") as out_file:
            for line in in_file:
                out_file.write(line.replace(str_to_find, new_str))


def is_writable_directory(directory_path: str) -> bool:
    """
    ensure a directory is writable
    Parameters
    ----------
    directory_path:
        the directory to be tested
    """
    out = True
    try:
        ensure_dir(directory_path)
    except Exception:
        out = False
    return out


def get_iota2_project_dir():
    """ """
    parent = os.path.abspath(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
    )
    iota2dir = os.path.abspath(os.path.join(parent, os.pardir))
    return iota2dir


def ensure_dir(dirname: str, raise_exe: bool = True):
    """
    Ensure that a named directory exists; if it does not, attempt to create it.
    """
    try:
        os.makedirs(dirname)
    except OSError as err:
        if err.errno != errno.EEXIST:
            if raise_exe:
                raise


def write_new_file(new_file, file_content):
    """
    write a new file
    """
    with open(new_file, "w") as new_f:
        new_f.write(file_content)


def memory_usage_psutil(unit="MB") -> float:
    """Return the memory usage
    Parameters
    ----------
    unit:
        the expect unit for return ram (MB or GB)
    """
    # return the memory usage in MB

    if unit == "MB":
        coeff = 1000.0
    elif unit == "GB":
        coeff = 1000.0 * 1000.0
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / coeff
    # print 'Memory usage: %s (MB)' % (mem)
    return mem


def date_interval(date_min, date_max, temp_res):
    """
    dateMin [string] : Ex -> 20160101
    dateMax [string] > dateMin
    tr [int/string] -> temporal resolution
    """
    start = datetime.date(int(date_min[0:4]), int(date_min[4:6]), int(date_min[6:8]))
    end = datetime.date(int(date_max[0:4]), int(date_max[4:6]), int(date_max[6:8]))
    delta = timedelta(days=int(temp_res))
    curr = start
    while curr <= end:
        yield curr
        curr += delta


def update_directory(src, dst):

    content = os.listdir(src)
    for current_content in content:
        if os.path.isfile(src + "/" + current_content):
            if not os.path.exists(dst + "/" + current_content):
                shutil.copy(src + "/" + current_content, dst + "/" + current_content)
        if os.path.isdir(src + "/" + current_content):
            if not os.path.exists(dst + "/" + current_content):
                try:
                    shutil.copytree(
                        src + "/" + current_content, dst + "/" + current_content
                    )
                # python >2.5
                except OSError as exc:
                    if exc.errno == errno.ENOTDIR:
                        shutil.copy(src, dst)
                    else:
                        raise


def get_date_landsat(path_landsat: str, tiles: list[str]):
    """
    Get the min and max dates for the given tile.
    """
    date_min = 30000000000
    date_max = 0
    for tile in tiles:
        # ~ folder = os.listdir(pathLandsat + "/" + sensor + "_" + tile)
        folder = os.listdir(path_landsat + "/" + tile)
        for i, _ in enumerate(folder):
            if (
                folder[i].count(".tgz") == 0
                and folder[i].count(".jpg") == 0
                and folder[i].count(".xml") == 0
            ):
                # ~ contenu = os.listdir(pathLandsat + "/" + sensor + "_" + tile + "/" + folder[i])
                contenu = os.listdir(path_landsat + "/" + tile + "/" + folder[i])
                for j, _ in enumerate(contenu):
                    if contenu[j].count(".TIF") != 0:
                        date = int(contenu[j].split("_")[3])
                        if date > date_max:
                            date_max = date
                        if date < date_min:
                            date_min = date
    return str(date_min), str(date_max)


def get_date_s2(path_s2: str, tiles: list[str]):
    """
    Get the min and max dates for the given tile.
    """
    date_pos = 2
    if "T" in tiles[0]:
        date_pos = 1
    date_min = 30000000000
    date_max = 0
    for tile in tiles:
        folder = os.listdir(path_s2 + "/" + tile)
        for i, _ in enumerate(folder):
            if (
                folder[i].count(".tgz") == 0
                and folder[i].count(".jpg") == 0
                and folder[i].count(".xml") == 0
            ):
                date = int(folder[i].split("_")[date_pos].split("-")[0])
                if date > date_max:
                    date_max = date
                if date < date_min:
                    date_min = date
    return str(date_min), str(date_max)


def get_date_s2_s2c(path_s2: str, tiles: list[str]):
    """
    Get the min and max dates for the given tile.
    """
    date_pos = 2
    date_min = 30000000000
    date_max = 0
    for tile in tiles:
        folder = os.listdir(path_s2 + "/" + tile)
        for i, _ in enumerate(folder):
            if (
                folder[i].count(".tgz") == 0
                and folder[i].count(".jpg") == 0
                and folder[i].count(".xml") == 0
            ):
                date = int(folder[i].split("_")[date_pos].split("T")[0])
                if date > date_max:
                    date_max = date
                if date < date_min:
                    date_min = date
    return str(date_min), str(date_max)


def common_pix_type_to_otb(string):
    """Convert a name to an otb piixel type
    Parameters
    ----------
    string:
        the name to convert
    """

    dico = {
        "complexDouble": otb.ComplexImagePixelType_double,
        "complexFloat": otb.ComplexImagePixelType_float,
        "double": otb.ImagePixelType_double,
        "float": otb.ImagePixelType_float,
        "int16": otb.ImagePixelType_int16,
        "int32": otb.ImagePixelType_int32,
        "uint16": otb.ImagePixelType_uint16,
        "uint32": otb.ImagePixelType_uint32,
        "uint8": otb.ImagePixelType_uint8,
    }
    try:
        return dico[string]
    except Exception:
        raise Exception(
            "Error in common_pix_type_to_otb function input parameter : "
            + string
            + " not available, choices are :"
            "'complexDouble','complexFloat','double','float',"
            "'int16','int32','uint16','uint32','uint8'"
        )


def split_list(input_list: list, nb_split: int):
    """
    IN :
    input_list [list]
    nb_split [int] : number of output fold

    OUT :
    split_list [list of nbSplit list]

    Examples :
        foo = ['a', 'b', 'c', 'd', 'e']
        print split_list(foo,4)
        >> [['e', 'c'], ['d'], ['a'], ['b']]

        print split_list(foo,8)
        >> [['b'], ['d'], ['c'], ['e'], ['a'], ['d'], ['a'], ['b']]
    """

    def chunk(xs, nb_fold):
        ys = list(xs)
        random.shuffle(ys)
        size = len(ys) // nb_fold
        leftovers = ys[size * nb_fold :]
        for c in range(nb_fold):
            if leftovers:
                extra = [leftovers.pop()]
            else:
                extra = []
            yield ys[c * size : (c + 1) * size] + extra

    split_list_in = list(chunk(input_list, nb_split))

    # check empty content (if nbSplit > len(Inlist))
    All = []
    for splits in split_list_in:
        for split in splits:
            if split not in All:
                All.append(split)

    for _, item in enumerate(split_list_in):
        if len(item) == 0:
            random_choice = random.sample(All, 1)[0]
            item.append(random_choice)

    return split_list_in


# TODO: remove as not used only tested
def findCurrentTileInString(string, allTiles):
    """
    IN:
    string [string]: string where we want to found a string in the string list 'allTiles'
    allTiles [list of strings]

    OUT:
    if there is a unique occurence of a string in allTiles,
    return this occurence. else, return Exception
    """
    # must contain same element
    tileList = [currentTile for currentTile in allTiles if currentTile in string]
    if len(set(tileList)) == 1:
        return tileList[0]
    raise Exception("more than one tile found into the string :'" + string + "'")


def sort_by_first_elem(my_list):
    """
    Example 1:
        MyList = [(1,2),(1,1),(6,1),(1,4),(6,7)]
        print sortByElem(MyList)
        >> [(1, [2, 1, 4]), (6, [1, 7])]

    Example 2:
        MyList = [((1,6),2),((1,6),1),((1,2),1),((1,6),4),((1,2),7)]
        print sortByElem(MyList)
        >> [((1, 2), [1, 7]), ((1, 6), [2, 1, 4])]
    """
    dic_temp = defaultdict(list)
    for key, value in my_list:
        dic_temp[key].append(value)
    return list(dic_temp.items())


def read_raster(name, data=False, band=1):
    """
    Open raster and return metadate information about it.

    in :
        name : raster name
    out :
        [datas] : numpy array from raster dataset
        xsize : xsize of raster dataset
        ysize : ysize of raster dataset
        projection : projection of raster dataset
        transform : coordinates and pixel size of raster dataset
    """

    try:
        if isinstance(name, str):
            raster = gdal.Open(name, 0)
        elif isinstance(name, osgeo.gdal.Dataset):
            raster = name
    except Exception:
        print(f"Problem on raster file path {name}")
        sys.exit()

    raster_band = raster.GetRasterBand(band)

    # property of raster
    projection = raster.GetProjectionRef()
    transform = raster.GetGeoTransform()
    xsize = raster.RasterXSize
    ysize = raster.RasterYSize

    if data:
        # convert raster to an array
        datas = raster_band.ReadAsArray()
        return datas, xsize, ysize, projection, transform
    return xsize, ysize, projection, transform


def array_to_raster(
    array: np.ndarray, output: str, model: str, driver: str = "GTiff"
) -> None:
    """convert a numpy array to a gdal raster

    Parameters:
    array:
        numpy nd-array
    output:
        output image name
    model:
        a reference raster to provide metadata
    driver:
        the expected driver
    """
    used_driver = gdal.GetDriverByName(driver)

    modelfile = read_raster(model, False)
    cols = modelfile[0]
    rows = modelfile[1]
    out_raster = used_driver.Create(output, cols, rows, 1, gdal.GDT_Byte)
    out_raster.SetGeoTransform(
        (modelfile[3][0], modelfile[3][1], 0, modelfile[3][3], 0, modelfile[3][5])
    )
    outband = out_raster.GetRasterBand(1)
    outband.WriteArray(array)
    out_raster_srs = osr.SpatialReference()
    out_raster_srs.ImportFromWkt(modelfile[2])
    out_raster.SetProjection(out_raster_srs.ExportToWkt())
    outband.FlushCache()


def get_raster_resolution(raster_in: Union[str, osgeo.gdal.Dataset]):
    """
    IN :
    rasterIn [string]:path to raster

    OUT :
    return pixelSizeX, pixelSizeY
    """

    if isinstance(raster_in, str):
        if not os.path.isfile(raster_in):
            return []
        raster = gdal.Open(raster_in, gdal.GA_ReadOnly)
    elif isinstance(raster_in, osgeo.gdal.Dataset):
        raster = raster_in

    if raster is None:
        raise Exception(f"can't open {raster_in}")
    geotransform = raster.GetGeoTransform()
    spacing_x = geotransform[1]
    spacing_y = geotransform[5]
    return spacing_x, spacing_y


def report_band_description(input_raster_path: str, output_raster_path: str):
    """
    IN :
    input_raster_path [string]: path to a raster of the band descriptions to copy
    output_raster_path [string]: path to a raster without the band description
    """
    output_raster = gdal.Open(output_raster_path, gdal.GA_Update)
    input_raster = gdal.Open(input_raster_path)
    if input_raster.RasterCount == output_raster.RasterCount:
        for band in range(1, input_raster.RasterCount + 1):
            output_band = output_raster.GetRasterBand(band)
            input_band = input_raster.GetRasterBand(band)
            label = input_band.GetDescription()
            output_band.SetDescription(label)
    else:
        LOGGER.warning(
            f"the rasters do not have the same number of bands,"
            f"so the band descriptions of {input_raster} will not"
            f"be copied to the bands of {output_raster}"
        )
    del output_raster
    del input_raster


def assembleTile_Merge(
    raster_list: list[str],
    spatial_resolution: tuple[float, float],
    output_path: str,
    output_pixel_type: str = "Int16",
    creation_options: Optional[dict] = None,
    input_no_data: int = 0,
    output_no_data: Optional[int] = None,
):
    """
    usage : wrapper around gdal_merge utility
        merges multiple raster into a single one

    IN :
    raster_list [list of strings] path of rasters to merge
    spatial_resolution [float,float] x,y spatial resolution in meters
    output_path [string] path of output raster
    output_pixel_type [string] (optional) output pixel type (gdal format)
        goes to `ot` gdal parameter
    creation_options [dictionary] (optional) gdal rasters creation options
        goes to `co` gdal parameter
    input_no_data [int] (optional) value of input raster ignored in merge
        goes to `n` gdal parameter
    output_no_data [int] (optional) value representing absence of data
        in output raster if set
        goes to `a_nodata` gdal parameter if not None

    OUT:
    a mosaic file at output_path composed of all images in raster_list
    """

    # explicit overwrite behavior
    if os.path.exists(output_path):
        os.remove(output_path)

    # spatial resolution
    spx = float(spatial_resolution[0])
    spy = -abs(float(spatial_resolution[1]))

    # creation options parameter only when non empty
    if creation_options:
        gdal_co = " -co " + " -co ".join(
            [
                f"{co_name}={co_value}"
                for co_name, co_value in list(creation_options.items())
            ]
        )
    else:
        gdal_co = ""

    # use no_data option only if output_no_data is set
    if output_no_data is None:
        a_nodata = " "
    else:
        a_nodata = f" -a_nodata {output_no_data} "

    # build command
    cmd = (
        f"gdal_merge.py {gdal_co} -ps {spx} {spy} "
        f"-o {output_path} -ot {output_pixel_type} "
        f"-n {input_no_data}{a_nodata}{' '.join(raster_list)}"
    )

    run(cmd, desc="gdal_merge.py")
    report_band_description(raster_list[0], output_path)


def getDateFromString(vardate: str) -> tuple[int, int, int]:
    """
    get date from string date
    Parameters
    ----------
    vardate:
        input date ex : 20160101
    """
    year = int(vardate[0:4])
    month = int(vardate[4:6])
    day = int(vardate[6 : len(vardate)])
    return year, month, day


def get_nb_date_in_tile(date_in_file, display=True, raw_dates=False):
    """
    usage : get available dates in file
    dateInFile [string] : path to txt containing one date per line
    raw_dates [bool] flag use to return all available dates
    """
    all_dates = []
    with open(date_in_file) as f:
        for i, l in enumerate(f):
            vardate = l.rstrip()
            try:
                year, month, day = getDateFromString(vardate)
                valid_date = datetime.datetime(int(year), int(month), int(day))
                all_dates.append(vardate)
                if display:
                    print(valid_date)
            except ValueError:
                raise Exception(
                    "unvalid date in : " + date_in_file + " -> '" + str(vardate) + "'"
                )
    if raw_dates:
        output = all_dates
    else:
        output = i + 1
    return output


def get_raster_projection_epsg(file_name):
    """
    usage get raster EPSG projection code
    """
    source_ds = gdal.Open(file_name, gdal.GA_ReadOnly)
    projection = osr.SpatialReference()
    projection.ImportFromWkt(source_ds.GetProjectionRef())
    projection_code = projection.GetAttrValue("AUTHORITY", 1)
    return projection_code


def get_raster_n_bands(raster):
    """
    usage get raster's number of bands
    """
    try:
        if isinstance(raster, str):
            src_ds = gdal.Open(raster)
        elif isinstance(raster, osgeo.gdal.Dataset):
            src_ds = raster
    except Exception:
        print("Problem on raster file path")
        sys.exit()

    if src_ds is None:
        raise Exception(raster + " doesn't exist")

    return int(src_ds.RasterCount)


def get_image_pixel_type(raster_in):
    """
    Get raster pixel type of raster_in from GetDataTypeName()
    ARGs:
        INPUT:
            - raster_in: gdal raster datatse or  osgeo.gdal.Dataset object
        OUTPUT
            - GDAL raster pixel type: str
    """

    if isinstance(raster_in, str):
        if not os.path.isfile(raster_in):
            return []
        raster = gdal.Open(raster_in, gdal.GA_ReadOnly)
    elif isinstance(raster_in, osgeo.gdal.Dataset):
        raster = raster_in

    band = raster.GetRasterBand(1)

    return gdal.GetDataTypeName(band.DataType)


def get_raster_extent(raster_in):
    """
    Get raster extent of raster_in from GetGeoTransform()
    ARGs:
        INPUT:
            - raster_in: input raster
        OUTPUT
            - ex: extent with [minX,maxX,minY,maxY]
    """

    if isinstance(raster_in, str):
        if not os.path.isfile(raster_in):
            print("no file")
            return []
        raster = gdal.Open(raster_in, gdal.GA_ReadOnly)
    elif isinstance(raster_in, osgeo.gdal.Dataset):
        raster = raster_in

    if raster is None:
        print("raster nonex")
        return []
    geotransform = raster.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    spacingX = geotransform[1]
    spacingY = geotransform[5]
    r, c = raster.RasterYSize, raster.RasterXSize

    minX = originX
    maxY = originY
    maxX = minX + c * spacingX
    minY = maxY + r * spacingY

    return minX, maxX, minY, maxY


def pix_to_geo(raster, col, row):
    """
    return geographical coordinates of col, row position
    """
    dsrast = gdal.Open(raster)
    originx, xres, rot1, originy, rot2, yres = dsrast.GetGeoTransform()
    xcoord = xres * col + rot1 * row + originx
    ycoord = rot2 * col + yres * row + originy
    return (xcoord, ycoord)


def gen_confusion_matrix(csv_f, AllClass):
    """
    IN:
    csv_f [list of list] : comes from conf_coordinates_csv function.
    AllClass [list of strings] : all class
    OUT :
    confMat [numpy array] : generate a numpy array representing a confusion matrix
    """
    NbClasses = len(AllClass)

    confMat = [[0] * NbClasses] * NbClasses
    confMat = np.asarray(confMat)
    row = 0
    for classRef in AllClass:
        # in order to manage the case "this reference label was never classified"
        for classRef_csv in csv_f:
            if classRef_csv[0] == classRef:
                col = 0
                for classProd in AllClass:
                    for classProd_csv in classRef_csv[1]:
                        if classProd_csv[0] == classProd:
                            confMat[row][col] = confMat[row][col] + classProd_csv[1]
                    col += 1
                # row +=1
        row += 1
        # if flag == 0:
        #   row+=1

    return confMat


def conf_coordinates_csv(csv_paths):
    """
    IN :
    csv_paths [string] : list of path to csv files
        ex : ["/path/to/file1.csv","/path/to/file2.csv"]
    OUT :
    out [list of lists] : containing csv's coordinates

    ex : file1.csv
        #Reference labels (rows):11
        #Produced labels (columns):11,12
        14258,52

         file2.csv
        #Reference labels (rows):12
        #Produced labels (columns):11,12
        38,9372

    out = [[12,[11,38]],[12,[12,9372]],[11,[11,14258]],[11,[12,52]]]
    """
    out = []
    for csv_path in csv_paths:
        cpty = 0
        FileMat = open(csv_path)
        while 1:
            data = FileMat.readline().rstrip("\n\r")
            if data == "":
                FileMat.close()
                break
            if data.count("#Reference labels (rows):") != 0:
                ref = data.split(":")[-1].split(",")
            elif data.count("#Produced labels (columns):") != 0:
                prod = data.split(":")[-1].split(",")
            else:
                y = ref[cpty]
                line = data.split(",")
                cptx = 0
                for val in line:
                    x = prod[cptx]
                    out.append([int(y), [int(x), float(val)]])
                    cptx += 1
                cpty += 1
    return out


def get_list_tile_from_model(model_in: str, path_to_config: str) -> list[str]:
    """
    Parameters
    ----------
    model_in:
        model name (generally an integer)
    path_to_config :
        path to the configuration file which
        link a model and all tiles uses to built him.
    Notes
    -----
        Export a list of tiles uses to built "modelIN"

    Example
    -------
    $cat /path/to/myConfigFile.cfg
    AllModel:
    [
        {
        modelName:'1'
        tilesList:'D0005H0001 D0005H0002'
        }
        {
        modelName:'22'
        tilesList:'D0004H0004 D0005H0008'
        }
    ]
    tiles = get_list_tile_from_model('22',/path/to/myConfigFile.cfg)
    print tiles
    >>tiles = ['D0004H0004','D0005H0008']
    """
    with open(path_to_config) as conf_file:
        cfg = Config(conf_file)
        all_model = cfg.AllModel

        for model in all_model.data:
            if model.modelName == model_in:
                return model.tilesList.split("_")


@RemoveInStringList(".aux.xml", ".sqlite-journal", ".pyc", "~")
def file_search_reg_ex(path_file):
    """
    get files by regEx
    """
    return [f for f in glob.glob(path_file.replace("[", "[[]"))]


# def getFeatStackName(list_indices: List[str],
def get_feat_stack_name(
    list_indices: list[str], user_feat_pattern: Optional[str] = None
) -> str:
    """
    usage : get Feature Stack name
    Parameters
    ----------
    list_indices: list(string)
        the features list
    user_feat_path: string
        the path to images
    user_feat_pattern: string
        contains features name separated by comma
    Return
    ------
    string: the image stack name
    """

    # if user_feat_path:
    #     user_feat_path = None
    if user_feat_pattern is None:
        user_feat_pattern = ""
    else:
        user_feat_pattern = "_".join(user_feat_pattern.split(","))

    stack_ind = f"SL_MultiTempGapF{user_feat_pattern}.tif"
    return_list_feat = True
    if len(list_indices) > 1:
        list_indices = list(list_indices)
        list_indices = sorted(list_indices)
        list_feat = "_".join(list_indices)
    elif len(list_indices) == 1:
        list_feat = list_indices[0]
    else:
        return_list_feat = False

    if return_list_feat is True:
        stack_ind = f"SL_MultiTempGapF_{list_feat}_{user_feat_pattern}_.tif"
    return stack_ind


def write_cmds(path, cmds, mode="w"):

    cmd_file = open(path, mode)
    for i in range(len(cmds)):
        if i == 0:
            cmd_file.write("%s" % (cmds[i]))
        else:
            cmd_file.write("\n%s" % (cmds[i]))
    cmd_file.close()


@RemoveInStringList(".aux.xml", ".sqlite-journal", ".pyc", "~")
def file_search_and(path_to_folder, AllPath, *names):
    """
    search all files in a folder or sub folder which contains all names in their name

    IN :
        - path_to_folder : target folder
                ex : /xx/xxx/xx/xxx
        - *names : target names
                ex : "target1","target2"
    OUT :
        - out : a list containing all file name (without extension) which are containing all name
    """
    out = []
    for path, _, files in os.walk(path_to_folder, followlinks=True):
        for _, file in enumerate(files):
            flag = 0
            for name in names:
                if file.count(name) != 0:
                    flag += 1
            if flag == len(names):
                if not AllPath:
                    out.append(file.split(".")[0])
                else:
                    path_out = path + "/" + file
                    out.append(path_out)
    return out
