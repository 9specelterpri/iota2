"""Module dedicated to provide asserts services."""
import filecmp
from pathlib import Path
from typing import Union


class AssertsFilesUtils:
    """Provides method to deal with files."""

    def assert_file_exist(self, path: Union[Path, str]) -> None:
        """Raise error if path is not a file or does not exists."""
        if isinstance(path, str):
            path = Path(path)
        if not path.exists():
            raise AssertionError(f"{path} does not exist")
        if not path.is_file():
            raise AssertionError(f"{path} is not a file")

    def assert_file_equal(self, path1: Path, path2: Path, equal=True):
        """Raise error if files are different by default, similar if equal=False."""
        if filecmp.cmp(path1, path2, shallow=False) != equal:
            raise AssertionError(f"{path1} and {path2} content are not equal")

    def assert_list_identical(self, list1, list2) -> None:
        """Raise error if lists are not identical, the order is check too."""
        if not len(list1) == len(list2):
            raise AssertionError("Lists have note the same "
                                 f"size : \n{list1} \n{list2}")
        for el1, el2 in zip(list1, list2):
            assert el1 == el2
