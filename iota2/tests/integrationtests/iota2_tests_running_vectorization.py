#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tests dedicated to launch iota2 runs."""

import os
import shutil

from iota2.common.file_utils import file_search_and
from iota2.configuration_files import read_config_file as rcf
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_launcher import run_cmd
from iota2.tests.utils.tests_utils_rasters import (
    generate_fake_classif,
    generate_range_raster,
)
from iota2.vector_tools.vector_functions import get_fields


class Iota2TestRunsCaseVectorization:
    """Tests dedicated to launch iota2 runs."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_ref = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_config_vecto.cfg"
        )
        cls.nomenclature_vecto = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "nomenclature.cfg"
        )
        cls.clip_file = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "fake_region.shp"
        )

        cls.zonal_vector = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "fake_zonal.shp"
        )

    def test_vectorization_with_clipfile(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test vectorization workflow with a clip file."""
        # prepare test inputs
        fake_classif = str(i2_tmpdir / "fake_classif.tif")
        fake_confidence = str(i2_tmpdir / "fake_confidence.tif")
        fake_validity = str(i2_tmpdir / "fake_validity.tif")
        generate_fake_classif(fake_classif)
        generate_range_raster(fake_confidence)
        generate_range_raster(fake_validity)

        test_output_path = str(i2_tmpdir / "vecto_clip")
        config_test = str(i2_tmpdir / "i2_config_vecto_clipfile.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = test_output_path
        cfg_test.cfg_as_dict["simplification"]["gridsize"] = 2
        cfg_test.cfg_as_dict["simplification"]["classification"] = fake_classif
        cfg_test.cfg_as_dict["simplification"]["confidence"] = fake_confidence
        cfg_test.cfg_as_dict["simplification"]["validity"] = fake_validity
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = self.clip_file
        cfg_test.cfg_as_dict["simplification"]["clipfield"] = "region"
        cfg_test.cfg_as_dict["simplification"]["nomenclature"] = self.nomenclature_vecto
        cfg_test.cfg_as_dict["simplification"]["prod"] = "oso"
        cfg_test.cfg_as_dict["simplification"]["outprefix"] = "departement_"
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 15 -scheduler_type debug"
        run_cmd(cmd)

        # asserts
        # check if the resulting file exists
        vecto_result_list = file_search_and(
            os.path.join(test_output_path, "vectors"), True, "departement", ".shp"
        )
        assert len(vecto_result_list) == 2

        # check if file content is ok (list of fields)
        expected_fields_list = [
            "Classe",
            "Validmean",
            "Validstd",
            "Confidence",
            "UrbainDens",
            "UrbainDiff",
            "ZoneIndCom",
            "Routes",
            "Colza",
            "CerealPail",
            "Proteagine",
            "Soja",
            "Tournesol",
            "Mais",
            "Riz",
            "TuberRacin",
            "Prairie",
            "Vergers",
            "Vignes",
            "Feuillus",
            "Coniferes",
            "Pelouse",
            "Landes",
            "SurfMin",
            "PlageDune",
            "GlaceNeige",
            "Eau",
            "Aire",
        ]
        for vecto_result in vecto_result_list:
            test_vecto_fields = get_fields(vecto_result)
            assert test_vecto_fields == expected_fields_list

    def test_vectorization_without_clipfile(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test vectorization workflow without a clip file."""
        # prepare test inputs
        fake_classif = str(i2_tmpdir / "fake_classif.tif")
        fake_confidence = str(i2_tmpdir / "fake_confidence.tif")
        fake_validity = str(i2_tmpdir / "fake_validity.tif")
        generate_fake_classif(fake_classif)
        generate_range_raster(fake_confidence)
        generate_range_raster(fake_validity)

        test_output_path = str(i2_tmpdir / "vecto_no_clip")
        config_test = str(i2_tmpdir / "i2_config_vecto_no_clipfile.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = test_output_path
        cfg_test.cfg_as_dict["simplification"]["gridsize"] = 2
        cfg_test.cfg_as_dict["simplification"]["classification"] = fake_classif
        cfg_test.cfg_as_dict["simplification"]["confidence"] = fake_confidence
        cfg_test.cfg_as_dict["simplification"]["validity"] = fake_validity
        cfg_test.cfg_as_dict["simplification"]["nomenclature"] = self.nomenclature_vecto
        cfg_test.cfg_as_dict["simplification"]["prod"] = "oso"
        cfg_test.cfg_as_dict["simplification"]["outprefix"] = "departement_"
        cfg_test.save(config_test)
        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 25 -scheduler_type debug"
        run_cmd(cmd)

        # asserts
        # check if the resulting file exists
        vecto_result_list = file_search_and(
            os.path.join(test_output_path, "vectors"), True, "departement_0.shp"
        )
        assert len(vecto_result_list) == 1
        vecto_result = vecto_result_list[0]

        # check if file content is ok (list of fields)
        expected_fields_list = [
            "Classe",
            "Validmean",
            "Validstd",
            "Confidence",
            "UrbainDens",
            "UrbainDiff",
            "ZoneIndCom",
            "Routes",
            "Colza",
            "CerealPail",
            "Proteagine",
            "Soja",
            "Tournesol",
            "Mais",
            "Riz",
            "TuberRacin",
            "Prairie",
            "Vergers",
            "Vignes",
            "Feuillus",
            "Coniferes",
            "Pelouse",
            "Landes",
            "SurfMin",
            "PlageDune",
            "GlaceNeige",
            "Eau",
            "Aire",
        ]
        test_vecto_fields = get_fields(vecto_result)
        assert test_vecto_fields == expected_fields_list

    def test_vectorization_without_grid(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test vectorization workflow without a clip file."""
        # prepare test inputs
        fake_classif = str(i2_tmpdir / "fake_classif.tif")
        fake_confidence = str(i2_tmpdir / "fake_confidence.tif")
        fake_validity = str(i2_tmpdir / "fake_validity.tif")
        generate_fake_classif(fake_classif)
        generate_range_raster(fake_confidence)
        generate_range_raster(fake_validity)

        test_output_path = str(i2_tmpdir / "vecto_no_grid")
        config_test = str(i2_tmpdir / "i2_config_vecto_no_grid.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = test_output_path
        cfg_test.cfg_as_dict["simplification"]["classification"] = fake_classif
        cfg_test.cfg_as_dict["simplification"]["confidence"] = fake_confidence
        cfg_test.cfg_as_dict["simplification"]["validity"] = fake_validity
        cfg_test.cfg_as_dict["simplification"]["nomenclature"] = self.nomenclature_vecto
        cfg_test.cfg_as_dict["simplification"]["prod"] = "oso"
        cfg_test.cfg_as_dict["simplification"]["outprefix"] = "departement_"
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 11 -scheduler_type debug"
        run_cmd(cmd)

        # asserts
        # check if the resulting file exists
        vecto_result_list = file_search_and(
            os.path.join(test_output_path, "vectors"), True, "departement_0.shp"
        )
        assert len(vecto_result_list) == 1
        vecto_result = vecto_result_list[0]

        # check if file content is ok (list of fields)
        expected_fields_list = [
            "Classe",
            "Validmean",
            "Validstd",
            "Confidence",
            "UrbainDens",
            "UrbainDiff",
            "ZoneIndCom",
            "Routes",
            "Colza",
            "CerealPail",
            "Proteagine",
            "Soja",
            "Tournesol",
            "Mais",
            "Riz",
            "TuberRacin",
            "Prairie",
            "Vergers",
            "Vignes",
            "Feuillus",
            "Coniferes",
            "Pelouse",
            "Landes",
            "SurfMin",
            "PlageDune",
            "GlaceNeige",
            "Eau",
            "Aire",
        ]
        test_vecto_fields = get_fields(vecto_result)
        assert test_vecto_fields == expected_fields_list

    def test_generalization(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test vectorization workflow when zonal vector is provided."""
        # prepare test inputs
        fake_classif = str(i2_tmpdir / "fake_classif.tif")
        fake_confidence = str(i2_tmpdir / "fake_confidence.tif")
        fake_validity = str(i2_tmpdir / "fake_validity.tif")
        generate_fake_classif(fake_classif)
        generate_range_raster(fake_confidence)
        generate_range_raster(fake_validity)

        test_output_path = str(i2_tmpdir / "vecto_zonal")
        config_test = str(i2_tmpdir / "i2_config_vecto_zonal.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = test_output_path
        cfg_test.cfg_as_dict["simplification"]["classification"] = fake_classif
        cfg_test.cfg_as_dict["simplification"]["confidence"] = fake_confidence
        cfg_test.cfg_as_dict["simplification"]["validity"] = fake_validity
        cfg_test.cfg_as_dict["simplification"]["zonal_vector"] = self.zonal_vector
        cfg_test.cfg_as_dict["simplification"]["outprefix"] = "departement_"
        cfg_test.cfg_as_dict["simplification"]["nomenclature"] = self.nomenclature_vecto
        cfg_test.cfg_as_dict["simplification"]["prod"] = "carhab"
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 11 -scheduler_type debug"
        run_cmd(cmd)

        # asserts
        # check if the resulting file exists
        vecto_result_list = file_search_and(
            os.path.join(test_output_path, "vectors"), True, "departement_0.shp"
        )
        assert len(vecto_result_list) == 1
        vecto_result = vecto_result_list[0]

        # check if file content is ok (list of fields)
        expected_fields_list = [
            "UrbainDens",
            "UrbainDiff",
            "ZoneIndCom",
            "Routes",
            "Colza",
            "CerealPail",
            "Proteagine",
            "Soja",
            "Tournesol",
            "Mais",
            "Riz",
            "TuberRacin",
            "Prairie",
            "Vergers",
            "Vignes",
            "Feuillus",
            "Coniferes",
            "Pelouse",
            "Landes",
            "SurfMin",
            "PlageDune",
            "GlaceNeige",
            "Eau",
            "Major",
            "Pourc_majo",
            "Conf_maj_m",
            "Conf_maj_e",
            "Conf_moy",
        ]
        test_vecto_fields = get_fields(vecto_result)
        assert test_vecto_fields == expected_fields_list
