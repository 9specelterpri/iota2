#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tests dedicated to launch iota2 runs."""
import logging
import os
import shutil
import sqlite3
import sys
from pathlib import Path

import numpy as np
import pandas as pd
import pytest

import iota2.tests.utils.tests_utils_files as TUF
import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.common import i2_constants as i2_const
from iota2.common.raster_utils import raster_to_array
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2 import run
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_launcher import run_cmd
from iota2.vector_tools.vector_functions import get_field_element, get_fields

I2_CONST = i2_const.Iota2Constants()

IOTA2DIR = Path(os.environ.get("IOTA2DIR"))

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")

IOTA2_SCRIPTS = IOTA2DIR / "iota2"
sys.path.append(IOTA2_SCRIPTS)

LOGGER = logging.getLogger("distributed.worker")


@pytest.mark.classification
class Iota2ClassificationRunCases(AssertsFilesUtils):
    # pylint: disable=too-many-public-methods
    """Tests dedicated to launch iota2 runs."""

    # before launching tests
    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        # input data
        cls.config_ref = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_config.cfg"
        )
        cls.config_ref_keep = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_builder.cfg"
        )

        cls.config_ref_scikit = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_config_scikit.cfg"
        )
        cls.ground_truth_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "ground_truth.shp"
        )
        cls.region_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "region.shp"
        )
        cls.nomenclature_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "nomenclature.txt"
        )
        cls.color_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "color.txt"
        )

    def test_multi_model_s2_theia_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2 (theia format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["chain"]["region_path"] = self.region_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        # asserts
        self.assert_file_exist(
            Path(running_output_path) / "final" / "Classif_Seed_0_ColorIndexed.tif"
        )
        self.assert_file_exist(
            Path(running_output_path) / "final" / "Classif_Seed_0.tif"
        )
        self.assert_file_exist(
            Path(running_output_path) / "final" / "Confidence_Seed_0.tif"
        )
        self.assert_file_exist(
            Path(running_output_path) / "final" / "Confusion_Matrix_Classif_Seed_0.png"
        )
        self.assert_file_exist(Path(running_output_path) / "final" / "diff_seed_0.tif")
        self.assert_file_exist(
            Path(running_output_path) / "final" / "PixelsValidity.tif"
        )
        self.assert_file_exist(Path(running_output_path) / "final" / "RESULTS.txt")
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0
        # check vector content
        formatting_vector = (
            Path(running_output_path) / "formattingVectors" / "T31TCJ.shp"
        )
        self.assert_file_exist(formatting_vector)
        region_content = get_field_element(
            str(formatting_vector), field="region", mode="all", elem_type="str"
        )
        code_content = get_field_element(
            str(formatting_vector),
            field=I2_CONST.re_encoding_label_name,
            mode="all",
            elem_type="str",
        )
        tile_origin_content = get_field_element(
            str(formatting_vector), field="tile_o", mode="all", elem_type="str"
        )
        # check if all regions are castable in integer
        for elem in region_content:
            try:
                _ = int(elem)
            except Exception as err:
                raise TypeError(
                    f"missformatted content in {formatting_vector}, "
                    f"'region' column contains {elem}, not castable in integer"
                ) from err

        # check if all lables are castable in integer
        for elem in code_content:
            try:
                _ = int(elem)
            except Exception as err:
                raise TypeError(
                    f"missformatted content in {formatting_vector}, 'code' "
                    f"column contains {elem}, not castable in integer"
                ) from err

        # check if all lables are castable in integer
        print("\n" * 3, tile_origin_content, "\n" * 3)
        for elem in tile_origin_content:
            assert elem == "T31TCJ"

    # Tests definitions

    def test_data_augmentation_s2_theia_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Run using s2 (theia format) with data augmentation algorithm."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path

        sample_augmentation = {
            "target_models": ["1"],
            "strategy": "jitter",
            "strategy.jitter.stdfactor": 10,
            "samples.strategy": "balance",
            "activate": True,
        }
        cfg_test.cfg_as_dict["arg_train"]["sample_augmentation"] = sample_augmentation

        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1"
            " -ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        # asserts
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_multi_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2 (theia format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["arg_train"]["runs"] = 2
        cfg_test.cfg_as_dict["arg_classification"]["merge_final_classifications"] = True
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            f"-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        # asserts
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
            multi_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classifications_fusion.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2 (theia format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            f"-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        # asserts
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_s2c_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2 (peps format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        mtd_files = [
            str(Path(IOTA2DIR) / "data" / "MTD_MSIL2A_20190501.xml"),
            str(Path(IOTA2DIR) / "data" / "MTD_MSIL2A_20190506.xml"),
        ]
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_s2c_dir = str(i2_tmpdir / "s2_s2c_data")

        TUR.generate_fake_s2_s2c_data(
            fake_s2_s2c_dir,
            tile_name,
            mtd_files,
            fake_raster=TUR.fun_array("iota2_binary"),
            fake_scene_classification=1 + TUR.fun_array("iota2_binary") * 2,
            origin_x=566377.0,
            origin_y=6284029.0,
            epsg_code=2154,
        )

        config_test = str(i2_tmpdir / "i2_config_s2_s2c.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_s2c_path"] = fake_s2_s2c_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 19 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_l8_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using l8 (theia format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_l8_theia_dir = str(i2_tmpdir / "l8_data")
        TUR.generate_fake_l8_data(
            fake_l8_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=30.0
        )
        config_test = str(i2_tmpdir / "i2_config_l8.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["l8_path"] = fake_l8_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = [30, 30]
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            f"-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_l8_old_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using l8 (peps format)."""
        # prepare inputs data
        tile_name = "D0005H0002"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_l8_old_dir = str(i2_tmpdir / "l8_old_data")
        TUR.generate_fake_l8_old_data(
            fake_l8_old_dir, tile_name, ["20200101", "20200112", "20200127"], res=30.0
        )
        config_test = str(i2_tmpdir / "i2_config_l8_old.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = fake_l8_old_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = [30, 30]
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_l5_old_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using l5 (peps format)."""
        # prepare inputs data
        tile_name = "D0005H0002"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_l5_old_dir = str(i2_tmpdir / "l5_old_data")
        TUR.generate_fake_l5_old_data(
            fake_l5_old_dir, tile_name, ["20200101", "20200112", "20200127"], res=30.0
        )
        config_test = str(i2_tmpdir / "i2_config_l5_old.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["l5_path_old"] = fake_l5_old_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = [30, 30]
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details

    def test_s2_l3a_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2_l3a (theia format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_l3a_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_l3a_data(
            fake_s2_l3a_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l3a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_l3a_path"] = fake_s2_l3a_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_custom_features_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Run using s2 (theia format) and custom features."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir,
            tile_name,
            ["20200101", "20200112", "20200127"],
            res=10.0,
            array_name="ones",
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.cfg_as_dict["external_features"][
            "functions"
        ] = "test_index test_index_sum"
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            " -ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        expected_columns = [
            "ogc_fid",
            "geometry",
            "i2label",
            "region",
            "code",
            "originfid",
            "tile_o",
            "sentinel2_b2_20200101",
            "sentinel2_b3_20200101",
            "sentinel2_b4_20200101",
            "sentinel2_b5_20200101",
            "sentinel2_b6_20200101",
            "sentinel2_b7_20200101",
            "sentinel2_b8_20200101",
            "sentinel2_b8a_20200101",
            "sentinel2_b11_20200101",
            "sentinel2_b12_20200101",
            "sentinel2_b2_20200111",
            "sentinel2_b3_20200111",
            "sentinel2_b4_20200111",
            "sentinel2_b5_20200111",
            "sentinel2_b6_20200111",
            "sentinel2_b7_20200111",
            "sentinel2_b8_20200111",
            "sentinel2_b8a_20200111",
            "sentinel2_b11_20200111",
            "sentinel2_b12_20200111",
            "sentinel2_b2_20200121",
            "sentinel2_b3_20200121",
            "sentinel2_b4_20200121",
            "sentinel2_b5_20200121",
            "sentinel2_b6_20200121",
            "sentinel2_b7_20200121",
            "sentinel2_b8_20200121",
            "sentinel2_b8a_20200121",
            "sentinel2_b11_20200121",
            "sentinel2_b12_20200121",
            "sentinel2_ndvi_20200101",
            "sentinel2_ndvi_20200111",
            "sentinel2_ndvi_20200121",
            "sentinel2_ndwi_20200101",
            "sentinel2_ndwi_20200111",
            "sentinel2_ndwi_20200121",
            "sentinel2_brightness_20200101",
            "sentinel2_brightness_20200111",
            "sentinel2_brightness_20200121",
            "custfeat_1_b1",
            "custfeat_1_b2",
            "custfeat_1_b3",
            "custfeat_2_b1",
        ]

        con = sqlite3.connect(
            str(
                Path(running_output_path)
                / "learningSamples"
                / "Samples_region_1_seed0_learn.sqlite"
            )
        )
        pdf = pd.read_sql_query("SELECT * from output", con)
        columns = pdf.columns
        self.assert_list_identical(columns, expected_columns)
        # assert columns == expected_colums
        # (all(k == j for k, j in zip(columns, expected_colums)))

        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_scikit_learn(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Run using s2 (theia format) and scikit-learn workflow."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir,
            tile_name,
            ["20200101", "20200112", "20200127"],
            res=10.0,
            array_name="ones",
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_scikit_learn.cfg")
        shutil.copy(self.config_ref_scikit, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["scikit_models_parameters"]["standardization"] = False
        cfg_test.cfg_as_dict["scikit_models_parameters"][
            "model_type"
        ] = "RandomForestClassifier"
        cfg_test.cfg_as_dict["scikit_models_parameters"]["min_samples_split"] = 25
        cfg_test.cfg_as_dict["scikit_models_parameters"]["random_state"] = 0
        cfg_test.cfg_as_dict["scikit_models_parameters"][
            "cross_validation_parameters"
        ] = {"n_estimators": [50, 100, 150]}
        cfg_test.cfg_as_dict["scikit_models_parameters"][
            "cross_validation_grouped"
        ] = True
        cfg_test.cfg_as_dict["scikit_models_parameters"]["cross_validation_folds"] = 2
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1"
            " -ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_s2c_custom_features_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Run using s2 (S2C format) and custom features."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results_cust_s2s2c")
        mtd_files = [
            str(IOTA2DIR / "data" / "MTD_MSIL2A_20190501.xml"),
            str(IOTA2DIR / "data" / "MTD_MSIL2A_20190506.xml"),
        ]
        fake_s2_s2c_dir = str(i2_tmpdir / "s2_s2c_data")

        TUR.generate_fake_s2_s2c_data(
            fake_s2_s2c_dir,
            tile_name,
            mtd_files,
            fake_raster=TUR.fun_array("ones"),
            fake_scene_classification=1 + TUR.fun_array("ones") * 2,
            origin_x=566377.0,
            origin_y=6284029.0,
            epsg_code=2154,
        )

        config_test = str(i2_tmpdir / "i2_config_s2_s2c.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_s2c_path"] = fake_s2_s2c_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        # cfg_test.cfg_as_dict["external_features"]module = str(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.cfg_as_dict["external_features"]["functions"] = (
            "test_index_s2_s2c " "test_index_sum_s2_s2c"
        )
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details

    def test_l5_old_custom_features_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Run using Landsat5 (old format) and custom features."""
        # prepare inputs data
        tile_name = "D0005H0002"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_l5_old_dir = str(i2_tmpdir / "l5_old_data")
        TUR.generate_fake_l5_old_data(
            fake_l5_old_dir,
            tile_name,
            ["20200101", "20200112", "20200127"],
            res=30.0,
            array_name="ones",
        )
        config_test = str(i2_tmpdir / "i2_config_l5_old.cfg")

        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["l5_path_old"] = fake_l5_old_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        # cfg_test.cfg_as_dict["external_features"]module = str(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.cfg_as_dict["external_features"]["functions"] = (
            "test_index_l5_old " "test_index_sum_l5_old"
        )
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = [30, 30]
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1"
            " -ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_l8_old_custom_features_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Run using s2 (theia format) and custom features."""
        # prepare inputs data
        tile_name = "D0005H0002"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_l8_old_dir = str(i2_tmpdir / "l8_old_data")
        TUR.generate_fake_l8_old_data(
            fake_l8_old_dir,
            tile_name,
            ["20200101", "20200112", "20200127"],
            res=30.0,
            array_name="ones",
        )
        config_test = str(i2_tmpdir / "i2_config_l8_old.cfg")

        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = fake_l8_old_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        # cfg_test.cfg_as_dict["external_features"]module = str(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.cfg_as_dict["external_features"]["functions"] = (
            "test_index_l8_old" " test_index_sum_l8_old"
        )
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = [30, 30]
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_l8_custom_features_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Run using Landsat8 (theia format) and custom features."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_l8_theia_dir = str(i2_tmpdir / "l8_data")
        TUR.generate_fake_l8_data(
            fake_l8_theia_dir,
            tile_name,
            ["20200101", "20200112", "20200127"],
            res=30.0,
            array_name="ones",
        )
        config_test = str(i2_tmpdir / "i2_config_l8.cfg")

        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["l8_path"] = fake_l8_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        # cfg_test.cfg_as_dict["external_features"]module = str(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.cfg_as_dict["external_features"][
            "functions"
        ] = "test_index_l8 test_index_sum_l8"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = [30, 30]
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_l3a_custom_features_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Run using s2 (L3A format) and custom features."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_l3a_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_l3a_data(
            fake_s2_l3a_dir,
            tile_name,
            ["20200101", "20200112", "20200127"],
            res=10.0,
            array_name="ones",
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l3a.cfg")

        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_l3a_path"] = fake_s2_l3a_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path

        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        # cfg_test.cfg_as_dict["external_features"]module = str(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.cfg_as_dict["external_features"]["functions"] = (
            "test_index_s2_l3a " "test_index_sum_s2_l3a"
        )
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1"
            " -ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_custom_features_concat_test_run(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):
        # pylint: disable=unused-argument
        """Run using s2 (theia format) and custom features."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir,
            tile_name,
            ["20200101", "20200112", "20200127"],
            res=10.0,
            array_name="ones",
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        # cfg_test.cfg_as_dict["external_features"]module = str(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.cfg_as_dict["external_features"][
            "functions"
        ] = "test_index test_index_sum"
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["external_features"]["concat_mode"] = False
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_run_keep_bands(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2 (theia format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref_keep, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = [10, 10]
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["Sentinel_2"]["keep_bands"] = ["B2", "B3"]
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_custom_features_raw_and_exo_test_run(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):
        # pylint: disable=unused-argument
        """Run using s2 (theia format) and custom features."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir,
            tile_name,
            ["20200101", "20200112", "20200127"],
            res=10.0,
            array_name="ones",
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["external_features"]["concat_mode"] = False
        cfg_test.cfg_as_dict["external_features"][
            "functions"
        ] = "get_raw_data get_soi use_raw_data use_exogeneous_data"
        cfg_test.cfg_as_dict["external_features"]["exogeneous_data"] = str(
            IOTA2DIR / "data" / "numpy_features" / "fake_exo_multi_band.tif"
        )
        cfg_test.cfg_as_dict["python_data_managing"]["data_mode_access"] = "both"
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1"
            " -ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        con = sqlite3.connect(
            str(
                Path(running_output_path)
                / "learningSamples"
                / "Samples_region_1_seed0_learn.sqlite"
            )
        )
        pdf = pd.read_sql_query("SELECT * from output", con)
        columns = pdf.columns

        expected_columns = [
            "ogc_fid",
            "geometry",
            "i2label",
            "region",
            "code",
            "originfid",
            "tile_o",
            "sentinel2_b2_20200101",
            "sentinel2_b3_20200101",
            "sentinel2_b4_20200101",
            "sentinel2_b5_20200101",
            "sentinel2_b6_20200101",
            "sentinel2_b7_20200101",
            "sentinel2_b8_20200101",
            "sentinel2_b8a_20200101",
            "sentinel2_b11_20200101",
            "sentinel2_b12_20200101",
            "sentinel2_b2_20200112",
            "sentinel2_b3_20200112",
            "sentinel2_b4_20200112",
            "sentinel2_b5_20200112",
            "sentinel2_b6_20200112",
            "sentinel2_b7_20200112",
            "sentinel2_b8_20200112",
            "sentinel2_b8a_20200112",
            "sentinel2_b11_20200112",
            "sentinel2_b12_20200112",
            "sentinel2_b2_20200127",
            "sentinel2_b3_20200127",
            "sentinel2_b4_20200127",
            "sentinel2_b5_20200127",
            "sentinel2_b6_20200127",
            "sentinel2_b7_20200127",
            "sentinel2_b8_20200127",
            "sentinel2_b8a_20200127",
            "sentinel2_b11_20200127",
            "sentinel2_b12_20200127",
            "sentinel2_mask_20200101",
            "sentinel2_mask_20200112",
            "sentinel2_mask_20200127",
            "soi_1",
            "soi_2",
            "soi_3",
            "custfeat_3_b1",
            "custfeat_3_b2",
            "custfeat_3_b3",
            "exo_0",
            "exo_1",
            "exo_2",
        ]
        self.assert_list_identical(columns, expected_columns)
        # assert columns, expected_columns
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_split_model_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2 (theia format) split models."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["chain"]["region_path"] = self.region_path
        cfg_test.cfg_as_dict["arg_train"]["mode_outside_regionsplit"] = 0.01
        cfg_test.cfg_as_dict["arg_train"]["random_seed"] = 1
        cfg_test.cfg_as_dict["arg_classification"]["classif_mode"] = "fusion"

        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -scheduler_type debug"
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details

        self.assert_file_exist(
            Path(running_output_path)
            / "samplesSelection"
            / "samples_region_1_seed_0_outrates.csv"
        )
        self.assert_file_exist(
            Path(running_output_path)
            / "samplesSelection"
            / "samples_region_2f1_seed_0_outrates.csv"
        )
        self.assert_file_exist(
            Path(running_output_path)
            / "samplesSelection"
            / "samples_region_2f2_seed_0_outrates.csv"
        )
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        assert np.sum(classif) > 0

    def test_s2_theia_pytorch_irregular_time_series(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):
        # pylint: disable=unused-argument
        """Run using s2 (theia format) and pytorch workflow.

        Use irregular time series (without gapfilling)
        """
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_pytorch.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["arg_train"]["features_from_raw_dates"] = True
        cfg_test.cfg_as_dict["arg_train"]["deep_learning_parameters"] = {
            "dl_name": "ANN"
        }
        cfg_test.cfg_as_dict["arg_train"]["classifier"] = None
        cfg_test.cfg_as_dict["arg_train"]["otb_classifier_options"] = {}
        cfg_test.cfg_as_dict["arg_classification"]["enable_probability_map"] = True
        cfg_test.cfg_as_dict["arg_classification"][
            "generate_final_probability_map"
        ] = True
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_pytorch_gapfilling_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """
        Test iota2's run using s2 (theia format) and pytorch workflow.

        Use regular time series (with gapfilling)
        """
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_pytorch.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["arg_train"]["classifier"] = None
        cfg_test.cfg_as_dict["arg_train"]["otb_classifier_options"] = {}
        cfg_test.cfg_as_dict["arg_train"]["deep_learning_parameters"] = {
            "dl_name": "ANN"
        }
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)

        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_duplicated_dates(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2 and duplicated dates."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir,
            tile_name,
            ["20200110", "20200112", "20200112-2"],
            res=10.0,
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_dupl.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["chain"]["region_path"] = self.region_path
        cfg_test.cfg_as_dict["arg_train"]["random_seed"] = 1
        cfg_test.cfg_as_dict["sensors_data_interpolation"] = {"use_gapfilling": False}
        cfg_test.cfg_as_dict["Sentinel_2"] = {"temporal_resolution": 1}
        cfg_test.save(config_test)

        # check if iota2 throw an error if user is asking no gapfilling
        # and duplicated dates are detected
        pytest.raises(
            ValueError,
            run,
            config_test,
            None,
            "debug",
            1,
            20,
            False,
            None,
            [],
            False,
            1,
            None,
        )

        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["use_gapfilling"] = True
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -scheduler_type debug"
        run_cmd(cmd)

        test_fields = get_fields(
            str(
                Path(running_output_path)
                / "learningSamples"
                / "Samples_region_1_seed0_learn.sqlite"
            ),
            "SQLite",
        )
        expected_field_list = [
            "i2label",
            "region",
            "code",
            "originfid",
            "tile_o",
            "sentinel2_b2_20200110",
            "sentinel2_b3_20200110",
            "sentinel2_b4_20200110",
            "sentinel2_b5_20200110",
            "sentinel2_b6_20200110",
            "sentinel2_b7_20200110",
            "sentinel2_b8_20200110",
            "sentinel2_b8a_20200110",
            "sentinel2_b11_20200110",
            "sentinel2_b12_20200110",
            "sentinel2_b2_20200111",
            "sentinel2_b3_20200111",
            "sentinel2_b4_20200111",
            "sentinel2_b5_20200111",
            "sentinel2_b6_20200111",
            "sentinel2_b7_20200111",
            "sentinel2_b8_20200111",
            "sentinel2_b8a_20200111",
            "sentinel2_b11_20200111",
            "sentinel2_b12_20200111",
            "sentinel2_b2_20200112",
            "sentinel2_b3_20200112",
            "sentinel2_b4_20200112",
            "sentinel2_b5_20200112",
            "sentinel2_b6_20200112",
            "sentinel2_b7_20200112",
            "sentinel2_b8_20200112",
            "sentinel2_b8a_20200112",
            "sentinel2_b11_20200112",
            "sentinel2_b12_20200112",
            "sentinel2_ndvi_20200110",
            "sentinel2_ndvi_20200111",
            "sentinel2_ndvi_20200112",
            "sentinel2_ndwi_20200110",
            "sentinel2_ndwi_20200111",
            "sentinel2_ndwi_20200112",
            "sentinel2_brightness_20200110",
            "sentinel2_brightness_20200111",
            "sentinel2_brightness_20200112",
        ]
        assert test_fields, expected_field_list
        # msg="wrong fields into the learning database")

        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details

        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_pytorch_gapfilling_external_features_run(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):
        # pylint: disable=unused-argument
        """
        Test iota2's run using s2 (theia format) and pytorch workflow.

        Use with regular time series (with gapfilling) and
         external features
        """
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_pytorch.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["arg_train"]["classifier"] = None
        cfg_test.cfg_as_dict["arg_train"]["otb_classifier_options"] = {}
        cfg_test.cfg_as_dict["arg_train"]["deep_learning_parameters"] = {
            "dl_name": "ANN"
        }
        cfg_test.cfg_as_dict["external_features"]["functions"] = "get_soi_s2"
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.save(config_test)

        # Launch the chain

        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_autocontext_run(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Run using s2 (theia format) with autoContext workflow."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_pytorch.cfg")
        shutil.copy(self.config_ref, config_test)

        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["arg_train"]["classifier"] = None
        cfg_test.cfg_as_dict["arg_train"]["otb_classifier_options"] = {}
        cfg_test.cfg_as_dict["arg_train"]["enable_autocontext"] = True
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_run_boundary_fusion(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2 (theia format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["region_path"] = self.region_path
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"

        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["arg_classification"]["boundary_comparison_mode"] = False
        cfg_test.cfg_as_dict["arg_classification"]["enable_probability_map"] = True
        cfg_test.cfg_as_dict["arg_classification"]["boundary_interior_buffer_size"] = 5
        cfg_test.cfg_as_dict["arg_classification"]["boundary_exterior_buffer_size"] = 5
        cfg_test.cfg_as_dict["arg_classification"]["enable_boundary_fusion"] = True
        cfg_test.cfg_as_dict["arg_classification"]["boundary_fusion_epsilon"] = 0.001
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 2
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 "
            "-ending_step 20 -scheduler_type debug"
        )
        run_cmd(cmd)
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details

        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_s2_theia_run_boundary_fusion_comparison(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):
        # pylint: disable=unused-argument
        """Tests iota2's run using s2 (theia format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["region_path"] = self.region_path
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["random_seed"] = 1
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["arg_classification"]["boundary_comparison_mode"] = True
        cfg_test.cfg_as_dict["arg_classification"]["enable_probability_map"] = True
        cfg_test.cfg_as_dict["arg_classification"][
            "generate_final_probability_map"
        ] = False
        cfg_test.cfg_as_dict["arg_classification"]["boundary_interior_buffer_size"] = 5
        cfg_test.cfg_as_dict["arg_classification"]["boundary_exterior_buffer_size"] = 5
        cfg_test.cfg_as_dict["arg_classification"]["enable_boundary_fusion"] = True
        cfg_test.cfg_as_dict["arg_classification"]["boundary_fusion_epsilon"] = 0.001
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 2
        cfg_test.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1"
            " -ending_step 21 -scheduler_type debug"
        )
        run_cmd(cmd)
        # ensure results in standard folder
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final" / "standard",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details

        # ensure results in boundary folder
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final" / "boundary",
            classif=True,
            classif_color=True,
            confidence=True,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details

        # ensure results in final folder
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "standard" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

        classif_bound = raster_to_array(
            str(Path(running_output_path) / "final" / "boundary" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif_bound))
        assert np.sum(classif_bound) > 0
