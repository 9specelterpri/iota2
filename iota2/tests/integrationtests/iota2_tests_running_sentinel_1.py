#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Integration S1 sensors test."""

import configparser
from pathlib import Path

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.common.file_utils import ensure_dir
from iota2.sensors.Sentinel_1 import sentinel_1
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_launcher import get_large_i2_data_test


class Iota2TestSentinel1(AssertsFilesUtils):
    """Test sentinel_1."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        get_large_i2_data_test()

        cls.expected_labels = [
            'sentinel1_desvv_20151231', 'sentinel1_desvh_20151231',
            'sentinel1_ascvv_20170518', 'sentinel1_ascvh_20170518'
        ]
        cls.sar_config_test = str(IOTA2DIR / "data" / "references" /
                                  "running_iota2" / "large_i2_data_test" /
                                  "s1_data" / "i2_config_sar.cfg")
        cls.srtm_db = str(IOTA2DIR / "data" / "references" / "running_iota2" /
                          "large_i2_data_test" / "s1_data" / "srtm.shp")
        cls.srtm_dir = str(IOTA2DIR / "data" / "references" / "running_iota2" /
                           "large_i2_data_test" / "s1_data" / "SRTM")
        cls.geoid_file = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "large_i2_data_test" /
                             "s1_data" / "egm96.grd")
        cls.acquisitions = str(IOTA2DIR / "data" / "references" /
                               "running_iota2" / "large_i2_data_test" /
                               "s1_data" / "acquisitions")
        cls.tiles_grid_db = str(IOTA2DIR / "data" / "references" /
                                "running_iota2" / "large_i2_data_test" /
                                "s1_data" / "Features.shp")

    def test_instance_s1(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests if the class sentinel_1 can be instanciate."""
        config_path_test_s1 = str(i2_tmpdir / "s1_config.cfg")
        S2_data = str(i2_tmpdir)
        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101"],
        )

        s1_output_data = str(i2_tmpdir / "tilled_s1")
        ensure_dir(s1_output_data)
        config = configparser.ConfigParser()
        config.read(self.sar_config_test)
        paths = config["Paths"]
        paths["output"] = s1_output_data
        paths["srtm"] = self.srtm_dir
        paths["geoidfile"] = self.geoid_file
        paths["s1images"] = self.acquisitions
        processing = config["Processing"]

        processing["referencesfolder"] = S2_data
        processing["srtmshapefile"] = self.srtm_db
        processing["tilesshapefile"] = self.tiles_grid_db
        processing["TemporalResolution"] = "10"

        with open(config_path_test_s1, 'w') as configfile:
            config.write(configfile)

        args = {
            "tile_name": "T31TCJ",
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": config_path_test_s1,
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False
        }

        Path.mkdir(i2_tmpdir / "features" / "T31TCJ" / "tmp",
                   parents=True,
                   exist_ok=True)
        s1_sensor = sentinel_1(**args)
        (sar_features, _), features_labels = s1_sensor.get_features()
        sar_features.ExecuteAndWriteOutput()
        expected_output = sar_features.GetParameterString("out")
        self.assert_file_exist(expected_output)
        assert features_labels == self.expected_labels
