#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.validation import classification_shaping as CS


class Iota2TestClassificationShaping:
    """ Test the classification shaping workflow."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data = str(IOTA2DIR / "data" / "references" /
                           "ClassificationShaping")

    def test_classification_shaping(self, i2_tmpdir, rm_tmpdir_on_success):
        # Prepare data
        path_classif = i2_tmpdir / "classif"
        classif_final = i2_tmpdir / "final"

        Path.mkdir(path_classif / "MASK", parents=True, exist_ok=True)
        Path.mkdir(path_classif / "tmpClassif", parents=True, exist_ok=True)
        Path.mkdir(classif_final / "TMP", parents=True, exist_ok=True)
        classif_final = str(classif_final)
        path_classif = str(path_classif)
        # copy input file
        src_files = os.listdir(self.ref_data + "/Input/Classif/MASK")
        for file_name in src_files:
            full_file_name = str(
                Path(self.ref_data) / "Input" / "Classif" / "MASK" / file_name)
            shutil.copy(full_file_name, str(Path(path_classif) / "MASK"))

        src_files = os.listdir(
            str(Path(self.ref_data) / "Input" / "Classif" / "classif"))
        for file_name in src_files:
            full_file_name = str(
                Path(self.ref_data) / "Input" / "Classif" / "classif" /
                file_name)
            shutil.copy(full_file_name, path_classif)

        # run test
        region_file = str(IOTA2DIR / "data" / "regionShape" /
                          "regionTiles.shp")
        features_ref = str(IOTA2DIR / "data" / "references" / "features")
        features_ref_test = str(i2_tmpdir / "features")
        os.mkdir(features_ref_test)
        shutil.copytree(features_ref + "/D0005H0002",
                        features_ref_test + "/D0005H0002")
        shutil.copytree(features_ref + "/D0005H0003",
                        features_ref_test + "/D0005H0003")

        nb_run = 1
        colortable = str(IOTA2DIR / "data" / "references" / "color.txt")
        CS.classification_shaping(
            path_classif=path_classif,
            ds_sar_opt=False,
            final_folder=str(i2_tmpdir / "final"),
            final_tmp_folder=str(i2_tmpdir / "final" / "TMP"),
            validation_input_folder=str(i2_tmpdir / "dataAppVal"),
            nb_view_folder=str(i2_tmpdir / "features"),
            runs=nb_run,
            classif_mode="separate",
            proba_map_flag=False,
            spatial_resolution=[30, 30],
            output_statistics=False,
            region_shape=region_file,
            color_path=colortable,
            data_field="code",
            tiles_from_cfg=["D0005H0002", "D0005H0003"],
            labels_conversion={"12": 12})

        # file comparison to ref file
        service_compare_image_file = TUR.service_compare_image_file()
        src_files = os.listdir(self.ref_data + "/Output/")
        for file_name in src_files:
            file1 = os.path.join(classif_final, file_name)
            referencefile1 = os.path.join(self.ref_data + "/Output/",
                                          file_name)
            nbdiff = service_compare_image_file.gdalFileCompare(
                file1, referencefile1)
            assert nbdiff == 0, f"{file1}, {referencefile1}"
