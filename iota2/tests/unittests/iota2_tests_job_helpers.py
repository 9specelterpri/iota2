#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import pytest

from iota2.steps import i2_job_helpers
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


def force_env(self, arg) -> str:
    """Substitution function."""
    return "export ...\n"


class Iota2TestJobWriter(AssertsFilesUtils):
    """Check job writer services."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        # useful to force task env
        i2_job_helpers.JobFileHelper.task_env = force_env

        cls.pbs_ref_file = str(IOTA2DIR / "data" / "references" / "JobHelper" /
                               "pbs_ref_file.pbs")
        cls.pbs_exe_ref_file = str(IOTA2DIR / "data" / "references" /
                                   "JobHelper" / "pbs_exe_ref_file.pbs")
        cls.slurm_ref_file = str(IOTA2DIR / "data" / "references" /
                                 "JobHelper" / "slurm_ref_file.slurm")
        cls.resources_file = str(IOTA2DIR / "data" / "config" /
                                 "resources.cfg")

    def test_job_helper(self):
        """Test base class services."""
        pytest.raises(
            FileNotFoundError,
            i2_job_helpers.JobHelper,
            self.resources_file.replace(".cfg", "_NO.cfg"),
            "iota2_chain",
        )
        job_helper = i2_job_helpers.JobHelper(self.resources_file,
                                              "iota2_chain")
        pytest.raises(
            ValueError,
            job_helper.write,
            "wrong_scheduler",
            "i2_job_name",
            "job_file",
            "log_out_file",
            "log_err_file",
            "task_function",
            "kwargs",
            "logger_lvl",
        )
        new_nb_cpu = 10
        job_helper.force_resource("cpu", new_nb_cpu)
        assert job_helper.resources["cpu"] == new_nb_cpu

    def test_pbs_writer(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests pbs writer."""
        job_helper = i2_job_helpers.JobHelper(self.resources_file,
                                              "iota2_chain")
        job_file = str(i2_tmpdir / "i2_job.pbs")
        log_err_file = "log_err.txt"
        log_out_file = "log_out.txt"
        job_helper.write(
            "pbs",
            "i2_job_name",
            job_file,
            log_out_file,
            log_err_file,
            self.dummy_task_function,
            {"arg1": 1},
            "INFO",
        )
        self.assert_file_exist(job_file)
        self.assert_file_equal(job_file, self.pbs_ref_file)

        # pbs_exe_ref_file
        job_file = str(i2_tmpdir / "i2_job_froce_exe.pbs")
        job_helper.write(
            "pbs",
            "i2_job_name_force_exe",
            job_file,
            log_out_file,
            log_err_file,
            self.dummy_task_function,
            {"arg1": 1},
            "INFO",
            force_executable="python Iota.py",
        )
        self.assert_file_exist(job_file)
        self.assert_file_equal(job_file, self.pbs_exe_ref_file)

    def test_slurm_writer(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests slurm writer."""
        job_helper = i2_job_helpers.JobHelper(self.resources_file,
                                              "iota2_chain")
        job_file = str(i2_tmpdir / "i2_job.slurm")
        log_err_file = "log_err.txt"
        log_out_file = "log_out.txt"
        job_helper.write(
            "slurm",
            "i2_job_name",
            job_file,
            log_out_file,
            log_err_file,
            self.dummy_task_function,
            {"arg1": 1},
            "INFO",
        )
        self.assert_file_exist(job_file)
        self.assert_file_equal(job_file, self.slurm_ref_file)

    @staticmethod
    def dummy_task_function(arg1):
        """Dummy function to serialize."""
        pass
