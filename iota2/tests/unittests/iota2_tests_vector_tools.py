#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsVectorTools

import os
import shutil
import sqlite3
from pathlib import Path

import pandas as pd
import pytest

from iota2.simplification import vector_generalize as vg
from iota2.vector_tools import add_field_perimeter as afp
from iota2.vector_tools import buffer_ogr as bfo
from iota2.vector_tools import change_name_field as cnf
from iota2.vector_tools import check_geometry_area_thresh_field as check
from iota2.vector_tools import conditional_field_recode as cfr
from iota2.vector_tools import spatial_operations as so
from iota2.vector_tools import vector_functions as vf

IOTA2DIR = os.environ.get("IOTA2DIR")

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR)


@pytest.mark.vecto
class Iota2TestVectorTools:
    """Test some of vector tools features."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.classif = str(
            IOTA2DIR / "data" / "references" / "vectortools" / "classif.shp"
        )
        cls.inter = str(IOTA2DIR / "data" / "references" / "vectortools" / "region.shp")
        cls.classifout = str(
            IOTA2DIR / "data" / "references" / "vectortools" / "classifout.shp"
        )
        cls.fake_region = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "fake_region.shp"
        )
        cls.classif_raster_file = str(
            IOTA2DIR
            / "data"
            / "references"
            / "Runs_standards"
            / "Usual_s2_theia_run"
            / "test_results"
            / "final"
            / "Classif_Seed_0.tif"
        )
        cls.classif_vector_file = str(
            IOTA2DIR / "data" / "references" / "vectortools" / "i2_classif.sqlite"
        )

    def test_iota2_vectortools(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test how many samples must be add to the sample set."""
        outinter = str(i2_tmpdir / "inter.shp")
        classifwd = i2_tmpdir / "out" / "classif.shp"
        if not classifwd.parent.exists():
            classifwd.parent.mkdir(parents=True, exist_ok=True)
        classifwd = str(classifwd)
        # Add Field
        for ext in [".shp", ".dbf", ".shx", ".prj"]:
            shutil.copyfile(
                os.path.splitext(self.classif)[0] + ext,
                os.path.splitext(classifwd)[0] + ext,
            )

        afp.add_fieldPerimeter(classifwd)
        tmpbuff = str(i2_tmpdir / "tmpbuff.shp")
        bfo.buffer_poly(classifwd, tmpbuff, -10)
        for ext in [".shp", ".dbf", ".shx", ".prj"]:
            shutil.copyfile(
                os.path.splitext(tmpbuff)[0] + ext, os.path.splitext(classifwd)[0] + ext
            )

        cnf.change_name(classifwd, "Classe", "class")
        assert vf.get_nb_feat(classifwd) == 144, "Number of features does not fit"
        assert vf.get_fields(classifwd) == [
            "Validmean",
            "Validstd",
            "Confidence",
            "Hiver",
            "Ete",
            "Feuillus",
            "Coniferes",
            "Pelouse",
            "Landes",
            "UrbainDens",
            "UrbainDiff",
            "ZoneIndCom",
            "Route",
            "PlageDune",
            "SurfMin",
            "Eau",
            "GlaceNeige",
            "Prairie",
            "Vergers",
            "Vignes",
            "Perimeter",
            "class",
        ], "List of fields does not fit"
        assert vf.list_value_fields(classifwd, "class") == [
            "11",
            "12",
            "211",
            "222",
            "31",
            "32",
            "36",
            "42",
            "43",
            "51",
        ], "Values of field 'class' do not fit"
        assert (
            vf.get_field_type(classifwd, "class") == str
        ), f"Type of field 'class' ({vf.get_field_type(classifwd, 'class')}) do not fit, 'str' expected"

        cfr.con_field_recode(classifwd, "class", "mask", 11, 0)
        so.intersectSqlites(
            classifwd,
            self.inter,
            outinter,
            2154,
            [
                "class",
                "Validmean",
                "Validstd",
                "Confidence",
                "ID",
                "Perimeter",
                "Aire",
                "mask",
            ],
        )
        check.check_geometry_area_thresh_field(outinter, 100, 1, classifwd)
        assert vf.get_nb_feat(classifwd) == 102, "Number of features does not fit"

    def test_topological_polygonize(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test vector_generalize.topological_polygonize function behaviour."""
        _, in_raster_name = os.path.split(self.classif_raster_file)
        out_vector = str(i2_tmpdir / in_raster_name.replace(".tif", ".sqlite"))
        if os.path.exists(out_vector):
            os.remove(out_vector)
        vg.topological_polygonize(
            str(i2_tmpdir),
            self.classif_raster_file,
            angle=True,
            out=out_vector,
            outformat="SQLite",
            debulvl="info",
            epsg="2154",
            working_dir=None,
            clipfile=self.fake_region,
            clipfield="region",
            clipvalue=1,
            bufferclip="20000",
        )
        con = sqlite3.connect(out_vector)
        df = pd.read_sql_query("SELECT * from vectile", con)
        assert list(df.columns) == ["ogc_fid", "GEOMETRY", "cat", "label"]
        assert len(df) >= 1

    def test_generalization(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test vector_generalize.generalize_vector function behaviour."""
        _, in_vector_name = os.path.split(self.classif_vector_file)

        out_vector_file = str(
            i2_tmpdir / in_vector_name.replace(".sqlite", "_douglas.sqlite")
        )

        if os.path.exists(out_vector_file):
            os.remove(out_vector_file)
        vg.generalize_vector(
            str(i2_tmpdir),
            self.classif_vector_file,
            paramgene=10,
            method="douglas",
            mmu="",
            ncolumns="cat",
            out=out_vector_file,
            outformat="SQLite",
            debulvl="info",
            epsg="2154",
            working_dir=None,
            clipfile=None,
            snap=1,
        )
        con = sqlite3.connect(out_vector_file)
        df = pd.read_sql_query("SELECT * from tmp", con)
        assert list(df.columns) == ["ogc_fid", "GEOMETRY", "cat", "area", "id"]
        assert len(df) >= 1
