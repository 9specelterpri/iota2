#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Test sampler applications
"""
import os
import shutil
from pathlib import Path

from iota2.common import file_utils as fut
from iota2.common import iota2_directory
from iota2.common.tools import create_regions_by_tiles as RT
from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import tile_area as area
from iota2.sampling import tile_envelope as env
from iota2.sampling import vector_sampler
from iota2.sensors.process_launcher import common_mask
from iota2.tests.utils import tests_utils_vectors as TUV
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_rasters import prepare_annual_features
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.add_field import add_field

IOTA2_DATATEST = str(IOTA2DIR / "data")


class Iota2TestSamplerApplications:
    """Class dedicated to launch several sampler tests"""

    @classmethod
    def setup_class(cls):

        # Create folder containing samples used for tests
        cls.group_test_name = "iota_test_sampler_applications"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data", cls.group_test_name)
        # Tests directory
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

        # cls.test_vector = os.path.join(cls.iota2_tests_directory,
        #                                "test_vector")
        cls.referenceShape = (
            IOTA2_DATATEST + "/references/sampler/D0005H0002_polygons_To_Sample.shp"
        )
        cls.referenceShape_test = TUV.shape_reference_vector(
            cls.referenceShape, "D0005H0002"
        )
        cls.configSimple_NO_bindings = IOTA2_DATATEST + "/config/test_config.cfg"
        cls.configSimple_bindings = IOTA2_DATATEST + "/config/test_config_bindings.cfg"
        cls.configSimple_bindings_uDateFeatures = (
            IOTA2_DATATEST + "/config/test_config_bindings_uDateFeatures.cfg"
        )
        cls.configCropMix_NO_bindings = (
            IOTA2_DATATEST + "/config/test_config_cropMix.cfg"
        )
        cls.configCropMix_bindings = (
            IOTA2_DATATEST + "/config/test_config_cropMix_bindings.cfg"
        )
        cls.configClassifCropMix_NO_bindings = (
            IOTA2_DATATEST + "/config/test_config_classifCropMix.cfg"
        )
        cls.configClassifCropMix_bindings = (
            IOTA2_DATATEST + "/config/test_config_classifCropMix_bindings.cfg"
        )
        cls.configPrevClassif = IOTA2_DATATEST + "/config/prevClassif.cfg"

        cls.regionShape = os.path.join(
            IOTA2_DATATEST, "references", "region_need_To_env.shp"
        )
        cls.features = (
            IOTA2_DATATEST + "/references/features/D0005H0002/Final/"
            "SL_MultiTempGapF_Brightness_NDVI_NDWI__.tif"
        )
        cls.MNT = IOTA2_DATATEST + "/references/MNT/"
        cls.expectedFeatures = {11: 74, 12: 34, 42: 19, 51: 147}
        cls.SensData = IOTA2_DATATEST + "/L8_50x50"
        cls.iota2_directory = os.environ.get("IOTA2DIR")

        cls.selection_test = os.path.join(
            cls.iota2_tests_directory, "D0005H0002.sqlite"
        )

        cls.multi_param_cust = {
            "enable_raw": False,
            "enable_gap": True,
            "fill_missing_dates": False,
            "all_dates_dict": {},
            "mask_valid_data": None,
            "mask_value": 0,
            "exogeneous_data": None,
        }
        raster_ref = fut.file_search_and(
            os.path.join(cls.SensData, "D0005H0002"), True, ".TIF"
        )[0]

        TUV.prepare_test_selection(
            cls.referenceShape_test,
            raster_ref,
            cls.selection_test,
            cls.iota2_tests_directory,
            "code",
        )

    @classmethod
    def teardown_class(cls):
        """"""
        if os.path.exists(cls.selection_test):
            os.remove(cls.selection_test)

    # Tests definitions
    def test_samplerSimple_bindings(self, i2_tmpdir, rm_tmpdir_on_success):
        def prepareTestsFolder(output_directory: str, workingDirectory=False):
            wD = None
            if not os.path.exists(output_directory):
                os.mkdir(output_directory)
            testPath = output_directory + "/simpleSampler_vector_bindings"
            if os.path.exists(testPath):
                shutil.rmtree(testPath)
            os.mkdir(testPath)
            os.mkdir(os.path.join(testPath, "features"))
            featuresOutputs = output_directory + "/simpleSampler_features_bindings"
            if os.path.exists(featuresOutputs):
                shutil.rmtree(featuresOutputs)
            os.mkdir(featuresOutputs)
            if workingDirectory:
                wD = output_directory + "/simpleSampler_bindingsTMP"
                if os.path.exists(wD):
                    shutil.rmtree(wD)
                os.mkdir(wD)
            return testPath, featuresOutputs, wD

        reference = (
            IOTA2_DATATEST + "/references/sampler/D0005H0002_polygons_"
            "To_Sample_Samples_ref_bindings.sqlite"
        )

        # load configuration file
        testPath, featuresOutputs, wD = prepareTestsFolder(str(i2_tmpdir), True)
        config_path = os.path.join(
            self.iota2_directory, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )

        config_path_test = os.path.join(wD, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        L8_rasters = os.path.join(self.iota2_directory, "data", "L8_50x50")

        cfg = rcf.read_internal_config_file(config_path_test)
        cfg.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg.cfg_as_dict["chain"]["output_path"] = testPath
        cfg.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters
        cfg.cfg_as_dict["chain"]["l8_path"] = None
        cfg.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg.cfg_as_dict["chain"]["region_field"] = "region"
        cfg.cfg_as_dict["arg_train"]["crop_mix"] = False
        cfg.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg.cfg_as_dict["sensors_data_interpolation"]["use_additional_features"] = False
        cfg.cfg_as_dict["chain"]["region_path"] = None
        cfg.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg.cfg_as_dict["simplification"]["grasslib"] = None
        cfg.cfg_as_dict["simplification"]["clipfile"] = None
        cfg.cfg_as_dict["simplification"]["bingdal"] = None
        cfg.save(config_path_test)

        os.mkdir(featuresOutputs + "/D0005H0002")
        os.mkdir(featuresOutputs + "/D0005H0002/tmp")
        tile = "D0005H0002"
        self.config = rcf.read_config_file(config_path_test)
        """
        TEST :
        prepare data to gapFilling -> gapFilling -> features generation
        -> samples extraction
        with otb's applications connected in memory
        and compare resulting samples extraction with reference.
        """
        # launch test case

        data_field = self.config.getParam("chain", "data_field")
        output_path = self.config.getParam("chain", "output_path")
        annual_crop = self.config.getParam("arg_train", "annual_crop")
        crop_mix = self.config.getParam("arg_train", "crop_mix")
        auto_context_enable = self.config.getParam("arg_train", "enable_autocontext")
        region_field = (self.config.getParam("chain", "region_field")).lower()
        proj = self.config.getParam("chain", "proj")
        runs = self.config.getParam("arg_train", "runs")
        samples_classif_mix = self.config.getParam("arg_train", "samples_classif_mix")

        # annual_config_file = cfg.getParam('arg_train', "prev_features")
        # output_path_annual = rcf.read_config_file(
        #    annual_config_file).getParam("chain", "output_path")
        output_path_annual = None
        ram = 128
        w_mode = False
        folder_annual_features = self.config.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = self.config.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = self.config.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(
            rcf.read_config_file(config_path_test)
        ).get_sensors_parameters(tile)
        try:
            sar_optical_flag = self.config.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False

        vector_sampler.generate_samples(
            train_shape_dic={"usually": self.referenceShape_test},
            path_wd=None,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        # assert
        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(test_vector, reference, cmp_mode="coordinates")
        assert compare
        del self.config
        # """
        # TEST :
        # prepare data to gapFilling -> gapFilling -> features generation -> samples extraction
        # with otb's applications connected in memory and writing tmp files
        # and compare resulting samples extraction with reference.
        # """
        testPath, featuresOutputs, wD = prepareTestsFolder(
            str(i2_tmpdir),
        )
        config_path_test = os.path.join(testPath, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        L8_rasters = os.path.join(self.iota2_directory, "data", "L8_50x50")

        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = True
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None

        cfg_test.save(config_path_test)

        os.mkdir(os.path.join(featuresOutputs, "D0005H0002"))
        os.mkdir(os.path.join(featuresOutputs, "D0005H0002", "tmp"))

        # launch test case
        config_test = rcf.read_config_file(config_path_test)
        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        # annual_config_file = cfg.getParam('arg_train', "prev_features")
        # output_path_annual = rcf.read_config_file(
        #    annual_config_file).getParam("chain", "output_path")
        output_path_annual = None
        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(
            rcf.read_config_file(config_path_test)
        ).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False
        vector_sampler.generate_samples(
            train_shape_dic={"usually": self.referenceShape_test},
            path_wd=None,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        # assert
        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(test_vector, reference, cmp_mode="coordinates")
        assert compare
        del config_test

        # """
        # TEST :
        # prepare data to gapFilling -> gapFilling -> features generation -> samples extraction
        # with otb's applications connected in memory, write all necessary
        # tmp files in a working directory and compare resulting samples
        # extraction with reference.
        # """
        testPath, featuresOutputs, wD = prepareTestsFolder(
            str(i2_tmpdir),
        )
        config_path_test = os.path.join(testPath, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        L8_rasters = os.path.join(self.iota2_directory, "data", "L8_50x50")
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = True
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.save(config_path_test, "w")

        os.mkdir(os.path.join(featuresOutputs, "D0005H0002"))
        os.mkdir(os.path.join(featuresOutputs, "D0005H0002", "tmp"))
        config_test = rcf.read_config_file(config_path_test)
        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        # annual_config_file = cfg.getParam('arg_train', "prev_features")
        # output_path_annual = rcf.read_config_file(
        #    annual_config_file).getParam("chain", "output_path")
        output_path_annual = None
        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False
        vector_sampler.generate_samples(
            train_shape_dic={"usually": self.referenceShape_test},
            path_wd=wD,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(test_vector, reference, cmp_mode="coordinates")
        assert compare
        del config_test

        # #Test user features and additional features
        reference = (
            IOTA2_DATATEST
            + "/references/sampler/D0005H0002_polygons_To_Sample_Samples_UserFeat_UserExpr.sqlite"
        )
        # """
        # TEST :
        # prepare data to gapFilling -> gapFilling
        #                            -> features generation (userFeatures + userDayFeatures)
        #                            -> samples extraction
        # """
        testPath, featuresOutputs, wD = prepareTestsFolder(
            str(i2_tmpdir), workingDirectory=False
        )
        config_path_test = os.path.join(testPath, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        L8_rasters = os.path.join(self.iota2_directory, "data", "L8_50x50")
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = os.path.join(
            self.iota2_directory, "data/references/MNT/"
        )
        cfg_test.cfg_as_dict["userFeat"]["arbo"] = "/*"
        cfg_test.cfg_as_dict["userFeat"]["patterns"] = "MNT"
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = True
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = True
        cfg_test.cfg_as_dict["Landsat8_old"][
            "additionalFeatures"
        ] = "b1+b2,(b1-b2)/(b1+b2)"
        cfg_test.save(config_path_test)

        os.mkdir(os.path.join(featuresOutputs, "D0005H0002"))
        os.mkdir(os.path.join(featuresOutputs, "D0005H0002", "tmp"))
        config_test = rcf.read_config_file(config_path_test)
        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        # annual_config_file = cfg.getParam('arg_train', "prev_features")
        # output_path_annual = rcf.read_config_file(
        #    annual_config_file).getParam("chain", "output_path")
        output_path_annual = None
        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False
        vector_sampler.generate_samples(
            train_shape_dic={"usually": self.referenceShape_test},
            path_wd=wD,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            samples_classif_mix=samples_classif_mix,
            sampling_validation=False,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(test_vector, reference, cmp_mode="coordinates")
        assert compare
        del config_test

        # """
        # TEST :
        # prepare data to gapFilling -> gapFilling
        #                            -> features generation (userFeatures + userDayFeatures)
        #                            -> samples extraction
        # with otb's applications connected in memory,
        # write all necessary tmp files in a working directory
        # and compare resulting sample extraction with reference.
        # """
        testPath, featuresOutputs, wD = prepareTestsFolder(
            str(i2_tmpdir), workingDirectory=True
        )
        config_path_test = os.path.join(testPath, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        L8_rasters = os.path.join(self.iota2_directory, "data", "L8_50x50")
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = os.path.join(
            self.iota2_directory, "data/references/MNT/"
        )
        cfg_test.cfg_as_dict["userFeat"]["arbo"] = "/*"
        cfg_test.cfg_as_dict["userFeat"]["patterns"] = "MNT"
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = True
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = True
        cfg_test.cfg_as_dict["Landsat8_old"][
            "additionalFeatures"
        ] = "b1+b2,(b1-b2)/(b1+b2)"
        cfg_test.save(config_path_test)

        os.mkdir(os.path.join(featuresOutputs, "D0005H0002"))
        os.mkdir(os.path.join(featuresOutputs, "D0005H0002", "tmp"))
        config_test = rcf.read_config_file(config_path_test)
        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        # annual_config_file = cfg.getParam('arg_train', "prev_features")
        # output_path_annual = rcf.read_config_file(
        #    annual_config_file).getParam("chain", "output_path")
        output_path_annual = None
        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False
        vector_sampler.generate_samples(
            train_shape_dic={"usually": self.referenceShape_test},
            path_wd=wD,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(test_vector, reference, cmp_mode="coordinates")
        assert compare
        del config_test

    def test_samplerCropMix_bindings(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        TEST crop_mix 1 algorithm
        using connected OTB applications :

        Step 1 : on non annual class
        prepare data to gapFilling -> gapFilling
                                   -> features generation
                                   -> samples extraction non annual

        Step 2 : on annual class
        prepare data to gapFilling -> gapFilling
                                   -> features generation
                                   -> samples extraction annual

        Step 3 : merge samples extration nonAnnual / annual

        Step 4 : compare the merged sample to reference
        """

        def prepareTestsFolder(output_directory: str, workingDirectory=False):

            testPath = output_directory + "/cropMixSampler_bindings/"
            if os.path.exists(testPath):
                shutil.rmtree(testPath)
            os.mkdir(testPath)
            os.mkdir(os.path.join(testPath, "features"))

            featuresNonAnnualOutputs = (
                output_directory + "/cropMixSampler_featuresNonAnnual_bindings"
            )
            if os.path.exists(featuresNonAnnualOutputs):
                shutil.rmtree(featuresNonAnnualOutputs)
            os.mkdir(featuresNonAnnualOutputs)
            os.mkdir(featuresNonAnnualOutputs + "/D0005H0002")
            os.mkdir(featuresNonAnnualOutputs + "/D0005H0002/tmp")

            featuresAnnualOutputs = (
                output_directory + "/cropMixSampler_featuresAnnual_bindings"
            )
            if os.path.exists(featuresAnnualOutputs):
                shutil.rmtree(featuresAnnualOutputs)
            os.mkdir(featuresAnnualOutputs)
            os.mkdir(featuresAnnualOutputs + "/D0005H0002")
            os.mkdir(featuresAnnualOutputs + "/D0005H0002/tmp")

            wD = output_directory + "/cropMixSampler_bindingsTMP"
            if os.path.exists(wD):
                shutil.rmtree(wD)
            wD = None
            if workingDirectory:
                wD = output_directory + "/cropMixSampler_bindingsTMP"
                os.mkdir(wD)
            return testPath, featuresNonAnnualOutputs, featuresAnnualOutputs, wD

        def generate_annual_config(directory, annualFeaturesPath, features_A_Outputs):

            config_path = os.path.join(
                IOTA2DIR, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg"
            )
            annual_config_path = os.path.join(directory, "AnnualConfig.cfg")
            shutil.copy(config_path, annual_config_path)
            cfg_test = rcf.read_internal_config_file(annual_config_path)
            cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
                IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
            )
            cfg_test.cfg_as_dict["chain"]["region_path"] = None
            cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = annual_config_path
            cfg_test.cfg_as_dict["chain"]["color_table"] = annual_config_path
            cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
            cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
            cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
            cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
            cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
            cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
            cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
            cfg_test.cfg_as_dict["chain"]["l8_path_old"] = annualFeaturesPath
            cfg_test.cfg_as_dict["chain"]["l8_path"] = None
            cfg_test.cfg_as_dict["chain"]["output_path"] = str(
                Path(self.iota2_tests_directory) / "output_annual"
            )
            cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
            cfg_test.cfg_as_dict["arg_train"][
                "annual_classes_extraction_source"
            ] = False
            cfg_test.cfg_as_dict["sensors_data_interpolation"][
                "use_additional_features"
            ] = False
            cfg_test.save(annual_config_path)
            featuresPath = Path(
                Path(cfg_test.cfg_as_dict["chain"]["output_path"]) / "features"
            )

            if not Path.exists(featuresPath):
                Path.mkdir(featuresPath, parents=True, exist_ok=True)
            return annual_config_path

        reference = (
            IOTA2_DATATEST
            + "/references/sampler/D0005H0002_polygons_To_Sample_Samples_CropMix_bindings.sqlite"
        )

        # prepare outputs test folders
        testPath, _, features_A_Outputs, wD = prepareTestsFolder(str(i2_tmpdir), True)
        annualFeaturesPath = testPath + "/annualFeatures"

        # prepare annual configuration file
        annual_config_path = generate_annual_config(
            wD, annualFeaturesPath, features_A_Outputs
        )

        testPath, _, features_A_Outputs, wD = prepareTestsFolder(str(i2_tmpdir), True)

        # load configuration file
        config_path = os.path.join(
            self.iota2_directory, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        # self.config = rcf.read_config_file(config_path)

        L8_rasters_non_annual = os.path.join(self.iota2_directory, "data", "L8_50x50")
        L8_rasters_annual = os.path.join(wD, "annualData")
        os.mkdir(L8_rasters_annual)
        # annual sensor data generation (pix annual = 2 * pix non_annual)
        prepare_annual_features(
            L8_rasters_annual,
            L8_rasters_non_annual,
            "CORR_PENTE",
            rename=("2016", "2015"),
        )
        # prepare annual configuration file
        annual_config_path = os.path.join(wD, "AnnualConfig.cfg")
        shutil.copy(config_path, annual_config_path)

        cfg_test = rcf.read_internal_config_file(annual_config_path)
        cfg_test.cfg_as_dict["chain"]["output_path"] = os.path.join(
            IOTA2DIR, "data", "tmp"
        )
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = annual_config_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = annual_config_path
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None

        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_annual
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["featuresPath"] = features_A_Outputs
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.save(annual_config_path)

        # fill up configuration file
        """
        TEST
        using a working directory and write temporary files on disk
        """
        # fill up configuration file
        config_path_test = os.path.join(wD, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_non_annual
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = True
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = annual_config_path
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = True
        cfg_test.save(config_path_test)

        tile = "D0005H0002"
        config_test = rcf.read_config_file(config_path_test)
        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        annual_config_file = config_test.getParam("arg_train", "prev_features")
        output_path_annual = rcf.read_config_file(annual_config_file).getParam(
            "chain", "output_path"
        )
        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False
        # Launch sampler
        vector_sampler.generate_samples(
            train_shape_dic={"usually": self.referenceShape_test},
            path_wd=None,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        # compare to reference
        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(
            test_vector, reference, cmp_mode="coordinates", ignored_fields=["originfid"]
        )
        assert compare
        del config_test
        """
        TEST
        using a working directory and without temporary files
        """
        testPath, _, features_A_Outputs, wD = prepareTestsFolder(str(i2_tmpdir), True)

        # prepare annual configuration file
        annual_config_path = os.path.join(wD, "AnnualConfig.cfg")
        shutil.copy(config_path, annual_config_path)

        cfg = rcf.read_internal_config_file(annual_config_path)
        cfg.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg.cfg_as_dict["chain"]["region_path"] = None
        cfg.cfg_as_dict["chain"]["nomenclature_path"] = annual_config_path
        cfg.cfg_as_dict["chain"]["color_table"] = annual_config_path
        cfg.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg.cfg_as_dict["simplification"]["grasslib"] = None
        cfg.cfg_as_dict["simplification"]["clipfile"] = None
        cfg.cfg_as_dict["simplification"]["bingdal"] = None
        cfg.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_annual
        cfg.cfg_as_dict["chain"]["output_path"] = str(i2_tmpdir)
        cfg.cfg_as_dict["chain"]["l8_path"] = None
        cfg.cfg_as_dict["chain"]["featuresPath"] = features_A_Outputs
        cfg.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg.cfg_as_dict["sensors_data_interpolation"]["use_additional_features"] = False
        cfg.save(annual_config_path)

        config_path_test = os.path.join(wD, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_non_annual
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = True
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = annual_config_path
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test.save(config_path_test)

        # annual sensor data generation (pix annual = 2 * pix non_annual)
        os.mkdir(L8_rasters_annual)
        prepare_annual_features(
            L8_rasters_annual,
            L8_rasters_non_annual,
            "CORR_PENTE",
            rename=("2016", "2015"),
        )

        config_test = rcf.read_config_file(config_path_test)

        # #Launch sampler
        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        annual_config_file = config_test.getParam("arg_train", "prev_features")
        output_path_annual = rcf.read_config_file(annual_config_file).getParam(
            "chain", "output_path"
        )

        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False
        vector_sampler.generate_samples(
            train_shape_dic={"usually": self.referenceShape_test},
            path_wd=wD,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(
            test_vector, reference, cmp_mode="coordinates", ignored_fields=["originfid"]
        )
        assert compare
        del config_test

        # """
        # TEST
        # without a working directory and without temporary files on disk
        # """

        testPath, _, features_A_Outputs, wD = prepareTestsFolder(str(i2_tmpdir), True)

        # prepare annual configuration file
        annual_config_path = os.path.join(wD, "AnnualConfig.cfg")
        shutil.copy(config_path, annual_config_path)

        cfg = rcf.read_internal_config_file(annual_config_path)
        cfg.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg.cfg_as_dict["chain"]["region_path"] = None
        cfg.cfg_as_dict["chain"]["nomenclature_path"] = annual_config_path
        cfg.cfg_as_dict["chain"]["color_table"] = annual_config_path
        cfg.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg.cfg_as_dict["simplification"]["grasslib"] = None
        cfg.cfg_as_dict["simplification"]["clipfile"] = None
        cfg.cfg_as_dict["simplification"]["bingdal"] = None
        cfg.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_annual
        cfg.cfg_as_dict["chain"]["l8_path"] = None
        cfg.cfg_as_dict["chain"]["featuresPath"] = features_A_Outputs
        cfg.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg.cfg_as_dict["sensors_data_interpolation"]["use_additional_features"] = False
        cfg.save(annual_config_path)

        config_path_test = os.path.join(wD, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_non_annual
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = True
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = annual_config_path
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test.save(config_path_test)
        config_test = rcf.read_config_file(config_path_test)

        # annual sensor data generation (pix annual = 2 * pix non_annual)
        os.mkdir(L8_rasters_annual)
        prepare_annual_features(
            L8_rasters_annual,
            L8_rasters_non_annual,
            "CORR_PENTE",
            rename=("2016", "2015"),
        )

        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        annual_config_file = config_test.getParam("arg_train", "prev_features")
        output_path_annual = rcf.read_config_file(annual_config_file).getParam(
            "chain", "output_path"
        )

        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False

        # Launch sampler
        output_path_annual = str(i2_tmpdir / "annual")
        vector_sampler.generate_samples(
            train_shape_dic={"usually": self.referenceShape_test},
            path_wd=None,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )
        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(
            test_vector, reference, cmp_mode="coordinates", ignored_fields=["originfid"]
        )
        assert compare
        del config_test
        # """
        # TEST
        # without a working directory and write temporary files on disk
        # """
        testPath, _, features_A_Outputs, wD = prepareTestsFolder(str(i2_tmpdir), True)

        # prepare annual configuration file
        annual_config_path = os.path.join(wD, "AnnualConfig.cfg")
        shutil.copy(config_path, annual_config_path)
        cfg = rcf.read_internal_config_file(annual_config_path)
        cfg.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg.cfg_as_dict["chain"]["region_path"] = None
        cfg.cfg_as_dict["chain"]["nomenclature_path"] = annual_config_path
        cfg.cfg_as_dict["chain"]["color_table"] = annual_config_path
        cfg.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg.cfg_as_dict["simplification"]["grasslib"] = None
        cfg.cfg_as_dict["simplification"]["clipfile"] = None
        cfg.cfg_as_dict["simplification"]["bingdal"] = None
        cfg.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_annual
        cfg.cfg_as_dict["chain"]["l8_path"] = None
        cfg.cfg_as_dict["chain"]["featuresPath"] = features_A_Outputs
        cfg.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg.cfg_as_dict["sensors_data_interpolation"]["use_additional_features"] = False
        cfg.save(annual_config_path)

        config_path_test = os.path.join(wD, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_non_annual
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = True
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = annual_config_path
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = True
        cfg_test.save(config_path_test)
        config_test = rcf.read_config_file(config_path_test)
        # annual sensor data generation (pix annual = 2 * pix non_annual)
        os.mkdir(L8_rasters_annual)
        prepare_annual_features(
            L8_rasters_annual,
            L8_rasters_non_annual,
            "CORR_PENTE",
            rename=("2016", "2015"),
        )

        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        annual_config_file = config_test.getParam("arg_train", "prev_features")
        output_path_annual = rcf.read_config_file(annual_config_file).getParam(
            "chain", "output_path"
        )

        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False
        # Launch Sampling
        output_path_annual = i2_tmpdir / "annual"
        Path.mkdir(output_path_annual, exist_ok=True)
        vector_sampler.generate_samples(
            train_shape_dic={"usually": self.referenceShape_test},
            path_wd=None,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        # Compare vector produce to reference
        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(
            test_vector, reference, cmp_mode="coordinates", ignored_fields=["originfid"]
        )
        assert compare
        del config_test

    def test_samplerClassifCropMix_bindings(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        TEST crop_mix 2 algorithm

        Step 1 : on non Annual classes, select samples
        Step 2 : select randomly annual samples into a provided land cover map.
        Step 3 : merge samples from step 1 and 2
        Step 4 : Compute feature to samples.

        random part in this script could not be control, no reference vector can be done.
        Only number of features can be check.
        """
        config_path = os.path.join(
            self.iota2_directory, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )

        def prepareTestsFolder(output_directory, workingDirectory=False):
            wD = None
            testPath = output_directory + "/classifCropMixSampler_bindings/"

            if os.path.exists(testPath):
                shutil.rmtree(testPath)
            os.mkdir(testPath)

            featuresOutputs = (
                output_directory + "/classifCropMixSampler_features_bindings"
            )
            if os.path.exists(featuresOutputs):
                shutil.rmtree(featuresOutputs)
            os.mkdir(featuresOutputs)

            if workingDirectory:
                wD = output_directory + "/classifCropMixSampler_bindingsTMP"
                if os.path.exists(wD):
                    shutil.rmtree(wD)
                os.mkdir(wD)
            return testPath, featuresOutputs, wD

        L8_rasters_old = os.path.join(self.iota2_directory, "data", "L8_50x50")
        classifications_path = os.path.join(
            self.iota2_directory, "data", "references", "sampler"
        )

        testPath, _, wD = prepareTestsFolder(str(i2_tmpdir), True)

        # rename reference shape
        vector = TUV.shape_reference_vector(self.referenceShape, "D0005H0002")

        # self.config = rcf.read_config_file(config_path)
        # fill up configuration file
        config_path_test = os.path.join(wD, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_old
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = True
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = True
        cfg_test.cfg_as_dict["arg_train"][
            "annual_classes_extraction_source"
        ] = classifications_path
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = True
        cfg_test.save(config_path_test)
        config_test = rcf.read_config_file(config_path_test)
        """
        TEST
        with a working directory and with temporary files on disk
        """
        # generate IOTA output directory
        iota2_directory.generate_directories(
            testPath, False, config_test.getParam("chain", "list_tile").split(" ")
        )
        tile = "D0005H0002"
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        # shapes genereation
        common_mask(tile, testPath, sensors_params)
        env.generate_shape_tile(["D0005H0002"], None, testPath, 2154, 0)
        shapeRegion = os.path.join(wD, "MyFakeRegion.shp")
        area.generate_region_shape(
            testPath + "/envelope", shapeRegion, "region", testPath, None
        )
        RT.create_regions_by_tiles(
            shapeRegion,
            "region",
            testPath + "/envelope",
            testPath + "/shapeRegion/",
            None,
        )

        # launch sampling
        add_field(vector, "region", "1", str)

        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        # annual_config_file = config_test.getParam('arg_train', "prev_features")
        output_path_annual = None
        # output_path_annual = rcf.read_config_file(
        #    annual_config_file).getParam("chain", "output_path")

        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False
        vector_sampler.generate_samples(
            train_shape_dic={"usually": vector},
            path_wd=wD,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )
        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]

        same = []
        for key, _ in list(self.expectedFeatures.items()):
            if (
                len(vf.get_field_element(test_vector, "SQLite", "code", "all"))
                != self.expectedFeatures[key]
            ):
                same.append(True)
            else:
                same.append(False)

        if False in same:
            assert False
        else:
            assert True
        del config_test
        """
        TEST
        with a working directory and without temporary files on disk
        """
        testPath, _, wD = prepareTestsFolder(str(i2_tmpdir), True)

        config_path_test = os.path.join(wD, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_old
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = True
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = True
        cfg_test.cfg_as_dict["arg_train"][
            "annual_classes_extraction_source"
        ] = classifications_path
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test.save(config_path_test)
        config_test = rcf.read_config_file(config_path_test)

        # generate IOTA output directory
        iota2_directory.generate_directories(
            testPath, False, config_test.getParam("chain", "list_tile").split(" ")
        )

        # shapes genereation
        vector = TUV.shape_reference_vector(self.referenceShape, "D0005H0002")
        tile = "D0005H0002"
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        # shapes genereation
        common_mask(tile, testPath, sensors_params)
        env.generate_shape_tile(["D0005H0002"], None, testPath, 2154, 0)
        shapeRegion = os.path.join(wD, "MyFakeRegion.shp")
        area.generate_region_shape(
            testPath + "/envelope", shapeRegion, "region", testPath, None
        )
        RT.create_regions_by_tiles(
            shapeRegion,
            "region",
            testPath + "/envelope",
            testPath + "/shapeRegion/",
            None,
        )

        add_field(vector, "region", "1", str)

        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        output_path_annual = None
        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False

        vector_sampler.generate_samples(
            train_shape_dic={"usually": vector},
            path_wd=wD,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        same = []
        for key, _ in list(self.expectedFeatures.items()):
            if (
                len(vf.get_field_element(test_vector, "SQLite", "code", "all"))
                != self.expectedFeatures[key]
            ):
                same.append(True)
            else:
                same.append(False)

        if False in same:
            assert False
        else:
            assert True
        del config_test
        # """
        # TEST
        # without a working directory and without temporary files on disk
        # """
        testPath, _, wD = prepareTestsFolder(str(i2_tmpdir), True)

        config_path_test = os.path.join(wD, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_old
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = True
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = True
        cfg_test.cfg_as_dict["arg_train"][
            "annual_classes_extraction_source"
        ] = classifications_path
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test.save(config_path_test)

        config_test = rcf.read_config_file(config_path_test)

        # generate IOTA output directory

        iota2_directory.generate_directories(
            testPath, False, config_test.getParam("chain", "list_tile").split(" ")
        )

        # shapes genereation
        vector = TUV.shape_reference_vector(self.referenceShape, "D0005H0002")
        tile = "D0005H0002"
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        common_mask(tile, testPath, sensors_params)
        env.generate_shape_tile(["D0005H0002"], None, testPath, 2154, 0)
        shapeRegion = os.path.join(wD, "MyFakeRegion.shp")
        area.generate_region_shape(
            testPath + "/envelope", shapeRegion, "region", testPath, None
        )
        RT.create_regions_by_tiles(
            shapeRegion,
            "region",
            testPath + "/envelope",
            testPath + "/shapeRegion/",
            None,
        )

        add_field(vector, "region", "1", str)

        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        output_path_annual = None
        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False

        vector_sampler.generate_samples(
            train_shape_dic={"usually": vector},
            path_wd=None,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        same = []
        for key, _ in list(self.expectedFeatures.items()):
            if (
                len(vf.get_field_element(test_vector, "SQLite", "code", "all"))
                != self.expectedFeatures[key]
            ):
                same.append(True)
            else:
                same.append(False)

        if False in same:
            assert False
        else:
            assert True
        del config_test
        """
        TEST
        without a working directory and with temporary files on disk
        """
        testPath, _, wD = prepareTestsFolder(str(i2_tmpdir), True)
        config_path_test = os.path.join(wD, "Config_TEST.cfg")
        shutil.copy(config_path, config_path_test)
        cfg_test = rcf.read_internal_config_file(config_path_test)
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "references", "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = testPath
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "D0005H0002"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = L8_rasters_old
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = True
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = True
        cfg_test.cfg_as_dict["arg_train"][
            "annual_classes_extraction_source"
        ] = classifications_path
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = True
        cfg_test.save(config_path_test)
        config_test = rcf.read_config_file(config_path_test)
        # generate IOTA output directory
        iota2_directory.generate_directories(
            testPath, False, config_test.getParam("chain", "list_tile").split(" ")
        )

        # shapes genereation
        vector = TUV.shape_reference_vector(self.referenceShape, "D0005H0002")
        tile = "D0005H0002"
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        common_mask(tile, testPath, sensors_params)
        env.generate_shape_tile(["D0005H0002"], None, testPath, 2154, 0)
        shapeRegion = os.path.join(wD, "MyFakeRegion.shp")
        area.generate_region_shape(
            testPath + "/envelope", shapeRegion, "region", testPath, None
        )
        RT.create_regions_by_tiles(
            shapeRegion,
            "region",
            testPath + "/envelope",
            testPath + "/shapeRegion/",
            None,
        )

        add_field(vector, "region", "1", str)

        data_field = config_test.getParam("chain", "data_field")
        output_path = config_test.getParam("chain", "output_path")
        annual_crop = config_test.getParam("arg_train", "annual_crop")
        crop_mix = config_test.getParam("arg_train", "crop_mix")
        auto_context_enable = config_test.getParam("arg_train", "enable_autocontext")
        region_field = (config_test.getParam("chain", "region_field")).lower()
        proj = config_test.getParam("chain", "proj")
        runs = config_test.getParam("arg_train", "runs")
        samples_classif_mix = config_test.getParam("arg_train", "samples_classif_mix")

        output_path_annual = None

        ram = 128
        w_mode = False
        folder_annual_features = config_test.getParam(
            "arg_train", "output_prev_features"
        )
        previous_classif_path = config_test.getParam(
            "arg_train", "annual_classes_extraction_source"
        )
        validity_threshold = config_test.getParam("arg_train", "validity_threshold")
        sensors_params = rcf.iota2_parameters(config_test).get_sensors_parameters(tile)
        try:
            sar_optical_flag = config_test.getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
        except Exception as exc:
            print(exc)
            sar_optical_flag = False

        vector_sampler.generate_samples(
            train_shape_dic={"usually": vector},
            path_wd=None,
            data_field=data_field,
            output_path=output_path,
            annual_crop=annual_crop,
            crop_mix=crop_mix,
            auto_context_enable=auto_context_enable,
            region_field=region_field,
            proj=proj,
            runs=runs,
            sensors_parameters=sensors_params,
            sar_optical_post_fusion=sar_optical_flag,
            sampling_validation=False,
            samples_classif_mix=samples_classif_mix,
            output_path_annual=output_path_annual,
            ram=ram,
            w_mode=w_mode,
            folder_annual_features=folder_annual_features,
            previous_classif_path=previous_classif_path,
            validity_threshold=validity_threshold,
            multi_param_cust=self.multi_param_cust,
            sample_selection=self.selection_test,
        )

        test_vector = fut.file_search_reg_ex(testPath + "/learningSamples/*sqlite")[0]
        same = []
        for key, _ in list(self.expectedFeatures.items()):
            if (
                len(vf.get_field_element(test_vector, "SQLite", "code", "all"))
                != self.expectedFeatures[key]
            ):
                same.append(True)
            else:
                same.append(False)

        if False in same:
            assert False
        else:
            assert True
        del config_test
