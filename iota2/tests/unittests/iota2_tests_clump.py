#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsClump

import os
from pathlib import Path

import numpy as np
import pytest

from iota2.common.raster_utils import raster_to_array
from iota2.simplification import clump_classif as clump
from iota2.tests.utils.asserts_utils import AssertsFilesUtils

IOTA2DIR = os.environ.get('IOTA2DIR')

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")

# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


@pytest.mark.vecto
class Iota2TestClump(AssertsFilesUtils):
    """Test clump"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""

        cls.rasterreg = Path(
            IOTA2DIR
        ) / "data" / "references" / "posttreat" / "classif_regul.tif"
        cls.raster_clump = Path(
            IOTA2DIR
        ) / "data" / "references" / "posttreat" / "classif_clump.tif"

    # Tests definitions
    def test_iota2_clump(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test how many samples must be add to the sample set."""

        (i2_tmpdir / "out").mkdir()
        outfilename = i2_tmpdir / "out" / "classif_clump.tif"
        clump.clump_and_stack_classif(str(i2_tmpdir), str(self.rasterreg),
                                      str(outfilename), "1000", False)

        # assert
        outtest = raster_to_array(str(outfilename))
        outref = raster_to_array(str(self.raster_clump))
        assert np.array_equal(outtest, outref)
