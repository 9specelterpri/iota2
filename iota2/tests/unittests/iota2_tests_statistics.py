#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import filecmp
import os
import shutil
from pathlib import Path

import pytest

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.common.file_utils import ensure_dir
from iota2.common.utils import run
from iota2.sampling.samples_stat import samples_stats
from iota2.simplification import zonal_stats as zs
from iota2.tests.utils import tests_utils_rasters as TUR
from iota2.vector_tools.split_vector import split_vector_in_features

IOTA2DIR = Path(os.environ.get("IOTA2DIR"))

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")


@pytest.mark.vecto
class Iota2TestsZonalStats:
    """Test zonal stats functions."""

    @classmethod
    def setup_class(cls):
        """Variables avail across tests."""
        cls.classif = str(
            IOTA2DIR
            / "data"
            / "references"
            / "sampler"
            / "final"
            / "Classif_Seed_0.tif"
        )
        cls.validity = str(
            IOTA2DIR / "data" / "references/sampler/final/PixelsValidity.tif"
        )
        cls.confid = str(
            IOTA2DIR / "data" / "references/sampler/final/PixelsValidity.tif"
        )
        cls.vector = str(
            IOTA2DIR
            / "data"
            / "references"
            / "posttreat"
            / "vectors"
            / "classifsmooth.shp"
        )

        cls.vectorstatsref = str(
            IOTA2DIR
            / "data"
            / "references"
            / "posttreat"
            / "vectors"
            / "classifiota2.shp"
        )

        cls.statslist = {1: "rate", 2: "statsmaj", 3: "statsmaj"}
        cls.nomenclature = os.path.join(
            IOTA2DIR, "data", "references/posttreat/nomenclature_17.cfg"
        )

        cls.outzipref = str(
            IOTA2DIR / "data" / "references" / "posttreat" / "vectors" / "classif.zip"
        )

    def test_iota2_samples_statistics(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test generation of statistics by tiles."""
        ensure_dir(str(i2_tmpdir / "samplesSelection"))
        ensure_dir(str(i2_tmpdir / "shapeRegion"))

        raster_ref = str(i2_tmpdir / "RASTER_REF.tif")
        arr_test = TUR.fun_array("iota2_binary")
        TUR.array_to_raster(arr_test, raster_ref, origin_x=566377, origin_y=6284029)
        shutil.copy(raster_ref, str(i2_tmpdir / "shapeRegion" / "T31TCJ_region_1_.tif"))

        references_directory = str(Path(IOTA2DIR) / "data" / "references")
        region_shape = str(Path(references_directory) / "region_target.shp")
        shutil.copy(
            region_shape,
            str(i2_tmpdir / "samplesSelection" / "T31TCJ_region_1_seed_0.shp"),
        )
        shutil.copy(
            region_shape.replace(".shp", ".shx"),
            str(i2_tmpdir / "samplesSelection" / "T31TCJ_region_1_seed_0.shx"),
        )
        shutil.copy(
            region_shape.replace(".shp", ".dbf"),
            str(i2_tmpdir / "samplesSelection" / "T31TCJ_region_1_seed_0.dbf"),
        )
        shutil.copy(
            region_shape.replace(".shp", ".prj"),
            str(i2_tmpdir / "samplesSelection" / "T31TCJ_region_1_seed_0.prj"),
        )

        test_statistics = samples_stats(
            region_seed_tile=("1", "0", "T31TCJ"),
            iota2_directory=str(i2_tmpdir),
            data_field="region",
            working_directory=None,
            sampling_validation=False,
        )
        assert filecmp.cmp(
            str(
                Path(IOTA2DIR)
                / "data"
                / "references"
                / "T31TCJ_region_1_seed_0_stats.xml"
            ),
            test_statistics,
        )

    # TODO: wrong functions are used here @thierionv
    def test_iota2_statistics(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test vector statistics computing."""
        wd = i2_tmpdir / "wd"
        Path.mkdir(wd, exist_ok=True)
        out = i2_tmpdir / "out"
        Path.mkdir(out, exist_ok=True)
        vectorstats = str(out / "classifstats.shp")
        outzip = str(out / "classif.zip")

        wd = str(wd)
        out = str(out)
        # Statistics test
        params = split_vector_in_features(self.vector, wd, 1)
        zs.zonal_stats(
            wd,
            [self.classif, self.confid, self.validity],
            params,
            vectorstats,
            self.statslist,
            classes=self.nomenclature,
        )
        zs.compress_shape(vectorstats, outzip)

        # Final integration test
        run(f"unzip {self.outzipref} -d {wd}")
        for ext in [".shp", ".dbf", ".shx", ".prj", ".cpg"]:
            os.remove(os.path.splitext(vectorstats)[0] + ext)

        run(f"unzip {outzip} -d {str(out)}")

        assert TUV.compare_vector_file(
            self.vectorstatsref,
            os.path.join(wd, "classifiota2.shp"),
            "coordinates",
            "polygon",
            "ESRI Shapefile",
        ), "Generated shapefile vector does not fit with shapefile reference file"
