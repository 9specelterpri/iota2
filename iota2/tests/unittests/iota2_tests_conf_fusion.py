#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsConfFusion

import filecmp
import os
import shutil
from pathlib import Path

from iota2.validation import confusion_fusion as confFus

IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(os.environ.get('IOTA2DIR'))


class Iota2TestConfFusion:
    """Test confusion_fusion function."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data = str(IOTA2DIR / "data" / "references" / "ConfFusion")

    def test_confusion_fusion(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test confusion_fusion function."""
        # Prepare data
        final = i2_tmpdir / "final"
        final_tmp = final / "TMP"

        final_tmp.mkdir(parents=True, exist_ok=True)

        # copy input data
        src_files = os.listdir(
            str(Path(self.ref_data) / "Input" / "final" / "TMP"))
        for file_name in src_files:
            full_file_name = str(
                Path(self.ref_data) / "Input" / "final" / "TMP" / file_name)
            shutil.copy(full_file_name, final_tmp)

        # run test
        datafield = 'CODE'
        shape_data = str(IOTA2DIR / "data" / "references" /
                         "D5H2_groundTruth_samples.shp")
        confFus.confusion_fusion(shape_data, datafield, str(final / "TMP"),
                                 str(final / "TMP"), str(final / "TMP"), 1,
                                 False, [], None)

        file1 = os.path.join(final, "TMP", "Results_seed_0.txt")
        referencefile1 = str(
            Path(self.ref_data) / "Output" / "Results_seed_0.txt")
        assert filecmp.cmp(file1, referencefile1)
