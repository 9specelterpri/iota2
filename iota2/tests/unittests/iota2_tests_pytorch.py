#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import pickle
import random
import shutil
import sqlite3
from itertools import product
from pathlib import Path
from typing import Dict

import numpy as np
import pandas as pd
import pytest
import torch
import xarray as xr

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.classification.py_classifiers import pytorch_predict
from iota2.common.otb_app_bank import (
    CreatePolygonClassStatisticsApplication,
    CreateSampleSelectionApplication,
)
from iota2.common.raster_utils import raster_to_array
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.sections.cfg_utils import ConfigError
from iota2.Iota2 import run
from iota2.learning.pytorch.dataloaders import (
    database_to_dataloader,
    database_to_dataloader_batch_stream,
    split_train_valid_by_fid,
)
from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetwork
from iota2.learning.pytorch.torch_utils import batch_provider, early_stop
from iota2.learning.pytorch.train_pytorch_model import torch_learn
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.vector_tools.merge_files import merge_wide_sqlite
from iota2.vector_tools.vector_functions import get_layer_name

IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")


@pytest.mark.pytorch
class Iota2TestsPyTorch(AssertsFilesUtils):
    """Test pytorch usage in iota2."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.group_test_name = "Iota2TestsPyTorch"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data", cls.group_test_name)
        cls.config_ref = os.path.join(
            IOTA2DIR, "data", "references", "running_iota2", "i2_config.cfg"
        )
        cls.ground_truth_path = os.path.join(
            IOTA2DIR, "data", "references", "running_iota2", "ground_truth.shp"
        )
        cls.nomenclature_path = os.path.join(
            IOTA2DIR, "data", "references", "running_iota2", "nomenclature.txt"
        )
        cls.color_path = os.path.join(
            IOTA2DIR, "data", "references", "running_iota2", "color.txt"
        )

        cls.sql_db = os.path.join(
            IOTA2DIR,
            "data",
            "references",
            "sampler",
            "D0005H0002_polygons_To_Sample_Samples_ref.sqlite",
        )

        cls.model_file = os.path.join(
            IOTA2DIR, "data", "references", "pytorch", "model_1_seed_0.txt"
        )
        cls.model_parameters_file = os.path.join(
            IOTA2DIR,
            "data",
            "references",
            "pytorch",
            "model_1_seed_0_parameters.pickle",
        )
        cls.encoder_convert_file = os.path.join(
            IOTA2DIR, "data", "references", "pytorch", "model_1_seed_0_encoder.pickle"
        )
        cls.user_nn_ok = os.path.join(
            IOTA2DIR, "data", "references", "pytorch", "user_nn_ok.py"
        )
        cls.bad_user_nn_forward = os.path.join(
            IOTA2DIR, "data", "references", "pytorch", "bad_user_nn_forward.py"
        )
        cls.bad_user_nn_baseclass = os.path.join(
            IOTA2DIR, "data", "references", "pytorch", "bad_user_nn_baseclass.py"
        )
        cls.all_tests_ok = []
        cls.database = os.path.join(
            IOTA2DIR, "data", "references", "running_iota2", "ground_truth.shp"
        )

    def test_i2_nn_services(self):
        """Test Iota2NeuralNetwork.nans_imputation/Iota2NeuralNetworkstandardize class methods."""
        batch_size = 2
        nb_dates = 3
        nb_bands = 10

        data = np.random.rand(batch_size, nb_dates, nb_bands)
        nans_index = np.random.randint(
            0, 2, size=(batch_size, nb_dates, nb_bands), dtype=bool
        )
        data[nans_index] = np.nan
        data = torch.tensor(data).float()
        imputation_vect = torch.ones(nb_dates * nb_bands)

        new_data = Iota2NeuralNetwork.nans_imputation(data, imputation_vect, -1)

        assert new_data.shape == data.shape, "shape after imputation are not equals"
        new_data = np.array(new_data)
        unique = np.unique(new_data[nans_index])
        assert len(unique) == 1, "nans imputation failure"
        assert unique[0] == 1, "nans imputation failure"

        means = torch.zeros(nb_dates * nb_bands)
        stds = torch.ones(nb_dates * nb_bands)
        standardized_data = Iota2NeuralNetwork.standardize(
            torch.tensor(new_data), means, stds, default_mean=0.0, default_std=1.0
        )
        assert np.array_equal(
            np.array(standardized_data), new_data
        ), "standization failure"

    def test_stats(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test statistics computation."""

        def cmp_stats(stats_full: Dict, stats_stream: Dict) -> bool:
            """cmp iota2's deep stats."""
            if not list(stats_stream.keys()) == list(stats_full.keys()):
                return False

            s2_stats_full = stats_full["s2_theia"]
            s2_stats_stream = stats_stream["s2_theia"]
            if not list(s2_stats_full.keys()) == list(s2_stats_stream.keys()):
                return False

            stats_eq = []
            for stats_name, stats_values in s2_stats_full.items():
                stats_eq.append(torch.equal(s2_stats_stream[stats_name], stats_values))
            return all(stats_eq)

        nb_bands = 10
        label_field = "code"
        bands = [f"sentinel2_b{b_num+1}" for b_num in range(nb_bands)]
        dates = ["20210224", "20210324"]
        columns = [f"{band}_{date}" for date, band in list(product(dates, bands))]
        data = {}
        # values may contains np.nan values
        lab_values = [4, 3, 2, 1, 0]
        data[label_field] = lab_values
        data_values = [3, 2, 1, 0, np.nan]
        data["originfid"] = [1, 2, 3, 4, 5]
        for col in columns:
            data[col] = data_values
        df_features = pd.DataFrame.from_dict(data)
        features_database_file = str(i2_tmpdir / "fake_db.sqlite")
        out_conn = sqlite3.connect(features_database_file)
        df_features.to_sql("output", out_conn, if_exists="replace", index=False)
        vector_list = [features_database_file]
        output_netcdf = features_database_file.replace(".sqlite", ".nc")
        merge_wide_sqlite(
            vector_list,
            output_netcdf,
            fid_col="fid",
            cols_order=None,
            specific_nans_rules=None,
        )
        # get stats
        # from the 'full' dataloader
        (_, _, _, _, stats_full) = database_to_dataloader(
            output_netcdf, "code", columns, additional_statistics_percentage=1.0
        )
        # from the 'stream' dataloader
        (_, _, _, _, stats, _, _) = database_to_dataloader_batch_stream(
            output_netcdf, "code", columns, additional_statistics_percentage=1.0
        )
        assert cmp_stats(stats_full, stats)
        sensor_stats = stats["s2_theia"]
        # check stat's names
        assert sorted(sensor_stats.keys()) == [
            "max",
            "mean",
            "min",
            "quantile_0.1",
            "quantile_0.5",
            "quantile_0.95",
            "var",
        ]
        # check stats lenght
        for stats_name, stats_values in sensor_stats.items():
            assert len(stats_values) == len(columns), (
                f"stats '{stats_name}' has the wrong number of features"
                f" {len(stats_values)} instead of {len(columns)}"
            )
        # check mean
        expected_mean = np.nanmean(data_values)
        mean_values = list([val == expected_mean for val in list(sensor_stats["mean"])])
        assert all(mean_values)
        # check max
        expected_max = np.nanmax(data_values)
        max_values = list([val == expected_max for val in list(sensor_stats["max"])])
        assert all(max_values)
        # check min
        expected_min = np.nanmin(data_values)
        min_values = list([val == expected_min for val in list(sensor_stats["min"])])
        assert all(min_values)
        # check var
        expected_var = np.nanvar(data_values)
        var_values = list([val == expected_var for val in list(sensor_stats["var"])])
        assert all(var_values)
        # check quantile
        expected_q = np.nanquantile(data_values, 0.95)
        q_values = list(
            [val == expected_q for val in list(sensor_stats["quantile_0.95"])]
        )
        assert all(q_values)

    def test_early_stop(self):
        """Test the early_stop class."""
        epoch_to_trigger = 5
        patience = 3
        tol = 0.01
        metric = "val_loss"
        # no stops, val_loss
        early_stop_obj = early_stop(epoch_to_trigger, patience, tol, metric)
        scores = [-0.1, -0.2, -0.3, -0.4, -0.5, -0.6, -0.7, -0.8, -0.9, -1]
        stops = [early_stop_obj.step(score) for score in scores]
        assert True not in stops

        # stops, val_loss
        early_stop_obj = early_stop(epoch_to_trigger, patience, tol, metric)
        scores = [-0.1, -0.2, -0.3, -0.4, -0.5, -0.4, -0.3, -0.2, -0.1, -0.05]
        stops = [early_stop_obj.step(score) for score in scores]
        assert stops == [
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            True,
            True,
            True,
        ]

        # not stops, kappa
        metric = "kappa"
        early_stop_obj = early_stop(epoch_to_trigger, patience, tol, metric)
        scores = [0.2, 0.3, 0.4, 0.5, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
        stops = [early_stop_obj.step(score) for score in scores]
        assert True not in stops

        # stops, kappa
        early_stop_obj = early_stop(epoch_to_trigger, patience, tol, metric)
        scores = [0.2, 0.3, 0.4, 0.5, 0.4, 0.409, 0.399, 0.401, 0.3, 0.5]
        stops = [early_stop_obj.step(score) for score in scores]
        assert stops == [
            False,
            False,
            False,
            False,
            False,
            False,
            True,
            True,
            True,
            False,
        ]

        early_stop_obj = early_stop(epoch_to_trigger, patience, tol, metric)
        scores = [0.2, 0.3, 0.4, 0.5, 0.5, 0.501, 0.502, 0.503, 0.504, 0.506]
        stops = [early_stop_obj.step(score) for score in scores]
        assert stops == [
            False,
            False,
            False,
            False,
            False,
            False,
            True,
            True,
            True,
            True,
        ]

    def test_batch_provider(self):
        """Test batch provider class."""
        random.seed(1)
        fids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        batch_size = 5
        nb_epoch = 2
        batch_p = batch_provider(fids, batch_size, nb_epoch)
        ep_1 = batch_p.get_epoch(0)
        ep_2 = batch_p.get_epoch(1)
        assert ep_1 == [[7, 9, 10, 8, 6], [4, 1, 5, 2, 3]]
        assert ep_2 == [[6, 2, 10, 1, 4], [3, 7, 5, 9, 8]]

    def test_sqlite_to_netcdf(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test the merge of multiple sqlite into one netcdf database."""
        db_1 = str(i2_tmpdir / "db1.sqlite")
        db_2 = str(i2_tmpdir / "db2.sqlite")
        shutil.copy(self.sql_db, db_1)
        shutil.copy(self.sql_db, db_2)

        conn = sqlite3.connect(db_1)
        layer_name = get_layer_name(db_1, "SQLite")
        df_in = pd.read_sql_query(f"select * from {layer_name}", conn)
        inputs_cols = list(df_in.columns)
        input_len = len(df_in)

        vector_list = [db_1, db_2]
        netcdf_db = str(i2_tmpdir / "merged.nc")
        merge_wide_sqlite(
            vector_list,
            netcdf_db,
            fid_col="fid",
            cols_order=None,
            nan_values=0.0,
            specific_nans_rules=None,
        )

        netcdf_data = xr.open_dataset(netcdf_db)
        netcdf_data = netcdf_data.compute()
        net_array = netcdf_data.to_array()
        df = net_array.to_dataframe("my_data_frame")
        df = df.unstack()
        df.columns = [str(col_name) for _, col_name in list(df.columns)]

        # asserts
        assert len(df) == 2 * input_len
        fids_vals = [val for _, val in list(df.index)]
        assert fids_vals == [val for val in range(2 * input_len)]

        excluded_cols = ["ogc_fid", "GEOMETRY"]
        in_cols = sorted([val for val in inputs_cols if val not in excluded_cols])
        out_cols = sorted(list(df.columns))
        assert in_cols == out_cols

    def test_split_by_fid(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test the split by fid function."""
        # prepare data
        raster_ref = str(i2_tmpdir / "raster_ref.tif")
        TUR.generate_fake_raster(raster_ref, res=10)
        stats_file = str(i2_tmpdir / "stats.xml")
        stats_app = CreatePolygonClassStatisticsApplication(
            {"in": raster_ref, "vec": self.database, "field": "code", "out": stats_file}
        )
        stats_app.ExecuteAndWriteOutput()
        samples_files = str(i2_tmpdir / "database.sqlite")
        sampling_app = CreateSampleSelectionApplication(
            {
                "in": raster_ref,
                "vec": self.database,
                "out": samples_files,
                "instats": stats_file,
                "strategy": "all",
                "field": "code",
            }
        )
        sampling_app.ExecuteAndWriteOutput()
        vector_list = [samples_files]
        output_netcdf = samples_files.replace(".sqlite", ".nc")
        merge_wide_sqlite(
            vector_list,
            output_netcdf,
            fid_col="fid",
            cols_order=None,
            nan_values=0.0,
            specific_nans_rules=None,
        )
        # function
        fids_train, fids_valid = split_train_valid_by_fid(
            output_netcdf, "code", "originfid"
        )

        # asserts
        expected_fids_train = [5, 18, 4, 7, 8, 6, 11, 12, 17, 15, 13, 16]
        expected_fids_valid = [3, 9, 10, 14]

        assert expected_fids_train == fids_train
        assert expected_fids_valid == fids_valid

    def test_dataloaders(self, i2_tmpdir, rm_tmpdir_on_success):
        """Check iota2 dataloaders consistency."""
        # prepare data
        raster_ref = str(i2_tmpdir / "raster_ref.tif")
        TUR.generate_fake_raster(raster_ref, res=10)
        stats_file = str(i2_tmpdir / "stats.xml")
        stats_app = CreatePolygonClassStatisticsApplication(
            {"in": raster_ref, "vec": self.database, "field": "code", "out": stats_file}
        )
        stats_app.ExecuteAndWriteOutput()
        samples_files = str(i2_tmpdir / "database.sqlite")
        sampling_app = CreateSampleSelectionApplication(
            {
                "in": raster_ref,
                "vec": self.database,
                "out": samples_files,
                "instats": stats_file,
                "strategy": "all",
                "field": "code",
            }
        )
        sampling_app.ExecuteAndWriteOutput()

        layer_name = get_layer_name(samples_files, "SQLite")
        conn = sqlite3.connect(samples_files)
        df_features = pd.read_sql_query(
            f"select originfid, code from {layer_name}", conn
        )
        df_features["sentinel2_b1_20210224"] = df_features["code"]
        df_features["superfid"] = df_features["originfid"]
        features_database_file = samples_files.replace(".sqlite", "_features.sqlite")

        out_conn = sqlite3.connect(features_database_file)
        df_features.to_sql("output", out_conn, if_exists="replace", index=False)
        vector_list = [features_database_file]
        output_netcdf = features_database_file.replace(".sqlite", ".nc")
        merge_wide_sqlite(
            vector_list,
            output_netcdf,
            fid_col="fid",
            cols_order=None,
            nan_values=0.0,
            specific_nans_rules=None,
        )
        # function
        (train_loader, valid_loader, convert_dict, _, _) = database_to_dataloader(
            output_netcdf, "code", ["sentinel2_b1_20210224"]
        )

        (
            train_loader_b,
            valid_loader_b,
            convert_dict_b,
            _,
            _,
            _,
            _,
        ) = database_to_dataloader_batch_stream(
            output_netcdf, "code", ["sentinel2_b1_20210224"], fids_column="fid"
        )

        train_loader_b.dataset.load_batch_series(0)
        valid_loader_b.dataset.load_batch_series(0)

        # asserts
        ref_convert_dict = {0: 1, 1: 2, 2: 3, 3: 4}
        assert ref_convert_dict == convert_dict == convert_dict_b

        dataloader_it = iter(train_loader)
        dataloader_it_b = iter(train_loader_b)

        data_batch = next(dataloader_it)
        data_batch_b = next(dataloader_it_b)

        # learning batch's lenght
        assert len(data_batch) == len(data_batch_b) == 3  # data, targets, weights

        # learning content
        inputs_batch_b, targets_batch_b, weights_b = data_batch_b
        inputs_batch, targets_batch, weights = data_batch
        assert (
            len(inputs_batch)
            == len(inputs_batch_b[0])
            == len(targets_batch)
            == len(targets_batch_b[0])
            == len(weights)
            == len(weights_b[0])
            == 250
        )

        full_mem_features = np.sort(
            np.concatenate(
                [np.ravel(inputs_batch), np.ravel(targets_batch), np.ravel(weights)]
            ),
            axis=None,
        )
        batch_mem_features = np.sort(
            np.concatenate(
                [
                    np.ravel(inputs_batch_b[0]),
                    np.ravel(targets_batch_b[0]),
                    np.ravel(weights_b[0]),
                ]
            ),
            axis=None,
        )
        assert np.allclose(
            full_mem_features, batch_mem_features
        ), "learning loaders returns different batch content"

        # validation batch's lenght
        dataloader_it = iter(valid_loader)
        dataloader_it_b = iter(valid_loader_b)
        data_batch = next(dataloader_it)
        data_batch_b = next(dataloader_it_b)
        # validation content
        assert len(data_batch) == len(data_batch_b) == 3
        inputs_batch, targets_batch, weights = data_batch
        inputs_batch_b, targets_batch_b, weights_b = data_batch_b
        assert (
            len(inputs_batch)
            == len(inputs_batch_b[0])
            == len(targets_batch)
            == len(targets_batch_b[0])
            == len(weights)
            == len(weights_b[0])
            == 94
        )
        full_mem_features = np.sort(
            np.concatenate(
                [np.ravel(inputs_batch), np.ravel(targets_batch), np.ravel(weights)]
            ),
            axis=None,
        )
        batch_mem_features = np.sort(
            np.concatenate(
                [
                    np.ravel(inputs_batch_b[0]),
                    np.ravel(targets_batch_b[0]),
                    np.ravel(weights_b[0]),
                ]
            ),
            axis=None,
        )
        assert np.allclose(
            full_mem_features, batch_mem_features
        ), "validation loaders returns different batch content"

    def test_torch_learn(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test learning workflow."""
        # prepare data
        raster_ref = str(i2_tmpdir / "raster_ref.tif")
        TUR.generate_fake_raster(raster_ref, res=10)
        stats_file = str(i2_tmpdir / "stats.xml")
        stats_app = CreatePolygonClassStatisticsApplication(
            {"in": raster_ref, "vec": self.database, "field": "code", "out": stats_file}
        )
        stats_app.ExecuteAndWriteOutput()
        samples_files = str(i2_tmpdir / "database.sqlite")
        sampling_app = CreateSampleSelectionApplication(
            {
                "in": raster_ref,
                "vec": self.database,
                "out": samples_files,
                "instats": stats_file,
                "strategy": "all",
                "field": "code",
            }
        )
        sampling_app.ExecuteAndWriteOutput()

        layer_name = get_layer_name(samples_files, "SQLite")
        conn = sqlite3.connect(samples_files)
        df_features = pd.read_sql_query(
            f"select ogc_fid, originfid, code from {layer_name}", conn
        )
        df_features["sentinel2_b1_20210224"] = df_features["code"]
        features_database_file = samples_files.replace(".sqlite", "_features.sqlite")

        out_conn = sqlite3.connect(features_database_file)
        df_features.to_sql("output", out_conn, if_exists="replace", index=False)
        vector_list = [features_database_file]
        output_netcdf = features_database_file.replace(".sqlite", ".nc")
        merge_wide_sqlite(
            vector_list,
            output_netcdf,
            fid_col="fid",
            cols_order=None,
            nan_values=0.0,
            specific_nans_rules=None,
        )
        # function
        model_path = str(i2_tmpdir / "model.txt")
        model_parameters_file = str(i2_tmpdir / "model_parameters.pickle")
        encoder_convert_file = str(i2_tmpdir / "model_encoder.pickle")
        torch_learn(
            output_netcdf,
            ["sentinel2_b1_20210224"],
            model_path,
            model_parameters_file,
            encoder_convert_file,
            "code",
            "ANN",
            {},
            "",
            learning_rate=0.1,
            masks_labels=[],
            epochs=5,
        )
        # asserts
        self.assert_file_exist(Path(model_path))
        self.assert_file_exist(Path(model_parameters_file))
        self.assert_file_exist(Path(encoder_convert_file))

    def test_user_nn(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test user neural network usage."""
        # wrong class name
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_pytorch.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = ""
        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["arg_train"]["deep_learning_parameters"] = {
            "dl_name": "random_name",
            "dl_module": self.user_nn_ok,
        }
        cfg_test.save(config_test)
        pytest.raises(AttributeError, rcf.read_config_file, config_test)

        # wrong forward definition
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_pytorch.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = ""
        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["arg_train"]["deep_learning_parameters"] = {
            "dl_name": "ANNFailForward",
            "dl_module": self.bad_user_nn_forward,
        }
        cfg_test.save(config_test)
        pytest.raises(ValueError, rcf.read_config_file, config_test)

        # wrong inheritance base class
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_pytorch.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = ""
        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["arg_train"]["deep_learning_parameters"] = {
            "dl_name": "ANNFailInheritance",
            "dl_module": self.bad_user_nn_baseclass,
        }
        cfg_test.save(config_test)

        pytest.raises(
            ConfigError,
            run,
            config_test,
            None,
            "debug",
            1,
            1,
            False,
            "",
            None,
            False,
            1,
            None,
        )

    def test_torch_prediction(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test prediction workflow : classif, confidence, proba-map."""
        # prepare inputs
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir, tile_name, ["20200101"], res=10.0)
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_pytorch.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 1
        cfg_test.cfg_as_dict["arg_train"]["deep_learning_parameters"] = {
            "dl_name": "ANN"
        }
        cfg_test.cfg_as_dict["arg_train"]["features_from_raw_dates"] = True
        cfg_test.save(config_test)

        out_classif = str(i2_tmpdir / "classif_test.tif")
        out_confidence = str(i2_tmpdir / "confidence_test.tif")
        out_proba = str(i2_tmpdir / "proba_test.tif")
        sensors_params = rcf.iota2_parameters(
            rcf.read_config_file(config_test)
        ).get_sensors_parameters(tile_name)

        hyperparam_pickle_file = str(i2_tmpdir / "hyperparameters_nn.pickle")
        a = {"batch_size": 1000}
        with open(hyperparam_pickle_file, "wb") as batch:
            pickle.dump(a, batch, protocol=pickle.HIGHEST_PROTOCOL)
        expected_out_class = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        # launch function
        pytorch_predict(
            nn_name="ANN",
            mask=None,
            model=self.model_file,
            model_parameters_file=self.model_parameters_file,
            encoder_convert_file=self.encoder_convert_file,
            out_classif=out_classif,
            out_confidence=out_confidence,
            out_proba=out_proba,
            working_dir=None,
            tile_name="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_params,
            all_class_labels=expected_out_class,
            number_of_chunks=1,
            targeted_chunk=0,
            hyperparam_file=hyperparam_pickle_file,
            sar_optical_post_fusion=False,
        )

        # asserts
        expected_classif_file = out_classif.replace(".tif", "_SUBREGION_0.tif")
        expected_confidence_file = out_confidence.replace(".tif", "_SUBREGION_0.tif")
        expected_proba_file = out_proba.replace(".tif", "_SUBREGION_0.tif")
        self.assert_file_exist(Path(expected_classif_file))
        self.assert_file_exist(Path(expected_confidence_file))
        self.assert_file_exist(Path(expected_proba_file))

        classif_arr = raster_to_array(expected_classif_file)
        confidence_arr = raster_to_array(expected_confidence_file)
        proba_arr = raster_to_array(expected_proba_file)
        assert classif_arr.shape == (16, 86)
        assert confidence_arr.shape == (16, 86)
        assert proba_arr.shape == (len(expected_out_class), 16, 86)

        # check if the sum along first probability map axis == 1000
        # indeed probabilities must be in [0;1000]
        ref_sum = np.ones((16, 86), dtype=float) * 1000
        assert np.allclose(ref_sum, np.sum(proba_arr, axis=0))

        # check if the last band of probility map == 0
        # -> input class are [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        #    output asked class are [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        ref_zeros = np.zeros((16, 86), dtype=float)
        assert np.allclose(ref_zeros, proba_arr[-1, :, :])
