#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import shutil

from iota2.learning import get_model

IOTA2DIR = os.environ.get("IOTA2DIR")
if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")


class Iota2TestsGetModel:
    @classmethod
    def setup_class(cls):
        """Variables avail across tests."""
        cls.reference_directory = os.path.join(
            IOTA2DIR, "data", "references", "getModel", "Input"
        )
        cls.tile_for_region_number = [
            [0, ["tile0"]],
            [1, ["tile0", "tile4"]],
            [2, ["tile0", "tile1", "tile2", "tile3", "tile4"]],
            [3, ["tile0", "tile1", "tile6"]],
        ]

    def test_get_model(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        Test the 'get_region_model' method.

        It's purpose is to guess from files which tiles is involved in which models
        """
        shutil.copytree(self.reference_directory, str(i2_tmpdir), dirs_exist_ok=True)
        # We execute the function get_region_model()
        outputstr = get_model.get_region_model(str(i2_tmpdir))
        # the function produces a list of regions and its tiles
        # We check if we have all the expected element in the list
        for element in outputstr:
            # We get the region number and all the tiles for the region number
            region = int(element[0])
            tiles = element[1]

            # We check if we have this region number in the list
            # tileForRegionNumber
            irn = -1
            for i, val in enumerate(self.tile_for_region_number):
                if region == val[0]:
                    irn = i
            assert irn == region

            # for each tile for this region, we check if we have
            # this value in the list of the expected values
            for tile in tiles:
                itfrn = self.tile_for_region_number[irn][1].index(tile)
                assert tile == self.tile_for_region_number[irn][1][itfrn]
