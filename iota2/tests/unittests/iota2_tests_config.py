#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Check class iota2_parameters"""
import os

import pytest

import iota2.configuration_files.read_config_file as rcf

IOTA2DIR = os.environ.get('IOTA2DIR')
RM_IF_ALL_OK = True
if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")


@pytest.mark.config
class Iota2TestConfig:
    """Check rcf.iota2_parameters."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.group_test_name = "iota2_test_config"
        cls.config_ref = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "i2_config.cfg")
        cls.config_test = os.path.join(
            IOTA2DIR, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg")

    # Tests definitionss
    def test_config_dictionaries(self, i2_tmpdir, rm_tmpdir_on_success):
        """Check iota2_parameters class."""
        i2_cfg = rcf.read_internal_config_file(self.config_test)
        i2_cfg.cfg_as_dict["simplification"]["grasslib"] = self.config_test
        i2_cfg.cfg_as_dict["simplification"]["clipfile"] = self.config_test
        i2_cfg.cfg_as_dict["simplification"]["bingdal"] = self.config_test
        i2_cfg.cfg_as_dict["chain"]["nomenclature_path"] = self.config_test
        i2_cfg.cfg_as_dict["chain"]["color_table"] = self.config_test
        i2_cfg.cfg_as_dict["chain"]["region_path"] = self.config_test
        i2_cfg.cfg_as_dict["chain"]["ground_truth"] = self.config_test
        i2_cfg.cfg_as_dict["chain"]["user_feat_path"] = os.path.join(
            IOTA2DIR, "data")
        i2_cfg.cfg_as_dict["chain"]["l8_path"] = os.path.join(IOTA2DIR, "data")
        i2_cfg.cfg_as_dict["chain"]["l8_path_old"] = os.path.join(
            IOTA2DIR, "data")
        i2_cfg.cfg_as_dict["arg_train"]["sample_management"] = self.config_test
        i2_cfg.cfg_as_dict["arg_train"]["prev_features"] = self.config_test
        i2_cfg.cfg_as_dict["arg_train"][
            "annual_classes_extraction_source"] = self.config_test
        i2_cfg.cfg_as_dict["arg_train"][
            "output_prev_features"] = self.config_test

        test_cfg = str(i2_tmpdir / "config_test.cfg")
        i2_cfg.save(test_cfg)
        sensors_parameters = rcf.iota2_parameters(
            rcf.read_config_file(test_cfg)).get_sensors_parameters("T31TCJ")
        assert len(sensors_parameters.keys()) == 3
