#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Test of undecision_management for post classification fusion."""

import shutil
from pathlib import Path

from iota2.classification import fusion as FUS
from iota2.classification import undecision_management as UM
from iota2.common import file_utils as fut
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestUndecisionManagement(AssertsFilesUtils):
    "Test undecision management in post classification fusion."

    def test_undecision_management(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test undecision_management."""

        shutil.copytree(
            str(
                IOTA2DIR
                / "data"
                / "references"
                / "NoData"
                / "Input"
                / "Classif"
                / "classif"
            ),
            i2_tmpdir / "classif",
        )
        classi_dir = i2_tmpdir / "classif"

        shutil.copytree(
            str(IOTA2DIR / "data" / "references" / "NoData" / "Input" / "config_model"),
            str(i2_tmpdir / "config_model"),
        )

        Path.mkdir(i2_tmpdir / "cmd" / "fusion", parents=True, exist_ok=True)

        shutil.copy(
            str(classi_dir / "Classif_D0005H0002_model_1_seed_0.tif"),
            str(classi_dir / "Classif_D0005H0002_model_1f2_seed_0.tif"),
        )
        shutil.copy(
            str(classi_dir / "Classif_D0005H0002_model_1_seed_0.tif"),
            str(classi_dir / "Classif_D0005H0002_model_1f1_seed_0.tif"),
        )

        shutil.copy(
            str(classi_dir / "D0005H0002_model_1_confidence_seed_0.tif"),
            str(classi_dir / "D0005H0002_model_1f2_confidence_seed_0.tif"),
        )
        shutil.copy(
            str(classi_dir / "D0005H0002_model_1_confidence_seed_0.tif"),
            str(classi_dir / "D0005H0002_model_1f1_confidence_seed_0.tif"),
        )

        in_classif = [
            str(classi_dir / "Classif_D0005H0002_model_1f1_seed_0.tif"),
            str(classi_dir / "Classif_D0005H0002_model_1f2_seed_0.tif"),
        ]
        out_classif = str(classi_dir / "D0005H0002_FUSION_model_1_seed_0.tif")
        out_pix_type = "uint8"
        fusion_options = "-nodatalabel 0 -method majorityvoting"
        FUS.fusion(in_classif, fusion_options, out_classif, out_pix_type)

        fusion_files = fut.file_search_and(str(classi_dir), True, "_FUSION_")
        pixtype = "uint8"

        for fusionpath in fusion_files:
            UM.undecision_management(
                str(i2_tmpdir),
                fusionpath,
                "region",
                str(IOTA2DIR / "data" / "references" / "features"),
                str(
                    IOTA2DIR
                    / "data"
                    / "references"
                    / "GenerateRegionShape"
                    / "region_need_To_env.shp"
                ),
                "maxConfidence",
                None,
                ["NDVI", "NDWI", "Brightness"],
                pixtype,
                str(IOTA2DIR / "data" / "regionShape" / "4Tiles.shp"),
                "ALT,ASP,SLP",
                False,
            )

        self.assert_file_equal(
            str(classi_dir / "D0005H0002_FUSION_model_1_seed_0.tif"),
            str(
                IOTA2DIR
                / "data"
                / "references"
                / "NoData"
                / "Output"
                / "D0005H0002_FUSION_model_1_seed_0.tif"
            ),
        )
