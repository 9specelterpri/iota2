# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Test Split Samples"""
import os
import shutil
import sqlite3
from pathlib import Path

import pandas as pd
from osgeo import ogr

from iota2.common import file_utils as fu
from iota2.sampling import split_samples

IOTA2DIR = os.environ.get('IOTA2DIR')

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR)


class Iota2TestSplitSamples:
    """Test split samples module functions."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.data_field = "CODE"
        cls.region_field = "region"
        cls.region_threshold = 0.0098
        cls.proj = 2154
        cls.runs = 2
        cls.ratio = 0.5
        cls.regions_split = 2
        cls.regions = "1"
        cls.areas = 12399.173485632864

    def test_split_samples(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test split samples workflow."""
        shutil.copytree(
            IOTA2DIR / "data" / 'references' / 'splitSamples' / 'Input',
            i2_tmpdir / "Inputs")
        regions_pos = -2

        formatting_vectors_dir = str(i2_tmpdir / "Inputs" /
                                     "formattingVectors")
        shape_region_dir = str(i2_tmpdir / "Inputs" / "shapeRegion")

        vectors = fu.file_search_and(formatting_vectors_dir, True, ".shp")

        shapes_region = fu.file_search_and(shape_region_dir, True, ".shp")

        regions = list(
            set([
                os.path.split(shape)[-1].split("_")[regions_pos]
                for shape in shapes_region
            ]))
        # We check we have the correct value
        assert self.regions == regions[0]

        areas, regions_tiles, data_to_rm = split_samples.get_regions_area(
            vectors, regions, formatting_vectors_dir, None, self.region_field)
        # We check we have the correct values
        # self.assertAlmostEqual(self.areas, areas['1'], 9e-3)
        assert abs(self.areas - areas['1']) < 9e-3
        formatting_vect = str(i2_tmpdir / "Inputs" / "formattingVectors" /
                              "T31TCJ.sqlite")
        assert formatting_vect == os.path.abspath(regions_tiles['1'][0])
        assert formatting_vect == os.path.abspath(data_to_rm[0])

        regions_split = split_samples.get_splits_regions(
            areas, self.region_threshold)
        # We check we have the correct value
        assert self.regions_split == regions_split['1']

        updated_vectors = split_samples.split(regions_split, regions_tiles,
                                              self.data_field,
                                              self.region_field)

        # We check we have the correct file
        assert formatting_vect == os.path.abspath(updated_vectors[0])

        new_regions_shapes = split_samples.transform_to_shape(
            updated_vectors, formatting_vectors_dir)
        # We check we have the correct file
        assert str(i2_tmpdir / "Inputs" / "formattingVectors" /
                   "T31TCJ.shp") == os.path.abspath(new_regions_shapes[0])

        for data in data_to_rm:
            os.remove(data)

        data_app_val_dir = str(i2_tmpdir / "Inputs" / "dataAppVal")

        split_samples.update_learning_validation_sets(new_regions_shapes,
                                                      data_app_val_dir,
                                                      self.data_field,
                                                      self.region_field,
                                                      self.ratio,
                                                      self.runs,
                                                      self.proj,
                                                      random_seed=1)
        expected_files = sorted(
            fu.file_search_and(data_app_val_dir, True, ".sqlite"))
        assert len(expected_files) == 2 * self.runs

        driver = ogr.GetDriverByName("SQLite")
        for expected_file in expected_files:
            ds = driver.Open(expected_file, 0)
            layer_name = ds.GetLayer(0).GetName()
            conn = sqlite3.connect(expected_file)
            df = pd.read_sql_query(f"select * from {layer_name}", conn)
            assert len(df) == 10
            assert len(df.columns) == 5
