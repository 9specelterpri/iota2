#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import shutil

from iota2.common import file_utils as fut
from iota2.sampling import data_augmentation
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestSamplesAugmentationUser:
    """Test samples augmentation by csv parsing."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.vector = str(
            IOTA2DIR
            / "data"
            / "references"
            / "sampler"
            / "D0005H0002_polygons_To_Sample_Samples_ref_bindings.sqlite"
        )
        cls.csv_path = str(IOTA2DIR / "data" / "references" / "dataAugmentation.csv")

    def test_iota2_data_augmentation_by_copy(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST the function data_augmentationByCopy.

        test if the function AugmentationSamplesUser.samples_management_csv
        works as expected.
        """
        expected = [152, 42, 24, 152]
        vector_1 = shutil.copyfile(
            self.vector, i2_tmpdir / "vector_TEST_1_seed0.sqlite"
        )
        vector_2 = shutil.copyfile(
            self.vector, i2_tmpdir / "vector_TEST_2_seed0.sqlite"
        )

        data_augmentation.data_augmentation_by_copy(
            "CODE", self.csv_path, [vector_1, vector_2]
        )
        count = [
            data_augmentation.count_class_in_sqlite(vector_2, "CODE", "11"),
            data_augmentation.count_class_in_sqlite(vector_2, "CODE", "12"),
            data_augmentation.count_class_in_sqlite(vector_2, "CODE", "42"),
            data_augmentation.count_class_in_sqlite(vector_2, "CODE", "51"),
        ]

        assert all(ex == co for ex, co in zip(expected, count))

    def test_parse_csv(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST parsing csv."""
        expected = [
            ["1", "2", "11", "-1"],
            ["1", "2", "12", "5"],
            ["1", "2", "42", "5"],
            ["1", "2", "51", "5"],
        ]
        test_separators = [",", ":", ";", " "]

        for cpt, sep in enumerate(test_separators):
            test_csv = str(i2_tmpdir / f"test_sep_{cpt}.csv")
            fut.find_and_replace_file(
                input_file=self.csv_path,
                output_file=test_csv,
                str_to_find=",",
                new_str=sep,
            )
            csv_test = data_augmentation.get_user_samples_management(test_csv)
            assert (
                expected == csv_test
            ), f"csv '{test_csv}' can not be parsed with '{sep}' separator"

    def test_count(self):
        """TEST AugmentationSamplesUser.count_class_in_sqlite."""
        expected = [76, 37, 19, 147]
        count = [
            data_augmentation.count_class_in_sqlite(self.vector, "CODE", "11"),
            data_augmentation.count_class_in_sqlite(self.vector, "CODE", "12"),
            data_augmentation.count_class_in_sqlite(self.vector, "CODE", "42"),
            data_augmentation.count_class_in_sqlite(self.vector, "CODE", "51"),
        ]
        assert all(ex == co for ex, co in zip(expected, count))
