#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.sampling import tile_area as area
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestGenerateRegionShape:
    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.path_envelope = str(IOTA2DIR / "data" / "references" / "GenerateShapeTile")
        cls.field_region = "DN"

    def test_generate_region_shape(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test generate_region_shape function."""
        shape_region = str(i2_tmpdir / "region_need_To_env.shp")
        area.generate_region_shape(
            self.path_envelope, shape_region, self.field_region, str(i2_tmpdir), None
        )

        reference_shape_file = str(
            IOTA2DIR
            / "data"
            / "references"
            / "GenerateRegionShape"
            / "region_need_To_env.shp"
        )
        shape_file = str(i2_tmpdir / "region_need_To_env.shp")
        service_compare_vector_file = TUV.ServiceCompareVectorFile()

        assert service_compare_vector_file.test_same_shapefiles(
            reference_shape_file, shape_file
        )
