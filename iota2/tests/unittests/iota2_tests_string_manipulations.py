# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import random
import string

import iota2.common.file_utils as fu
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


def generate_random_string(size):
    """Gnerate a random string of 'size' character.

    IN
    size [int] : size of output string

    OUT
    a random string of 'size' character
    """
    return "".join(
        random.SystemRandom().choice(
            string.ascii_uppercase + string.digits + string.ascii_lowercase
        )
        for _ in range(size)
    )


class Iota2TestStringManipulations:
    """Test iota2 string manipulations."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.all_l8_tiles = (
            "D00005H0010 D0002H0007 D0003H0006 D0004H0004 "
            "D0005H0002 D0005H0009 D0006H0006 D0007H0003 "
            "D0007H0010 D0008H0008 D0009H0007 D0010H0006 "
            "D0000H0001 D0002H0008 D0003H0007 D0004H0005 "
            "D0005H0003 D0005H0010 D0006H0007 D0007H0004 "
            "D0008H0002 D0008H0009 D0009H0008 D0010H0007 "
            "D0000H0002 D0003H0001 D0003H0008 D0004H0006 "
            "D0005H0004 D0006H0001 D0006H0008 D0007H0005 "
            "D0008H0003 D0009H0002 D0009H0009 D0010H0008 "
            "D00010H0005 D0003H0002 D0003H0009 D0004H0007 "
            "D0005H0005 D0006H0002 D0006H0009 D0007H0006 "
            "D0008H0004 D0009H0003 D0010H0002 D0001H0007 "
            "D0003H0003 D0004H0001 D0004H0008 D0005H0006 "
            "D0006H0003 D0006H0010 D0007H0007 D0008H0005 "
            "D0009H0004 D0010H0003 D0001H0008 D0003H0004 "
            "D0004H0002 D0004H0009 D0005H0007 D0006H0004 "
            "D0007H0001 D0007H0008 D0008H0006 D0009H0005 "
            "D0010H0004 D0002H0006 D0003H0005 D0004H0003 "
            "D0005H0001 D0005H0008 D0006H0005 D0007H0002 "
            "D0007H0009 D0008H0007 D0009H0006 D0010H0005".split()
        )
        cls.all_s2_tiles = (
            "T30TVT T30TXP T30TYN T30TYT T30UWU T30UYA T31TCK "
            "T31TDJ T31TEH T31TEN T31TFM T31TGL T31UCR T31UDS "
            "T31UFP T32TLP T32TMN T32ULV T30TWP T30TXQ T30TYP "
            "T30UUU T30UWV T30UYU T31TCL T31TDK T31TEJ T31TFH "
            "T31TFN T31TGM T31UCS T31UEP T31UFQ T32TLQ T32TNL "
            "T32UMU T30TWS T30TXR T30TYQ T30UVU T30UXA T30UYV "
            "T31TCM T31TDL T31TEK T31TFJ T31TGH T31TGN T31UDP "
            "T31UEQ T31UFR T32TLR T32TNM T32UMV T30TWT T30TXS "
            "T30TYR T30UVV T30UXU T31TCH T31TCN T31TDM T31TEL "
            "T31TFK T31TGJ T31UCP T31UDQ T31UER T31UGP T32TLT "
            "T32TNN T30TXN T30TXT T30TYS T30UWA T30UXV T31TCJ "
            "T31TDH T31TDN T31TEM T31TFL T31TGK T31UCQ T31UDR "
            "T31UES T31UGQ T32TMM T32ULU".split()
        )
        cls.dateFile = str(IOTA2DIR / "data" / "references" / "dates.txt")
        cls.fakeDateFile = str(IOTA2DIR / "data" / "references" / "fakedates.txt")

    def test_gettile(self):
        """Get tile name in random string."""
        rstring_head = generate_random_string(100)
        rstring_tail = generate_random_string(100)

        s2_flag = True
        for current_tile in self.all_s2_tiles:
            try:
                fu.findCurrentTileInString(
                    rstring_head + current_tile + rstring_tail, self.all_s2_tiles
                )
            except Exception:
                s2_flag = False
        assert s2_flag
        l8_flag = True
        for current_tile in self.all_l8_tiles:
            try:
                fu.findCurrentTileInString(
                    rstring_head + current_tile + rstring_tail, self.all_l8_tiles
                )
            except Exception:
                l8_flag = False
        assert l8_flag

    def test_getdates(self):
        """Get number of dates."""
        try:
            nb_dates = fu.get_nb_date_in_tile(self.dateFile, display=False)
            assert nb_dates == 35
        except Exception:
            assert False

        try:
            fu.get_nb_date_in_tile(self.fakeDateFile, display=False)
            assert False
        except:
            assert True
