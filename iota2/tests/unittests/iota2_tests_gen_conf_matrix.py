#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import shutil
from pathlib import Path

from iota2.validation import gen_confusion_matrix as GCM

IOTA2DIR = os.environ.get('IOTA2DIR')

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR)


class Iota2TestGenConfMatrix:
    """check confusion matrix gen as csv"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data = IOTA2DIR / "data" / "references" / "GenConfMatrix"

    def test_gen_conf_matrix(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test generate confusion matrix."""
        # Prepare data
        pathappval = str(i2_tmpdir / "dataAppVal")
        pathclassif = str(i2_tmpdir / "classif")
        final = str(i2_tmpdir / "final")

        # test and creation of pathClassif
        if not os.path.exists(pathclassif):
            os.mkdir(pathclassif)
        if not os.path.exists(pathclassif + "/MASK"):
            os.mkdir(pathclassif + "/MASK")
        if not os.path.exists(pathclassif + "/tmpClassif"):
            os.mkdir(pathclassif + "/tmpClassif")
        # test and creation of Final
        if not os.path.exists(final):
            os.mkdir(final)
        if not os.path.exists(final + "/TMP"):
            os.mkdir(final + "/TMP")

        # test and creation of pathAppVal
        if not os.path.exists(pathappval):
            os.mkdir(pathappval)

        # copy input data
        src_files = os.listdir(str(self.ref_data / "Input" / "dataAppVal"))
        for file_name in src_files:
            full_file_name = str(self.ref_data / "Input" / "dataAppVal" /
                                 file_name)
            shutil.copy(full_file_name, pathappval)
        src_files = os.listdir(
            str(self.ref_data / "Input" / "Classif" / "MASK"))
        for file_name in src_files:
            full_file_name = str(self.ref_data / "Input" / "Classif" / "MASK" /
                                 file_name)
            shutil.copy(full_file_name, pathclassif + "/MASK")
        src_files = os.listdir(
            str(self.ref_data / "Input" / "Classif" / "classif"))
        for file_name in src_files:
            full_file_name = str(self.ref_data / "Input" / "Classif" /
                                 "classif" / file_name)
            shutil.copy(full_file_name, pathclassif)
        src_files = os.listdir(str(self.ref_data / "Input" / "final" / "TMP"))
        for file_name in src_files:
            full_file_name = str(self.ref_data / "Input" / "final" / "TMP" /
                                 file_name)
            shutil.copy(full_file_name, final + "/TMP")

        # Run test
        data_field = 'CODE'
        in_classif = str(i2_tmpdir / "classif" /
                         "D0005H0003_model_1_confidence_seed_0.tif")

        ref_vec = str(Path(pathappval) / "D0005H0003_region_1_seed0_val.shp")
        out_csv = str(i2_tmpdir / "csv_test.csv")
        GCM.gen_conf_matrix(in_classif,
                            out_csv,
                            data_field,
                            ref_vec,
                            128,
                            labels_table={"class": "class"})
        csv_content = ("#Reference labels (rows):1\n"
                       "#Produced labels (columns):1\n"
                       "4\n")
        with open(out_csv, "r") as file_csv:
            file_csv_content = file_csv.read()

        assert file_csv_content == csv_content, "csv file generation failed"

    def test_gen_conf_matrix_int_nomen(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test generate confusion matrix."""
        pathappval = str(i2_tmpdir / "dataAppVal")
        pathclassif = str(i2_tmpdir / "classif")
        final = str(i2_tmpdir / "final")
        cmdpath = str(i2_tmpdir / "cmd")

        # test and creation of pathClassif
        if not os.path.exists(pathclassif):
            os.mkdir(pathclassif)
        if not os.path.exists(pathclassif + "/MASK"):
            os.mkdir(pathclassif + "/MASK")
        if not os.path.exists(pathclassif + "/tmpClassif"):
            os.mkdir(pathclassif + "/tmpClassif")
        # test and creation of Final
        if not os.path.exists(final):
            os.mkdir(final)
        if not os.path.exists(final + "/TMP"):
            os.mkdir(final + "/TMP")

        # test and creation of cmdPath
        if not os.path.exists(cmdpath):
            os.mkdir(cmdpath)
        if not os.path.exists(cmdpath + "/confusion"):
            os.mkdir(cmdpath + "/confusion")

        # test and creation of pathAppVal
        if not os.path.exists(pathappval):
            os.mkdir(pathappval)

        # copy input data
        src_files = os.listdir(str(self.ref_data / "Input" / "dataAppVal"))
        for file_name in src_files:
            full_file_name = str(self.ref_data / "Input" / "dataAppVal" /
                                 file_name)
            shutil.copy(full_file_name, pathappval)
        src_files = os.listdir(
            str(self.ref_data / "Input" / "Classif" / "MASK"))
        for file_name in src_files:
            full_file_name = str(self.ref_data / "Input" / "Classif" / "MASK" /
                                 file_name)
            shutil.copy(full_file_name, pathclassif + "/MASK")
        src_files = os.listdir(
            str(self.ref_data / "Input" / "Classif" / "classif"))
        for file_name in src_files:
            full_file_name = str(self.ref_data / "Input" / "Classif" /
                                 "classif" / file_name)
            shutil.copy(full_file_name, pathclassif)
        src_files = os.listdir((self.ref_data / "Input" / "final" / "TMP"))
        for file_name in src_files:
            full_file_name = str(self.ref_data / "Input" / "final" / "TMP" /
                                 file_name)
            shutil.copy(full_file_name, final + "/TMP")

        data_field = 'CODE'
        in_classif = str(i2_tmpdir / "classif" /
                         "D0005H0003_model_1_confidence_seed_0.tif")

        ref_vec = os.path.join(pathappval, "D0005H0003_region_1_seed0_val.shp")
        out_csv = str(i2_tmpdir / "csv_test.csv")
        GCM.gen_conf_matrix(in_classif,
                            out_csv,
                            data_field,
                            ref_vec,
                            128,
                            labels_table={1: 12})
        csv_content = ("#Reference labels (rows):12\n"
                       "#Produced labels (columns):1\n"
                       "4\n")
        with open(out_csv, "r", encoding="utf-8") as file_csv:
            file_csv_content = file_csv.read()
        assert file_csv_content == csv_content, "csv file generation failed"
