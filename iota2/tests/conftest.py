"""Available fixtures through all iota2 tests directories."""

import shutil
from pathlib import Path, PurePath

import pytest


@pytest.fixture(scope="function")
def i2_tmpdir(request):
    """Make test temporary directory."""
    test_name = request.node.nodeid.split("::")[-1]
    i2_tests_tmp_path = Path(__file__).parent
    Path.mkdir(i2_tests_tmp_path, exist_ok=True)
    test_working_dir = Path(PurePath(i2_tests_tmp_path, test_name))
    if test_working_dir.exists():
        # Prevent that previous results interfere with current test
        shutil.rmtree(test_working_dir)
    Path.mkdir(test_working_dir, exist_ok=True)
    return test_working_dir


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item):
    """Make test result information available in fixtures.

    from https://docs.pytest.org/en/latest/example/simple.html#making-test-result-information-available-in-fixtures. # noqa: E501
    """
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"
    setattr(item, "rep_" + rep.when, rep)


@pytest.fixture
def rm_tmpdir_on_success(request):
    """Remove tmpdir from the 'i2_tmpdir' fixture if test succeeded."""
    yield
    if request.node.rep_setup.passed and request.node.rep_call.passed:
        shutil.rmtree(request.getfixturevalue("i2_tmpdir"), ignore_errors=True)
