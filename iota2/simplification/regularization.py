"""Make regularization.

10 m regularization
10 m to 20 m resampling
20 m regularization
inland and sea water differentiation)
"""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================


import argparse
import logging
import os
import shutil
import sys
import time

logger = logging.getLogger("distributed.worker")
import multiprocessing as mp

from simplification import adapt_regul_23

from iota2.common import otb_app_bank, utils
from iota2.common.otb_app_bank import executeApp


def rast_to_vect_recode(
    path,
    classif,
    vector,
    output_name,
    ram="10000",
    dtype="uint8",
    valvect=255,
    valrastout=255,
):
    """
    Convert vector in raster file and change background value

    Parameters
    ----------
    path : string
        working directory
    classif : string
        path to landcover classification
    vector : string
        vector file to rasterize
    output_name : string
        output filename and path
    ram : string
        ram for OTB applications
    dtype : string
        pixel type of the output raster
    valvect : integer
        value of vector to search
    valrastout : integer
        value to use to recode
    """

    # Empty raster
    bmapp = otb_app_bank.CreateBandMathApplication(
        {
            "il": classif,
            "exp": "im1b1*0",
            "ram": str(1 * float(ram)),
            "pixType": dtype,
            "out": os.path.join(path, "temp.tif"),
        }
    )
    # bandmath_appli.ExecuteAndWriteOutput()
    p = mp.Process(target=executeApp, args=[bmapp])
    p.start()
    p.join()

    # Burn
    tif_masque_mer_recode = os.path.join(path, "masque_mer_recode.tif")
    rast_app = otb_app_bank.CreateRasterizationApplication(
        {
            "in": vector,
            "im": os.path.join(path, "temp.tif"),
            "background": 1,
            "out": tif_masque_mer_recode,
        }
    )

    # bandmath_appli.ExecuteAndWriteOutput()
    p = mp.Process(target=executeApp, args=[rast_app])
    p.start()
    p.join()

    # Differenciate inland water and sea water
    bandmath_appli = otb_app_bank.CreateBandMathApplication(
        {
            "il": [classif, tif_masque_mer_recode],
            "exp": f"(im2b1=={valvect})?im1b1:{valrastout}",
            "ram": str(1 * float(ram)),
            "pixType": dtype,
            "out": output_name,
        }
    )

    # bandmath_appli.ExecuteAndWriteOutput()
    p = mp.Process(target=executeApp, args=[bandmath_appli])
    p.start()
    p.join()
    os.remove(tif_masque_mer_recode)


def oso_regularization(
    classif,
    umc1,
    core,
    path,
    output,
    ram="10000",
    no_sea_vector=None,
    rssize=None,
    umc2=None,
    logger=logger,
):

    if not os.path.exists(output):
        # OTB Number of threads
        os.environ["ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS"] = str(core)

        # first regularization
        regul_classif, time_regul1 = adapt_regul_23.regularisation(
            classif, umc1, core, path, ram
        )

        logger.info(
            " ".join(
                [" : ".join(["First regularization", str(time_regul1)]), "seconds"]
            )
        )

        # second regularization
        if umc2 is not None:
            if rssize is not None:
                if os.path.exists(os.path.join(path, "reechantillonnee.tif")):
                    os.remove(os.path.join(path, "reechantillonnee.tif"))

                command = (
                    f"gdalwarp -q -multi -wo NUM_THREADS={core} "
                    "-r mode -tr {rssize} {rssize} {regul_classif} "
                    "{path}/reechantillonnee.tif"
                )
                utils.run(command)
                logger.info(
                    " ".join(
                        [
                            " : ".join(["Resample", str(time.time() - time_regul1)]),
                            "seconds",
                        ]
                    )
                )

            regul_classif, time_regul2 = adapt_regul_23.regularisation(
                os.path.join(path, "reechantillonnee.tif"), umc2, core, path, ram
            )
            os.remove(os.path.join(path, "reechantillonnee.tif"))
            logger.info(
                " ".join(
                    [" : ".join(["Second regularization", str(time_regul2)]), "seconds"]
                )
            )

        if no_sea_vector is not None:
            outfilename = os.path.basename(output)
            rast_to_vect_recode(
                path,
                regul_classif,
                no_sea_vector,
                os.path.join(path, outfilename),
                ram,
                "uint8",
            )
        else:
            outfilename = regul_classif

        shutil.copyfile(os.path.join(path, outfilename), output)
        os.remove(os.path.join(path, outfilename))

    else:
        logger.info(
            (
                f"One regularised file '{output}' "
                "already exists for this classification"
            )
        )


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        parser = argparse.ArgumentParser(
            description="Regularization and resampling a classification raster"
        )
        parser.add_argument(
            "-wd", dest="path", action="store", help="Working directory", required=True
        )

        parser.add_argument(
            "-in",
            dest="classif",
            action="store",
            help="Name of classification",
            required=True,
        )

        parser.add_argument(
            "-inland",
            dest="inland",
            action="store",
            help="inland water limit shapefile",
            required=False,
        )

        parser.add_argument(
            "-nbcore",
            dest="core",
            action="store",
            help="Number of CPU / Threads to use for OTB applications (ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS)",
            required=True,
        )

        parser.add_argument(
            "-ram",
            dest="ram",
            action="store",
            help="RAM for otb applications",
            default="10000",
            required=False,
        )

        parser.add_argument(
            "-umc1",
            dest="umc1",
            action="store",
            help="MMU for first regularization",
            required=True,
        )

        parser.add_argument(
            "-umc2",
            dest="umc2",
            action="store",
            help="MMU for second regularization",
            required=False,
        )

        parser.add_argument(
            "-rssize",
            dest="rssize",
            action="store",
            help="Pixel size for resampling",
            required=False,
        )

        parser.add_argument(
            "-outfile",
            dest="out",
            action="store",
            help="output file name",
            required=True,
        )

        args = parser.parse_args()

        oso_regularization(
            args.classif,
            args.umc1,
            args.core,
            args.path,
            args.out,
            args.ram,
            args.inland,
            args.rssize,
            args.umc2,
        )
