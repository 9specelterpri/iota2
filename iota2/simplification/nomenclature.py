"""Module dedicated to handle class nomenclature during vectorization process."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import codecs
import csv
import unicodedata
from collections import Counter, defaultdict
from xml.etree import ElementTree as ET

from pandas import DataFrame, MultiIndex

from iota2.common import file_utils as fu
from iota2.configuration_files.read_config_file import read_internal_config_file


def convert_hex_to_rgb(color):  # pylint: disable=C0116
    hexa = color.lstrip("#")
    return tuple(int(hexa[i : i + 2], 16) for i in (0, 2, 4))


def convert_rgb_to_hex(rgb):  # pylint: disable=C0116
    return f"#{rgb[0]:02x}{rgb[1]:02x}{rgb[2]:02x}"


def get_unique(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def convert_dict_to_list(dictnomenc):
    """Convert dict obect in list

    Parameters
    ----------
    dictnomenc : dict
        dict from config file (cfg) which discribe a multi-level nomenclature

    Return
    ------
    list
        list of classes (code, classename, color, alias) to instanciate a Iota2Nomenclature object
    """
    levels = list(dictnomenc.keys())
    lastlevel = levels[len(levels) - 1]
    tabclasses = []
    for idx, cls in enumerate(dictnomenc[lastlevel]):
        tabclasses.append(
            [
                cls,
                str(dictnomenc[lastlevel][cls]["code"]),
                dictnomenc[lastlevel][cls]["color"],
                dictnomenc[lastlevel][cls]["alias"],
            ]
        )
        for level in reversed(levels):
            if level != lastlevel:
                for classe in dictnomenc[level]:
                    if (
                        dictnomenc[level][classe]["code"]
                        == dictnomenc[lastlevel][cls]["parent"]
                    ):
                        tabclasses[idx].insert(0, dictnomenc[level][classe]["alias"])
                        tabclasses[idx].insert(0, dictnomenc[level][classe]["color"])
                        tabclasses[idx].insert(0, dictnomenc[level][classe]["code"])
                        tabclasses[idx].insert(0, classe)
        tabclasses[idx] = tuple(tabclasses[idx])

    return tabclasses


def get_classes_from_vector_qml(qml):
    """Get classes from QGIS layer style (QML).

    Parameters
    ----------
    qml : str
        path of a QGIS layer style file

    Return
    ------
    list
        list of classes (code, classename, color, alias (auto-generated)) to
        instanciate a Iota2Nomenclature object
    """
    tree = ET.parse(qml).getroot()

    classes = []
    nomenclature = []
    for item in tree.findall("renderer-v2"):
        for subitem in item.findall("categories"):
            for category in subitem:
                classes.append(
                    [
                        category.attrib["symbol"],
                        category.attrib["value"],
                        category.attrib["label"],
                    ]
                )

    for classe in classes:
        for item in tree.findall("renderer-v2"):
            for subitem in item.findall("symbols"):
                for symbol in subitem:
                    if int(symbol.attrib["name"]) == int(classe[0]):
                        for layer in symbol.findall("layer"):
                            for prop in layer.findall("prop"):
                                if prop.attrib["k"] == "color":
                                    nomenclature.append(
                                        [
                                            classe[2].encode("utf-8"),
                                            classe[1],
                                            [
                                                int(x)
                                                for x in prop.attrib["v"].split(",")[
                                                    0:3
                                                ]
                                            ],
                                        ]
                                    )

    out = [
        [
            x[0],
            x[1],
            convert_rgb_to_hex(tuple(x[2])),
            "".join(
                str(
                    unicodedata.normalize("NFKD", x[0][0:11].decode("utf-8", "ignore"))
                    .encode("ascii", "ignore")
                    .split()
                )
            ),
        ]
        for x in nomenclature
    ]
    for line in out:
        line[3] = "".join(e for e in line[3] if e.isalnum())

    duplicates = [
        item for item, count in list(Counter([x[3] for x in out]).items()) if count > 1
    ]

    if duplicates is not None:
        print(
            "Automatic procedure generated duplicates of Alias, "
            "please change manually aliases of the nomenclature file"
        )
        print(duplicates)
    else:
        print(
            "Please, check aliases in the nomenclature file "
            "automatically generated by this procedure"
        )

    return out


class SymbolRaster:
    """Class for manipulating QGIS raster symbol.

    Parameters
    ----------
    typec : cElementTree object (colorrampshader)
        cElementTree object of QML in which create nomenclature style

    codefield : string
        field name to represent (map)

    valeurs : list
        list of values (code, classname, red, green, blue, hex color) to describe each classe

    Return
    ------
    symbol
        SymbolRaster object
    """

    def __init__(self, colorrampshader, codefield, valeurs=[]):  # pylint: disable=C0116
        self.valeurs = valeurs
        self.cle = [codefield, "classname", "R", "G", "B", "HEX"]
        self.donnees = dict(list(zip(self.cle, self.valeurs)))
        self.item = ET.SubElement(colorrampshader, "item")

    def creation(self, codefield):  # pylint: disable=C0116
        self.item.set("alpha", "255")
        self.item.set("value", str(self.donnees[codefield]))
        self.item.set("label", self.donnees[self.cle[1]])
        self.item.set("color", self.donnees["HEX"])


class symbol:
    """Class for manipulating QGIS vector symbol.

    Parameters
    ----------
    typec : cElementTree object
        cElementTree object of QML in which create nomenclature style

    codefield : string
        field name to represent (map)

    valeurs : list
        list of values (code, classname, red, green, blue, hex color) to describe each classe

    Return
    ------
    symbol
        symbol object
    """

    def __init__(self, typec, codefield, valeurs=[]):

        self.typec = typec
        self.valeurs = valeurs
        self.cle = [codefield, "classname", "R", "G", "B", "HEX"]
        self.donnees = dict(list(zip(self.cle, self.valeurs)))
        self.symb = ET.SubElement(typec, "symbol")
        self.lower = ET.SubElement(self.symb, "lowervalue")
        self.upper = ET.SubElement(self.symb, "uppervalue")
        self.label = ET.SubElement(self.symb, "label")
        self.outline = ET.SubElement(self.symb, "outlinecolor")
        self.outsty = ET.SubElement(self.symb, "outlinestyle")
        self.outtail = ET.SubElement(self.symb, "outlinewidth")
        self.fillc = ET.SubElement(self.symb, "fillcolor")
        self.fillp = ET.SubElement(self.symb, "fillpattern")

    def creation(self, codefield, outlinestyle):
        self.lower.text = str(self.donnees[codefield])
        self.upper.text = str(self.donnees[codefield])
        self.label.text = self.donnees[self.cle[1]]
        self.outsty.text = outlinestyle
        self.outtail.text = "0.26"
        self.outline.set("red", self.donnees["R"])
        self.outline.set("green", self.donnees["G"])
        self.outline.set("blue", self.donnees["B"])
        self.fillc.set("red", self.donnees["R"])
        self.fillc.set("green", self.donnees["G"])
        self.fillc.set("blue", self.donnees["B"])
        self.fillp.text = "SolidPattern"


class Iota2Nomenclature:
    """Class for manipulating multi-level nomenclature.

    - extract specific attributes (code, name, alias, color) on a specfic level
    - prepare multi-level index (nomenclature)
    - prepare multi-level confusion matrix (OTB style)
    - prepare QGIS layer style (QML)
    - extract nomenclature from QML file

    Parameters
    ----------
    nomenclature : string
        Path to a nomenclature file

    Example
    -------
    >>> nomenclature = Iota2Nomenclature("nomenclature.csv")

    Return
    ------
    iota_nomenclature
        nomenclature object
    """

    def __init__(self, nomenclature, typefile="csv"):

        self.nomenclature = self.read_nomenclature_file(nomenclature, typefile)
        self.level = self.get_level_number()
        self.hierarchical_nomenclature = self.set_hierarchical_nomenclature(
            self.nomenclature
        )

    def __str__(self):

        return f"Nomenclature : {self.level} level(s) with {len(self)} classes for the last level"

    def __repr__(self):

        return str(self.nomenclature)

    def __len__(self):

        return len(self.nomenclature)

    def get_level_number(self):
        """Get the number of levels of a nomenclature.

        Return
        ------
        int
            number of levels.
        """
        return len(self.nomenclature[0]) / 4

    def get_color(self, level, typec="RGB"):
        """Get colors list of given level

        Parameters
        ----------
        level : int
            level of the nomenclature
        typec : string
            format of input colors (HEX / RGB)

        Return
        ------
        list
            list of colors of a given level.
        """
        if level <= self.level and level > 0:
            index = self.set_hierarchical_nomenclature(self.nomenclature)
            if typec == "RGB":
                if "#" in list(index.get_level_values(level - 1))[0][2]:
                    return get_unique(
                        [
                            (convert_hex_to_rgb(x[2]))
                            for x in list(index.get_level_values(level - 1))
                        ]
                    )
                raise Exception("Color already in RGB format or in unknown format")
            if typec == "HEX":
                return get_unique(
                    [x[2] for x in list(index.get_level_values(level - 1))]
                )
            raise Exception("Unknown color format provided")

        raise Exception(f"Level {level} does not exists")

    def get_class(self, level):
        """Get classes list of given level.

        Parameters
        ----------
        level : int
            level of the nomenclature

        Return
        ------
        list
            list of classes of a given level.
        """
        if level <= self.level and level > 0:
            index = self.set_hierarchical_nomenclature(self.nomenclature)
            return get_unique([x[1] for x in list(index.get_level_values(level - 1))])
        raise Exception(f"Level {level} does not exists")

    def get_alias(self, level):
        """Get alias list of given level.

        Parameters
        ----------
        level : int
            level of the nomenclature

        Return
        ------
        list
            list of aliases of a given level.
        """
        if level <= self.level and level > 0:
            index = self.set_hierarchical_nomenclature(self.nomenclature)
            return get_unique([x[3] for x in list(index.get_level_values(level - 1))])
        raise Exception(f"Level {level} does not exists")

    def get_code(self, level):
        """Get codes list of given level.

        Parameters
        ----------
        level : int
            level of the nomenclature

        Return
        ------
        list
            list of codes of a given level.
        """
        if level <= self.level and level > 0:
            index = self.set_hierarchical_nomenclature(self.nomenclature)
            return get_unique(
                [int(x[0]) for x in list(index.get_level_values(level - 1))]
            )
        raise Exception(f"Level {level} does not exists")

    def set_hierarchical_nomenclature(self, nomen):
        """Set a multi-level pandas nomenclature.

        Parameters
        ----------
        nomen: csv file
            nomenclature file

        Return
        ------
        pandas.MultiIndex
            Multi-levels nomenclature
        """
        classeslist = []
        levelname = []

        for ind, line in enumerate(nomen):
            classeslist.append([])
            for lev in range(int(self.level)):
                if len(levelname) < self.level:
                    levelname.append(f"level{lev + 1}")

                classeslist[ind].append(
                    (
                        int(line[(4 * lev) + 1]),
                        line[4 * lev],
                        line[(4 * lev) + 2],
                        line[(4 * lev) + 3],
                    )
                )

        index = MultiIndex.from_tuples([tuple(x) for x in classeslist], names=levelname)

        return index

    def read_nomenclature_file(self, nomen, typefile="csv"):
        """Read a csv nomenclature file.

        Example of csv structure for 2 nested levels (l1 = level 1, l2 = level 2) :

           classname_l1, code_l1, colour_l1, alias_l1, classname_l2, code_l2, colour_l2, alias_l2
           Urbain, 100, #b106b1, Urbain, Urbain dense, 1, #b186c1, Urbaindens

        Example of config structure for 2 nested levels (l1 = level 1, l2 = level 2) :
                   >>> Classes:
                   >>> {
                   >>>     Level1:
                   >>>     {
                   >>>         "Urbain":
                   >>>         {
                   >>>         code:100
                   >>>         alias:'Urbain'
                   >>>         color:'#b106b1'
                   >>>         }
                   >>>         ...
                   >>>     }
                   >>>     Level2:
                   >>>     {
                   >>>         "Urbain dense":
                   >>>         {
                   >>>         code:1
                   >>>         ...
                   >>>         parent:100
                   >>>         }
                   >>>     ...
                   >>>     }
                   >>> }

        Example of config structure for 2 nested levels (l1 = level 1, l2 = level 2) :
                   >>> {'Level1': {'Urbain': {'color': '#b106b1',
                                              'alias': 'Urbain', 'code': 100}, ...},
                        'Level2': {'Zones indus. et comm.': {'color': ...,
                                                             'parent' : 100}, ...}}

        alias : maximum 10 caracters
        color : HEX or RGB

        Parameters
        ----------
        nomen: csv or config file or dict object
            nomenclature description

        Return
        ------
        list of tuples
            raw nomenclature
        """
        if isinstance(nomen, dict):
            try:
                tabnomenc = convert_dict_to_list(nomen)
            except OSError:
                raise Exception(
                    "Nomenclature reading failed, nomenclature is not properly built"
                )

        elif typefile == "cfg":
            try:
                cfg = read_internal_config_file(nomen).cfg_as_dict
                dictnomenc = {}
                for level in cfg["Classes"]:
                    cls = {}
                    for classe in cfg["Classes"][level]:
                        cls[classe] = cfg["Classes"][level][classe]
                    dictnomenc[level] = cls

                tabnomenc = convert_dict_to_list(dictnomenc)

            except OSError:
                raise Exception(
                    "Nomenclature reading failed, nomenclature is not properly built"
                )

        elif typefile == "csv" or isinstance(nomen, list):
            if isinstance(nomen, list):
                tabnomenc = nomen
            else:
                try:
                    fileclasses = codecs.open(nomen, "r", "utf-8")
                    tabnomenc = [
                        tuple(line.rstrip("\n").split(","))
                        for line in fileclasses.readlines()
                    ]
                except OSError:
                    raise Exception(
                        "Nomenclature reading failed, nomenclature is not properly built"
                    )

        else:
            raise Exception("The type of nomenclature file is not handled")

        return tabnomenc

    def create_vector_qml(self, outpath, codefield, level, outlinestyle="SolidLine"):
        """Create a QML QGIS style file for vector data.

        Parameters
        ----------
        outpath: str
            output path of QML QGIS style file

        codefield: str
            field name to represent (map)

        level: int
            level of the nomenclature

        outlinestyle: str
            SolidLine or not for outlinestyle of polygons

        """
        intro = ET.Element("qgis")
        transp = ET.SubElement(intro, "transparencyLevelInt")
        transp.text = "255"
        classatr = ET.SubElement(intro, "classificationattribute")
        classatr.text = codefield
        typec = ET.SubElement(intro, "uniquevalue")
        classif = ET.SubElement(typec, "classificationfield")
        classif.text = codefield

        if len(self.get_class(level)) == len(self.get_color(level)):
            color_rgb_list = [
                [
                    x[0],
                    x[1].decode("utf-8"),
                    str(x[2][0]),
                    str(x[2][1]),
                    str(x[2][2]),
                    x[3],
                ]
                for x in zip(
                    self.get_code(level),
                    self.get_class(level),
                    self.get_color(level),
                    self.get_color(level, "HEX"),
                )
            ]

            for elem in color_rgb_list:
                symb = symbol(typec, codefield, elem)
                symb.creation(codefield, outlinestyle)

            # write QML style file
            fich_style = ET.ElementTree(intro)
            fich_style.write(outpath)

        else:
            raise Exception(
                "Duplicates colors or classes found in the nomenclature file"
            )

    def create_raster_qml(self, outpath, classfield, level, nodata="0"):
        """Create a QML QGIS style file for raster data.

        Parameters
        ----------
        outpath: str
            output path of QML QGIS style file

        classfield: str
            field name to represent (map)

        level: int
            level of the nomenclature

        nodata: str
            pixel value of nodata (default: 0)

        """
        intro = ET.Element("qgis")
        pipe = ET.SubElement(intro, "pipe")
        rasterrenderer = ET.SubElement(
            pipe,
            "rasterrenderer",
            opacity="1",
            alphaBand="0",
            classificationMax="211",
            classificationMinMaxOrigin="CumulativeCutFullExtentEstimated",
            band="1",
            classificationMin="0",
            type="singlebandpseudocolor",
        )
        rastershader = ET.SubElement(rasterrenderer, "rastershader")
        colorrampshader = ET.SubElement(
            rastershader, "colorrampshader", colorRampType="INTERPOLATED", clip="0"
        )

        if len(self.get_class(level)) == len(self.get_color(level)):
            color_rgb_list = [
                [
                    x[0],
                    x[1].decode("utf-8"),
                    str(x[2][0]),
                    str(x[2][1]),
                    str(x[2][2]),
                    x[3],
                ]
                for x in zip(
                    self.get_code(level),
                    self.get_class(level),
                    self.get_color(level),
                    self.get_color(level, "HEX"),
                )
            ]

            for elem in color_rgb_list:
                symb = SymbolRaster(colorrampshader, classfield, elem)
                symb.creation(classfield)

            _ = ET.SubElement(
                colorrampshader,
                "item",
                alpha="0",
                value=f"{nodata}",
                label="",
                color="#0000ff",
            )
            _ = ET.SubElement(pipe, "brightnesscontrast", brightness="0", contrast="0")
            _ = ET.SubElement(
                pipe,
                "huesaturation",
                colorizeGreen="128",
                colorizeOn="0",
                colorizeRed="255",
                colorizeBlue="128",
                grayscaleMode="0",
                saturation="0",
                colorizeStrength="100",
            )
            _ = ET.SubElement(pipe, "rasterresampler", maxOversampling="2")
            tree = ET.ElementTree(intro)
            tree.write(outpath, xml_declaration=True, encoding="utf-8", method="xml")

        else:
            raise Exception(
                "Duplicates colors or classes found in the nomenclature file"
            )

    def create_confusion_matrix(self, otbmatrix):
        """Create a multi-level confusion matrix (pandas DataFrame).

        otbmatrix : csv file
            an OTB confusionMatrix file

        Return
        ------
        pandas.core.frame.DataFrame
            vector or raster QML style file
        """
        mat = fu.conf_coordinates_csv(otbmatrix)
        d = defaultdict(list)
        for k, v in mat:
            d[k].append(v)
        csv_f = list(d.items())

        # get matrix from csv
        matrix = fu.gen_confusion_matrix(csv_f, self.get_code(self.level))

        # MultiIndex Pandas dataframe
        cmdf = DataFrame(
            matrix,
            index=self.hierarchical_nomenclature,
            columns=self.hierarchical_nomenclature,
        )
        cmdf.sort_index(level=self.level - 1, inplace=True, axis=1)
        cmdf.sort_index(level=self.level - 1, inplace=True, axis=0)

        return cmdf

    def create_color_file_theia(self, filecolor):
        """Create Color file for THEIA distribution.

        filecolor :  colors file path
            text/csv file (csv structure : code R G B)
        """
        tabcolors = [
            [x, y[0], y[1], y[2]]
            for x, y in zip(self.get_code(self.level), self.get_color(self.level))
        ]
        tabcolors.insert(0, [0, 255, 255, 255])
        tabcolors.append([255, 0, 0, 0])
        with open(filecolor, "w") as ofile:
            writer = csv.writer(ofile, delimiter=" ")
            writer.writerows(tabcolors)

    def create_nomenclature_file_theia(self, filenom):
        """Create Nomencature file for THEIA distribution.

        filecolor :  nomenclature file path
            text/csv file (csv structure : classname:code)
        """
        tabclasses = [
            [x.encode("utf8"), y]
            for x, y in zip(self.get_class(self.level), self.get_code(self.level))
        ]
        tabclasses.append([255, "autres"])
        with open(filenom, "w") as ofile:
            writer = csv.writer(ofile, delimiter=":")
            writer.writerows(tabclasses)
