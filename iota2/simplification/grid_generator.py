"""Grid shapefile generation from an input raster."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================


import argparse
import os
import sys

import numpy as np
from osgeo import ogr, osr
from osgeo.gdalconst import *

from iota2.common import file_utils as fut


def create_polygon_shape(name, epsg, driver):

    out_driver = ogr.GetDriverByName(driver)
    if os.path.exists(name):
        os.remove(name)

    out_coordsys = osr.SpatialReference()
    out_coordsys.ImportFromEPSG(epsg)
    out_datasource = out_driver.CreateDataSource(name)
    _ = out_datasource.CreateLayer(name, srs=out_coordsys, geom_type=ogr.wkbPolygon)
    out_datasource.Destroy()


def grid_generate(outname, xysize, epsg=2154, raster=None, coordinates=None):
    """
    Grid generation from raster.

    in :
        outname : out name of grid
        raster : raster name
        xysize : coefficient for tile size
    out :
        shapefile

    """
    if raster is not None:
        xsize, ysize, _, transform = fut.read_raster(raster, False, 1)

        xmin = float(transform[0])
        xmax = float((transform[0] + transform[1] * xsize))
        ymin = float((transform[3] + transform[5] * ysize))
        ymax = float(transform[3])
    else:
        xmin = float(coordinates[0])
        xmax = float(coordinates[1])
        ymin = float(coordinates[2])
        ymax = float(coordinates[3])

    x_size = (xmax - xmin) / xysize
    interval_x = np.arange(xmin, xmax + x_size, x_size)

    y_size = (ymax - ymin) / xysize
    interval_y = np.arange(ymin, ymax + y_size, y_size)

    # create output file
    create_polygon_shape(outname, epsg, "ESRI Shapefile")
    driver = ogr.GetDriverByName("ESRI Shapefile")
    shape = driver.Open(outname, 1)
    out_layer = shape.GetLayer()
    feature_defn = out_layer.GetLayerDefn()

    # create grid cel
    countcols = 0
    while countcols < len(interval_x) - 1:
        countrows = 0
        while countrows < len(interval_y) - 1:
            # create vertex
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(interval_x[countcols], interval_y[countrows])
            ring.AddPoint(interval_x[countcols], interval_y[countrows + 1])
            ring.AddPoint(interval_x[countcols + 1], interval_y[countrows + 1])
            ring.AddPoint(interval_x[countcols + 1], interval_y[countrows])
            ring.AddPoint(interval_x[countcols], interval_y[countrows])
            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)

            # add new geom to layer
            out_feature = ogr.Feature(feature_defn)
            out_feature.SetGeometry(poly)
            out_layer.CreateFeature(out_feature)
            out_feature.Destroy()

            ymin = interval_y[countrows]
            countrows += 1

        xmin = interval_x[countcols]
        countcols += 1

    # Close DataSources
    shape.Destroy()

    print(f"Grid has {int(xysize * xysize)} tiles")

    return int(xysize * xysize)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        parser = argparse.ArgumentParser(
            description="Grid shapefile generation from an input raster"
        )
        parser.add_argument(
            "-o",
            dest="outname",
            action="store",
            help="ouput grid shapefile path",
            required=True,
        )
        parser.add_argument(
            "-epsg",
            dest="epsg",
            action="store",
            help="ouput grid shapefile projection (EPSG code)",
            type=int,
            required=True,
        )
        parser.add_argument(
            "-r", dest="raster", action="store", help="Input raster file", default=None
        )
        parser.add_argument(
            "-c",
            dest="xysize",
            action="store",
            type=int,
            help="Number of vertical / horizontal tile",
            required=True,
        )
        parser.add_argument(
            "-coords",
            dest="coords",
            nargs="*",
            action="store",
            help="xmin, xmax, ymin, ymax coordinates of the resulting bounding box",
        )
        args = parser.parse_args()

        if args.raster is not None:
            nbtiles = grid_generate(args.outname, args.xysize, args.epsg, args.raster)
        elif args.coords is not None:
            nbtiles = grid_generate(
                args.outname, args.xysize, args.epsg, None, args.coords
            )
        else:
            print("One of -r or coords option has to be filled")
            sys.exit()
