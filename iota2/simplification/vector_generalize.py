"""Vectorisation and simplification of a raster file with grass library."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================


import logging
import os
import shutil
import sys
import time

from iota2.common import file_utils as fut
from iota2.common import utils
from iota2.vector_tools import add_field_area as afa
from iota2.vector_tools import buffer_ogr as buff
from iota2.vector_tools import check_geometry_area_thresh_field as checkGeom
from iota2.vector_tools import delete_duplicate_geometries_sqlite as ddg
from iota2.vector_tools import delete_field as df
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def init_grass(path, debuglvl, epsg="2154"):
    """
    Initialisation of Grass GIS in lambert 93.

    in :
        path : directory where create grassdata directory
        debuglvl : DEBUG level
    """
    global gscript

    # Grass folder Initialisation
    if not os.path.exists(os.path.join(path, "grassdata")):
        os.mkdir(os.path.join(path, "grassdata"))
    path_grassdata = os.path.join(path, "grassdata")

    # Init Grass environment

    gisbase = os.environ["GRASSDIR"]

    gisdb = os.path.join(path_grassdata)
    sys.path.append(os.path.join(gisbase, "etc", "python"))
    os.environ["GISBASE"] = gisbase

    # Overwrite and verbose parameters
    debugdic = {
        "critical": "0",
        "error": "0",
        "warning": "1",
        "info": "2",
        "debug": "3",
    }
    os.environ["GRASS_OVERWRITE"] = "1"
    os.environ["GRASS_VERBOSE"] = debugdic[debuglvl]

    import grass.script as gscript
    import grass.script.setup as gsetup

    # Init Grass
    gsetup.init(gisbase, gisdb)

    # Delete existing location
    if os.path.exists(os.path.join(gisdb, "demolocation")):
        shutil.rmtree(os.path.join(gisdb, "demolocation"))

    # Create the location in Lambert 93
    gscript.run_command("g.proj", flags="c", epsg=epsg, location="demolocation")

    # Create datas mapset
    if not os.path.exists(os.path.join(gisdb, "/demolocation/datas")):
        try:
            gscript.start_command(
                "g.mapset",
                flags="c",
                mapset="datas",
                location="demolocation",
                dbase=gisdb,
            )
        except OSError as err:
            raise Exception(f"Folder '{gisdb}' does not own to current user") from err


def get_driver_from_ext(vector):

    ext = os.path.splitext(vector)[1]
    if ext == ".shp":
        driver = "ESRI Shapefile"
    elif ext == ".sqlite":
        driver = "SQLite"
    elif ext == ".gpkg":
        driver = "GPKG"
    else:
        print("the vector file format is not handled")
        driver = None

    return driver


def get_extension_from_driver(driver):

    if driver == "ESRI Shapefile":
        ext = ".shp"
    elif driver == "SQLite":
        ext = ".sqlite"
    elif driver == "GPKG":
        ext = ".gpkg"
    else:
        print("the driver does not exist. ESRI Shapefile driver will be used")
        ext = ".shp"

    return ext


def topological_polygonize(
    path,
    raster,
    angle,
    out="",
    outformat="SQLite",
    debulvl="info",
    epsg="2154",
    working_dir=None,
    clipfile="",
    clipfield="",
    clipvalue="",
    bufferclip="",
    logger=LOGGER,
):
    """
    Topological polygonization of a raster
    (interface to r.to.vect Grass function)
    """
    if working_dir:
        path = working_dir
    timeinit = time.time()

    extout = get_extension_from_driver(outformat)

    if out == "":
        if outformat != "":
            out = os.path.splitext(raster)[0] + extout
        else:
            out = os.path.splitext(raster)[0] + ".shp"
    elif get_driver_from_ext(out) != outformat:
        print(
            "output driver and output file extension are not compatible."
            "ESRI Shapefile driver will be used"
        )
        out = os.path.splitext(raster)[0] + ".shp"

    if not os.path.exists(out) and os.path.exists(raster):
        print(f"Polygonize of raster file {os.path.basename(raster)}")
        logger.info("Polygonize of raster file %s", os.path.basename(raster))
        # local environnement
        localenv = os.path.join(
            path, f"tmp{os.path.basename(os.path.splitext(raster)[0])}"
        )
        if os.path.exists(localenv):
            shutil.rmtree(localenv)
        os.mkdir(localenv)

        init_grass(localenv, debulvl, epsg)

        # classification raster import
        gscript.run_command(
            "r.in.gdal", flags="e", input=raster, output="tile", overwrite=True
        )
        gscript.run_command("r.null", map="tile@datas", setnull=0)

        timeimport = time.time()
        logger.info(
            " ".join(
                [
                    " : ".join(
                        ["Classification raster import", str(timeimport - timeinit)]
                    ),
                    "seconds",
                ]
            )
        )

        # manage grass region
        gscript.run_command("g.region", raster="tile")

        if angle:
            # vectorization with corners of pixel smoothing
            gscript.run_command(
                "r.to.vect",
                flags="sv",
                input="tile@datas",
                output="vectile",
                type="area",
                overwrite=True,
            )

        else:
            # vectorization without corners of pixel smoothing
            gscript.run_command(
                "r.to.vect",
                flags="v",
                input="tile@datas",
                output="vectile",
                type="area",
                overwrite=True,
            )

        timevect = time.time()
        logger.info(
            " ".join(
                [
                    " : ".join(
                        ["Classification vectorization", str(timevect - timeimport)]
                    ),
                    "seconds",
                ]
            )
        )

        # Export vector file
        if outformat == "ESRI Shapefile":
            outgrass = "ESRI_Shapefile"
        elif outformat.lower() == "sqlite":
            outgrass = "SQLite"

        gscript.run_command("v.out.ogr", input="vectile", output=out, format=outgrass)

        if clipfile:
            clippedvectorpath = os.path.dirname(out)
            idxchar = os.path.basename(out).rindex("_")
            tmpname = os.path.basename(out)[:idxchar]

            tmpout = os.path.join(localenv, f"toclip{extout}")
            vf.copy_vector(out, localenv, f"toclip{extout}")

            clip_vector_file(
                path,
                tmpout,
                clipfile,
                clipfield,
                clipvalue,
                outpath=clippedvectorpath,
                buffersize=bufferclip,
                prefix=tmpname,
                outformat=outformat,
            )

        timeexp = time.time()
        logger.info(
            " ".join(
                [
                    " : ".join(["Vectorization exportation", str(timeexp - timevect)]),
                    "seconds",
                ]
            )
        )

        shutil.rmtree(localenv)

        vf.check_valid_geom(out, outformat)
        vf.check_empty_geom(out, outformat)

    else:
        logger.info(f"Output vector {os.path.basename(out)} file already exists")

    return out


def generalize_vector(
    path,
    vector,
    paramgene,
    method,
    mmu="",
    ncolumns="cat",
    out="",
    outformat="SQLite",
    debulvl="info",
    epsg="2154",
    working_dir=None,
    clipfile=None,
    clipfield="",
    clipvalue="",
    bufferclip="",
    snap=1,
    logger=LOGGER,
):
    """
    Topological generalization of a vector file
    (interface to v.generalize Grass function)
    """
    if working_dir:
        path = working_dir

    timeinit = time.time()

    # driver
    driver = get_driver_from_ext(vector)
    extout = get_extension_from_driver(outformat)

    if out == "":
        if outformat != "":
            out = os.path.splitext(vector)[0] + f"_{method}{extout}"
        else:
            out = os.path.splitext(vector)[0] + f"_{method}.shp"
    elif get_driver_from_ext(out) != outformat:
        print(
            "output driver and output file extension are not compatible."
            "ESRI Shapefile driver will be used"
        )
        out = os.path.splitext(vector)[0] + f"_{method}.shp"

    if clipfile:
        clip = os.path.join(path, f"clip_{clipvalue}.shp")
        layer = os.path.splitext(os.path.basename(clipfile))[0]
        command = (
            f'ogr2ogr -sql "SELECT * FROM {layer} '
            f"WHERE {clipfield} = '{clipvalue}'\" {clip} {clipfile}"
        )
        utils.run(command)

        clipbuff = clip.replace(".shp", "_buff.shp")
        buff.buffer_poly(clip, clipbuff, bufferclip)

        clipped = os.path.join(path, f"input_{clipvalue}{extout}")
        command = (
            f"ogr2ogr -f {driver} -skipfailures -select "
            f"cat -clipsrc {clipbuff} {clipped} {vector}"
        )
        utils.run(command)

        vector = clipped

        checkGeom.check_geometry_area_thresh_field(vector, 1, 0)

    else:
        try:
            vector = fut.file_search_and(
                os.path.dirname(vector), True, os.path.basename(vector)
            )[0]
        except Exception:
            print(f"vector file {vector} not exist")
            sys.exit()

    if not os.path.exists(out) and os.path.exists(vector):
        logger.info(f"Generalize ({method}) of vector file {os.path.basename(vector)}")

        # layer
        layer = vf.get_layer_name(vector, driver)

        # local environnement
        localenv = os.path.join(path, f"tmp{layer}")
        if os.path.exists(localenv):
            shutil.rmtree(localenv)
        os.mkdir(localenv)
        init_grass(localenv, debulvl, epsg)

        # remove non "cat" fields
        for field in vf.get_fields(vector, driver):
            if field != "cat":
                df.deleteField(vector, field)

        gscript.run_command(
            "v.in.ogr",
            flags="e",
            input=vector,
            output=layer,
            columns=["id", ncolumns],
            snap=snap,
            overwrite=True,
        )

        try:
            gscript.run_command(
                "v.generalize",
                input=f"{layer}@datas",
                method=method,
                threshold=f"{paramgene}",
                output=f"generalize{method}",
                overwrite=True,
            )
        except ValueError as err:
            raise Exception(
                "Something goes wrong with generalization parameters "
                f"(method '{method}' or input data)"
            ) from err

        if mmu != "":
            gscript.run_command(
                "v.clean",
                input=f"generalize{method}",
                output="cleanarea",
                tool="rmarea",
                thres=mmu,
                type="area",
            )

            if outformat == "ESRI Shapefile":
                outgrass = "ESRI_Shapefile"
            elif outformat.lower() == "sqlite":
                outgrass = "SQLite"
            gscript.run_command(
                "v.out.ogr", input="cleanarea", output=out, format=outgrass
            )
        else:
            if outformat == "ESRI Shapefile":
                outgrass = "ESRI_Shapefile"
            elif outformat.lower() == "sqlite":
                outgrass = "SQLite"
            gscript.run_command(
                "v.out.ogr",
                input=f"generalize{method}",
                output=out,
                format=outgrass,
            )

        timedouglas = time.time()
        logger.info(
            " ".join(
                [
                    " : ".join(
                        [
                            "Douglas simplification and exportation",
                            str(timedouglas - timeinit),
                        ]
                    ),
                    "seconds",
                ]
            )
        )

        # clean geometries
        tmp = os.path.join(localenv, f"tmp{extout}")
        checkGeom.check_geometry_area_thresh_field(out, 1, 0, tmp, outformat)

        vf.copy_vector(tmp, os.path.dirname(out), os.path.basename(out))

        shutil.rmtree(localenv)

    else:
        logger.info("Output vector file already exists")

    return out


def get_field_values(shpfile, field):
    """Return list of unique values of a given field"""
    classes = []
    dsshp = vf.open_to_read(shpfile)
    layer = dsshp.GetLayer()
    for feature in layer:
        val = feature.GetField(field)
        if val not in classes:
            classes.append(val)

    return classes


def clip_vector_file(
    path,
    vector,
    clipfile,
    clipfield="",
    clipvalue="",
    outpath="",
    prefix="",
    buffersize="",
    working_dir=None,
    outformat="",
    logger=LOGGER,
):
    """
    Clip input vector file with another vector file
    (filtered on field and field value)
    """
    if working_dir:
        path = working_dir

    timeinit = time.time()

    extout = get_extension_from_driver(outformat)
    extin = os.path.splitext(vector)[1]
    driverin = vf.get_driver(vector)
    driverclip = vf.get_driver(clipfile)
    extclip = os.path.splitext(clipfile)[1]

    if outpath == "":
        out = os.path.join(
            os.path.dirname(vector), f"{prefix}_{str(clipvalue)}{extout}"
        )
    else:
        out = os.path.join(outpath, f"{prefix}_{str(clipvalue)}{extout}")

    epsgin = vf.get_vector_proj(vector, driverin)
    if vf.get_vector_proj(clipfile, driverclip) != epsgin:
        logger.error(
            "Land cover vector file and clip file projections are different "
            "please provide a clip file with same projection "
            f"as Land cover file (EPSG = {epsgin})"
        )
        sys.exit(-1)

    # clean input geometries
    tmp = os.path.join(path, f"tmp{extin}")
    checkGeom.check_geometry_area_thresh_field(vector, 1, 0, tmp, driverin)

    # copy file
    vf.copy_vector(tmp, os.path.dirname(vector), os.path.basename(vector))

    if not os.path.exists(out):

        if clipfile is not None:
            logger.info(
                f"Clip vector file {os.path.basename(vector)} with "
                f"{os.path.basename(clipfile)} ({clipfield} == {clipvalue})"
            )

            # local environnement
            localenv = os.path.join(path, f"tmp{str(clipvalue)}")

            if os.path.exists(localenv):
                shutil.rmtree(localenv)
            os.mkdir(localenv)

            vf.copy_vector(clipfile, localenv)
            clipfile = os.path.join(localenv, os.path.basename(clipfile))

            if vf.get_nb_feat(clipfile) != 1:
                clip = os.path.join(localenv, f"clip{extclip}")
                layer = vf.get_first_layer(clipfile, driverclip)
                field_type = vf.get_field_type(
                    os.path.join(localenv, clipfile), clipfield, driverclip
                )

                if field_type == str:
                    command = (
                        f'ogr2ogr -f "{driverclip}" -sql "SELECT * FROM {layer} '
                        f"WHERE {clipfield} = '{clipvalue}'\" {clip} {clipfile}"
                    )
                    utils.run(command)
                elif field_type in (int, float):
                    command = (
                        f'ogr2ogr -f "{driverclip}" -sql "SELECT * FROM {layer} '
                        f'WHERE {clipfield} = {clipvalue}" {clip} {clipfile}'
                    )
                    utils.run(command)
                else:
                    raise Exception(f"Field type {field_type} not handled")
            else:
                clip = os.path.join(path, clipfile)
                logger.info(
                    f"'{clip}' shapefile has only one feature "
                    "which will used to clip data"
                )

            # buffer
            extclip = get_extension_from_driver(driverclip)
            if buffersize != "":
                clipbuff = clip.replace(extclip, f"_buff{extclip}")
                buff.buffer_poly(clip, clipbuff, buffersize, driverclip)
                clip = clipbuff
                logger.info(f"buffer of clip {clip} file is now ready")

            # clip
            clipped = os.path.join(localenv, f"clipped{extout}")

            command = (
                f'ogr2ogr -f "{outformat}" -skipfailures -select '
                f"cat -clipsrc {clip} {clipped} {vector}"
            )

            utils.run(command)

        else:
            clipped = os.path.join(localenv, f"merge{extout}")

        timeclip = time.time()
        logger.info(
            " ".join(
                [
                    " : ".join(["Clip final shapefile", str(timeclip - timeinit)]),
                    "seconds",
                ]
            )
        )

        # Delete duplicate geometries
        ddg.delete_duplicate_geometries_sqlite(clipped, outformat)
        vf.copy_vector(clipped, localenv, "clean")

        timedupli = time.time()
        logger.info(
            " ".join(
                [
                    " : ".join(
                        ["Delete duplicated geometries", str(timedupli - timeclip)]
                    ),
                    "seconds",
                ]
            )
        )

        # Check geom
        vf.check_valid_geom(os.path.join(localenv, f"clean{extout}"), outformat)

        # Add Field Area (hectare)
        afa.add_fieldArea(os.path.join(localenv, f"clean{extout}"), 10000)

        vf.copy_vector(
            os.path.join(localenv, f"clean{extout}"),
            os.path.dirname(out),
            os.path.basename(out),
        )

        shutil.rmtree(localenv)

        timeclean = time.time()
        logger.info(
            " ".join(
                [
                    " : ".join(
                        [
                            "Clean empty geometries and compute areas (ha)",
                            str(timeclean - timedupli),
                        ]
                    ),
                    "seconds",
                ]
            )
        )

    else:
        logger.info(f"Output vector file '{out}' already exists")
