#!/usr/bin/env python3

import inspect
import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import warnings
from collections import OrderedDict
from typing import Optional, Union

import fiona
import rtree
from fiona.crs import from_epsg
from osgeo import ogr
from shapely.geometry import mapping, shape

from iota2.common import otb_app_bank as otb
from iota2.common.utils import run
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def prepare_initial_segmentation(
    in_seg,
    tile,
    iota2_directory,
    region_path=None,
    region_field="region",
    seg_field="DN",
    is_slic=False,
    logger=LOGGER,
):
    """
    The raster cover all tiles.
    Use the common mask to extract ROI then vectorize if raster.
    If vector, clip using common mask

    Parameters
    ----------
    in_seg:
        raster (tiff) or shapefile (shp, sqlite)
    tile:
        the name of the tile processed
    iota2_directory:
        path to iota2 output path. Used to access data iota2 produce.
    region_path:
        optional path to a region shape. If not given all tiles are
         considered as a unique region.
    region_field:
        the name for the region field in shape.
    seg_field:
        the segment ID field name
    is_slic:
        If slic, just copy the tif images and then vectorize.
        If not slic, clip the segmentation to the tile footprint then vectorize
    """
    import geopandas as gpd

    seg_path = os.path.join(iota2_directory, "segmentation")
    if region_path is not None:
        out_segmentation_raster = os.path.join(seg_path, "tmp", f"seg_{tile}_tmp.tif")
    else:
        out_segmentation_raster = os.path.join(seg_path, "tmp", f"seg_{tile}.tif")

    # Vector file is stored in tmp as intersection with regions must be
    # computed
    out_segmentation_vector = os.path.join(seg_path, "tmp", f"seg_{tile}.shp")

    reference_vector = os.path.join(
        iota2_directory, "features", tile, "tmp", "MaskCommunSL.shp"
    )
    raster = False
    reference_raster = os.path.join(
        iota2_directory, "features", tile, "tmp", "MaskCommunSL.tif"
    )
    if in_seg.endswith("tif"):
        raster = True

        roi, _ = otb.CreateSuperimposeApplication(
            {
                "inm": in_seg,
                "out": out_segmentation_raster,
                "interpolator": "nn",
                "inr": reference_raster,
            }
        )
        if is_slic:
            # if the segmentation comes from slic per tile
            # no needs to crop it
            # Just copy to segmentation folder
            # This file is used later for zonal stats
            cmd = f"cp {in_seg} {out_segmentation_raster}"
            run(cmd, logger=logger)

        else:
            print("exec")
            roi.ExecuteAndWriteOutput()

    elif in_seg.endswith("shp") or in_seg.endswith("sqlite"):
        # GML is excluded as we lost projection information
        # clip segmentation to tile extent

        cmd = (
            f"ogr2ogr -clipsrc {reference_vector} "
            f"{out_segmentation_vector} {in_seg}"
            " -nlt POLYGON -skipfailures"
        )
        print(cmd)
        run(cmd, logger=logger)
        # Rasterizer pour plus tard
    else:
        raise ValueError(
            f"{in_seg} not correspond to allowed format"
            "only tif, shp or sqlite files are allowed"
        )

    # Handle region
    region_out = os.path.join(
        iota2_directory, "segmentation", "tmp", f"regions_{tile}.shp"
    )
    if region_path is None:
        # create a shape for region == mask commum
        df = gpd.GeoDataFrame.from_file(reference_vector)
        df[region_field] = 1
        df.to_file(region_out)
    else:  # if raster:
        df_reg = gpd.GeoDataFrame.from_file(region_path)
        if seg_field in df_reg.columns:
            df_reg = df_reg.drop(seg_field, axis="columns")
        df_ref = gpd.GeoDataFrame.from_file(reference_vector)
        if seg_field in df_ref.columns:
            df_ref = df_ref.drop(seg_field, axis="columns")
        inter = gpd.overlay(df_reg, df_ref, how="intersection")
        inter.to_file(region_out)
        if raster:
            # Masking the raster
            # Avoid to rasterize as it can lost segment
            inter["fake_id"] = 1
            inter = inter.dissolve(by="fake_id")
            dissolved = os.path.join(
                iota2_directory, "segmentation", "tmp", f"dissolved_{tile}.shp"
            )
            inter.to_file(dissolved)
            region_raster = os.path.join(
                iota2_directory, "segmentation", "tmp", f"regions_{tile}.tif"
            )

            cmd = (
                f"otbcli_Rasterization -in {dissolved} -out {region_raster} "
                f"-im {reference_raster} -mode binary"
            )
            print(cmd)
            run(cmd)
            final_seg = os.path.join(seg_path, "tmp", f"seg_{tile}.tif")

            cmd = (
                f"otbcli_BandMath -il {region_raster}"
                f" {out_segmentation_raster} "
                f"-out {final_seg} -exp im1b1==0?0:im2b1"
            )
            print(cmd)
            run(cmd, logger=logger)
            out_segmentation_raster = final_seg

    # Vectorize the segmentation
    if os.path.exists(out_segmentation_vector) and raster:
        vf.remove_shape(
            out_segmentation_vector.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )

    if raster:
        cmd = (
            f"gdal_polygonize.py -f 'ESRI Shapefile' {out_segmentation_raster}"
            f" {out_segmentation_vector}"
        )
        logging.info("Vectorize segmentation")
        print(cmd)
        run(cmd, logger=logger)


def compute_intersection_with_regions(
    in_seg,
    seg_field,
    out_seg,
    region_path,
    region_priority,
    region_field,
    logger=LOGGER,
):
    """
    Find the first intersection between a shapefile and a region shapefile.
    Create a new column in output with the region found.
    There is no clip in this function

    Parameters
    ----------

    in_seg:
        the shapefile to be intersected, the geometry of this file is kept
    seg_field:
        the column ID in first shapefile
    out_seg:
        the resulting shapefile with the shape of in_seg and a new column
    region_path:
        the region shapefile
    region_priority:
        all regions are looked in numerical order.
        If not None this priority list is used instead of.
    region_field:
        the region field in second shapefile.
        This name is also used to add a new column to output
    """
    import geopandas as gpd

    df_seg = gpd.GeoDataFrame.from_file(in_seg)
    df_seg[seg_field] = range(1, len(df_seg.index) + 1)
    df_seg.to_file(out_seg)
    ds_seg = ogr.Open(out_seg, 1)
    print("Segmentation loaded")
    ds_reg = ogr.Open(region_path)
    layer_seg = ds_seg.GetLayer()
    layer_reg = ds_reg.GetLayer()
    new_field = ogr.FieldDefn(region_field, ogr.OFTString)
    if region_priority is None:
        region_priority = []
        for reg in layer_reg:
            region_priority.append(reg.GetField(region_field))
    region_geom = {}
    layer_reg.ResetReading()
    for reg in layer_reg:
        region_geom[reg.GetField(region_field)] = reg
    layer_seg.CreateField(new_field)
    total = layer_seg.GetFeatureCount()
    logger.info(f"Clone done, iterate over {total} features")
    for i, feat in enumerate(layer_seg):
        found = False
        index = 0
        while index < len(region_priority) and not found:
            logger.debug(f"{index}/{len(region_priority)}")
            reg = region_geom[region_priority[index]]
            index += 1
            region = reg.GetField(region_field)
            logger.debug(f"REGION: {region}")
            if feat.GetGeometryRef().Intersects(reg.GetGeometryRef()):
                layer_seg.SetFeature(feat)
                feat.SetField(region_field, region)
                layer_seg.SetFeature(feat)
                logger.debug(f"found {i}/{total}   {round((i*100.0)/total, 2)}%")
                found = True
            if not found:
                if index > len(region_priority):
                    logger.debug("Not intersection found")


def compute_intersection_segmentation_regions(
    iota2_directory: str,
    tile: str,
    seg_field: str,
    region_priority: Union[list[str], None],
    region_field: str = "region",
):
    """
    This function handle the case when only one region is used.
    If several regions are available, then it call
    :meth:compute_intersection_segmentation_regions_using_ogr

    Parameters
    ----------

    iota2_directory:
        the output iota2 directories root
    tile:
        the tile name
    region_path:
        the region shape file
    region_field:
        the name of region field in region_path
    region_priority:
        list of all region ordered by priority
        by default the increase order is used.
        first region will have more polygons
    seg_field:
        a custom segmentation column name. Used to relabel each segment
        after cropping the initial segmentation.
    """
    import geopandas as gpd

    in_seg = os.path.join(iota2_directory, "segmentation", "tmp", f"seg_{tile}.shp")
    out_seg_path = os.path.join(iota2_directory, "segmentation")
    out_seg = os.path.join(out_seg_path, f"seg_{tile}.shp")

    in_region = os.path.join(
        iota2_directory, "segmentation", "tmp", f"regions_{tile}.shp"
    )
    gdf = gpd.GeoDataFrame().from_file(in_region)

    regions = gdf[region_field].unique()
    if len(regions) == 1:

        df_seg = gpd.GeoDataFrame().from_file(in_seg)
        df_seg[region_field] = 1
        df_seg[seg_field] = range(1, len(df_seg.index) + 1)
        df_seg.to_file(out_seg)
    else:
        compute_intersection_with_regions(
            in_seg, seg_field, out_seg, in_region, region_priority, region_field
        )


def generate_grid(bsize, df_in):
    """
    Take a geodataframe and a buffer size and return a dataframe
    containing a regular grid corresponding to the extent

    Parameters
    ----------
    bsize:
        the buffer size in meters (buffer * resolution)
    df_in:
        a geodataframe opened
    """
    import geopandas as gpd
    import numpy as np
    from shapely.geometry import Polygon

    # create the buffer grid
    xmin, ymin, xmax, ymax = df_in.total_bounds
    cols = list(range(int(np.floor(xmin)), int(np.ceil(xmax)), bsize))
    rows = [np.ceil(ymax)]
    while rows[-1] - bsize > ymin:
        rows.append(rows[-1] - bsize)
    polygons = []
    for x in cols:
        for y in rows:
            polygons.append(
                Polygon(
                    [(x, y), (x + bsize, y), (x + bsize, y - bsize), (x, y - bsize)]
                )
            )

    grid = gpd.GeoDataFrame(
        {"geometry": polygons, "gridid": range(1, len(polygons) + 1)}
    )

    return grid


def compute_intersection_between_segmentation_and_app_samples(
    iota2_directory: str,
    tile: str,
    seed: int,
    region_field: str,
    seg_field: str,
    spatial_resolution: list[Union[float, int]],
    full_segment: bool,
    working_directory=None,
    logger=LOGGER,
):
    """Step designed function to compute intersection between the
    learning polygons and the segmentation in OBIA workflow

    Parameters
    ----------
    iota2_directory:
        iota2 output path
    tile:
        tile name
    seed:
        seed number
    region_field:
        region field name
    seg_field:
        segments field name
    spatial_resolution:
        spatial resolution as list with two elements
        ex: [10,-10] for S2
    full_segment:
        If True, the segments are not clipped to the learning shape
        If False only the intersection between learning polygon and segment are
        kept
    working_directory:
        working directory if different than iota2_directory

    """
    work_dir = os.path.join(iota2_directory, "segmentation", "tmp")
    if working_directory:
        work_dir = working_directory
    # replace old format_sample_to_segmentation
    data_app_val_folder = os.path.join(iota2_directory, "dataAppVal")

    samples_file_sql = os.path.join(
        data_app_val_folder, f"{tile}_seed_{seed}_learn.sqlite"
    )
    samples_file = os.path.join(data_app_val_folder, f"{tile}_seed_{seed}_learn.shp")
    # Convert to shape to simply use geopandas
    cmd = f"ogr2ogr -f 'ESRI Shapefile' {samples_file} {samples_file_sql}"
    logger.info(cmd)
    run(cmd, logger=logger)
    print("open segs")
    seg_folder = os.path.join(iota2_directory, "segmentation")
    segmentation_file = os.path.join(seg_folder, f"seg_{tile}.shp")
    out_name = os.path.join(
        iota2_directory, "segmentation", f"learning_samples_{tile}_seed_{seed}.shp"
    )
    find_intersections_between_samples_and_segments(
        samples_file,
        segmentation_file,
        work_dir,
        region_field,
        tile,
        seg_field,
        full_segment,
        spatial_resolution,
        out_name,
    )


def find_intersections_between_samples_and_segments(
    samples_file: str,
    segmentation_file: str,
    work_dir: str,
    region_field: str,
    tile: str,
    seg_field: str,
    full_segment: bool,
    spatial_resolution: list[Union[float, int]],
    out_name: str,
):
    """
    Function which compute the intersection between two shapefiles.

    Parameters
    ----------
    samples_file:
        the first shapefile
    segmentation_file:
        the second shapefile
    work_dir:
        the working directory
    tile:
        tile name
    seed:
        seed number
    region_field:
        region field name
    seg_field:
        segments field name
    spatial_resolution:
        spatial resolution as list with two elements
        ex: [10,-10] for S2
    full_segment:
        If True, the second shapefile geometry is kept
        If False only the intersection between learning polygon and segment are
       kept
    out_name:
        The output shapefile resulting of the intersection
        Only polygons which intersects are kept
    """
    import geopandas as gpd

    # segmentation_file has a column named region
    # df_seg = gpd.GeoDataFrame.from_file(segmentation_file)
    df_samples = gpd.GeoDataFrame.from_file(samples_file)
    # remove region field as it not corresponding to OBIA spec
    df_samples = df_samples.drop(region_field, axis="columns")
    samples_without_region = os.path.join(
        work_dir, f"{tile}_samples_without_region.shp"
    )
    df_samples.to_file(samples_without_region)
    intersection = os.path.join(work_dir, f"intersect_{tile}_learning_samples.shp")
    epsg = int(df_samples.crs.to_epsg())
    # find all intersections between the two shapes
    intersect_keep_duplicates(
        data_base1=segmentation_file,
        data_base2=samples_without_region,
        output=intersection,
        epsg=epsg,
        keepfields=[seg_field, "region", "i2label"],
    )
    intersections = gpd.GeoDataFrame.from_file(intersection)
    # If duplicates on seg_field that means at least two learning
    # polygons intersect the same segment. Then removes them.
    intersections.drop_duplicates(seg_field, keep=False, inplace=True)
    # Use overlay to cut learning samples
    if full_segment:
        intersections["idlearn"] = range(1, len(intersections.index) + 1)
        intersections.to_file(out_name)
    else:
        print("start intersection")
        # remove lazy columns
        for col in ["originfid", "i2label", "index_right", "grid_id"]:
            if col in intersections.columns:
                intersections = intersections.drop(col, axis="columns")
        overlay = gpd.overlay(intersections, df_samples, how="intersection")
        # geopandas considers that multipolygon and polygon are the same
        overlay = overlay.explode()
        # remove to small polygons ?
        overlay["area"] = overlay.area
        # arbitrary choice of minimum resolution
        # observed from issue when run with polygons < 200m2
        overlay = overlay[
            overlay["area"]
            > 2 * abs(spatial_resolution[0]) * abs(spatial_resolution[1])
        ]
        if not overlay.empty:
            overlay["idlearn"] = range(1, len(overlay.index) + 1)
            overlay.to_file(out_name)


def compute_zonal_stats_for_learning_samples_by_tiles(
    iota2_directory: str,
    tile: str,
    sensors_parameters,
    seed: int,
    spatial_res: int,
    seg_field: str,
    buffer_size: int,
    region_field: str,
    stat_used: list[str],
    working_dir: str,
):
    """iota2 step designed function to compute zonal statistics for the
    learning data

    Parameters
    ----------
    iota2_directory:
        the output path of iota2
    tile:
        tile name
    sensors_parameters:
        dictionnary containing all parameters about sensors
    seed:
        the seed number
    spatial_res:
        the working resolution (to compute buffer realsize)
    seg_field:
        segments field name
    buffer_size:
        buffer size in number of pixels
    region_field:
        region field name
    stat_used:
        list of stats which can be used
        choices are: mean, count, min, max, std

    """
    from iota2.sensors.sensors_container import sensors_container

    # Access to spectral features for each sensors
    sensor_tile_container = sensors_container(
        tile, working_dir, iota2_directory, **sensors_parameters
    )
    sensors_features = sensor_tile_container.get_sensors_features(available_ram=1000)
    in_seg = os.path.join(
        iota2_directory, "segmentation", f"learning_samples_{tile}_seed_{seed}.shp"
    )
    # split_tile_regions_subgrid
    keepfields = ["i2label", "region", seg_field, "gridid", "idlearn"]
    dico_reg_parts = split_classify_tiles(
        iota2_directory,
        in_seg,
        tile,
        int(spatial_res),
        seg_field,
        region_field,
        int(buffer_size),
        keepfields,
        seed,
    )
    mask = os.path.join(iota2_directory, "features", tile, "tmp", "MaskCommunSL.tif")
    for region in dico_reg_parts.keys():
        for shape_part in dico_reg_parts[region]:
            part = os.path.splitext(os.path.basename(shape_part))[0].split("_")[-3]

            seg = prepare_sub_tile_to_stats(shape_part, mask, "idlearn")
            xml_files, labels = do_zonal_stats(
                os.path.join(iota2_directory, "learningSamples"),
                seg,
                sensors_features,
                tile,
                seed,
                region,
                part,
            )
            _, _ = convert_xml_to_shape(
                os.path.join(iota2_directory, "learningSamples"),
                xml_files,
                labels,
                shape_part,
                seed,
                "idlearn",
                stat_used,
            )


# TODO
def compute_stats_for_normalization():
    """Not usefull for RF"""
    # get functions from regression, using scikit learn


def learning_models_by_region(
    iota2_directory: str,
    region: str,
    seed: int,
    data_field: str,
    classifier_name: str,
    enable_stats: bool,
    classifier_options: Union[None, dict[str, Union[str, int]]] = None,
):
    """
    This function trains a classifier for each region

    Parameters
    ----------
    iota2_directory:
        iota2 output directory
    region:
        region value
    seed:
        seed number
    data_field:
        label column name used for train the classifier
    classifier_name:
        classifier name. Only OTB classifiers allowed
    enable_stats:
        compute mean and std stats for normalization.
        Not working for now.
    classifier_options:
        dictionnary of OTB allowed parameters for classifier
        ex: {"classifier.sharkrf.nbtrees" : 100}
    """
    print(
        "\n" * 2
        + "*" * 30
        + "\n"
        + f"{inspect.stack()[0][3]} start"
        + "\n" * 2
        + "*" * 30
    )
    import iota2.common.file_utils as fu

    # import iota2.common.otb_app_bank as otb
    # Get all samples file for the target region and seed
    samples_files = fu.file_search_and(
        os.path.join(iota2_directory, "learningSamples"),
        True,
        f"_region_{region}_",
        f"_seed_{seed}.shp",
    )

    print(samples_files)
    model = os.path.join(
        iota2_directory, "model", f"model_region_{region}_seed_{seed}.txt"
    )
    # Get features name from text file
    feature_file = os.path.join(iota2_directory, "learningSamples", "Stats_labels.txt")
    with open(feature_file) as cont_file:
        feats = cont_file.read().split("\n")

    # if samples  normalization is enable get the file
    features_stats = None
    if enable_stats:
        features_stats = os.path.join(
            iota2_directory,
            "stats",
            f"learn_samples_region_{region}_seed_{seed}_stats_label.txt",
        )

    # samples_file is a list of shapefile, where each one have the same
    # columns name
    app_learning = otb.train_vector_classifier(
        samples_files,
        data_field,
        feats,
        classifier_name,
        model,
        features_stats,
        classifier_options,
    )
    app_learning.ExecuteAndWriteOutput()


def prepare_sub_tile_to_stats(shape_file: str, mask: str, seg_field: str):
    """
    Rasterize the shapefile according to the mask for zonal stats efficiency

    Parameters
    ----------
    shape_file:
        the shapefile to rasterize
    mask:
        the mask used for reference
    seg_field:
        the field used to rasterize
    """
    # Create Ref
    out_roi = os.path.splitext(shape_file)[0] + "_mask.tif"
    roi = otb.CreateExtractROIApplication(
        {"in": mask, "out": out_roi, "mode": "fit", "mode.fit.vect": shape_file}
    )
    roi.ExecuteAndWriteOutput()
    out = os.path.splitext(shape_file)[0] + ".tif"
    raster = otb.CreateRasterizationApplication(
        {
            "in": shape_file,
            "out": out,
            "im": out_roi,
            "mode": "attribute",
            "mode.attribute.field": seg_field,
            "ram": 2000,
        }
    )
    raster.ExecuteAndWriteOutput()
    return out


def do_zonal_stats(
    out_directory: str,
    segmentation: str,
    sensors_features,
    tile: str,
    seed: int,
    region: str,
    part: int,
):
    """
    Compute the zonal stats. Tile are splitted before for handling
     memory issues.

    Parameters
    ----------
    out_directory:
        the working directory
    segmentation:
        the rasterized segmentation
    sensors_features:
        otb pipeline containing spectral features
    tile:
        tile name
    seed:
        seed number
    region:
        the region id
    part:
        the part number (for indexing results)
    """

    classif_dir = os.path.join(out_directory, "zonal_stats")
    labels = []
    outputs_list = []
    for sensor_name, (
        (sensor_features, sensor_features_dep),  # pylint: disable=W0612
        features_labels,
    ) in sensors_features:
        # Stack every feature
        sensor_features.Execute()
        labels.append(features_labels)
        output_xml = os.path.join(
            classif_dir,
            f"{sensor_name}_{tile}_"
            f"samples_seed_{seed}_region_{region}_part_{part}_stats.xml",
        )
        extract_roi = otb.CreateExtractROIApplication(
            {"in": sensor_features, "mode": "fit", "mode.fit.im": segmentation}
        )

        extract_roi.Execute()
        zonal_stats_app = otb.CreateZonalStatistics(
            {
                "in": extract_roi,
                "inbv": "0",
                "inzone.labelimage.in": segmentation,
                "inzone.labelimage.nodata": 0,
                "out.xml.filename": output_xml,
                "ram": 2000,
            }
        )
        if not os.path.exists(output_xml):
            zonal_stats_app.ExecuteAndWriteOutput()
        else:
            print("Stats found skipping", output_xml)
        outputs_list.append(output_xml)
    return outputs_list, labels


def convert_xml_to_shape(
    out_directory: str,
    xml_files: list[str],
    labels: list[str],
    shape_file: str,
    seed: int,
    merge_field: str,
    stat_used: list[str],
):
    """
    This function associates statisctics produced by OTB in xml to
    the shapefile containing the geometry.
    The result is stored as shapefile as required for the VectorClassifier

    Parameters
    ----------

    out_directory:
        the path to output directory created by iota2
    xml_files:
        the sensor ordered list of statisctics files
    labels:
        a list of list of string labels generated by sensors_container
    shape_file:
        the file containing the geometry corresponding to the statisctics
    merge_field:
        the merge along which the statisitcs and geometry will be merged
    stat_used:
        a list of which statisctics are keept for classification
        Allowed values are: mean, max, min, std, count
    """
    import geopandas as gpd
    import pandas as pd
    import xmltodict

    allowed_stats = ["mean", "max", "min", "std", "count"]
    stat_used = [stat for stat in stat_used if stat in allowed_stats]
    # Init the final dataframe to None
    df_full = None
    print(
        "\n" * 2
        + "*" * 30
        + "\n"
        + f"{inspect.stack()[0][3]} start"
        + "\n" * 2
        + "*" * 30
    )
    final_labels = []
    # Parse the xml file using external lib xmltodict
    # Fill a pandas dataframe with the stats values
    for id_sens, (xml_file, label) in enumerate(zip(xml_files, labels)):
        with open(xml_file) as cfi:
            doc = xmltodict.parse(cfi.read())
            list_stats = doc["GeneralStatistics"]["Statistic"]
            names = {elem["@name"]: elem["StatisticMap"] for elem in list_stats}
            for stat_use in stat_used:
                if isinstance(names[stat_use], list):
                    value_dict = {
                        stat["@key"]: [
                            float(i) for i in stat["@value"][1:-1].split(",")
                        ]
                        for stat in names[stat_use]
                    }
                else:
                    # if not list type means that only one polygon in file
                    # then the split is different
                    stat = names[stat_use]
                    value_dict = {}
                    value_dict[stat["@key"]] = [
                        float(i) for i in stat["@value"][1:-1].split(",")
                    ]
                label_sens = [f"S{id_sens}{stat_use}{i}" for i, _ in enumerate(label)]
                final_labels += label_sens
                if df_full is None:
                    df_full = pd.DataFrame.from_dict(
                        value_dict, orient="index", columns=label_sens
                    )
                else:
                    df = pd.DataFrame.from_dict(
                        value_dict, orient="index", columns=label_sens
                    )
                    df_full = pd.concat([df_full, df], axis=1)
    ch = "\n".join(final_labels)
    with open(os.path.join(out_directory, "Stats_labels.txt"), "w") as fid:
        fid.write(ch)
    bname = os.path.splitext(os.path.basename(shape_file))[0]
    df_full[merge_field] = [int(i) for i in df_full.index]
    df_full.to_csv(os.path.join(out_directory, f"{bname}_seed_{seed}.csv"))
    df_shape = gpd.GeoDataFrame.from_file(shape_file)
    inter = df_shape.merge(df_full, on=merge_field)
    out_file = os.path.join(out_directory, bname + f"_seed_{seed}.shp")
    if not os.path.exists(out_file):
        inter.to_file(out_file)
    else:
        print("skipped : ", out_file)
    return final_labels, out_file


def do_vector_classification(
    output_path: str,
    shape_file: str,
    model: str,
    features: list[str],
    kept_field: list[str],
):
    """
    Launch OTB vector classification.
    The input `shape_file` is updated while processing.
    Look at OTB documentation about VectorClassifier application.

    Parameters
    ----------

    output_path:
        path for resulting shape (without features)
    shape_file:
        the shapefile containing the features. A new column will be added.
    model:
        the OTB model. Output of TrainVectorClassifier
    features:
        the list of features used for classification
    kept_field:
        list of field to be kept in the final result
    """
    import geopandas as gpd

    bname = os.path.basename(shape_file)
    classif_shape = shape_file
    # Call vector classifier
    vector_app = otb.create_vector_classifier_application(
        {"in": shape_file, "model": model, "feat": features, "confmap": True}
    )
    vector_app.ExecuteAndWriteOutput()
    # Remove all columns to final classif
    gdf = gpd.GeoDataFrame.from_file(classif_shape)
    gdf = gdf[kept_field + ["predicted", "confidence", "geometry"]]
    gdf.to_file(os.path.join(output_path, bname))


def classify_zonal_stats(
    iota2_directory: str,
    tile: str,
    sensors_parameters,
    seed: int,
    spatial_res: int,
    stats: list[str],
    seg_field: str,
    region_field: str,
    buffer_size: int,
    working_dir: str,
):
    """
    Compute Zonal statisctics over a tile
    Depending on a segmentation input and a stack of spectral features

    Parameters
    ----------
    iota2_directory:
        iota2 output path
    tile:
        tile name
    sensors_parameters:
        dictionary containing all parameters for sensors
    seed:
        seed number
    spatial_res:
        working resolution. Used for compute buffer can be different that
        real resolution
    stats:
        list of statistics used for classification.
        Must be the same than for training.
    seg_field:
        segmentation field name
    region_field:
        region field name
    buffer_size:
        buffer size in pixel unit
    working_dir:
        working directory
    """
    from iota2.sensors.sensors_container import sensors_container

    # Access to spectral features for each sensors
    sensor_tile_container = sensors_container(
        tile, working_dir, iota2_directory, **sensors_parameters
    )
    sensors_features = sensor_tile_container.get_sensors_features(available_ram=1000)
    in_seg = os.path.join(iota2_directory, "segmentation", f"seg_{tile}.shp")
    keepfields = ["DN", "region", seg_field, "gridid"]
    dico_reg_parts = split_classify_tiles(
        iota2_directory,
        in_seg,
        tile,
        int(spatial_res),
        seg_field,
        region_field,
        int(buffer_size),
        keepfields,
        seed=None,
    )

    mask = os.path.join(iota2_directory, "features", tile, "tmp", "MaskCommunSL.tif")
    for region in dico_reg_parts.keys():
        model = os.path.join(
            iota2_directory, "model", f"model_region_{region}_seed_{seed}.txt"
        )
        for shape_part in dico_reg_parts[region]:
            part = os.path.splitext(os.path.basename(shape_part))[0].split("_")[-1]
            seg = prepare_sub_tile_to_stats(shape_part, mask, seg_field)
            xml_files, labels = do_zonal_stats(
                os.path.join(iota2_directory, "classif"),
                seg,
                sensors_features,
                tile,
                seed,
                region,
                part,
            )

            features, shape_stats = convert_xml_to_shape(
                os.path.join(iota2_directory, "classif", "zonal_stats"),
                xml_files,
                labels,
                shape_part,
                seed,
                seg_field,
                stats,
            )
            output_path = os.path.join(iota2_directory, "classif", "reduced")
            do_vector_classification(
                output_path, shape_stats, model, features, [seg_field]
            )


def reassemble_vectors_classification(
    iota2_directory: str,
    tile: str,
    seed: int,
    data_field: str,
    labels_vector_table: dict[int, int],
    logger=LOGGER,
):
    """
    This function gets all vector classification produced in
    classif/reduced path and merge them as a big shapefile

    Parameters
    ----------
    iota2_directory:
        iota2 output path
    tile:
        tile name
    seed:
        seed number
    data_field:
        output label field (after decoding)
    labels_vector_table:
        the encoding label dictionnary

    """
    import geopandas as gpd
    import pandas as pd

    from iota2.common import file_utils as fu

    classif_dir = os.path.join(iota2_directory, "classif")
    shp_parts = fu.file_search_and(
        os.path.join(classif_dir, "reduced"), True, tile, ".shp"
    )
    crs_env = gpd.read_file(shp_parts[0]).crs
    merged_parts = gpd.GeoDataFrame(
        pd.concat([gpd.read_file(i) for i in shp_parts], ignore_index=True), crs=crs_env
    )
    vector_base = os.path.join(classif_dir, f"Vectorized_map_tile_{tile}_seed_{seed}")
    # Re encode labels
    reverse_labels_encode = {v: k for k, v in labels_vector_table.items()}
    # Verifier cohérence
    merged_parts[data_field] = merged_parts["predicted"].map(reverse_labels_encode)

    # used for validation
    # no overlap with other tiles as validation samples are clipped by envelope
    merged_parts.to_file(vector_base + ".shp")

    # used for the final product
    envelope = os.path.join(iota2_directory, "envelope", f"{tile}.shp")
    cmd = (
        f"ogr2ogr -clipsrc {envelope} "
        f"{vector_base}_clipped.shp {vector_base}.shp "
        "-nlt POLYGON -skipfailures"
    )
    print(cmd)
    logger.info(cmd)
    run(cmd, logger=logger)
    cmd = (
        f"ogr2ogr -f sqlite {vector_base}_clipped.sqlite" f" {vector_base}_clipped.shp"
    )
    logger.info(cmd)
    run(cmd, logger=logger)


def merge_clipped_tiles(iota2_directory: str, seed: int, logger=LOGGER):
    """
    Get all classification in sqlite format and merge them in a unique
     layer

    Parameters
    ----------
    iota2_directory:
        iota2 output directory
    seed:
        seed number

    """

    from iota2.common import file_utils as fu

    list_shapes = fu.file_search_and(
        os.path.join(iota2_directory, "classif"), True, "clipped.sqlite"
    )

    out_name = os.path.join(iota2_directory, "final", f"Classif_Seed_{seed}.sqlite")

    # Init the file
    cmd = f"ogr2ogr -f sqlite -nln layer {out_name} {list_shapes[0]}"
    print(cmd)
    logger.info(cmd)
    run(cmd, logger=logger)
    for shape_in in list_shapes[1:]:
        cmd = f"ogr2ogr -f sqlite -nln layer -append -update " f"{out_name} {shape_in}"
        print(cmd)
        logger.info(cmd)
        run(cmd, logger=logger)


def rasterize_vector_classification(
    iota2_directory: str,
    spatial_res: list[Union[int, float]],
    data_field: str,
    background_value: int,
    proj: int,
    seed: int,
):
    """
    This function take the vectorized classification
    and produce two rasters:

    - Classif_Seed_*.tif : the classification
    - Confidence_Seed_*.tif : the confidence map

    Parameters
    ----------
    iota2_directory:
        iota2 output directory
    spatial_res:
        two elements list for spatial resolution
    data_field:
        the class column name to rasterise on
    background_value:
        a value for no classified areas
    proj:
        epsg code
    seed:
        seed numer
    """
    vectorized_map = os.path.join(
        iota2_directory, "classif", f"Vectorized_map_seed_{seed}.shp"
    )
    if not os.path.exists(vectorized_map):
        raise OSError(f"File {vectorized_map} not found. Check previous step")
    final_map = os.path.join(iota2_directory, "final", f"Classif_Seed_{seed}.tif")
    conf_map = os.path.join(iota2_directory, "final", f"Confidence_Seed_{seed}.tif")
    classif_app = otb.CreateRasterizationApplication(
        {
            "in": vectorized_map,
            "out": final_map,
            "pixType": "uint8",
            "background": background_value,
            "epsg": proj,
            "spx": spatial_res[0],
            "spy": spatial_res[1],
            "mode": "attribute",
            "mode.attribute.field": data_field,
        }
    )

    classif_app.ExecuteAndWriteOutput()

    conf_app = otb.CreateRasterizationApplication(
        {
            "in": vectorized_map,
            "out": conf_map,
            "pixType": "float",
            "background": background_value,
            "epsg": proj,
            "spx": spatial_res[0],
            "spy": spatial_res[1],
            "mode": "attribute",
            "mode.attribute.field": "confidence",
        }
    )

    conf_app.ExecuteAndWriteOutput()


def compute_confusion_matrix_from_vector(
    classif_dir: str,
    validation_samples_dir: str,
    tile: str,
    output_dir: str,
    seed: int,
    ref_label_name: str,
    pred_label_name: str,
    seg_field: str,
):
    """
    Iota2 step desinged function to:
     Compute the confusion matrix of a vector file according to a validation
     shapefile
    Parameters
    ----------

    classif_dir:
        path where classification are stored
    validation_samples_dir:
        path where validation files are stored
    tile:
        tile name
    output_dir:
        output directory
    seed:
        seed number
    ref_label_name:
        label name in reference data
    pred_label_name:
        predicted class name
    seg_field:
        segment id name
    """
    from iota2.common import file_utils as fu

    classif_shape = fu.file_search_and(
        classif_dir, True, tile, f"{seed}", "clipped.shp"
    )[0]
    validation_shape = fu.file_search_and(
        validation_samples_dir, True, tile, f"{seed}", "val.sqlite"
    )[0]
    # Convert to shape for geopandas
    val_shape = os.path.splitext(validation_shape)[0] + ".shp"
    if not os.path.exists(val_shape):
        run(f"ogr2ogr -f 'ESRI Shapefile' " f"{val_shape} {validation_shape}")

    extract_matrix_from_vector(
        classif_shape,
        val_shape,
        tile,
        os.path.join(output_dir, "TMP"),
        seed,
        ref_label_name,
        pred_label_name,
        seg_field,
    )


def extract_matrix_from_vector(
    classif_shape: str,
    val_shape: str,
    tile: str,
    output_dir: str,
    seed: int,
    ref_label_name: str,
    pred_label_name: str,
    seg_field: str,
):
    """
    Compute the confusion matrix of a vector file according to a
     validation shapefile

    Parameters
    ----------
    classif_shape:
        the classsification shapefile
    val_shape:
        the validation shapefile
    tile:
        tile name
    output_dir:
        output directory
    seed:
        seed number
    ref_label_name:
        label name in reference data
    pred_label_name:
        predicted class name
    seg_field:
        segment id name
    """
    import geopandas as gpd
    import pandas as pd

    # Start
    df_classif = gpd.GeoDataFrame.from_file(classif_shape)

    df_val = gpd.GeoDataFrame.from_file(val_shape)
    df_val["i2valid"] = range(1, len(df_val.index) + 1)
    df_join = gpd.sjoin(df_val, df_classif, how="inner", op="intersects")
    # duplicates on seg field means that at least two validation polygons
    # intersects one segment
    df_join.drop_duplicates(seg_field, keep=False, inplace=True)
    df = pd.DataFrame(df_join, columns=[ref_label_name, pred_label_name])
    confusion_matrix = pd.crosstab(
        df[ref_label_name],
        df[pred_label_name],
        rownames=[ref_label_name],
        colnames=[pred_label_name],
    )
    confusion_matrix.to_csv(
        os.path.join(output_dir, f"{tile}_confusion_matrix_seed_{seed}.csv")
    )


def compute_metrics_obia(
    iota2_directory: str,
    tile: str,
    seed: int,
    labels_vector_table: dict[int, int],
    data_field: str,
    ref_label_name: str,
    seg_field: str,
    pred_label_name: str = "predicted",
):
    """
    Iota2 step designed function to:
    - reassemble a vector classification for parts
    - produce the confusion matrix

    Parameters
    ----------

    iota2_directory:
        iota2 output path
    tile:
        tile name
    seed:
        seed number
    label_vector_table:
        dictionary for label encoding
    data_field:
        column name for decoded labels in final classification
    ref_label_name:
        label name in reference data
    pred_label_name:
        predicted class name
    seg_field:
        segment id name
    """
    reassemble_vectors_classification(
        iota2_directory, tile, seed, data_field, labels_vector_table
    )
    compute_confusion_matrix_from_vector(
        os.path.join(iota2_directory, "classif"),
        os.path.join(iota2_directory, "dataAppVal"),
        tile,
        os.path.join(iota2_directory, "final"),
        seed,
        ref_label_name,
        pred_label_name,
        seg_field,
    )


def cumulate_confusion_matrix(
    out_directory: str,
    seed: int,
    data_field: str,
    nomenclature_file: str,
    label_vector_table: dict[int, int],
    tmp_dir: str,
):
    """
    Get all confusion_matrix_seed_x.csv files in tmp_dir,
     and accumulate values for each class

    Parameters
    ----------

    out_directory:
        output path
    seed:
        seed number
    data_field:
        label name
    nomenclature_file:
        path to nomenclature file
    label_vector_table:
        dictionnary of label encoding
    tmp_dir:
        path where are stored the confusion matrix of each tile
    """
    import collections

    import pandas as pd

    from iota2.common import file_utils as fu
    from iota2.validation import results_utils as ru

    matrix_files = fu.file_search_and(
        tmp_dir, True, "confusion_matrix", f"seed_{seed}.csv"
    )
    # Handle encoding
    df_nomen = pd.read_csv(
        nomenclature_file, sep=":", names=["name", "label"], header=None
    )

    names_nomen = df_nomen.name.values
    labels_nomen = df_nomen.label.values
    dico_lab = {}
    names = []
    labels = []
    ori_labels = []
    for label, name in zip(labels_nomen, names_nomen):
        if label in label_vector_table:
            dico_lab[label_vector_table[label]] = label
            labels.append(str(label_vector_table[label]))
            ori_labels.append(str(label))
            names.append(name)
        else:
            print(f"{label} in nomenclature not in reference data")

    # init dictionnary
    dict_matrix = {}
    for ref in labels:
        dict_matrix[ref] = collections.OrderedDict()
        for pred in labels:
            dict_matrix[ref][pred] = 0
    # iterate and fill the dictionnary
    labels_seen = []
    for matrix in matrix_files:
        df_mat = pd.read_csv(matrix)
        df_labels = df_mat[data_field]
        for i in df_labels.values:
            print(i)
            if str(i) not in labels_seen:
                labels_seen.append(str(i))
        mat_vals = df_mat.drop(data_field, axis=1)
        dict_labs = df_labels.to_dict()
        dict_vals = mat_vals.to_dict()
        for k in dict_vals.keys():
            # keys are columns ie predicted class
            d_t = dict_vals[k]
            for index in d_t.keys():
                dict_matrix[str(dict_labs[index])][k] += d_t[index]
    # print(dict_matrix)
    l_temp = []
    for label in labels:
        # if label in labels_seen:
        l_temp.append(str(dico_lab[int(label)]))
    # labels = l_temp
    # print(labels)
    matrix_file = os.path.join(tmp_dir, f"Classif_Seed_{seed}.csv")
    with open(matrix_file, "w") as fo:
        fo.write("#Reference labels (rows):" + ",".join(l_temp) + "\n")
        fo.write("#Produced labels (columns):" + ",".join(l_temp) + "\n")
        for ref in labels:
            line = []
            for pred in labels:
                line.append(f"{dict_matrix[ref][pred]}")
            fo.write(",".join(line) + "\n")

    report_txt = os.path.join(out_directory, "RESULTS.txt")
    report_fig = os.path.join(
        out_directory, f"Confusion_Matrix_Classif_Seed_{seed}.png"
    )
    ru.gen_confusion_matrix_fig(matrix_file, report_fig, nomenclature_file)
    ru.stats_report([matrix_file], nomenclature_file, report_txt)


def intersect_keep_duplicates(
    data_base1: str,
    data_base2: str,
    output: str,
    epsg: int,
    keepfields: list[str],
    data_base1_FID_field_name: Optional[str] = None,
    data_base2_FID_field_name: Optional[str] = None,
) -> bool:
    """Perform vector intersection by using fiona.
    If there is intersection return True, else False.

    Parameters
    ----------
    data_base1 :
        input vector path
    data_base2 :
        input vector path
    output:
        output vector path
    epsg:
        epsg code
    keepfields:
        list of fields to keep
    data_base1_FID_field_name:
        add FID column to first input database
    data_base1_FID_field_name
        add FID column to second input database
    """

    output_no_ext, output_ext = os.path.splitext(output)
    if output_ext == ".sqlite":
        output = f"{output_no_ext}.shp"

    db1_no_ext, db1_ext = os.path.splitext(data_base1)
    if db1_ext == ".sqlite":
        db1 = f"{db1_no_ext}.shp"
        if os.path.exists(db1):
            raise OSError(f"{db1} already exits, please remove it")
        run(f"ogr2ogr {db1} {data_base1}")
    else:
        db1 = data_base1

    db2_no_ext, db2_ext = os.path.splitext(data_base2)
    if db2_ext == ".sqlite":
        db2 = f"{db2_no_ext}.shp"
        if os.path.exists(db2):
            raise OSError(f"{db2} already exits, please remove it")
        run(f"ogr2ogr {db2} {data_base2}")
    else:
        db2 = data_base2

    with fiona.open(db1, "r") as layer1:
        with fiona.open(db2, "r") as layer2:
            new_schema = layer2.schema.copy()
            if keepfields:
                properties = OrderedDict()
                for field in keepfields:
                    if field in layer1.schema["properties"]:
                        properties[field] = layer1.schema["properties"][field]
                    elif field in layer2.schema["properties"]:
                        properties[field] = layer2.schema["properties"][field]
                    else:
                        warnings.warn(
                            f"Can't find field '{field}' in input databases :"
                            f" {vf.get_fields(db1)} and {vf.get_fields(db2)}"
                        )
                new_schema = {"properties": properties, "geometry": "Polygon"}
            else:
                for field_name, field_value in layer1.schema["properties"].items():
                    new_schema["properties"][field_name] = field_value

            if data_base1_FID_field_name:
                new_schema["properties"][data_base1_FID_field_name] = "int:9"
            if data_base2_FID_field_name:
                new_schema["properties"][data_base2_FID_field_name] = "int:9"

            crs = from_epsg(int(epsg))
            with fiona.open(
                output, "w", crs=crs, driver="ESRI Shapefile", schema=new_schema
            ) as layer_out:
                index = rtree.index.Index()
                for feat1 in layer1:
                    fid = int(feat1["id"])
                    geom1 = shape(feat1["geometry"])
                    index.insert(fid, geom1.bounds)
                for feat2 in layer2:
                    geom2 = shape(feat2["geometry"])
                    for fid in list(index.intersection(geom2.bounds)):
                        feat1 = layer1[fid]
                        geom1 = shape(feat1["geometry"])
                        geom_intersection = geom1.intersects(geom2)
                        if geom_intersection:
                            prop = {}
                            if keepfields:
                                for field in keepfields:
                                    try:
                                        if field in feat1["properties"]:
                                            prop[field] = feat1["properties"][field]
                                        else:
                                            prop[field] = feat2["properties"][field]
                                    except KeyError:
                                        pass
                            else:
                                for f_n, f_v in feat1["properties"].items():
                                    prop[f_n] = f_v
                                for f_n, f_v in feat2["properties"].items():
                                    prop[f_n] = f_v
                            if data_base1_FID_field_name:
                                prop[data_base1_FID_field_name] = feat1["id"]
                            if data_base2_FID_field_name:
                                print(dir(feat2))
                                print(feat2.keys())
                                prop[data_base2_FID_field_name] = feat2["properties"][
                                    "tile_grid_"
                                ]
                            layer_out.write(
                                {
                                    "geometry": mapping(shape(feat1["geometry"])),
                                    "properties": prop,
                                }
                            )

    output_field = vf.get_fields(output)[0]
    features = vf.get_field_element(output, field=output_field, elem_type="str")

    if output_ext == ".sqlite":
        expected_out = f"{output_no_ext}.sqlite"
        run(f"ogr2ogr -f 'SQLite' {expected_out} {output}")
        vf.remove_shape(
            output.replace(".shp", ""), [".cpg", ".dbf", ".prj", ".shp", ".shx"]
        )
    if db1_ext == ".sqlite":
        vf.remove_shape(
            db1.replace(".shp", ""), [".cpg", ".dbf", ".prj", ".shp", ".shx"]
        )
    if db2_ext == ".sqlite":
        vf.remove_shape(
            db2.replace(".shp", ""), [".cpg", ".dbf", ".prj", ".shp", ".shx"]
        )
    return bool(features)


def split_classify_tiles(
    iota2_directory: str,
    in_seg: str,
    tile: str,
    spatial_res: int,
    seg_field: str,
    region_field: str,
    buffer_size: int,
    keepfields: list[str],
    seed: Union[None, int] = None,
):
    """
    Iota2 Step designed function for split a shape file according to a grid

    Parameters
    ----------
    iota2_directory:
        iota2 output directory
    in_seg:
        the input shapefile
    seg_field:
        the polygons ID field
    tile:
        tile name
    buffer_size:
        buffer size in pixel unit
    spatial_res:
        spatial resolution in image resolution unit (meter for example)
    out_folder:
        output directory
    region_field:
        region column name
    keepfields:
        list of fields to retrieve in the resulting shapefiles

    """

    work_dir = os.path.join(iota2_directory, "segmentation", "tmp")

    if seed is not None:
        out_folder = os.path.join(
            iota2_directory, "segmentation", "grid_split_learn", tile
        )
        prefix = f"_seed_{seed}"
    else:
        out_folder = os.path.join(iota2_directory, "segmentation", "grid_split", tile)
        prefix = ""
    if not os.path.exists(out_folder):
        os.mkdir(out_folder)
    return do_intersect_with_grid(
        in_seg,
        seg_field,
        tile,
        prefix,
        work_dir,
        buffer_size,
        spatial_res,
        out_folder,
        region_field,
        keepfields,
    )


def do_intersect_with_grid(
    in_seg: str,
    seg_field: str,
    tile: str,
    prefix: str,
    work_dir: str,
    buffer_size: int,
    spatial_res: int,
    out_folder: str,
    region_field: str,
    keepfields: list[str],
):
    """Function to split a shapefile according to a grid. The grid is
    computed automatically according to the buffer_size in pixels and
    the spatial resoltion. Geometry are not cut, the first intersection
    found decide to which grid cell the polygon is given. This function
    produces as shapefiles as grid cells.

    Parameters
    ----------
    in_seg:
        the input shapefile
    seg_field:
        the polygons ID field
    tile:
        tile name
    prefix:
        empty string or not. To add to output file name before the extension
    work_dir:
        the working directory
    buffer_size:
        buffer size in pixel unit
    spatial_res:
        spatial resolution in image resolution unit (meter for example)
    out_folder:
        output directory
    region_field:
        region column name
    keepfields:
        list of fields to retrieve in the resulting shapefiles

    """
    import geopandas as gpd

    inter_seg = os.path.join(work_dir, f"grid_intersect_seg_{tile}{prefix}.shp")
    bsize = buffer_size * spatial_res
    if not os.path.exists(inter_seg):
        # Compute grid
        df_seg = gpd.GeoDataFrame.from_file(in_seg)

        epsg = df_seg.crs.to_epsg()
        print(epsg)
        grid = generate_grid(bsize, df_seg)

        grid_tile = os.path.join(out_folder, f"seg_{tile}_grid.shp")
        grid.to_file(grid_tile)
        # Compute intersection between segmentation and grid
        print("intersection ", in_seg, grid_tile)
        intersect_keep_duplicates(
            data_base1=in_seg,
            data_base2=grid_tile,
            output=inter_seg,
            epsg=epsg,
            keepfields=keepfields,
        )
    # Split the intersections
    df_inter = gpd.GeoDataFrame().from_file(inter_seg)
    df_inter.drop_duplicates(seg_field, keep="first", inplace=True)
    regions = df_inter[region_field].unique()
    dict_reg_parts = {}
    for region in regions:
        dict_reg_parts[region] = []
        df_split = df_inter[df_inter[region_field] == region]
        grid_sub = df_split["gridid"].unique()
        for grid_id in grid_sub:
            grid_file = os.path.join(
                out_folder, f"seg_{tile}_region_{region}_grid_{grid_id}{prefix}.shp"
            )
            dict_reg_parts[region].append(grid_file)
            if not os.path.exists(grid_file):
                df_write = df_split[df_split["gridid"] == grid_id]
                df_write.to_file(grid_file)
    return dict_reg_parts
