# !/usr/bin/env python3

import logging
import math

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import shutil
from typing import Optional, Union

from iota2.common.file_utils import ensure_dir
from iota2.common.generate_features import generate_features
from iota2.common.otb_app_bank import CreateSLICApplication, getInputParameterOutput

LOGGER = logging.getLogger("distributed.worker")
LOGGER.addHandler(logging.NullHandler())

sensors_params = dict[str, Union[str, list[str], int]]


def slic_segmentation(
    tile_name: str,
    output_path: str,
    sensors_parameters: sensors_params,
    ram: int = 128,
    working_dir: Optional[Union[str, None]] = None,
    force_spw: Optional[Union[int, None]] = None,
    logger=LOGGER,
):
    """generate segmentation using SLIC algorithm

    Parameters
    ----------
    tile_name : string
        tile's name
    output_path : string
        iota2 output path
    sensors_parameters : dict
        sensors parameters description
    ram : int
        available ram
    working_dir : string
        directory to store temporary data
    force_spw : int
        force segments' spatial width
    logger : logging
        root logger
    """

    slic_name = f"SLIC_{tile_name}.tif"

    apps_dict, _, dep = generate_features(  # pylint: disable=unused-variable
        working_dir,
        tile_name,
        sar_optical_post_fusion=False,
        output_path=output_path,
        sensors_parameters=sensors_parameters,
        mode="usually",
        logger=logger,
    )
    all_features = apps_dict["interp"]
    all_features.Execute()

    spx, _ = all_features.GetImageSpacing(getInputParameterOutput(all_features))

    tmp_dir = working_dir
    if working_dir is None:
        tmp_dir = os.path.join(output_path, "features", tile_name, "tmp", "SLIC_TMPDIR")
    else:
        tmp_dir = os.path.join(working_dir, tile_name)

    ensure_dir(tmp_dir)

    slic_seg_path = os.path.join(output_path, "features", tile_name, "tmp", slic_name)

    features_ram_estimation = all_features.PropagateRequestedRegion(
        key="out", region=all_features.GetImageRequestedRegion("out")
    )
    # increase estimation...
    features_ram_estimation = features_ram_estimation * 1.5
    xy_tiles = math.ceil(
        math.sqrt(float(features_ram_estimation) / (float(ram) * 1024**2))
    )
    slic_parameters = {
        "in": all_features,
        "tmpdir": tmp_dir,
        "spw": force_spw if force_spw else int(spx),
        "tiling": "manual",
        "tiling.manual.ny": int(xy_tiles),
        "tiling.manual.nx": int(xy_tiles),
        "out": slic_seg_path,
    }
    slic_seg = CreateSLICApplication(slic_parameters)

    if not os.path.exists(slic_seg_path):
        logger.info(
            f"Processing SLIC segmentation : {tile_name}\n\t\t"
            f"with parameters : {slic_parameters}"
        )
        slic_seg.ExecuteAndWriteOutput()

    if working_dir is None:
        shutil.rmtree(tmp_dir, ignore_errors=True)
