#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Mosaic classifications in order to produce final maps."""
# import argparse
import logging
import os
import re
import shutil
from typing import Dict, List, Optional, Tuple, Union

from osgeo import gdal

# import numpy as np
from iota2.common import create_indexed_color_image as color
from iota2.common import file_utils as fu
from iota2.common import otb_app_bank as otb
from iota2.common.file_utils import get_raster_resolution
from iota2.common.otb_app_bank import CreateBandMathXApplication
from iota2.common.raster_utils import compress_raster, re_encode_raster
from iota2.common.utils import run
from iota2.vector_tools.vector_functions import get_layer_name

# from osgeo.gdalconst import *

LOGGER = logging.getLogger("distributed.worker")


def generate_diff_map(
    runs: int,
    all_tiles: List[str],
    data_field: str,
    spatial_resolution: Tuple[Union[float, int], Union[float, int]],
    validation_input_folder: str,
    final_tmp_folder: str,
    final_folder: str,
    working_directory: Optional[str] = None,
    logger=LOGGER,
) -> None:
    """
    Produce a map of well/bad classified learning/validation pixels.

    Parameters
    ----------
    runs:
        the number of seeds
    all_tiles:
        the list of all tiles processed
    path_wd:
        the working directory
    data_field:
        the name of label field
    spatial_resolution:
        the spatial resolution (res X, res Y)
    path_test:
        the output path

    """
    wdir = working_directory if working_directory else final_tmp_folder
    # final_path = os.path.join(output_path, "final", "TMP")
    for seed in range(runs):
        for tile in all_tiles:
            val_tiles = fu.file_search_and(
                os.path.join(validation_input_folder),
                True,
                tile,
                "_seed_" + str(seed) + "_val.sqlite",
            )
            if not val_tiles:
                continue
            val_tile = val_tiles[0]
            learn_tile = fu.file_search_and(
                os.path.join(validation_input_folder),
                True,
                tile,
                "_seed_" + str(seed) + "_learn.sqlite",
            )[0]
            classif = os.path.join(final_tmp_folder, f"{tile}_seed_{seed}.tif")
            diff = os.path.join(final_tmp_folder, f"{tile}_seed_{seed}_CompRef.tif")

            compare_ref(
                shape_ref=val_tile,
                shape_learn=learn_tile,
                classif=classif,
                diff=diff,
                working_directory=working_directory,
                output_path=final_tmp_folder,
                data_field=data_field,
                spatial_res=spatial_resolution,
                logger=logger,
            )

        all_diff = fu.file_search_and(
            os.path.join(final_tmp_folder), True, f"_seed_{seed}_CompRef.tif"
        )
        diff_seed = os.path.join(final_folder, f"diff_seed_{seed}.tif")
        if working_directory:
            diff_seed = os.path.join(wdir, f"diff_seed_{seed}.tif")
        if all_diff:
            if not spatial_resolution:
                res_x, res_y = fu.get_raster_resolution(all_diff[0])
                spatial_resolution = (res_x, res_y)
            fu.assembleTile_Merge(
                all_diff,
                spatial_resolution,
                diff_seed,
                creation_options={"NUM_THREADS": "ALL_CPUS"},
                output_pixel_type="Byte",
            )
            if working_directory:
                shutil.copy(
                    wdir + f"/diff_seed_{seed}.tif",
                    os.path.join(final_folder, f"diff_seed_{seed}.tif"),
                )


def compare_ref(
    shape_ref: str,
    shape_learn: str,
    classif: str,
    diff: str,
    output_path: str,
    data_field: str,
    spatial_res: Tuple[Union[float, int], Union[float, int]],
    working_directory: Optional[str] = None,
    logger=LOGGER,
) -> str:
    """
    Compare the reference data with the predicted classification.

    Parameters
    ----------
    shape_ref:
        validation shapefile
    shape_learn:
        learning shapefile
    classif:
        produced classification
    diff:
        output raster file
    working_directory:
        path to working directory
    output_path:
        the final output path
    data_field:
        the label column
    spatial_res:
        tuple of spatial resolution

    """
    wdir = working_directory if working_directory else output_path

    min_x, max_x, min_y, max_y = fu.get_raster_extent(classif)

    shape_raster_val = os.path.join(
        wdir, os.path.basename(shape_ref).replace(".sqlite", ".tif")
    )

    shape_raster_learn = os.path.join(
        wdir, os.path.basename(shape_learn).replace(".sqlite", ".tif")
    )
    if not spatial_res:
        res_x, res_y = fu.get_raster_resolution(classif)
        spatial_res = (res_x, res_y)

    # Rasterise val
    shape_ref_table_name = get_layer_name(shape_ref, "SQLite")
    cmd = (
        f"gdal_rasterize -l {shape_ref_table_name} -a {data_field} -init 0 "
        f"-tr {spatial_res[0]} {spatial_res[-1]} {shape_ref}"
        f" {shape_raster_val} "
        f"-te {min_x} {min_y} {max_x} {max_y}"
    )
    run(cmd, logger=logger)
    # Rasterise learn
    shape_learn_table_name = get_layer_name(shape_learn, "SQLite")
    cmd = (
        f"gdal_rasterize -l {shape_learn_table_name} -a {data_field} -init "
        f"0 -tr {spatial_res[0]} {spatial_res[-1]} {shape_learn}"
        f" {shape_raster_learn}"
        f" -te {min_x} {min_y} {max_x} {max_y}"
    )
    run(cmd, logger=logger)

    # diff val

    diff_val = os.path.join(wdir, os.path.basename(diff).replace(".tif", "_val.tif"))
    # reference val == classif  -> 2  | reference val != classif -> 1 |
    # no reference -> 0
    bandmath_val = otb.CreateBandMathApplication(
        {
            "il": [shape_raster_val, classif],
            "out": diff_val,
            "pixType": "uint8",
            "exp": "im1b1==0?0:im1b1==im2b1?2:1",
        }
    )
    bandmath_val.ExecuteAndWriteOutput()
    os.remove(shape_raster_val)

    # diff learn
    diff_learn = os.path.join(
        wdir, os.path.basename(diff).replace(".tif", "_learn.tif")
    )
    # reference learn == classif -> 4  | reference learn != -> 3
    # | no reference -> 0
    bandmath_learn = otb.CreateBandMathApplication(
        {
            "il": [shape_raster_learn, classif],
            "out": diff_learn,
            "pixType": "uint8",
            "exp": "im1b1==0?0:im1b1==im2b1?4:3",
        }
    )
    bandmath_learn.ExecuteAndWriteOutput()
    os.remove(shape_raster_learn)

    # sum diff val + learn
    diff_tmp = os.path.join(wdir, os.path.basename(diff))
    bandmath_sum = otb.CreateBandMathApplication(
        {
            "il": [diff_val, diff_learn],
            "out": diff_tmp,
            "pixType": "uint8",
            "exp": "im1b1+im2b1",
        }
    )
    bandmath_sum.ExecuteAndWriteOutput()
    os.remove(diff_val)
    os.remove(diff_learn)

    if working_directory:  # and not os.path.exists(diff):
        shutil.copy(diff_tmp, diff)
        os.remove(diff_tmp)

    return diff


def create_dummy_rasters(
    missing_tiles: List[str], runs: int, classifications_dir: str, final_dir: str
) -> None:
    """Create empty rasters.

    Parameters
    ----------
    missing_tiles:
        list of tiles
    runs:
        the number of random seeds
    output_path:
        the output path

    Notes
    -----
    use when mode is 'one_region' but there is no validations / learning
    samples into a specific tile
    """

    for tile in missing_tiles:
        classif_tile = fu.file_search_and(
            classifications_dir, True, "Classif_" + str(tile)
        )[0]
        for seed in range(runs):
            dummy_raster_name = tile + "_seed_" + str(seed) + "_CompRef.tif"
            dummy_raster = final_dir + "/" + dummy_raster_name
            dummy_raster_cmd = (
                f"gdal_merge.py -ot Byte -n 0 -createonly -o "
                f"{ dummy_raster} {classif_tile}"
            )
            run(dummy_raster_cmd)


def remove_in_list_by_regex(input_list: List[str], regex: str) -> List[str]:
    """
    Remove all element in the list that match regex.

    Parameters
    ----------
    input_list:
        the list to filter
    regex:
        the regular expression
    """
    out_list = []
    for elem in input_list:
        match = re.match(regex, elem)
        if not match:
            out_list.append(elem)

    return out_list


def proba_map_fusion(
    proba_map_list: List[str],
    ram: int = 128,
    working_directory: Optional[str] = None,
    logger=LOGGER,
) -> None:
    """
    Fusion of probabilities map.

    Parameters
    ----------
    proba_map_list :
        list of probabilities map to merge
    ram :
        available ram in mb
    working_directory :
        working directory absolute path
    """
    model_pos = 3

    proba_map_fus_dir, proba_map_fus_name = os.path.split(proba_map_list[0])
    proba_map_fus_name = proba_map_fus_name.split("_")
    proba_map_fus_name[model_pos] = proba_map_fus_name[model_pos].split("f")[0]
    proba_map_fus_name = "_".join(proba_map_fus_name)
    list_exp = "+".join([f"im{i+1}".format(i + 1) for i in range(len(proba_map_list))])
    exp = f"({list_exp}) dv {len(proba_map_list)}"
    proba_map_fus_path = os.path.join(proba_map_fus_dir, proba_map_fus_name)
    if working_directory:
        proba_map_fus_path = os.path.join(working_directory, proba_map_fus_name)
    logger.info(
        f"Fusion of probality maps : {proba_map_list} " f"at {proba_map_fus_path}"
    )
    proba_merge = CreateBandMathXApplication(
        {"il": proba_map_list, "ram": str(ram), "out": proba_map_fus_path, "exp": exp}
    )
    proba_merge.ExecuteAndWriteOutput()
    if working_directory:
        copy_target = os.path.join(proba_map_fus_dir, proba_map_fus_name)
        logger.debug(f"copy {proba_map_fus_path} to {copy_target}")
        shutil.copy(proba_map_fus_path, copy_target)
        os.remove(proba_map_fus_path)


def build_confidence_cmd(
    final_tile: str,
    classif_tile: List[str],
    confidence: List[str],
    output_confidence: str,
    fact: int = 100,
    pix_type: str = "uint8",
) -> str:
    """Prepare the otb command to fuse confidence.

    Parameters
    ----------
    final_tile:
        merged tile classification
    classif_tile:
        the list of classification
    confidence:
        the list of confidence
    output_confidence:
        the output name for the confidence map
    fact:
        the factor to apply to confidence for storage
    pix_type:
        the pixel type used to store confidence

    Notes
    -----
    This function is used in 'fusion' classification mode.
    If the final decision is the same than the voter,
     the confidence value is added.
    If the final decision is not the same than the voter,
     1 - confidence is added
    The final confidence is divided by the number of voters
    """
    if len(classif_tile) != len(confidence):
        raise Exception(
            "number of confidence map and classifcation map must be the same"
        )

    number_of_classif = len(classif_tile)
    exp = []
    for i in range(len(classif_tile)):
        # if final == voting add the confidence value
        # if final != voting add 1-confidence
        # Divided by the number of votings
        exp.append(
            "(im"
            + str(i + 2)
            + "b1==0?0:im1b1!=im"
            + str(i + 2)
            + "b1?1-im"
            + str(i + 2 + number_of_classif)
            + "b1:im"
            + str(i + 2 + number_of_classif)
            + "b1)"
        )
    # im1b1==0 means image border here
    exp_confidence = "im1b1==0?0:(" + "+".join(exp) + ")/" + str(len(classif_tile))

    all_images = classif_tile + confidence
    all_images = " ".join(all_images)

    cmd = (
        f"otbcli_BandMath -ram 5120 -il {final_tile} {all_images} "
        f"-out {output_confidence} {pix_type} "
        f'-exp " {fact}*( {exp_confidence} )"'
    )

    return cmd


def merge_confidence_in_fusion_mode(
    final_tmp_folder: str,
    path_to_classif: str,
    tmp_classif: str,
    tile: str,
    seed: int,
    suffix: str,
    suffix_sar: str,
    proba_map_flag: bool,
    probamap_pattern: str,
    working_directory: Optional[str] = None,
    logger=LOGGER,
) -> List[str]:
    """
    Handle the case when models are splitted.

    Parameters
    ----------
    iota2_directory:
        path to iota2 output directory
    path_to_classif:
        path where are stored classifications
    tmp_classif:
        path where are stored intermediate classifications
    tile:
        the tile name
    seed:
        the considered seed
    suffix:
        the classification suffix
    suffix_sar:
        the classification suffix if sar is enabled
    proba_map_flag:
        enable the generation of probability maps
    probamap_pattern:
        the prefix of probability maps
    working_directory:
        path to store intermediate results
    """
    wdir = working_directory if working_directory else path_to_classif
    # if model have been splitted, it exists classifications
    # with a f in the region name
    classif_tile = fu.file_search_reg_ex(
        os.path.join(path_to_classif, f"Classif_{tile}*model_*f*_seed_{seed}{suffix}")
    )
    split_model = []
    confidence_out = []
    for classif in classif_tile:
        model = os.path.basename(classif).split("_")[3].split("f")[0]
        if model not in split_model:
            split_model.append(model)
    split_confidence = []
    confidence_all = fu.file_search_reg_ex(
        os.path.join(
            path_to_classif, f"{tile}*model_*_confidence" f"_seed_{seed}{suffix}"
        )
    )
    confidence_without_split = remove_in_list_by_regex(
        confidence_all, f".*model_.*f.*_confidence.{suffix}"
    )
    for model in split_model:
        # get all sub regions for the model selected
        # 'model'f* and not *f* in regex
        classif_tile = fu.file_search_reg_ex(
            os.path.join(
                path_to_classif,
                f"Classif_{tile}*model_{model}" f"f*_seed_{seed}{suffix}",
            )
        )
        # final_tile is the already existing result
        # of the fusion of classification
        final_tile = os.path.join(
            path_to_classif,
            f"Classif_{tile}_model_{model}" f"_seed_{seed}{suffix_sar}.tif",
        )
        confidence = fu.file_search_reg_ex(
            os.path.join(
                path_to_classif,
                f"{tile}*model_{model}f*_confidence" f"_seed_{seed}{suffix}",
            )
        )
        if proba_map_flag:
            proba_map_fusion(
                proba_map_list=fu.file_search_reg_ex(
                    os.path.join(
                        path_to_classif,
                        f"{probamap_pattern}_{tile}_model_"
                        f"{model}f*_seed_{seed}{suffix_sar}",
                    )
                ),
                working_directory=wdir,
                ram=2000,
                logger=logger,
            )
        classif_tile = sorted(classif_tile)
        confidence = sorted(confidence)
        output_confidence = os.path.join(
            tmp_classif,
            f"{tile}_model_{model}_" f"confidence_seed_{seed}{suffix_sar}.tif",
        )
        cmd = build_confidence_cmd(
            final_tile,
            classif_tile,
            confidence,
            output_confidence,
            fact=1,
            pix_type="float",
        )
        run(cmd, logger=logger)
        split_confidence.append(output_confidence)

    confidence_list = split_confidence + confidence_without_split
    output_confidence = os.path.join(
        tmp_classif, f"{tile}_GlobalConfidence_seed_{seed}.tif"
    )
    spatial_resolution = fu.get_raster_resolution(confidence_list[0])
    fu.assembleTile_Merge(
        confidence_list,
        spatial_resolution,
        output_confidence,
        output_pixel_type="Float32",
        creation_options={
            "COMPRESS": "LZW",
            "NUM_THREADS": "ALL_CPUS",
            "BIGTIFF": "YES",
        },
    )
    bandmath = otb.CreateBandMathApplication(
        {
            "il": output_confidence,
            "out": output_confidence,
            "pixType": "uint8",
            "exp": "100 * im1b1",
        }
    )
    bandmath.ExecuteAndWriteOutput()

    shutil.copyfile(
        output_confidence,
        os.path.join(final_tmp_folder, f"{tile}_GlobalConfidence_seed_{seed}.tif"),
    )
    os.remove(output_confidence)
    confidence_out.append(
        os.path.join(final_tmp_folder, f"{tile}_GlobalConfidence_seed_{seed}.tif")
    )

    return confidence_out


def gen_global_confidence(
    runs: int,
    final_tmp_folder: str,
    path_to_classif: str,
    classif_mode: str,
    all_tiles: List[str],
    ds_sar_opt: bool,
    proba_map_flag: bool,
    working_directory: Optional[str] = None,
    logger=LOGGER,
):
    """
    Get all confidence maps and generate the global product for each tile.

    Parameters
    ----------
    runs:
        the number of random seeds
    iota2_directory:
        the path where iota2 output directory was created
    classif_mode:
        the classification mode 'separate' or 'fusion'
    all_tiles:
        the list of all tiles processed
    shape_region:
        the region shape file
    ds_sar_opt:
        enable or disable the use of SAR
    proba_map_flag:
        enable the probability final product creation
    working_directory:
        the working directory where tmp files can be stored
    """
    # Proposal: refact again ?
    # identify mode first then iterate on seed and tile
    probamap_pattern = "PROBAMAP"
    # tmp_classif = os.path.join(iota2_directory, "classif", "tmpClassif")
    # path_to_classif = os.path.join(iota2_directory, "classif")

    wdir = working_directory if working_directory else path_to_classif
    tmp_classif = os.path.join(wdir, "tmpClassif")

    if not os.path.exists(tmp_classif):
        run(f"mkdir {tmp_classif}")

    suffix_sar = "_DS" if ds_sar_opt else ""
    confidences_out = []
    for seed in range(runs):
        for tile in all_tiles:
            suffix = f"*{suffix_sar}"
            if classif_mode != "separate":
                confidences_list = merge_confidence_in_fusion_mode(
                    final_tmp_folder=final_tmp_folder,
                    path_to_classif=path_to_classif,
                    tmp_classif=tmp_classif,
                    tile=tile,
                    seed=seed,
                    suffix=suffix,
                    suffix_sar=suffix_sar,
                    proba_map_flag=proba_map_flag,
                    probamap_pattern=probamap_pattern,
                    working_directory=working_directory,
                    logger=logger,
                )
                confidences_out += confidences_list
            else:
                # mode separate
                # 1 confidence masqued by region
                confidence = fu.file_search_reg_ex(
                    os.path.join(
                        path_to_classif, f"{tile}*confidence_" f"seed_{seed}{suffix}"
                    )
                )
                if not confidence:
                    return []
                # exp = "+".join(
                #     [f"im{i + 1}b1" for i in range(len(confidence))])
                # all_confidence = " ".join(confidence)
                # for currentConf in confidence:
                global_conf = os.path.join(
                    tmp_classif, f"{tile}_GlobalConfidence_seed_{seed}.tif"
                )
                global_conf_f = os.path.join(
                    final_tmp_folder, f"{tile}_GlobalConfidence_seed_{seed}.tif"
                )
                confidence = sorted(
                    confidence, key=lambda x: os.path.basename(x).split("_")[2]
                )
                spatial_resolution = fu.get_raster_resolution(confidence[0])
                fu.assembleTile_Merge(
                    confidence,
                    spatial_resolution,
                    global_conf,
                    output_pixel_type="Float32",
                    creation_options={
                        "COMPRESS": "LZW",
                        "NUM_THREADS": "ALL_CPUS",
                        "BIGTIFF": "YES",
                    },
                )

                bandmath = otb.CreateBandMathApplication(
                    {
                        "il": global_conf,
                        "out": global_conf,
                        "pixType": "uint8",
                        "exp": "100 * im1b1",
                    }
                )
                bandmath.ExecuteAndWriteOutput()
                shutil.copyfile(global_conf, global_conf_f)
                os.remove(global_conf)
                confidences_out.append(global_conf_f)
    return confidences_out


def classification_shaping(
    path_classif: str,
    ds_sar_opt: bool,
    # output_path: str,
    validation_input_folder: str,
    final_folder,
    final_tmp_folder,
    nb_view_folder,
    runs: int,
    classif_mode: str,
    region_shape: str,
    proba_map_flag: bool,
    output_statistics: bool,
    data_field: str,
    spatial_resolution: Tuple[Union[float, int], Union[float, int]],
    labels_conversion: Dict[int, Union[str, int]],
    color_path: str,
    tiles_from_cfg: List[str],
    working_directory: Optional[str] = None,
    logger=LOGGER,
) -> None:
    """
    Create the mosaic of all iota2 products.

    Parameters
    ----------
    path_classif:
        path to classifications
    ds_sar_opt:
        enable the use of SAR and Optical products together
    output_path:
        path to store final products
    iota2_directory:
        the iota2 output directories
    runs:
        the number of random seeds
    classif_mode:
        the classification mode 'separate' or 'fusion'
    region_shape:
        the region shapefile
    proba_map_flag:
        enable the generation of probabilities maps
    output_statistics:
        compute additional statistics on cloud coverage
    data_field:
        the class label column name
    spatial_resolution:
        the spatial resolution
    labels_conversion:
        dictionnary to decode internal labels to original labels
    color_path:
        the text file containing colors
    tiles_from_cfg:
        the full list of tiles provided by users
    working_directory:
        path to store intermediate products
    """
    wdir = working_directory if working_directory else final_folder
    assemble_folder = os.path.join(final_folder)
    tmp_dir = os.path.join(wdir, "TMP")
    if not os.path.exists(tmp_dir):
        os.mkdir(tmp_dir)
    # pix_type = "uint8"
    suffix = "*DS*" if ds_sar_opt else "*"

    all_tiles = sorted(
        list(
            {
                classif.split("_")[1]
                for classif in fu.file_search_and(
                    os.path.join(path_classif), False, "Classif", ".tif"
                )
            }
        )
    )
    print(all_tiles)
    # Prepare confidence and probabilities to be merged
    all_seed_confidence = gen_global_confidence(
        runs=runs,
        path_to_classif=path_classif,
        final_tmp_folder=final_tmp_folder,
        classif_mode=classif_mode,
        all_tiles=all_tiles,
        ds_sar_opt=ds_sar_opt,
        proba_map_flag=proba_map_flag,
        working_directory=working_directory,
        logger=logger,
    )

    if spatial_resolution:
        res_x = spatial_resolution[0]
        res_y = spatial_resolution[1]
    else:
        # use a classification as ref image
        reference = all_seed_confidence[0]
        res_x, res_y = get_raster_resolution(reference)
        res_y = -res_y
    target_spatial_resolution = (res_x, res_y)
    # classification
    prepare_classification(
        assemble_folder=assemble_folder,
        output_path=final_tmp_folder,
        path_classif=path_classif,
        classif_mode=classif_mode,
        region_shape=region_shape,
        ds_sar_opt=ds_sar_opt,
        runs=runs,
        target_spatial_resolution=target_spatial_resolution,
    )
    # confidence
    get_global_confidence(
        assemble_folder=assemble_folder,
        list_tile=all_tiles,
        confidence_path=final_tmp_folder,
        runs=runs,
        target_spatial_resolution=target_spatial_resolution,
    )
    # proba_map
    if proba_map_flag:
        get_tiled_proba_map(
            assemble_folder=assemble_folder,
            proba_path=path_classif,
            runs=runs,
            suffix=suffix,
            target_spatial_resolution=target_spatial_resolution,
        )
    # cloud
    get_final_cloud(
        assemble_folder=assemble_folder,
        cloud_path=nb_view_folder,
        output_path=final_folder,
        list_tile=all_tiles,
        output_statistics=output_statistics,
        target_spatial_resolution=target_spatial_resolution,
        working_directory=working_directory,
    )

    for seed in range(runs):
        re_encoded_raster_path = os.path.join(
            final_folder, f"Classif_Seed_{seed}_reencoded.tif"
        )
        raster_classif_seed_path = os.path.join(
            final_folder, f"Classif_Seed_{seed}.tif"
        )
        decode_and_color_index_map(
            raster_classif_seed_path=raster_classif_seed_path,
            re_encoded_raster_path=re_encoded_raster_path,
            labels_conversion=labels_conversion,
            color_path=color_path,
            logger=logger,
        )

    generate_diff_map(
        runs=runs,
        all_tiles=all_tiles,
        data_field=data_field,
        spatial_resolution=spatial_resolution,
        validation_input_folder=validation_input_folder,
        final_tmp_folder=final_tmp_folder,
        final_folder=final_folder,
        working_directory=working_directory,
        logger=logger,
    )
    missing_tiles = [elem for elem in all_tiles if elem not in tiles_from_cfg]
    create_dummy_rasters(missing_tiles, runs, path_classif, final_tmp_folder)


def prepare_classification(
    assemble_folder: str,
    output_path: str,
    path_classif: str,
    classif_mode: str,
    region_shape: str,
    ds_sar_opt: bool,
    runs: int,
    target_spatial_resolution,
) -> None:
    """
    Get all classifications and reconstruct them by tiles then merge all.

    Parameters
    ----------
    assemble_folder:
        the folder to store the mosaic
    output_path:
        the output path
    path_classif:
        path to classifications
    classif_mode:
        classification mode 'fusion' or 'separate'
    region_shape:
        the region shapefile
    ds_sar_opt:
        enable the use of SAR and optical together
    runs:
        the number of random seeds
    pix_type:
        the pixel type of the final product
    target_spatial_resolution:
        the spatial resolution
    """
    for seed in range(runs):
        sort = []
        classification = []
        # classification.append([])
        if classif_mode == "separate" or region_shape:
            all_classif_seed = fu.file_search_and(
                path_classif, True, ".tif", "Classif", "seed_" + str(seed)
            )
            if ds_sar_opt:
                all_classif_seed = fu.file_search_and(
                    path_classif, True, ".tif", "Classif", "seed_" + str(seed), "DS.tif"
                )
            ind = 1
        elif classif_mode == "fusion":
            all_classif_seed = fu.file_search_and(
                path_classif, True, "_FUSION_NODATA_seed" + str(seed) + ".tif"
            )
            if ds_sar_opt:
                all_classif_seed = fu.file_search_and(
                    path_classif, True, "_FUSION_NODATA_seed" + str(seed) + "_DS.tif"
                )
            ind = 0
        all_classif_seed = remove_in_list_by_regex(all_classif_seed, ".*model_.f._seed")
        for tile in all_classif_seed:
            sort.append((tile.split("/")[-1].split("_")[ind], tile))
        sort = fu.sort_by_first_elem(sort)
        for tile, paths in sort:
            paths = sorted(paths, key=lambda x: os.path.basename(x).split("_")[3])
            path_cl_final = os.path.join(output_path, f"{tile}_seed_{seed}.tif")
            classification.append(path_cl_final)
            fu.assembleTile_Merge(
                paths,
                target_spatial_resolution,
                path_cl_final,
                output_pixel_type="Byte",
                creation_options={
                    "COMPRESS": "LZW",
                    "NUM_THREADS": "ALL_CPUS",
                    "BIGTIFF": "YES",
                },
            )

        classif_mosaic_tmp = os.path.join(
            assemble_folder, f"Classif_Seed_{seed}_tmp.tif"
        )
        classif_mosaic_compress = os.path.join(
            assemble_folder, f"Classif_Seed_{seed}.tif"
        )
        fu.assembleTile_Merge(
            classification,
            target_spatial_resolution,
            classif_mosaic_tmp,
            output_pixel_type="Byte",
            creation_options={
                "COMPRESS": "LZW",
                "NUM_THREADS": "ALL_CPUS",
                "BIGTIFF": "YES",
            },
        )
        compress_raster(classif_mosaic_tmp, classif_mosaic_compress)
        os.remove(classif_mosaic_tmp)


def get_global_confidence(
    list_tile: List[str],
    confidence_path: str,
    runs: int,
    assemble_folder: str,
    target_spatial_resolution: Tuple[Union[float, int], Union[float, int]],
) -> None:
    """Get all confidence maps and merge them.

    Parameters
    ----------
    list_tile:
        the list of tiles produced
    confidence_path:
        path where confidence maps are stored
    runs:
        the number of random seeds
    assemble_folder:
        path to store the mosaic
    target_spatial_resolution:
        the expected spatial resolution
    """
    list_tile = sorted(list_tile)
    for seed in range(runs):
        confidence = []
        # confidence.append([])
        for tile in list_tile:
            tile_confidence = os.path.join(
                confidence_path, f"{tile}_GlobalConfidence_seed_{seed}.tif"
            )
            if os.path.exists(tile_confidence):
                confidence.append(tile_confidence)
        confidence_mosaic_tmp = os.path.join(
            assemble_folder, f"Confidence_Seed_{seed}_tmp.tif"
        )
        confidence_mosaic_compress = os.path.join(
            assemble_folder, f"Confidence_Seed_{seed}.tif"
        )
        if confidence:
            fu.assembleTile_Merge(
                confidence,
                target_spatial_resolution,
                confidence_mosaic_tmp,
                output_pixel_type="Byte",
                creation_options={
                    "COMPRESS": "LZW",
                    "NUM_THREADS": "ALL_CPUS",
                    "BIGTIFF": "YES",
                },
            )
            compress_raster(confidence_mosaic_tmp, confidence_mosaic_compress)
            os.remove(confidence_mosaic_tmp)


def get_tiled_proba_map(
    proba_path: str,
    runs: int,
    suffix: str,
    assemble_folder: str,
    target_spatial_resolution: Tuple[Union[float, int], Union[float, int]],
) -> None:
    """
    Get all probabilities map and merge them.

    Parameters
    ----------
    proba_path:
        path where probabilities maps are stored
    runs:
        the number of random seeds
    suffix:
        classificatin suffix
    assemble_folder:
        path to store the mosaic
    target_spatial_resolution:
        the expected spatial resolution
    """
    tile_ind = 1
    model_ind = 3
    for seed in range(runs):
        proba_map_list = fu.file_search_reg_ex(
            os.path.join(proba_path, f"PROBAMAP_*_seed_{seed}{suffix}.tif")
        )
        proba_map_list = remove_in_list_by_regex(
            proba_map_list, ".*model_.*f.*_seed." + suffix
        )

        sort = []
        for proba_map_file in proba_map_list:
            sort.append(
                (proba_map_file.split("/")[-1].split("_")[tile_ind], proba_map_file)
            )
        sort = fu.sort_by_first_elem(sort)
        sorted_files = []
        for _, paths in sort:
            sorted_files += sorted(
                paths, key=lambda x: os.path.basename(x).split("_")[model_ind]
            )
        # proba_map.append(proba_map_list)
        print(sorted_files)
        proba_map_mosaic_tmp = os.path.join(
            assemble_folder, f"ProbabilityMap_seed_{seed}_tmp.tif"
        )
        proba_map_mosaic_compress = os.path.join(
            assemble_folder, f"ProbabilityMap_seed_{seed}.tif"
        )
        fu.assembleTile_Merge(
            sorted_files,
            target_spatial_resolution,
            proba_map_mosaic_tmp,
            output_pixel_type="Int16",
            creation_options={
                "COMPRESS": "LZW",
                "NUM_THREADS": "ALL_CPUS",
                "BIGTIFF": "YES",
            },
        )
        compress_raster(proba_map_mosaic_tmp, proba_map_mosaic_compress)
        os.remove(proba_map_mosaic_tmp)


def get_final_cloud(
    cloud_path: str,
    output_path: str,
    list_tile: List[str],
    output_statistics: bool,
    assemble_folder: str,
    target_spatial_resolution: Tuple[Union[int, float], Union[int, float]],
    working_directory: Optional[str] = None,
) -> None:
    """Prepare validity masks to be merged.

    Parameters
    ----------
    cloud_path:
        path to cloud maps
    output_path:
        output path of by tiles products
    list_tile:
        the list of tiles
    assemble_folder:
        path to store the final mosaic
    output_statistics:
        compute additional statistics about cloud covering
    target_spatial_resolution:
        the expected spatial resolution
    working_directory:
        folder to store intermediate results
    """
    working_dir = (
        working_directory if working_directory else os.path.join(output_path, "TMP")
    )
    list_tile = sorted(list_tile)
    # for seed in range(runs):
    cloud = []
    # cloud.append([])
    for tile in list_tile:
        cloud_tile = os.path.join(cloud_path, f"{tile}", "nbView.tif")
        if os.path.exists(cloud_tile):
            classif_tile = os.path.join(output_path, "TMP", f"{tile}_seed_0.tif")
            cloud_tile_priority = f"{tile}_Cloud.tif"
            cloud_tile_priority_stats_ok = f"{tile}_Cloud_StatsOK.tif"
            cloud.append(os.path.join(output_path, "TMP", cloud_tile_priority))
            # if not os.path.exists(
            #         os.path.join(working_dir, cloud_tile_priority)):
            bandmath = otb.CreateBandMathApplication(
                {
                    "il": [cloud_tile, classif_tile],
                    "out": os.path.join(working_dir, cloud_tile_priority),
                    "pixType": "int16",
                    "exp": "im2b1>0?im1b1:0",
                }
            )
            bandmath.ExecuteAndWriteOutput()
            if output_statistics:
                bandmath = otb.CreateBandMathApplication(
                    {
                        "il": [cloud_tile, classif_tile],
                        "out": os.path.join(working_dir, cloud_tile_priority_stats_ok),
                        "pixType": "int16",
                        "exp": "im2b1>0?im1b1:-1",
                    }
                )
                bandmath.ExecuteAndWriteOutput()
                if working_directory:
                    shutil.copy(
                        os.path.join(working_dir, cloud_tile_priority_stats_ok),
                        os.path.join(output_path, "TMP", cloud_tile_priority_stats_ok),
                    )
                    os.remove(os.path.join(working_dir, cloud_tile_priority_stats_ok))
            if working_directory:
                shutil.copy(
                    os.path.join(working_dir, cloud_tile_priority),
                    os.path.join(output_path, "TMP", cloud_tile_priority),
                )
                os.remove(os.path.join(working_dir, cloud_tile_priority))
    cloud_mosaic_tmp = os.path.join(assemble_folder, "PixelsValidity_tmp.tif")
    cloud_mosaic_compress = os.path.join(assemble_folder, "PixelsValidity.tif")
    fu.assembleTile_Merge(
        cloud,
        target_spatial_resolution,
        cloud_mosaic_tmp,
        output_pixel_type="Byte",
        creation_options={
            "COMPRESS": "LZW",
            "NUM_THREADS": "ALL_CPUS",
            "BIGTIFF": "YES",
        },
    )
    compress_raster(cloud_mosaic_tmp, cloud_mosaic_compress)
    os.remove(cloud_mosaic_tmp)


def decode_and_color_index_map(
    raster_classif_seed_path: str,
    re_encoded_raster_path: str,
    labels_conversion: Dict[Union[str, int], Union[str, int]],
    color_path: str,
    logger=LOGGER,
) -> None:
    """
    For an input map decode the labels and produce the colored map.

    Parameters
    ----------
    raster_classif_seed_path:
        path to input classification
    re_encoded_raster_path:
        path to re encoded classification maps
    labels_conversion:
        dictionary to map label conversion
    color_path:
        path to color file
    """
    encoded_raster_bool, pix_type = re_encode_raster(
        raster_classif_seed_path,
        re_encoded_raster_path,
        labels_conversion,
        logger=logger,
    )
    raster_to_color_path = raster_classif_seed_path
    if encoded_raster_bool:
        shutil.move(re_encoded_raster_path, raster_classif_seed_path)
        labels_conversion_seed = None
    else:
        labels_conversion_seed = labels_conversion.copy()

    color_indexed_raster = color.create_indexed_color_image(
        raster_to_color_path,
        color_path,
        output_pix_type=gdal.GDT_Byte if pix_type == "uint8" else gdal.GDT_UInt16,
        labels_conversion=labels_conversion_seed,
    )

    color_indexed_raster_compress = color_indexed_raster.replace(
        ".tif", "_compress.tif"
    )
    compress_raster(color_indexed_raster, color_indexed_raster_compress)
    shutil.copy(color_indexed_raster_compress, color_indexed_raster)
    os.remove(color_indexed_raster_compress)


def provide_input_to_mosaic(
    classif_path_list: List[str],
    final_folder_list: List[str],
    final_tmp_folder_list: List[str],
    ds_sar_opt: bool,
    validation_input_folder: str,
    nb_view_folder,
    runs: int,
    classif_mode: str,
    region_shape: str,
    proba_map_flag: bool,
    output_statistics: bool,
    data_field: str,
    spatial_resolution: Tuple[Union[float, int], Union[float, int]],
    labels_conversion: Dict[int, Union[str, int]],
    color_path: str,
    tiles_from_cfg: List[str],
    working_directory: Optional[str] = None,
    logger=LOGGER,
):
    """Handle the case of several mosaic to produce."""
    for classif_path, final_folder, final_tmp in zip(
        classif_path_list, final_folder_list, final_tmp_folder_list
    ):
        classification_shaping(
            path_classif=classif_path,
            ds_sar_opt=ds_sar_opt,
            validation_input_folder=validation_input_folder,
            final_folder=final_folder,
            final_tmp_folder=final_tmp,
            nb_view_folder=nb_view_folder,
            runs=runs,
            classif_mode=classif_mode,
            region_shape=region_shape,
            proba_map_flag=proba_map_flag,
            output_statistics=output_statistics,
            data_field=data_field,
            spatial_resolution=spatial_resolution,
            labels_conversion=labels_conversion,
            color_path=color_path,
            tiles_from_cfg=tiles_from_cfg,
            working_directory=working_directory,
            logger=logger,
        )
