#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Generate Confusion Matrix."""
import argparse
import logging
import os
import sys
from logging import Logger
from typing import Dict, Tuple, Union

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fu
from iota2.common import otb_app_bank, utils
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")

I2_CONST = i2_const.Iota2Constants()


def run_gen_conf_matrix(
    classif_list, out_csv_list, data_field, ref_vector, ram, labels_table, logger=LOGGER
):
    """Iterate over list to produce confusion matrix.

    This allow to avoid locked sqlite database.
    """
    for classif_in, out_csv in zip(classif_list, out_csv_list):
        gen_conf_matrix(
            in_classif=classif_in,
            out_csv=out_csv,
            data_field=data_field,
            ref_vector=ref_vector,
            ram=ram,
            labels_table=labels_table,
            logger=logger,
        )


def gen_conf_matrix(
    in_classif: str,
    out_csv: str,
    data_field: str,
    ref_vector: str,
    ram: float,
    labels_table: Dict[int, Union[str, int]],
    logger=LOGGER,
) -> None:
    """Generate an otb confusion matrix.

    Parameters
    ----------
    in_classif : string
        input classification file
    out_csv : string
        output csv file
    data_field : string
        data field in the ground truth database
    ref_vector : string
        reference vector file (containing polygons)
    ram : float
        available ram in Mo
    labels_table:
        dictionary to convert labels labels_table[old_label] = new_label
    """
    if os.path.exists(ref_vector):
        re_encode_labels = utils.is_nomenclature_castable_to_int(labels_table)
        if not re_encode_labels:
            data_field = I2_CONST.re_encoding_label_name

        confusion_app = otb_app_bank.CreateConfusionMatrix(
            {
                "in": in_classif,
                "out": out_csv,
                "ref": "vector",
                "ref.vector.field": data_field.lower(),
                "ref.vector.in": ref_vector,
                "ram": ram,
            }
        )
        confusion_app.ExecuteAndWriteOutput()
    else:
        logger.warning(
            f"{ref_vector} does not exists, maybe there is "
            "no validation samples available in the tile"
        )


def confusion_sar_optical_parameter(iota2_dir: str, logger: Logger = LOGGER):
    """Provide the validation parameters.

    return a list of tuple containing the classification and the associated
    shapeFile to compute a confusion matrix

    Parameters
    ----------
    iota2_dir:
        the root iota2 output path
    logger:
        Optional logger instance
    """
    ref_vectors_dir = os.path.join(iota2_dir, "dataAppVal", "bymodels")
    classifications_dir = os.path.join(iota2_dir, "classif")

    vector_seed_pos = 4
    vector_tile_pos = 0
    vector_model_pos = 2
    classif_seed_pos = 5
    classif_tile_pos = 1
    classif_model_pos = 3

    vectors = fu.file_search_and(ref_vectors_dir, True, ".shp")
    classifications = fu.file_search_and(
        classifications_dir, True, "Classif", "model", "seed", ".tif"
    )

    group = []
    for vector in vectors:
        vec_name = os.path.basename(vector)
        seed = vec_name.split("_")[vector_seed_pos]
        tile = vec_name.split("_")[vector_tile_pos]
        model = vec_name.split("_")[vector_model_pos]
        key = (seed, tile, model)
        fields = vf.get_all_fields_in_shape(vector)
        if (
            len(
                vf.get_field_element(
                    vector,
                    driver_name="ESRI Shapefile",
                    field=fields[0],
                    mode="all",
                    elem_type="str",
                )
            )
            != 0
        ):
            group.append((key, vector))
    for classif in classifications:
        classif_name = os.path.basename(classif)
        seed = classif_name.split("_")[classif_seed_pos].split(".tif")[0]
        tile = classif_name.split("_")[classif_tile_pos]
        model = classif_name.split("_")[classif_model_pos]
        key = (seed, tile, model)
        group.append((key, classif))
    # group by keys
    groups_param_buff = [param for key, param in fu.sort_by_first_elem(group)]
    groups_param = []
    # check if all parameter to find are found.
    for group in groups_param_buff:
        if len(group) != 3:
            logger.debug(
                f"all parameter to use Dempster-Shafer fusion, " f"not found : {group}"
            )
        else:
            groups_param.append(group)

    # output
    output_parameters = []
    for param in groups_param:
        for sub_param in param:
            if ".shp" in sub_param:
                ref_vector = sub_param
            elif "SAR.tif" in sub_param:
                classif_sar = sub_param
            elif ".tif" in sub_param and "SAR.tif" not in sub_param:
                classif_opt = sub_param
        output_parameters.append((ref_vector, classif_opt))
        output_parameters.append((ref_vector, classif_sar))

    return output_parameters


def confusion_sar_optical(
    ref_vector_and_classif: Tuple[str, str],
    data_field: str,
    ram: int = 128,
    logger: Logger = LOGGER,
):
    """Compute confusion matrix for Dempster-Shaffer fusion.

    function use to compute a confusion matrix dedicated to the D-S
    classification fusion.

    Parameters
    ----------
    ref_vector :
        tuple containing (reference vector, classification raster)
    dataField :
        labels fields in reference vector
    ram :
        ram dedicated to produce the confusion matrix (OTB's pipeline size)
    LOGGER :
        root logger
    """
    ref_vector, classification = ref_vector_and_classif
    csv_out = ref_vector.replace(".shp", ".csv")
    if "SAR.tif" in classification:
        csv_out = csv_out.replace(".csv", "_SAR.csv")
    if os.path.exists(csv_out):
        os.remove(csv_out)

    confusion_parameters = {
        "in": classification,
        "out": csv_out,
        "ref": "vector",
        "ref.vector.in": ref_vector,
        "ref.vector.field": data_field.lower(),
        "ram": str(0.8 * ram),
    }

    confusion_matrix = otb_app_bank.CreateComputeConfusionMatrixApplication(
        confusion_parameters
    )

    logger.info(f"Launch : {csv_out}")
    confusion_matrix.ExecuteAndWriteOutput()
    logger.debug(f"{csv_out} done")


def main():
    """Provide args to entry_points."""
    parser = argparse.ArgumentParser(
        description="this function create a confusion matrix"
    )
    parser.add_argument(
        "-in_classif",
        help=("path to the classification to validate"),
        dest="in_classif",
        required=True,
    )
    parser.add_argument(
        "-ref_vector",
        help=("path to the vector file which contains validation samples"),
        dest="ref_vector",
        required=True,
    )
    parser.add_argument(
        "-data_field",
        dest="data_field",
        help="Column name for labels in the ref_vector",
        required=True,
        type=str,
    )
    parser.add_argument(
        "-out_csv", dest="out_csv", help="The output confusion matrix", required=True
    )

    parser.add_argument(
        "-ram", help="The amount of RAM (MB) available for OTB", dest="ram", default=256
    )

    args = parser.parse_args()
    labels_table = None
    labels_table = vf.get_reverse_encoding_labels_dic(args.ref_vector, args.data_field)
    gen_conf_matrix(
        in_classif=args.in_classif,
        out_csv=args.out_csv,
        data_field=args.data_field,
        ref_vector=args.ref_vector,
        ram=args.ram,
        labels_table=labels_table,
        logger=LOGGER,
    )


if __name__ == "__main__":
    sys.exit(main())
