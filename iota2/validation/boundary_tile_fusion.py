#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provides function to boundary fusion processing."""
import logging
import os
import shutil
from functools import partial
from typing import Optional

import geopandas as gpd
import numpy as np
import pandas as pd
import rasterio

from iota2.common import otb_app_bank as otb
from iota2.common import raster_utils as ru
from iota2.common.file_utils import file_search_and
from iota2.common.raster_utils import merge_rasters
from iota2.validation import confusion_fusion as cf
from iota2.validation import results_utils as rus

LOGGER = logging.getLogger("distributed.worker")


def take_fusion_decision(
    array: np.ndarray, num_class: int, all_regions: list[int], tile_regions: list[int]
) -> tuple[np.ndarray, list[None]]:
    """Compute the weights for probabilities fusion.

    Parameters
    ----------
    array:
        A numpy array containing all the weights for all regions and the proba.
    num_class:
        The number of classes i.e the dimension of all probabilities vectors.
    all_regions:
        The number of all regions i.e the number of probabilities maps.
    tile_regions:
        The list of regions involved in the tile.
    """
    acc = np.zeros((array.shape[0], array.shape[1], num_class))
    sum_weight = np.zeros((array.shape[0], array.shape[1], 1))
    for i, region in enumerate(tile_regions):
        # N first bands contains weights
        weight = array[:, :, all_regions.index(region)]
        weight = weight[:, :, np.newaxis]
        sum_weight += weight
        # after weights comes the probabilities per class
        proba = array[
            :,
            :,
            len(all_regions)
            + (i * num_class) : len(all_regions)
            + (i * num_class)
            + num_class,
        ]
        acc += weight * proba  # np.where(weight==0, 0, wei)
    # Apply a mask if the sum of proba is 0 (border of image)
    acc = np.where(sum_weight == 0, 0, acc / sum_weight)
    # Empty list is returned to match the format of insert_function_to_pipeline
    return acc, []


def proba_fusion_at_boundaries(
    tile: str,
    distance_map_folder: str,
    probabilities_map_folder: str,
    seed: int,
    boundary_folder_out: str,
    original_region_file: str,
    region_field: str,
    number_of_chunks: int,
    nb_class: int,
    target_chunk: int = 1,
    generate_final_probability_map: bool = False,
    working_directory: Optional[str] = None,
):
    """Process the fusion at boundaries.

    Parameters
    ----------
    tile:
        the processed tile name
    distance_map_folder:
        folder containing the distance maps
    probabilities_map_folder:
        folder containing probabilities maps
    seed:
        the seed targeted
    output_path:
        folder to store fusion result
    original_region_file:
        the region vector file
    region_field:
        the name of region column in vector file
    number_of_chunks:
        the number of chunks used to split a tile to fit RAM
    nb_class:
        the number of classes in the nomenclature
    target_chunk:
        process a particular chunk
    working_directory:
        temporary folder to store results
    """
    wdir = boundary_folder_out
    if working_directory:
        wdir = working_directory
    # Concatenate distance and probabilities maps
    # Find distance map
    all_distances = file_search_and(distance_map_folder, True, tile, "distance_map.tif")

    # Find proba maps
    all_proba = file_search_and(
        probabilities_map_folder, True, "PROBAMAP", tile, f"seed_{seed}", ".tif"
    )
    all_proba.sort(key=lambda x: int(os.path.basename(x).split("_")[3]))
    tile_regions = [int(os.path.basename(x).split("_")[3]) for x in all_proba]
    gdf_reg = gpd.read_file(original_region_file)
    all_regions = gdf_reg[region_field].unique()
    all_regions = [int(x) for x in all_regions]
    all_regions.sort()
    # check if all_proba len is no sup to all_distances bands ?

    tmp_res = os.path.join("", "concat.tif")
    concat = otb.CreateConcatenateImagesApplication(
        {"il": all_distances + all_proba, "out": tmp_res}
    )
    concat.Execute()
    partial_fun = partial(
        take_fusion_decision,
        num_class=nb_class,
        all_regions=all_regions,
        tile_regions=tile_regions,
    )
    (
        mosaic,
        _,
        out_trans,
        epsg_code,
        _,
        _,
        _,
        _,
    ) = ru.insert_external_function_to_pipeline(
        otb_pipelines={
            "interp": concat,
            "raw": concat,
            "masks": concat,
            "enable_interp": True,
            "enable_raw": False,
            "enable_masks": False,
        },
        working_dir=wdir,
        function=partial_fun,
        chunk_config=ru.ChunkConfig("split_number", number_of_chunks, 0, 0),
        targeted_chunk=target_chunk,
    )
    # mosaic contains the accumulated probability map
    # mosaic comes from rasterio then axis are inverted
    mask = np.sum(mosaic, axis=0)
    mask = mask[np.newaxis, :, :]
    mask = np.where(mask == 0, 0, 1)
    # confidence
    conf = np.max(mosaic, axis=0) / 10
    conf = conf[np.newaxis :, :]
    conf = np.where(mask == 0, 0, conf)
    # dec is the choosen class
    dec = np.argmax(mosaic, axis=0)
    dec = dec[np.newaxis, :, :]
    dec2 = np.where(mask == 0, 0, dec + 1)
    classif_path = os.path.join(
        boundary_folder_out, f"classif_boundary_{tile}_seed_{seed}_{target_chunk}.tif"
    )
    proba_path = os.path.join(
        boundary_folder_out, f"proba_boundary_{tile}_seed_{seed}_{target_chunk}.tif"
    )
    conf_path = os.path.join(
        boundary_folder_out,
        f"confidence_boundary_{tile}_seed_{seed}_{target_chunk}.tif",
    )

    if proba_path and generate_final_probability_map:

        with rasterio.open(
            proba_path,
            "w",
            driver="GTiff",
            height=mosaic.shape[1],
            width=mosaic.shape[2],
            count=mosaic.shape[0],
            crs=f"EPSG:{epsg_code}",
            transform=out_trans,
            dtype="uint16",
        ) as dest:
            dest.write(np.where(mask == 0, 0, mosaic).astype("uint16"))

    if conf_path:

        with rasterio.open(
            conf_path,
            "w",
            driver="GTiff",
            height=conf.shape[1],
            width=conf.shape[2],
            count=conf.shape[0],
            crs=f"EPSG:{epsg_code}",
            transform=out_trans,
            dtype="uint8",
        ) as dest:
            dest.write(conf.astype("uint8"))
    with rasterio.open(
        classif_path,
        "w",
        driver="GTiff",
        height=dec2.shape[1],
        width=dec2.shape[2],
        count=dec2.shape[0],
        crs=f"EPSG:{epsg_code}",
        transform=out_trans,
        dtype="uint8",
    ) as dest:
        dest.write(dec2.astype("uint8"))


def merge_map_and_boundary(
    tile: str,
    path_to_chunks: str,
    path_to_env: str,
    seed: int,
    output_path: str,
    epsg_code: int,
    working_directory: Optional[str] = None,
):
    """Merge all boundary products.

    Parameters
    ----------
    tile:
        the tile name
    path_to_chunks:
        folder which contains chunks
    path_to_env:
        envelope folder
    seed:
        the targeted seed
    output_path:
        folder to store results
    epsg_code:
        projection code. ex: 2154
    working_directory:
        temporary folder to store results
    """
    wdir = working_directory if working_directory is not None else output_path
    # output_classif = f"Classif_{tile}_seed_{seed}.tif"
    output_classif = f"Classif_{tile}_seed_{seed}.tif"
    # get all chunks for the boundary
    boundary_map = os.path.join(wdir, f"boundary_map_{tile}_seed_{seed}.tif")

    all_chunks = file_search_and(
        path_to_chunks, True, f"classif_boundary_{tile}", f"seed_{seed}", ".tif"
    )

    # merge them as one file
    merge_rasters(all_chunks, boundary_map, output_type="byte")

    # Apply classification map
    mask = os.path.join(path_to_env, f"{tile}.tif")
    bandmath = otb.CreateBandMathApplication(
        {
            "il": [boundary_map, mask],
            "exp": "im2b1==0?0:im1b1",
            "out": os.path.join(wdir, output_classif),
            "pixType": "uint8",
        }
    )
    bandmath.ExecuteAndWriteOutput()

    # confidence

    output_conf = os.path.join(output_path, f"{tile}_confidence_seed_{seed}.tif")
    # get all chunks for the boundary
    all_chunks_conf = file_search_and(
        path_to_chunks, True, f"confidence_boundary_{tile}", f"seed_{seed}", ".tif"
    )
    # merge them as one file
    merge_rasters(all_chunks_conf, output_conf, output_type="uint16")

    # Proba

    output_proba = os.path.join(output_path, f"PROBAMAP_{tile}_seed_{seed}.tif")
    # get all chunks for the boundary
    all_chunks_proba = file_search_and(
        path_to_chunks, True, f"proba_boundary_{tile}", f"seed_{seed}", ".tif"
    )
    # merge them as one file
    if all_chunks_proba:
        merge_rasters(all_chunks_proba, output_proba, output_type="uint16")

    if working_directory:
        shutil.copy(
            os.path.join(wdir, output_classif),
            os.path.join(output_path, output_classif),
        )


def prepare_boundary_validation_dataset(
    tile: str,
    iota2_directory: str,
    classification_1: str,
    classification_2: str,
    data_field: str,
    seed: int,
    working_directory: Optional[str] = None,
) -> None:
    """Prepare the validation dataset for comparison.

    Parameters
    ----------
    tile:
        Tile name
    iota2_directory:
        Output iota2 folder
    classification_1:
        First classification to compare
    classification_2:
        Second classification to compare
    data_field:
        label column name
    seed:
        number of seed
    working_directory:
        temporary folder to store intermediate results
    """
    output_path = os.path.join(iota2_directory, "final", "TMP")
    wdir = working_directory if working_directory is not None else output_path
    # Get the boundary area
    boundary_area = os.path.join(
        iota2_directory, "features", tile, "tmp", f"{tile}_buffer_area.shp"
    )

    # Get the validation shape file
    ref_shape = os.path.join(iota2_directory, "formattingVectors", f"{tile}.shp")
    gdf_ref = gpd.read_file(ref_shape)
    gdf_boundary = gpd.read_file(boundary_area)
    val_boundary = os.path.join(
        iota2_directory, "final", "TMP", f"{tile}_validation_boundary.shp"
    )
    # compute intersection with geopandas to prevent empty intersection
    # ogr can produce empty file which leads to segfault with OTB
    if not os.path.exists(val_boundary):
        res_intersection = gpd.overlay(gdf_ref, gdf_boundary, how="difference")
        res_intersection = res_intersection.loc[
            res_intersection[f"seed_{seed}"] == "validation"
        ]
        res_intersection = res_intersection.drop("originfid", axis="columns")
        if not res_intersection.empty:
            res_intersection.to_file(val_boundary)

        else:
            print("empty")
            return None
    else:
        res_intersection = gpd.read_file(val_boundary)

    # From polygons extract values
    extract_points = f"Boundary_Validation_points_{tile}_{seed}.gpkg"
    extract_labels = ["cl_1", "cl_2"]
    output_name = os.path.join(iota2_directory, "final", "TMP", extract_points)

    stats_file = os.path.join(wdir, f"{tile}_validation_boundary_stats.xml")
    stats = otb.CreatePolygonClassStatisticsApplication(
        {
            "in": classification_1,
            "out": stats_file,
            "vec": val_boundary,
            "field": data_field,
        }
    )
    stats.ExecuteAndWriteOutput()
    select_file = os.path.join(wdir, f"{tile}_validation_boundary_selection.shp")
    select = otb.CreateSampleSelectionApplication(
        {
            "in": classification_1,
            "out": select_file,
            "vec": val_boundary,
            "instats": stats_file,
            "sampler": "periodic",
            "strategy": "all",
            "field": data_field,
        }
    )
    select.ExecuteAndWriteOutput()
    concat = otb.CreateConcatenateImagesApplication(
        {"il": [classification_1, classification_2], "out": "fake_out.tif"}
    )
    concat.Execute()
    extract = otb.CreateSampleExtractionApplication(
        {
            "in": concat,
            "out": output_name,
            "vec": select_file,
            "field": data_field,
            "outfield": "list",
            "outfield.list.names": extract_labels,
        }
    )
    extract.ExecuteAndWriteOutput()
    return None


def merge_metrics_matrices(
    iota2_directory: str,
    data_field: str,
    seeds: int,
    nomenclature_path: str,
    logger=LOGGER,
):
    """Compute conditional matrices for boundary comparison.

    Parameters
    ----------
    iota2_directory:
        iota2 output folder
    data_field:
        label column name in reference data
    seeds:
        the number of seeds
    nomenclature_path:
        nomenclature file
    """
    cond_val_bound = []
    cond_val_standard = []
    val_bound = []
    val_standard = []

    for seed in range(seeds):
        all_validation_points = file_search_and(
            os.path.join(iota2_directory, "final", "TMP"),
            True,
            "Boundary_Validation_points_",
            f"{seed}.gpkg",
        )
        output_path = os.path.join(iota2_directory, "final")
        points_valid = gpd.GeoDataFrame(
            pd.concat(
                [gpd.read_file(i) for i in all_validation_points], ignore_index=True
            ),
            crs=gpd.read_file(all_validation_points[0]).crs,
        )
        if points_valid.empty:
            print(
                f"No valid points in {all_validation_points}."
                "Matrices can't be computed"
            )
            logger.info(
                f"No valid points in {all_validation_points}."
                "Matrices can't be computed"
            )
            continue
        # conf(M1|Y=Yref, M2)
        output_name = os.path.join(
            output_path,
            "Confusion_conditional_classification" f"_boundary_seed_{seed}.csv",
        )
        df_r1_true = points_valid[points_valid[data_field] == points_valid["cl_1"]]
        if df_r1_true.empty:
            logger.info(f"No valid points are found for {output_name}")
            print(f"No valid points are found for {output_name}")
        else:
            compute_metrics_with_ref(
                df_r1_true, output_name, ref_field=data_field, target_field="cl_2"
            )
            rus.stats_report(
                [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
            )
            cond_val_bound.append(output_name)
        # conf(M2|Y=Yref, M1)
        output_name = os.path.join(
            output_path,
            "Confusion_conditional_classification_" f"standard_seed_{seed}.csv",
        )
        df_r2_true = points_valid[points_valid[data_field] == points_valid["cl_2"]]
        if df_r2_true.empty:
            logger.info(f"No valid points are found for {output_name}")
            print(f"No valid points are found for {output_name}")
        else:
            compute_metrics_with_ref(
                df_r2_true, output_name, ref_field=data_field, target_field="cl_1"
            )
            rus.stats_report(
                [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
            )
            cond_val_standard.append(output_name)
        # conf(M1,Yref)
        output_name = os.path.join(
            output_path, "Confusion_classification_standard" f"_seed_{seed}.csv"
        )
        cross_tab = compute_metrics_with_ref(
            points_valid, output_name, ref_field=data_field, target_field="cl_1"
        )
        if cross_tab.empty:
            logger.info(f"Empty validation set {output_name}")
        else:
            rus.stats_report(
                [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
            )
            val_standard.append(output_name)
        # conf(M2,Yref)
        output_name = os.path.join(
            output_path, "Confusion_classification_boundary" f"_seed_{seed}.csv"
        )
        cross_tab = compute_metrics_with_ref(
            points_valid, output_name, ref_field=data_field, target_field="cl_2"
        )
        if cross_tab.empty:
            logger.info(f"Empty validation set {output_name}")
        else:
            rus.stats_report(
                [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
            )
            val_bound.append(output_name)

    if cond_val_standard:
        rus.stats_report(
            cond_val_standard,
            nomenclature_path,
            os.path.join(
                output_path, "Confusion_conditional_classification_standard.txt"
            ),
        )
        rus.stats_report(
            val_standard,
            nomenclature_path,
            os.path.join(output_path, "Confusion_classification_standard.txt"),
        )
    if cond_val_bound:
        rus.stats_report(
            cond_val_bound,
            nomenclature_path,
            os.path.join(
                output_path, "Confusion_conditional_classification_boundary.txt"
            ),
        )
        rus.stats_report(
            val_bound,
            nomenclature_path,
            os.path.join(output_path, "Confusion_classification_boundary.txt"),
        )


def compute_metrics_with_ref(
    points_valid: pd.DataFrame, output_path: str, ref_field: str, target_field: str
):
    """Extract labels from a dataframe and create confusion matrix.

    Parameters
    ----------
    points_valid:
        The datafrale containing labels.
    output_path:
        Name of the output csv file
    ref_field:
        The reference field name
    target_field:
        The target field name
    """
    cross_tab = pd.crosstab(
        points_valid[ref_field].astype(int), points_valid[target_field].astype(int)
    )
    ref_lab = [int(x) for x in points_valid[ref_field].unique()]
    pred_lab = [int(x) for x in points_valid[target_field].unique()]
    all_labels = list(set(ref_lab + pred_lab))
    dict_tab = cross_tab.to_dict()
    convert_dict_to_confusion_matrix(all_labels, dict_tab, output_path)
    return cross_tab


def convert_dict_to_confusion_matrix(
    all_labels: list[int], dict_tab: dict[int, int], output_name: str
):
    """Convert a dictionnary to a confusion martix array.

    Parameters
    ----------
    all_labels:
        The list of all labels in the nomenclature
    dict_tab:
        A dictionnary containing the confusion matrix
    output_name:
        The name of the output csv
    """
    matrix = np.zeros((len(all_labels), len(all_labels)))
    # Note that labels are already decoded in both maps
    all_labels_index = range(1, len(all_labels) + 1)
    for i in all_labels_index:  # row
        for j in all_labels_index:  # cols

            if i in dict_tab.keys():
                ss_dict = dict_tab[i]
                if j in ss_dict.keys():
                    matrix[j - 1, i - 1] += ss_dict[j]
    cf.write_csv(matrix, all_labels, output_name)
