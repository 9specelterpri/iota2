#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging
import os
import shutil
from collections import Counter, OrderedDict
from functools import partial
from typing import Optional

from osgeo import ogr

import iota2.common.i2_constants as i2_const
from iota2.common.file_utils import file_search_and, sort_by_first_elem
from iota2.common.iota2_directory import generate_directories
from iota2.common.utils import run
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.sections.cfg_utils import ConfigError
from iota2.sequence_builders.i2_sequence_builder import (
    check_databases_consistency,
    check_input_text_files,
    check_sensors_data,
    check_tiled_exogenous_data,
    i2_builder,
)
from iota2.steps.iota2_step import Step, StepContainer
from iota2.vector_tools.vector_functions import (
    get_field_element,
    get_fields,
    get_layer_name,
    get_re_encoding_labels_dic,
)

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()


class i2_classification(i2_builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """

    def __init__(
        self,
        cfg: str,
        config_resources: str,
        schduler_type: str,
        restart: bool = False,
        tasks_states_file: Optional[str] = None,
        hpc_working_directory: str = "TMPDIR",
    ):
        super().__init__(
            cfg,
            config_resources,
            schduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )

        self.config_resources = config_resources
        # create dict for control variables
        self.control_var = {}
        # steps definitions
        # self.steps_group = OrderedDict()

        self.steps_group["init"] = OrderedDict()
        self.steps_group["sampling"] = OrderedDict()
        self.steps_group["dimred"] = OrderedDict()
        self.steps_group["learning"] = OrderedDict()
        self.steps_group["classification"] = OrderedDict()
        self.steps_group["mosaic"] = OrderedDict()
        self.steps_group["validation"] = OrderedDict()

        # build steps
        self.sort_step()

    def get_dir(self):
        """
        usage : return iota2_directories
        """
        directories = [
            "classif",
            "config_model",
            "dataRegion",
            "envelope",
            "formattingVectors",
            "metaData",
            "samplesSelection",
            "stats",
            "dataAppVal",
            "dimRed",
            "final",
            "learningSamples",
            "model",
            "shapeRegion",
            "features",
        ]

        iota2_outputs_dir = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )

        return [os.path.join(iota2_outputs_dir, d) for d in directories]

    def models_distribution(self):
        """function to determine which models intersect which tiles
        by using intermediate iota2 results
        """
        formatting_dir = os.path.join(self.output_i2_directory, "formattingVectors")
        formatting_files = file_search_and(formatting_dir, True, ".shp")
        tiles = []
        regions_tiles = []
        for formatting_file in formatting_files:
            tile_name = os.path.splitext(os.path.basename(formatting_file))[0]
            regions_tile = get_field_element(
                formatting_file, field=self.region_field, mode="unique", elem_type="str"
            )
            if tile_name not in tiles:
                tiles.append(tile_name)
            for region in regions_tile:
                regions_tiles.append((region, tile_name))
        regions = sort_by_first_elem(regions_tiles)
        regions_distrib = {}
        for region_name, region_tiles in dict(regions).items():
            regions_distrib[region_name] = {"tiles": region_tiles}
        if not tiles:
            raise ValueError(
                "The chain stopped prematurely. Please check logs in"
                f"'{self.log_dir}'"
            )
        Step.set_models_spatial_information(tiles, regions_distrib)

        # manage tiles to classify
        shape_region_dir = os.path.join(self.output_i2_directory, "shapeRegion")
        region_tiles_files = file_search_and(shape_region_dir, False, ".shp")
        regions_to_classify = []
        for region_tile in region_tiles_files:
            region = region_tile.split("_")[-2]
            tile = region_tile.split("_")[-1]
            regions_to_classify.append((region, tile))
        regions_to_classify = sort_by_first_elem(regions_to_classify)
        dico_regions_to_classify = {}
        for region, tiles in regions_to_classify:
            dico_regions_to_classify[region] = {"tiles": tiles}

        regions_classif_distrib = {}

        for region_name, dico_tiles in regions_distrib.items():
            regions_classif_distrib[region_name] = {
                "tiles": dico_regions_to_classify[region_name.split("f")[0]]["tiles"]
            }
            if "nb_sub_model" in dico_tiles:
                regions_classif_distrib[region_name]["nb_sub_model"] = dico_tiles[
                    "nb_sub_model"
                ]

        regions_classif_distrib_no_split = {}
        for (
            region,
            region_meta,
        ) in Step.spatial_models_distribution_no_sub_splits.copy().items():
            regions_classif_distrib_no_split[region] = {
                "nb_sub_model": region_meta["nb_sub_model"],
                "tiles": dico_regions_to_classify[region]["tiles"].copy(),
            }
        Step.spatial_models_distribution_classify = regions_classif_distrib
        Step.spatial_models_distribution_no_sub_splits_classify = (
            regions_classif_distrib_no_split
        )

    def formatting_reference_data(self):
        """ """
        ref_data = self.control_var["ground_truth"]
        ref_data_field = self.control_var["data_field"]
        ref_data_i2_formatted = os.path.join(
            self.control_var["iota2_outputs_dir"], "reference_data.shp"
        )
        self.ref_data_formatting(ref_data, ref_data_field, ref_data_i2_formatted)

    def ref_data_formatting(
        self, ref_data: str, ref_data_field: str, ref_data_i2_formatted: str
    ) -> None:
        """re-encode reference data

        Parameters
        ----------
        ref_data
            input reference database
        ref_data_field
            column containing labels database
        ref_data_i2_formatted
            output formatted database
        """
        i2_label_column = I2_CONST.re_encoding_label_name
        ref_data_fields = get_fields(ref_data)
        if i2_label_column in ref_data_fields:
            raise ValueError(
                f"the reference data : {ref_data} contains the field"
                f" {i2_label_column}. Please rename the column name"
            )
        lut_labels = get_re_encoding_labels_dic(ref_data, ref_data_field)

        user_fields = get_fields(ref_data)
        layer_name = get_layer_name(ref_data)
        lower_clause = ",".join(
            [f"{user_field} AS {user_field.lower()}" for user_field in user_fields]
        )
        run(
            f"ogr2ogr {ref_data_i2_formatted} {ref_data} "
            f"-sql 'SELECT {lower_clause} FROM {layer_name}'"
        )

        ds = ogr.Open(ref_data_i2_formatted)
        driver = ds.GetDriver()
        data_src = driver.Open(ref_data_i2_formatted, 1)
        layer = data_src.GetLayer()
        layer.CreateField(ogr.FieldDefn(i2_label_column, ogr.OFTInteger))
        for feat in layer:
            feat.SetField(i2_label_column, lut_labels[feat.GetField(ref_data_field)])
            layer.SetFeature(feat)

    def build_steps(
        self, cfg: str, config_ressources: Optional[str] = None
    ) -> list[StepContainer]:
        """
        build steps
        """
        Step.set_models_spatial_information(self.tiles, {})

        # control variable
        self.init_dict_control_variables()

        # will contains all IOTA² steps
        s_container_pre_processing = StepContainer(name="preprocessing")
        s_container_pre_processing.prelaunch_function = partial(
            self.formatting_reference_data
        )
        s_container_init = StepContainer(name="classifications_init")
        if self.i2_cfg_params.getParam("external_features", "functions"):
            s_container_init.prelaunch_function = self.test_user_feature_with_fake_data

        s_container_spatial_distrib = StepContainer(
            name="classifications_spatial_distrib"
        )
        s_container_spatial_distrib.prelaunch_function = partial(
            self.models_distribution
        )

        # build chain
        # init steps
        self.init_step_group(s_container_pre_processing, s_container_init)
        # sampling steps
        self.sampling_step_group(s_container_init, s_container_spatial_distrib)
        # learning steps
        self.learning_step_group(s_container_spatial_distrib)

        # classifications
        self.classification_step_group(s_container_spatial_distrib)

        # mosaic step
        self.mosaic_step_group(s_container_spatial_distrib)

        # validation steps
        self.validation_steps_group(s_container_spatial_distrib)
        return [
            s_container_pre_processing,
            s_container_init,
            s_container_spatial_distrib,
        ]

    def pre_check(self) -> None:
        """Perform some checks on data provided by the user."""
        params = rcf.iota2_parameters(self.i2_cfg_params)
        check_sensors_data(
            params,
            self.__class__.__name__,
            self.i2_cfg_params.getParam("chain", "minimum_required_dates"),
            self.i2_cfg_params.getParam("sensors_data_interpolation", "use_gapfilling"),
            self.i2_cfg_params.getParam("arg_train", "features_from_raw_dates"),
        )
        check_databases_consistency(
            self.i2_cfg_params.getParam("chain", "ground_truth"),
            self.i2_cfg_params.getParam("chain", "data_field"),
            self.i2_cfg_params.getParam("chain", "region_path"),
            self.i2_cfg_params.getParam("chain", "region_field"),
        )
        check_input_text_files(
            self.i2_cfg_params.getParam("chain", "ground_truth"),
            self.i2_cfg_params.getParam("chain", "data_field"),
            self.i2_cfg_params.getParam("chain", "nomenclature_path"),
            self.i2_cfg_params.getParam("chain", "color_table"),
        )
        check_tiled_exogenous_data(
            self.i2_cfg_params.getParam("external_features", "exogeneous_data"),
            self.i2_cfg_params.getParam("chain", "list_tile"),
        )
        self.check_config_parameters()

    def check_config_parameters(self) -> None:
        """Check parameters consistency across the whole iota2's cfg file."""
        # parameters compatibilities check
        classier_probamap_avail = ["sharkrf"]
        neural_network_params = self.i2_cfg_params.getParam(
            "arg_train", "deep_learning_parameters"
        )
        nn_enable = "dl_name" in neural_network_params

        if (
            self.i2_cfg_params.getParam("arg_classification", "enable_probability_map")
            is True
            and self.i2_cfg_params.getParam("arg_train", "classifier")
            and (
                self.i2_cfg_params.getParam("arg_train", "classifier").lower()
                not in classier_probamap_avail
                and not nn_enable
            )
        ):
            raise ConfigError(
                "'enable_probability_map:True' only available with "
                "the 'sharkrf' and pytorch classifiers"
            )

        if (
            self.i2_cfg_params.getParam("chain", "region_path") is None
            and self.i2_cfg_params.getParam("arg_classification", "classif_mode")
            == "fusion"
        ):
            raise ConfigError(
                "you can't chose 'one_region' mode and ask a fusion of "
                "classifications\n"
            )
        if (
            self.i2_cfg_params.getParam(
                "arg_classification", "merge_final_classifications"
            )
            and self.i2_cfg_params.getParam("arg_train", "runs") == 1
        ):
            raise ConfigError(
                "these parameters are incompatible runs:1 and"
                " merge_final_classifications:True"
            )
        if (
            self.i2_cfg_params.getParam("arg_train", "split_ground_truth") is False
            and self.i2_cfg_params.getParam("arg_train", "runs") != 1
        ):
            raise ConfigError(
                "these parameters are incompatible split_ground_truth : False and "
                "runs different from 1"
            )
        if (
            self.i2_cfg_params.getParam(
                "arg_classification", "merge_final_classifications"
            )
            and self.i2_cfg_params.getParam("arg_train", "split_ground_truth") is False
        ):
            raise ConfigError(
                "these parameters are incompatible merge_final_classifications"
                ":True and split_ground_truth : False"
            )
        if self.i2_cfg_params.getParam(
            "arg_train", "dempster_shafer_sar_opt_fusion"
        ) and "None" in self.i2_cfg_params.getParam("chain", "s1_path"):
            raise ConfigError(
                "these parameters are incompatible dempster_shafer_SAR"
                "_Opt_fusion : True and s1_path : 'None'"
            )
        if (
            self.i2_cfg_params.getParam("arg_train", "dempster_shafer_sar_opt_fusion")
            and self.i2_cfg_params.getParam("chain", "user_feat_path") is None
            and self.i2_cfg_params.getParam("chain", "l5_path_old") is None
            and self.i2_cfg_params.getParam("chain", "l8_path") is None
            and self.i2_cfg_params.getParam("chain", "l8_path_old") is None
            and self.i2_cfg_params.getParam("chain", "s2_path") is None
            and self.i2_cfg_params.getParam("chain", "s2_s2c_path") is None
        ):
            raise ConfigError(
                "to perform post-classification fusion, optical data must be " "used"
            )
        if self.i2_cfg_params.getParam(
            "scikit_models_parameters", "model_type"
        ) is not None and self.i2_cfg_params.getParam(
            "external_features", "external_features_flag"
        ):
            raise ConfigError(
                "these parameters are incompatible external_features "
                "and scikit_models_parameters"
            )
        if self.i2_cfg_params.getParam(
            "external_features", "external_features_flag"
        ) and self.i2_cfg_params.getParam("arg_train", "enable_autocontext"):
            raise ConfigError(
                "these parameters are incompatible external_features "
                "and enable_autocontext"
            )
        self.exclusive_classification_workflow()

    def exclusive_classification_workflow(self) -> None:
        """Check if the user is asking only one classifier."""
        auto_context_enable = bool(
            self.i2_cfg_params.getParam("arg_train", "enable_autocontext")
        )
        otb_classifier_enable = bool(
            self.i2_cfg_params.getParam("arg_train", "classifier")
        )
        scikit_enable = bool(
            self.i2_cfg_params.getParam("scikit_models_parameters", "model_type")
        )

        deep_enable = False
        if self.i2_cfg_params.getParam("arg_train", "deep_learning_parameters"):
            deep_enable = bool(
                self.i2_cfg_params.getParam("arg_train", "deep_learning_parameters")[
                    "dl_name"
                ]
            )

        classifiers = [
            ("deep learning", deep_enable),
            ("auto-context", auto_context_enable),
            ("otb", otb_classifier_enable),
            ("scikit-learn", scikit_enable),
        ]
        if not any(enable for _, enable in classifiers):
            raise ConfigError(
                "No classifier detected in the configuration file, please set one."
            )
        count = Counter([enable for _, enable in classifiers])
        if count[True] > 1:
            classifiers_detected = []
            for classifier_name, enable in classifiers:
                if enable:
                    classifiers_detected.append(classifier_name)
            raise ConfigError(
                "Multiple classifiers detected please choose one among :"
                f" {', '.join(classifiers_detected)}"
            )

    def init_dict_control_variables(self):
        # control variable
        self.control_var["tile_list"] = self.i2_cfg_params.getParam(
            "chain", "list_tile"
        ).split(" ")
        self.control_var["check_inputs"] = self.i2_cfg_params.getParam(
            "chain", "check_inputs"
        )
        self.control_var["Sentinel1"] = self.i2_cfg_params.getParam("chain", "s1_path")
        self.control_var["shapeRegion"] = self.i2_cfg_params.getParam(
            "chain", "region_path"
        )
        self.control_var["classif_mode"] = self.i2_cfg_params.getParam(
            "arg_classification", "classif_mode"
        )
        self.control_var["sample_management"] = self.i2_cfg_params.getParam(
            "arg_train", "sample_management"
        )
        self.control_var["sample_augmentation"] = dict(
            self.i2_cfg_params.getParam("arg_train", "sample_augmentation")
        )
        self.control_var["sample_augmentation_flag"] = self.control_var[
            "sample_augmentation"
        ]["activate"]
        self.control_var["dimred"] = self.i2_cfg_params.getParam("dim_red", "dim_red")
        self.control_var["classifier"] = self.i2_cfg_params.getParam(
            "arg_train", "classifier"
        )
        self.control_var["ds_sar_opt"] = self.i2_cfg_params.getParam(
            "arg_train", "dempster_shafer_sar_opt_fusion"
        )
        self.control_var["keep_runs_results"] = self.i2_cfg_params.getParam(
            "arg_classification", "keep_runs_results"
        )
        self.control_var["merge_final_classifications"] = self.i2_cfg_params.getParam(
            "arg_classification", "merge_final_classifications"
        )
        self.control_var["ground_truth"] = self.i2_cfg_params.getParam(
            "chain", "ground_truth"
        )
        self.control_var["runs"] = self.i2_cfg_params.getParam("arg_train", "runs")
        self.control_var["data_field"] = self.i2_cfg_params.getParam(
            "chain", "data_field"
        )
        self.control_var["outStat"] = self.i2_cfg_params.getParam(
            "chain", "output_statistics"
        )
        self.control_var["VHR"] = self.i2_cfg_params.getParam(
            "coregistration", "vhr_path"
        )
        self.control_var["gridsize"] = self.i2_cfg_params.getParam(
            "simplification", "gridsize"
        )
        self.control_var["umc1"] = self.i2_cfg_params.getParam("simplification", "umc1")
        self.control_var["umc2"] = self.i2_cfg_params.getParam("simplification", "umc2")
        self.control_var["rssize"] = self.i2_cfg_params.getParam(
            "simplification", "rssize"
        )
        self.control_var["inland"] = self.i2_cfg_params.getParam(
            "simplification", "inland"
        )
        self.control_var["iota2_outputs_dir"] = self.i2_cfg_params.getParam(
            "chain", "output_path"
        )
        self.control_var["use_scikitlearn"] = (
            self.i2_cfg_params.getParam("scikit_models_parameters", "model_type")
            is not None
        )
        self.control_var["nomenclature"] = self.i2_cfg_params.getParam(
            "simplification", "nomenclature"
        )
        self.control_var["enable_autocontext"] = self.i2_cfg_params.getParam(
            "arg_train", "enable_autocontext"
        )
        self.control_var["enable_external_features"] = self.i2_cfg_params.getParam(
            "external_features", "external_features_flag"
        )
        self.control_var["remove_output_path"] = self.i2_cfg_params.getParam(
            "chain", "remove_output_path"
        )
        self.control_var["enable_neural_network"] = self.i2_cfg_params.getParam(
            "arg_train", "deep_learning_parameters"
        )
        self.control_var["features_from_raw_dates"] = self.i2_cfg_params.getParam(
            "arg_train", "features_from_raw_dates"
        )
        self.control_var["sampling_validation"] = self.i2_cfg_params.getParam(
            "arg_train", "sampling_validation"
        )
        self.control_var["enable_boundary_fusion"] = self.i2_cfg_params.getParam(
            "arg_classification", "enable_boundary_fusion"
        )
        self.control_var["comparison_mode"] = self.i2_cfg_params.getParam(
            "arg_classification", "boundary_comparison_mode"
        )

    def init_step_group(
        self, pre_processing_steps: StepContainer, init_steps: StepContainer
    ) -> None:
        """
        Initialize initialisation steps
        """
        from iota2.steps import (
            check_inputs_step,
            common_masks,
            pixel_validity,
            sensors_preprocess,
            slic_segmentation,
        )

        if self.control_var["check_inputs"]:
            pre_processing_steps.append(
                partial(
                    check_inputs_step.CheckInputsClassifWorkflow,
                    self.cfg,
                    self.config_resources,
                ),
                "init",
            )
        pre_processing_steps.append(
            partial(
                sensors_preprocess.SensorsPreprocess,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "init",
        )
        if self.control_var["VHR"]:
            raise ValueError(
                "You are trying to use the "
                "coregistration module which is curently disabled"
                "Look at https://framagit.org/iota2-project/iota2/-/issues/463"
                " for more information"
            )
            # s_container.append(
            #     partial(coregistration.Coregistration, self.cfg,
            #             self.config_resources, self.workingDirectory), "init")
        init_steps.append(
            partial(
                common_masks.CommonMasks,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "init",
        )
        init_steps.append(
            partial(
                pixel_validity.PixelValidity,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "init",
        )
        if self.control_var["enable_autocontext"]:
            init_steps.append(
                partial(
                    slic_segmentation.SlicSegmentation,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "init",
            )

    def sampling_step_group(
        self, first_container: StepContainer, second_container: StepContainer
    ):
        """
        Declare each step for samples management
        """
        from iota2.steps import (
            copy_samples,
            envelope,
            gen_region_vector,
            gen_synthetic_samples,
            samples_by_models,
            samples_by_tiles,
            samples_dim_reduction,
            samples_extraction,
            samples_merge,
            sampling_learning_polygons,
            split_samples,
            stats_samples_model,
            super_pix_pos,
            super_pix_split,
            vector_formatting,
        )

        first_container.append(
            partial(
                envelope.Envelope,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )
        if not self.control_var["shapeRegion"]:
            first_container.append(
                partial(
                    gen_region_vector.GenRegionVector,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "sampling",
            )
        first_container.append(
            partial(
                vector_formatting.VectorFormatting,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )
        if (
            self.control_var["shapeRegion"]
            and self.control_var["classif_mode"] == "fusion"
        ):
            first_container.append(
                partial(
                    split_samples.SplitSamples,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "sampling",
            )

        second_container.append(
            partial(
                samples_merge.SamplesMerge,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )
        second_container.append(
            partial(
                stats_samples_model.StatsSamplesModel,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )
        second_container.append(
            partial(
                sampling_learning_polygons.SamplingLearningPolygons,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )
        if self.control_var["enable_autocontext"]:
            second_container.append(
                partial(
                    super_pix_pos.SuperPixPos,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "sampling",
            )
        second_container.append(
            partial(
                samples_by_tiles.SamplesByTiles,
                self.cfg,
                self.config_resources,
                self.control_var["enable_autocontext"],
                self.workingDirectory,
            ),
            "sampling",
        )
        second_container.append(
            partial(
                samples_extraction.SamplesExtraction,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )
        if not self.control_var["enable_autocontext"]:
            second_container.append(
                partial(
                    samples_by_models.SamplesByModels, self.cfg, self.config_resources
                ),
                "sampling",
            )
            transfert_samples = False
            if (
                self.control_var["sample_management"]
                and self.control_var["sample_management"].lower() != "none"
            ):
                transfert_samples = True
                second_container.append(
                    partial(
                        copy_samples.CopySamples,
                        self.cfg,
                        self.config_resources,
                        self.workingDirectory,
                    ),
                    "sampling",
                )
            if self.control_var["sample_augmentation_flag"]:
                second_container.append(
                    partial(
                        gen_synthetic_samples.GenSyntheticSamples,
                        self.cfg,
                        self.config_resources,
                        transfert_samples,
                        self.workingDirectory,
                    ),
                    "sampling",
                )
            if self.control_var["dimred"]:
                second_container.append(
                    partial(
                        samples_dim_reduction.SamplesDimReduction,
                        self.cfg,
                        self.config_resources,
                        transfert_samples
                        and not self.control_var["sample_augmentation_flag"],
                        self.workingDirectory,
                    ),
                    "sampling",
                )
        else:
            second_container.append(
                partial(
                    super_pix_split.SuperPixSplit,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "sampling",
            )

    def learning_step_group(self, s_container: StepContainer):
        """Initialize learning step."""
        from iota2.steps import learn_model

        s_container.append(
            partial(
                learn_model.LearnModel,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "learning",
        )

    def classification_step_group(self, s_container: StepContainer):
        """Initialize classification steps."""
        from iota2.steps import (
            classification,
            classification_fusion,
            confusion_sar_opt,
            confusion_sar_opt_merge,
            fusions_indecisions,
            model_choice,
            sar_opt_fusion,
            sk_classifications_merge,
        )

        deep_learning_enabled = "dl_name" in self.control_var["enable_neural_network"]
        if self.control_var["features_from_raw_dates"] or deep_learning_enabled:
            s_container.append(
                partial(model_choice.ModelChoice, self.cfg, self.config_resources),
                "classification",
            )

        s_container.append(
            partial(
                classification.Classification,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "classification",
        )
        if (
            self.control_var["use_scikitlearn"]
            or self.control_var["enable_external_features"]
            or deep_learning_enabled
            or self.control_var["features_from_raw_dates"]
        ):
            s_container.append(
                partial(
                    sk_classifications_merge.ScikitClassificationsMerge,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "classification",
            )
        if self.control_var["ds_sar_opt"]:
            s_container.append(
                partial(
                    confusion_sar_opt.ConfusionSAROpt,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "classification",
            )
            s_container.append(
                partial(
                    confusion_sar_opt_merge.ConfusionSAROptMerge,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "classification",
            )
            s_container.append(
                partial(
                    sar_opt_fusion.SAROptFusion,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "classification",
            )
        if (
            self.control_var["classif_mode"] == "fusion"
            and self.control_var["shapeRegion"]
        ):
            s_container.append(
                partial(
                    classification_fusion.ClassificationsFusion,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "classification",
            )
            s_container.append(
                partial(
                    fusions_indecisions.FusionsIndecisions,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "classification",
            )

    def mosaic_step_group(self, s_container: StepContainer):
        """
        Initialize mosaic step
        """
        from iota2.steps import (
            merge_classification_boundary,
            mosaic,
            prob_boundary_fusion,
        )

        # fuse at boundary steps
        if self.control_var["enable_boundary_fusion"]:
            s_container.append(
                partial(
                    prob_boundary_fusion.ProbBoundaryFusion,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "mosaic",
            )
            s_container.append(
                partial(
                    merge_classification_boundary.MergeClassificationBoundary,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "mosaic",
            )
        s_container.append(
            partial(
                mosaic.Mosaic, self.cfg, self.config_resources, self.workingDirectory
            ),
            "mosaic",
        )

    def validation_steps_group(self, s_container: StepContainer):
        """Initialize validation steps."""
        from iota2.steps import (
            confusion_cmd,
            confusions_merge,
            generate_boundary_reports,
            merge_seed_classifications,
            produce_boundary_validation,
            report_generation,
        )

        s_container.append(
            partial(
                confusion_cmd.ConfusionCmd,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "validation",
        )
        if self.control_var["keep_runs_results"]:
            s_container.append(
                partial(
                    confusions_merge.ConfusionsMerge,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "validation",
            )
            s_container.append(
                partial(
                    report_generation.ReportGeneration,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "validation",
            )
            # only if comparison mode is enabled
            if self.control_var["comparison_mode"]:
                s_container.append(
                    partial(
                        produce_boundary_validation.ProduceBoundaryValidation,
                        self.cfg,
                        self.config_resources,
                        self.workingDirectory,
                    ),
                    "validation",
                )
                s_container.append(
                    partial(
                        generate_boundary_reports.GenerateBoundaryReports,
                        self.cfg,
                        self.config_resources,
                    ),
                    "validation",
                )

        if (
            self.control_var["merge_final_classifications"]
            and self.control_var["runs"] > 1
        ):
            s_container.append(
                partial(
                    merge_seed_classifications.MergeSeedClassifications,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "validation",
            )

    def get_outputs_data(self) -> dict[str, str]:
        data = {}
        data["mosaic_classifications"] = {}
        data["mosaic_confidence"] = {}
        for seed in range(self.control_var["runs"]):
            data["mosaic_classifications"][seed] = os.path.join(
                self.control_var["iota2_outputs_dir"],
                "final",
                f"Classif_Seed_{seed}.tif",
            )
            data["mosaic_confidence"][seed] = os.path.join(
                self.control_var["iota2_outputs_dir"],
                "final",
                f"Confidence_Seed_{seed}.tif",
            )
        data["mosaic_validity"] = os.path.join(
            self.control_var["iota2_outputs_dir"], "final", "PixelsValidity.tif"
        )
        if self.control_var["merge_final_classifications"]:
            data["mosaic_fusion_of_classifications"] = os.path.join(
                self.control_var["iota2_outputs_dir"],
                "final",
                "Classifications_fusion.tif",
            )
        return data

    def generate_output_directories(
        self, first_step_index: int, restart: bool, builder_index: int = 0
    ):
        """ """
        i2_output_dir = self.control_var["iota2_outputs_dir"]
        rm_if_exists = self.control_var["remove_output_path"]

        task_status_file = os.path.join(
            i2_output_dir, I2_CONST.i2_tasks_status_filename
        )

        restart = restart and os.path.exists(task_status_file)
        rm_if_exists = rm_if_exists and os.path.exists(i2_output_dir)
        if rm_if_exists and first_step_index != 0 or restart:
            pass
        elif builder_index == 0 and rm_if_exists:
            shutil.rmtree(i2_output_dir)
        if restart:
            pass
        elif (first_step_index == 0 and not restart) or not os.path.exists(
            i2_output_dir
        ):
            generate_directories(
                i2_output_dir,
                self.control_var["merge_final_classifications"],
                self.control_var["tile_list"],
                self.control_var["enable_boundary_fusion"],
                self.control_var["comparison_mode"],
            )

        else:
            pass
