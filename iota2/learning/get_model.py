# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import argparse
import logging

from iota2.common import file_utils as fu

LOGGER = logging.getLogger("distributed.worker")


def get_region_model(path_shapes: str):

    sort = []
    path_app_val = fu.file_search_and(path_shapes, True, "seed0", ".shp", "learn")
    for path in path_app_val:
        try:
            _ = sort.index(
                (
                    int(path.split("/")[-1].split("_")[-3]),
                    path.split("/")[-1].split("_")[0],
                )
            )
        except ValueError:
            sort.append(
                (path.split("/")[-1].split("_")[-3], path.split("/")[-1].split("_")[0])
            )
    return fu.sort_by_first_elem(sort)  # [(RegionNumber,[tile1,tile2,...]),(...),...]


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="This function link models and their tiles"
    )
    parser.add_argument(
        "-shapesIn",
        help=(
            "path to the folder which ONLY contains shapes for "
            "the classification (learning and validation) (mandatory)"
        ),
        dest="path_shapes",
        required=True,
    )
    args = parser.parse_args()

    print(get_region_model(args.path_shapes))
