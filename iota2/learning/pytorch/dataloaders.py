#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""iota2 module dedicated to provided DataLoader torch object
"""
import logging
import random
from collections import Counter, OrderedDict
from typing import Optional

import numpy as np
import torch
import xarray as xr
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import DataLoader, TensorDataset

from iota2.common.file_utils import sort_by_first_elem
from iota2.learning.pytorch.torch_utils import StatsCalculator, batch_provider
from iota2.vector_tools.vector_functions import get_netcdf_df_column_values

LOGGER = logging.getLogger("distributed.worker")


def get_encoder_conversion_dict(categories: list[float]) -> dict[int, int]:
    """traduction from original iota2 labels to LabelEncoder

    Parameters
    ----------
    categories
        input labels encoded by the LabelEncoder
    """
    convert_dic = {}
    for cat_index, categorie in enumerate(categories):
        convert_dic[cat_index] = int(categorie)

    return convert_dic


def sensors_mask_from_labels_to_index(masks_labels) -> OrderedDict:
    """guess sensors masks labels from the list of features mask"""
    sensors_list = []
    sensors_index = OrderedDict()
    for feat_label in list(masks_labels):
        sensor_name, feat_name, _ = feat_label.split("_")
        if sensor_name.lower() == "sentinel1":
            sensor_name = f"{sensor_name}_{feat_name[0:3]}".lower()
        if sensor_name not in sensors_list:
            sensors_list.append(sensor_name)
    for sensor in sensors_list:
        indexes = [
            ind
            for ind, label in enumerate(masks_labels)
            if sensor.lower() in label.lower()
        ]
        bands_per_dates = sort_by_first_elem(
            [(masks_labels[ind].split("_")[-1], masks_labels[ind]) for ind in indexes]
        )
        # check if every date contain the same number of bands
        bands_count = [len(date_bands) for _, date_bands in bands_per_dates]
        if bands_count.count(1) != len(bands_count):
            raise ValueError("duplicated mask found for the sensor '{sensor}'")
        sensors_index[sensor] = {
            "index": indexes,
        }
    return sensors_index


def sensors_from_labels_to_index(
    features_labels: list[str], masks_labels: Optional[list[str]] = None
) -> OrderedDict:
    """guess sensors positions thanks to features order

    Parameters
    ----------
    features_labels
        feature's names. Must not contains mask's name
    masks_labels
        mask's name
    """

    sensors_list = []
    sensors_index = OrderedDict()
    for feat_label in list(features_labels):
        sensor_name, feat_name, _ = feat_label.split("_")
        if sensor_name.lower() == "sentinel1":
            # sensor_name = f"{sensor_name}_{feat_name[0:3]}"
            sensor_name = f"{sensor_name}_{feat_name}".lower()
        if sensor_name not in sensors_list:
            sensors_list.append(sensor_name)

    # get index of interest and nb component for each sensors
    for sensor in sensors_list:
        indexes = [
            ind
            for ind, label in enumerate(features_labels)
            if sensor.lower() in label.lower()
        ]
        bands_per_dates = sort_by_first_elem(
            [
                (features_labels[ind].split("_")[-1], features_labels[ind])
                for ind in indexes
            ]
        )
        # check if every date contain the same number of bands
        bands_count = [len(date_bands) for _, date_bands in bands_per_dates]
        if bands_count.count(bands_count[0]) != len(bands_count):
            raise ValueError("nb dates component does not match")
        sensors_index[sensor] = {"index": indexes, "nb_date_comp": bands_count[0]}
    masks_dic = OrderedDict()
    if masks_labels:
        masks_dic = sensors_mask_from_labels_to_index(masks_labels)

        for sensor_name, sensor_dic in sensors_index.items():
            if "sentinel1_des" in sensor_name:
                sensor_dic["mask_index"] = masks_dic["sentinel1_des"]["index"]
            elif "sentinel1_asc" in sensor_name:
                sensor_dic["mask_index"] = masks_dic["sentinel1_asc"]["index"]
            else:
                sensor_dic["mask_index"] = masks_dic[sensor_name]["index"]
    return sensors_index


def get_proportion(
    database: str, proportion_column: str, target_column: str, target_values: list
) -> dict:
    """get proportion, as a percentage, of every class in the the database
       considering an other column and its values

    Parameters
    ----------
    database
        hdf input database
    proportion_column
        column containing class labels
    target_column
        discrimating column name
    target_values
        discrimating values in target_column

    Notes
    -----
    return proportion [0;1] as a dictionary.
    proportion[1] = 0.4 # 1 constitute 40% of the database.
    """
    netcdf_data = xr.open_dataset(database)
    netcdf_data = netcdf_data.sel(
        {
            "bands": [proportion_column, target_column],
            "fid": list(netcdf_data["fid"].values),
        }
    )
    netcdf_data = netcdf_data.compute()
    net_array = netcdf_data.to_array()
    df_prop = net_array.to_dataframe("sub_sel")
    df_prop = df_prop.unstack()
    df_prop.columns = [str(col_name) for _, col_name in list(df_prop.columns)]

    # sub_sel = netcdf_data[[proportion_column, target_column]]
    # df_prop = sub_sel.to_array().to_pandas().T
    df_prop = df_prop.loc[df_prop[target_column].isin(target_values)]

    values = list(np.ravel(df_prop[proportion_column].values))
    return {value: count / len(values) for value, count in Counter(values).items()}


def originfid_to_fid(database_file, values, origin_column):
    """
    target_colmun become the index
    """
    # print(target_colmun, origin_column)
    netcdf_data = xr.open_dataset(database_file)
    netcdf_data = netcdf_data.sel(
        {"bands": [origin_column], "fid": list(netcdf_data["fid"].values)}
    )
    netcdf_data = netcdf_data.compute()
    net_array = netcdf_data.to_array()
    full_df = net_array.to_dataframe("sub_sel")
    full_df = full_df.unstack()
    full_df.columns = [str(col_name) for _, col_name in list(full_df.columns)]

    # full_df = sub_sel.to_array().to_pandas().T
    full_df = full_df.loc[full_df[origin_column].isin(values)]
    fids = [val for _, val in list(full_df.index.values)]
    random.shuffle(fids)
    return fids


def split_train_valid_by_fid(
    database_file: str,
    labels_column: str,
    origin_fid_column: str,
    test_size: float = 0.2,
    seed: int = 42,
) -> tuple[list[int], list[int]]:
    """split database in train and valid subset according to a column name

    Parameters
    ----------

    database_file
        hdf database file
    labels_column
        labels column
    origin_fid_column
        column containing original fids
    test_size
        percentage of samples use to valid the model [0;1]
    seed
        random seed to split the database in train/validation sample-set

    Note
    ----

    Return a tuple of two list, the first list is dedicated to learn model
    and the second to validated it
    """
    netcdf_data = xr.open_dataset(database_file)
    netcdf_data = netcdf_data.sel(
        {
            "bands": [labels_column, origin_fid_column],
            "fid": list(netcdf_data["fid"].values),
        }
    )
    netcdf_data = netcdf_data.compute()
    net_array = netcdf_data.to_array()
    df_label_origin_fid = net_array.to_dataframe("sub_sel")
    df_label_origin_fid = df_label_origin_fid.unstack()
    # sub_sel = netcdf_data[[labels_column, origin_fid_column]]
    # df_label_origin_fid = sub_sel.to_array().to_pandas().T
    df_label_origin_fid.columns = [
        str(col_name) for _, col_name in list(df_label_origin_fid.columns)
    ]

    label_to_fids = dict(
        df_label_origin_fid.groupby(labels_column)[origin_fid_column].apply(set)
    )
    fids_train = []
    fids_valid = []
    for _, fids in label_to_fids.items():
        list_fids = list(fids)
        try:
            _, _, train_fids, valid_fids = train_test_split(
                list_fids, list_fids, test_size=test_size, random_state=seed
            )
            # in order to manage unique label case value
            if not train_fids:
                train_fids = valid_fids
        except ValueError:
            train_fids = valid_fids = fids
        fids_train += train_fids
        fids_valid += valid_fids

    return fids_train, fids_valid


def normalize_features(
    feat_label: str, normalization_dict: dict, value: float, logger=LOGGER
) -> float:
    """function dedicated to be use with pandas.DataFrame.apply method
    divides the values in the column using the denominator found
    into the input dictionary
    """
    if "mask" in feat_label.lower():
        normalize_value = value
    else:
        try:
            sensor, _, _ = feat_label.split("_")
            if sensor in normalization_dict:
                normalize_value = value / normalization_dict[sensor]
            else:
                normalize_value = value
        except ValueError:
            logger.warning(f"{feat_label} can't be normalize")
            print(f"{feat_label} can't be normalize")
            normalize_value = value
    return normalize_value


class BatchStreamDataSet(torch.utils.data.Dataset):
    """custom dataset to read the input database per batch"""

    def __init__(
        self,
        database_file: str,
        fids_provider: batch_provider,
        fids_column_name: str,
        enc: LabelEncoder,
        features_columns: list[str],
        masks_columns: list[str],
        label_column: str,
        sensor_ind: dict,
        class_distribution: dict,
        logger=LOGGER,
    ):
        """
        Parameters
        ----------
        database_file
            input hdf database file
        fids_provider
            all fids for every batch
        fids_column_name
            fids column's name (usually ogc_fid)
        enc
            LabelEncoder related to input labels
        features_columns
            column's name containing features
        masks_columns
            column's name containing masks
        label_column
            column's name containing labels
        sensor_ind
            sensor's position in the stack of data
        class_distribution
            It contains the distribution pourcentage [0;1] in
            the input database
        logger
            logging logger
        """
        self.database_file = database_file
        self.enc = enc
        self.fids_column_name = fids_column_name
        self.features_columns = features_columns
        self.masks_columns = masks_columns
        self.label_column = label_column
        self.sensor_ind = sensor_ind
        self.class_distribution = class_distribution
        self.logger = logger

        self.fids_provider = fids_provider
        self.batch_fids = None
        self.load_batch_series(0)

    def load_batch_series(self, current_epoch) -> list[list[int]]:
        """load a list of batch for a given epoch
        Parameters
        ----------
        current_epoch
            epoch to load
        """
        self.batch_fids = self.fids_provider.get_epoch(current_epoch)
        return self.batch_fids

    def get_sensor_tensor_position(self) -> dict:
        """ """
        sensors_datasets_ind = {}
        ind = 0
        for sensor_name, _ in self.sensor_ind.items():
            sensors_datasets_ind[sensor_name] = {"data": ind, "mask": None}
            ind += 1
            if self.masks_columns:
                sensors_datasets_ind[sensor_name]["mask"] = ind
                ind += 1
        sensors_datasets_ind["target"] = ind
        ind += 1
        sensors_datasets_ind["weight"] = ind
        return sensors_datasets_ind

    def __len__(self):
        return len(self.fids_provider)

    def __getitem__(self, index):
        """ """
        fids_of_interest = self.batch_fids[index]
        netcdf_data = xr.open_dataset(self.database_file)
        netcdf_data = netcdf_data.isel({"fid": fids_of_interest})
        netcdf_data = netcdf_data.compute()
        net_array = netcdf_data.to_array()
        full_df = net_array.to_dataframe("sub_sel")
        full_df = full_df.unstack()
        full_df.columns = [str(col_name) for _, col_name in list(full_df.columns)]
        df_features = full_df[self.features_columns].copy()

        df_labels = full_df[[self.label_column]].copy()

        if self.masks_columns:
            df_masks = full_df[self.masks_columns].copy()

        datasets = []
        for _, sensor_dict in self.sensor_ind.items():
            df_features_sensor = df_features.iloc[:, sensor_dict["index"]]
            sensor_nb_bands = sensor_dict["nb_date_comp"]
            sensor_nb_dates = len(sensor_dict["index"]) / sensor_nb_bands
            if sensor_nb_dates.is_integer():
                sensor_nb_dates = int(sensor_nb_dates)
            else:
                raise ValueError(
                    "number of dates is inconsistent with sensor component"
                )
            tensor_feat = torch.tensor(
                df_features_sensor.values.astype(np.float32)
            ).reshape(-1, sensor_nb_dates, sensor_nb_bands)
            datasets.append(tensor_feat)
            if self.masks_columns:
                tensor_mask = torch.tensor(
                    df_masks.iloc[:, sensor_dict["mask_index"]].values.astype(
                        np.float32
                    )
                ).reshape(-1, len(sensor_dict["mask_index"]), 1)
                datasets.append(tensor_mask)
        tensor_labels = torch.tensor(
            self.enc.transform(np.ravel(df_labels.values))
        ).type(torch.LongTensor)

        datasets.append(tensor_labels)
        weights_tensor = torch.tensor(
            df_labels[self.label_column]
            .apply(lambda x: 1.0 / self.class_distribution[x])
            .to_numpy()
            .astype(np.float32)
        ).unsqueeze_(-1)
        datasets.append(weights_tensor)
        return datasets


def database_to_dataloader_batch_stream(
    database_file: str,
    labels_column: str,
    features_columns: list[str],
    origin_fid_column: str = "originfid",
    masks_columns: list[str] = [],
    batch_size: int = 1000,
    nb_epochs: int = 100,
    num_workers: int = 1,
    fids_column: str = "fid",
    train_batch_prov: Optional[batch_provider] = None,
    valid_batch_prov: Optional[batch_provider] = None,
    compute_stats: bool = True,
    additional_statistics_percentage: Optional[float] = None,
) -> tuple[
    DataLoader,
    DataLoader,
    dict,
    dict,
    dict[str, dict[str, torch.tensor]],
    batch_provider,
    batch_provider,
]:
    """generate two dataloader (train, valid) from a database

    the input database will be read by batch and send to
    nn by batch

    Parameters
    ----------

    database_file
        netcdf database file
    labels_column
        labels column
    origin_fid_column
        column containing original fids, usally 'originfid' from otb
    masks_columns
        columns containing masks informations
    batch_size
        nn batch size
    nb_epochs
        number of epochs
    num_workers
       number of learning tasks in parallel
    fids_column
       fids column, usally 'ogc_fid'
    train_batch_prov
       will provid fids for every training batch
    valid_batch_prov
       will provid fids for every validation batch
    compute_stats
       flag to compute stats
    additional_statistics_percentage
       percentage of the input database to perform stats

    Note
    ----
    Return a set of variable in a tuple
    (train_loader, valid_loader, conver_dic, sensors_datasets_ind,
            stats, train_batch_prov, valid_batch_prov)
    two torch DataLoader:
        train_loader, valid_loader
    a dictionary to convert pytorch labels to the original ones
        convert_dic[int(py torch class label)] = int(origin class label)
    a dictionary 'sensors_datasets_ind'
        containing keys 'target', 'weight', 'data' and 'masks' to
        get the correct data index in the dataloader
    a dictionnary of statistics : stats
        stats = {'sensor_name':{'stat_name':torch.tensor}}
    two batch_provider object train_batch_prov and valid_batch_prov
    """
    fids_train, fids_valid = split_train_valid_by_fid(
        database_file, labels_column, origin_fid_column
    )
    ogc_fids_train = originfid_to_fid(database_file, fids_train, origin_fid_column)
    ogc_fids_valid = originfid_to_fid(database_file, fids_valid, origin_fid_column)
    sensor_ind_dic = sensors_from_labels_to_index(features_columns, masks_columns)
    labels_train_distrib = get_proportion(
        database=database_file,
        proportion_column=labels_column,
        target_column=origin_fid_column,
        target_values=fids_train + fids_valid,
    )
    labels_valid_distrib = labels_train_distrib
    labels = get_netcdf_df_column_values(database_file, labels_column)
    models_labels = sorted(list(set(labels)))
    if train_batch_prov is None:
        train_batch_prov = batch_provider(ogc_fids_train, batch_size, nb_epochs)
    if valid_batch_prov is None:
        valid_batch_prov = batch_provider(ogc_fids_valid, batch_size, nb_epochs)
    enc = LabelEncoder().fit(models_labels)
    train_dataset = BatchStreamDataSet(
        database_file,
        train_batch_prov,
        fids_column,
        enc,
        features_columns,
        masks_columns,
        labels_column,
        sensor_ind_dic,
        labels_train_distrib,
    )
    valid_dataset = BatchStreamDataSet(
        database_file,
        valid_batch_prov,
        fids_column,
        enc,
        features_columns,
        masks_columns,
        labels_column,
        sensor_ind_dic,
        labels_valid_distrib,
    )

    sensors_datasets_ind = train_dataset.get_sensor_tensor_position()
    train_loader = DataLoader(
        train_dataset,
        batch_size=1,  # must be 1 here
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    valid_loader = DataLoader(
        valid_dataset,
        batch_size=1,  # must be 1 here
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    stats = None
    if compute_stats:
        stats = StatsCalculator(
            train_loader,
            sensors_datasets_ind,
            subsample=additional_statistics_percentage,
            min_stat=True,
            max_stat=True,
            var_stat=True,
        ).compute()
    conver_dic = get_encoder_conversion_dict(models_labels)
    return (
        train_loader,
        valid_loader,
        conver_dic,
        sensors_datasets_ind,
        stats,
        train_batch_prov,
        valid_batch_prov,
    )


def database_to_dataloader(
    database_file: str,
    labels_column: str,
    features_columns: list[str],
    origin_fid_column: str = "originfid",
    masks_columns: list[str] = [],
    batch_size: int = 1000,
    num_workers: int = 1,
    compute_stats: bool = True,
    additional_statistics_percentage: Optional[float] = None,
) -> tuple[DataLoader, DataLoader, dict, dict, dict]:
    """generate two dataloader (train, valid) from a database

    the entire input database will be read once and then distributed by batch

    Parameters
    ----------

    database_file
        netcdf database file
    labels_column
        labels column
    origin_fid_column
        column containing original fids
    masks_columns
        columns containing masks informations
    batch_size
        nn batch size
    num_workers
       number of learning tasks in parallel
    compute_stats
       flag to compute stats
    additional_statistics_percentage
       percentage of the input database to perform stats

    Note
    ----
    Return a set of variable in a tuple
    (train_loader, valid_loader, conver_dic, sensors_datasets_ind, stats)
    two torch DataLoader:
        train_loader, valid_loader
    a dictionary to convert pytorch labels to the original ones
        conver_dic[int(py torch class label)] = int(origin class label)
    a dictionary 'sensors_datasets_ind'
        containing keys 'target', 'weight', 'data' and 'masks' to
        get the correct data index in the dataloader
    a dictionnary of statistics : stats
        stats = {'sensor_name':{'stat_name':torch.tensor}}
    """
    fids_train, fids_valid = split_train_valid_by_fid(
        database_file, labels_column, origin_fid_column
    )
    netcdf_data = xr.open_dataset(database_file)
    netcdf_data = netcdf_data.compute()
    net_array = netcdf_data.to_array()
    full_df = net_array.to_dataframe("sub_sel")
    full_df = full_df.unstack()
    full_df.columns = [str(col_name) for _, col_name in list(full_df.columns)]

    df_features_train = full_df.loc[full_df[origin_fid_column].isin(fids_train)]
    df_features_train = df_features_train[features_columns]
    df_labels_train = full_df.loc[full_df[origin_fid_column].isin(fids_train)]
    df_labels_train = df_labels_train[[labels_column]]

    labels_distrib = get_proportion(
        database=database_file,
        proportion_column=labels_column,
        target_column=origin_fid_column,
        target_values=fids_train + fids_valid,
    )

    weigths_train_df = df_labels_train.copy()
    weigths_train_df = weigths_train_df[labels_column].apply(
        lambda x: 1 / labels_distrib[x]
    )

    df_features_valid = full_df.loc[full_df[origin_fid_column].isin(fids_valid)]
    df_features_valid = df_features_valid[features_columns]

    df_labels_valid = full_df.loc[full_df[origin_fid_column].isin(fids_valid)]
    df_labels_valid = df_labels_valid[[labels_column]]

    weigths_valid_df = df_labels_valid.copy()
    weigths_valid_df = weigths_valid_df[labels_column].apply(
        lambda x: 1 / labels_distrib[x]
    )

    if masks_columns:
        df_masks_train = full_df.loc[full_df[origin_fid_column].isin(fids_train)]
        df_masks_train = df_masks_train[masks_columns]

        df_masks_valid = full_df.loc[full_df[origin_fid_column].isin(fids_valid)]
        df_masks_valid = df_masks_valid[masks_columns]

    labels_train = set(np.ravel(df_labels_train))
    labels_valid = set(np.ravel(df_labels_valid))

    all_labels = sorted(list(labels_train.union(labels_valid)))
    enc = LabelEncoder().fit(all_labels)

    sensor_ind_dic = sensors_from_labels_to_index(
        df_features_train.columns, masks_columns
    )

    train_datasets = []
    valid_datasets = []
    sensors_datasets_ind = {}
    ind = 0
    for sensor_name, sensor_dict in sensor_ind_dic.items():
        sensors_datasets_ind[sensor_name] = {"data": None, "mask": None}
        df_features_train_sensor = df_features_train.iloc[:, sensor_dict["index"]]
        df_features_valid_sensor = df_features_valid.iloc[:, sensor_dict["index"]]
        sensor_nb_bands = sensor_dict["nb_date_comp"]
        sensor_nb_dates = len(sensor_dict["index"]) / sensor_nb_bands
        if sensor_nb_dates.is_integer():
            sensor_nb_dates = int(sensor_nb_dates)
        else:
            raise ValueError("number of dates is inconsistent with sensor component")

        # tensors
        tensor_feat_train = torch.tensor(
            df_features_train_sensor.values.astype(np.float32)
        ).reshape(-1, sensor_nb_dates, sensor_nb_bands)
        tensor_feat_valid = torch.tensor(
            df_features_valid_sensor.values.astype(np.float32)
        ).reshape(-1, sensor_nb_dates, sensor_nb_bands)
        train_datasets.append(tensor_feat_train)
        valid_datasets.append(tensor_feat_valid)
        sensors_datasets_ind[sensor_name]["data"] = ind
        ind += 1
        if masks_columns:
            tensor_mask_train = torch.tensor(
                df_masks_train.iloc[:, sensor_dict["mask_index"]].values.astype(
                    np.float32
                )
            ).reshape(-1, len(sensor_dict["mask_index"]), 1)
            tensor_mask_valid = torch.tensor(
                df_masks_valid.iloc[:, sensor_dict["mask_index"]].values.astype(
                    np.float32
                )
            ).reshape(-1, len(sensor_dict["mask_index"]), 1)
            train_datasets.append(tensor_mask_train)
            valid_datasets.append(tensor_mask_valid)
            sensors_datasets_ind[sensor_name]["mask"] = ind
            ind += 1

    tensor_labels_train = torch.tensor(enc.transform(df_labels_train)).type(
        torch.LongTensor
    )
    tensor_labels_valid = torch.tensor(enc.transform(df_labels_valid)).type(
        torch.LongTensor
    )

    train_datasets.append(tensor_labels_train)
    valid_datasets.append(tensor_labels_valid)

    sensors_datasets_ind["target"] = ind
    ind += 1
    tensor_weights_train = torch.tensor(
        weigths_train_df.to_numpy().astype(np.float32)
    ).unsqueeze_(-1)
    tensor_weights_valid = torch.tensor(
        weigths_valid_df.to_numpy().astype(np.float32)
    ).unsqueeze_(-1)
    train_datasets.append(tensor_weights_train)
    valid_datasets.append(tensor_weights_valid)
    sensors_datasets_ind["weight"] = ind

    train_dataset = TensorDataset(*train_datasets)
    valid_dataset = TensorDataset(*valid_datasets)

    conver_dic = get_encoder_conversion_dict(all_labels)

    train_loader = DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers
    )
    valid_loader = DataLoader(
        valid_dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers
    )
    stats = None
    if compute_stats:
        stats = StatsCalculator(
            train_loader,
            sensors_datasets_ind,
            subsample=additional_statistics_percentage,
            min_stat=True,
            max_stat=True,
            var_stat=True,
        ).compute()
    return train_loader, valid_loader, conver_dic, sensors_datasets_ind, stats
