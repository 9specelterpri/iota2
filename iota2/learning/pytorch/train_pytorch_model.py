#!/usr/bin/env python3

import logging
import math

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import pickle
import shutil
import time
from collections import OrderedDict
from datetime import datetime
from typing import Optional

import numpy as np
import torch
import torch.nn.functional as F
from sklearn.metrics import accuracy_score, cohen_kappa_score, f1_score
from tabulate import tabulate
from torch.optim.lr_scheduler import ReduceLROnPlateau

from iota2.learning.pytorch.dataloaders import (
    database_to_dataloader,
    database_to_dataloader_batch_stream,
)
from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetworkFactory
from iota2.learning.pytorch.torch_utils import (
    early_stop,
    plot_loss,
    plot_metrics,
    tensors_to_kwargs,
    tensors_to_kwargs_full,
    update_confusion_matrix,
)
from iota2.vector_tools.vector_functions import get_netcdf_df_column_values

LOGGER = logging.getLogger("distributed.worker")


def learning_state_initialization(
    dl_name: str,
    dl_parameters: dict,
    nb_features: int,
    nb_class: int,
    model_parameters_file: str,
    dl_module: str,
    learning_rate: float,
    enable_early_stop: bool,
    epoch_to_trigger: int,
    early_stop_patience: int,
    early_stop_tol: float,
    early_stop_metric: str,
    adaptive_lr: dict,
    checkpoint_model_file: str,
    restart_from_checkpoint: bool,
    device: str,
    dataloader_mode: str,
    dataset_path: str,
    data_field: str,
    features_labels: list[str],
    masks_labels: list[str],
    batch_size: int,
    epochs: int,
    num_workers: int,
    additional_statistics_percentage: float,
):
    """
    dl_name
        neural network name
    dl_parameters
        kwargs to intanciate the neural network
    nb_features
        number of input features
    nb_class
        number of input class
    model_parameters_file
        file to pickle 'dl_parameters'
    dl_module
        file the a user python module containing it's own neural
        network
    learning_rate
        learning rate
    enable_early_stop
        flag to enable early stop
    epoch_to_trigger
        epoch to start watching metric evolution and
        triggered early stop
    early_stop_patience
        early stop patience
    early_stop_tol
        early stop tolerance
    early_stop_metric
        metric to monitor
    adaptive_lr
        dictionary containing parameters to enable
        dynamic learning rate
    checkpoint_model_file
        file to save checkpoint
    restart_from_checkpoint
        flag to restart from checkpoint if exists
    device
        'cpu' or 'cuda'
    dataloader_mode
        'full' or 'stream'
    dataset_path
        input database
    data_field
        field containing labels into the input database
    features_labels
        features labels
    masks_labels
        masks labels
    batch_size
        batch size
    epochs
        number of epochs
    num_workers
        number of worker to prepare every batches
    additional_statistics_percentage
        percentage of the input database to perform statistics on
    """
    model = Iota2NeuralNetworkFactory().get_nn(
        dl_name, dl_parameters, nb_features, nb_class, model_parameters_file, dl_module
    )

    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    train_loss = []
    valid_loss = []
    valid_kappa = []
    valid_fscore = []
    valid_oa = []
    state_dict_best_coeff = {
        "kappa": {
            "score": -1,
            "state_dict": None,
            "epoch": None,
            "hyperparameters": None,
        },
        "loss": {
            "score": 10000,
            "state_dict": None,
            "epoch": None,
            "hyperparameters": None,
        },
        "fscore": {
            "score": -1,
            "state_dict": None,
            "epoch": None,
            "hyperparameters": None,
        },
        "oa": {
            "score": -1,
            "state_dict": None,
            "epoch": None,
            "hyperparameters": None,
        },
    }
    first_epoch = 0
    batch_prov_train = batch_prov_valid = None
    early_stop_obj: early_stop = None
    if enable_early_stop:
        early_stop_obj = early_stop(
            epoch_to_trigger, early_stop_patience, early_stop_tol, early_stop_metric
        )
    scheduler: ReduceLROnPlateau = None
    if adaptive_lr:
        adaptive_lr["optimizer"] = optimizer
        scheduler = ReduceLROnPlateau(**adaptive_lr)
    compute_stats = True
    checkpoint_stats = None
    if os.path.isfile(checkpoint_model_file) and restart_from_checkpoint:
        compute_stats = False
        (
            model,
            optimizer,
            checkpoint_epoch,
            state_dict_best_coeff,
            valid_kappa,
            valid_fscore,
            valid_oa,
            valid_loss,
            train_loss,
            batch_prov_train,
            batch_prov_valid,
            early_stop_obj,
            scheduler,
            checkpoint_stats,
        ) = load_checkpoint(checkpoint_model_file, model, optimizer)
        first_epoch = checkpoint_epoch + 1
        # noqa: E501 according to https://discuss.pytorch.org/t/loading-a-saved-model-for-continue-training/17244/3
        for state in optimizer.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.to(device)

    # dataloader instance must be created
    # after the variables coming from the checkpoint
    if dataloader_mode == "stream":
        (
            train_loader,
            valid_loader,
            label_convert_dic,
            dataset_sensor_pos,
            stats,
            batch_prov_train,
            batch_prov_valid,
        ) = database_to_dataloader_batch_stream(
            dataset_path,
            data_field,
            features_labels,
            "originfid",
            masks_labels,
            batch_size,
            epochs,
            num_workers,
            "fid",
            batch_prov_train,
            batch_prov_valid,
            compute_stats,
            additional_statistics_percentage,
        )
    elif dataloader_mode == "full":
        batch_prov_train = batch_prov_valid = None
        (
            train_loader,
            valid_loader,
            label_convert_dic,
            dataset_sensor_pos,
            stats,
        ) = database_to_dataloader(
            dataset_path,
            data_field,
            features_labels,
            "originfid",
            masks_labels,
            batch_size,
            num_workers,
            compute_stats,
            additional_statistics_percentage,
        )
    stats = checkpoint_stats if checkpoint_stats else stats
    return (
        model,
        stats,
        first_epoch,
        early_stop_obj,
        scheduler,
        train_loader,
        valid_loader,
        label_convert_dic,
        dataset_sensor_pos,
        dataset_sensor_pos,
        optimizer,
        train_loss,
        valid_loss,
        valid_kappa,
        valid_fscore,
        valid_oa,
        state_dict_best_coeff,
        batch_prov_train,
        batch_prov_valid,
    )


def update_state_dict(
    o_acc: float,
    kappa: float,
    weighted_f1_score: float,
    valid_loss_mean: float,
    state_dict_best_coeff: dict[str, dict],
    model: torch.nn,
    current_epoch: int,
    model_parameters_file: str,
    encoder_convert_file: str,
    learning_rate: float,
    batch_size: int,
) -> dict:
    """update best state dict regarding input coeff

    Parameters
    ----------
    o_acc
        overall accuracy
    kappa
        kappa
    weighted_f1_score
        f-score
    valid_loss_mean
        loss mean accross batches
    state_dict_best_coeff
        dictionary containing model.state_dict for every coeff
    model
        neural network
    current_epoch
        current epoch
    model_parameters_file
        path to a file containing a serialized dict
        to instanciate the model
    encoder_convert_file
        path to a file containing a serialized dict
        to decode labels
    learning_rate
        learning rate
    batch_size
        batch size

    Note
    ----
    return an update of the input best step_dictionary
    """
    new_state_dict_best_coeff = state_dict_best_coeff.copy()
    if o_acc > new_state_dict_best_coeff["oa"]["score"]:
        new_state_dict_best_coeff["oa"]["score"] = o_acc
        new_state_dict_best_coeff["oa"]["state_dict"] = model.state_dict()
        new_state_dict_best_coeff["oa"]["epoch"] = current_epoch + 1
        new_state_dict_best_coeff["oa"]["parameter_file"] = model_parameters_file
        new_state_dict_best_coeff["oa"]["enc"] = encoder_convert_file
        new_state_dict_best_coeff["oa"]["hyperparameters"] = {
            "learning_rate": learning_rate,
            "batch_size": batch_size,
        }
    if kappa > new_state_dict_best_coeff["kappa"]["score"]:
        new_state_dict_best_coeff["kappa"]["score"] = kappa
        new_state_dict_best_coeff["kappa"]["state_dict"] = model.state_dict()
        new_state_dict_best_coeff["kappa"]["epoch"] = current_epoch + 1
        new_state_dict_best_coeff["kappa"]["parameter_file"] = model_parameters_file
        new_state_dict_best_coeff["kappa"]["enc"] = encoder_convert_file
        new_state_dict_best_coeff["kappa"]["hyperparameters"] = {
            "learning_rate": learning_rate,
            "batch_size": batch_size,
        }
    if weighted_f1_score > new_state_dict_best_coeff["fscore"]["score"]:
        new_state_dict_best_coeff["fscore"]["score"] = weighted_f1_score
        new_state_dict_best_coeff["fscore"]["state_dict"] = model.state_dict()
        new_state_dict_best_coeff["fscore"]["epoch"] = current_epoch + 1
        new_state_dict_best_coeff["fscore"]["parameter_file"] = model_parameters_file
        new_state_dict_best_coeff["fscore"]["enc"] = encoder_convert_file
        new_state_dict_best_coeff["fscore"]["hyperparameters"] = {
            "learning_rate": learning_rate,
            "batch_size": batch_size,
        }
    if valid_loss_mean < new_state_dict_best_coeff["loss"]["score"]:
        new_state_dict_best_coeff["loss"]["score"] = valid_loss_mean
        new_state_dict_best_coeff["loss"]["state_dict"] = model.state_dict()
        new_state_dict_best_coeff["loss"]["epoch"] = current_epoch + 1
        new_state_dict_best_coeff["loss"]["parameter_file"] = model_parameters_file
        new_state_dict_best_coeff["loss"]["enc"] = encoder_convert_file
        new_state_dict_best_coeff["loss"]["hyperparameters"] = {
            "learning_rate": learning_rate,
            "batch_size": batch_size,
        }
    return new_state_dict_best_coeff


def doy_by_sensors(features_labels: list[str], logger=LOGGER) -> dict[str, list[int]]:
    """for each sensor's features return the number of days from the first feature

    Example
    -------
    features_labels = ["sentinel2_b1_20150101", "sentinel2_b1_20150110"]
    doy = doy_by_sensors(features_labels)
    >>> doy["doy"] = [0, 9]
    >>> doy["features_per_date"] = 1
    """
    sensors_dates: OrderedDict = OrderedDict()
    sensors_doy: OrderedDict = OrderedDict()
    for feature_labels in features_labels:
        try:
            sensor_name, feat, date_time_str = feature_labels.split("_")
            if sensor_name == "sentinel1":
                sensor_name = f"{sensor_name}_{feat}"
            if sensor_name not in sensors_dates:
                sensors_dates[sensor_name] = {"dates": [], "features_per_date": []}
            date_time_obj = datetime.strptime(date_time_str, "%Y%m%d")
            if date_time_obj not in sensors_dates[sensor_name]["dates"]:
                sensors_dates[sensor_name]["dates"].append(date_time_obj)
            nb_component = len(
                [
                    elem
                    for elem in features_labels
                    if sensor_name in elem and date_time_str in elem
                ]
            )
            sensors_dates[sensor_name]["features_per_date"].append(nb_component)
        except ValueError:
            logger.warning(f"{feature_labels} cannot be converted to DOY")
    sensors_dates_sorted: OrderedDict = OrderedDict()
    for sensor_name, _ in sensors_dates.items():
        sensors_dates_sorted[sensor_name] = {}
        sensors_dates_sorted[sensor_name]["dates"] = sorted(
            sensors_dates[sensor_name]["dates"]
        )
        nb_components = sensors_dates[sensor_name]["features_per_date"]
        if nb_components.count(nb_components[0]) != len(nb_components):
            raise ValueError(
                f"There is not the same number of features per date in the sensor {sensor_name}"
            )
        sensors_dates_sorted[sensor_name]["features_per_date"] = sensors_dates[
            sensor_name
        ]["features_per_date"][0]

    for sensor_name, dic in sensors_dates_sorted.items():
        first_date = dic["dates"][0]
        delta_days = [(date - first_date).days for date in dic["dates"]]
        sensors_doy[sensor_name] = {
            "doy": delta_days,
            "features_per_date": sensors_dates_sorted[sensor_name]["features_per_date"],
        }
    return sensors_doy


def load_checkpoint(checkpoint_file: str, model, optimizer) -> tuple:
    """ """
    if not os.path.exists(checkpoint_file):
        raise ValueError(f"checkpoint file : '{checkpoint_file} does not exists'")
    with open(checkpoint_file, "rb") as checkpoint:
        checkpoint_dic = pickle.load(checkpoint)
        batch_prov_train, batch_prov_valid = checkpoint_dic["batch_provider"]
        ckp_ep = checkpoint_dic["epoch_checkpoint"]
        ckp_model = checkpoint_dic["current_model_state"]
        ckp_optim = checkpoint_dic["current_optimizer_state"]
        ckp_coeff = checkpoint_dic["best_coeff"]
        ckp_kappa = checkpoint_dic["coeff_history"]["kappa"]
        ckp_fscore = checkpoint_dic["coeff_history"]["fscore"]
        ckp_oa = checkpoint_dic["coeff_history"]["oa"]
        ckp_loss_val = checkpoint_dic["coeff_history"]["loss_val"]
        ckp_loss_learn = checkpoint_dic["coeff_history"]["loss_train"]
        ckp_early_stop = checkpoint_dic["early_stop"]
        ckp_scheduler = checkpoint_dic["scheduler"]
        ckp_stats = checkpoint_dic["stats"]
    optimizer.load_state_dict(ckp_optim)
    model.load_state_dict(ckp_model)
    return (
        model,
        optimizer,
        ckp_ep,
        ckp_coeff,
        ckp_kappa,
        ckp_fscore,
        ckp_oa,
        ckp_loss_val,
        ckp_loss_learn,
        batch_prov_train,
        batch_prov_valid,
        ckp_early_stop,
        ckp_scheduler,
        ckp_stats,
    )


def torch_learn(
    dataset_path: str,
    features_labels: list[str],
    model_path: str,
    model_parameters_file: str,
    encoder_convert_file: str,
    data_field: str,
    dl_name: str,
    dl_parameters: dict,
    dl_module: Optional[str] = None,
    checkpoints_interval: int = 1,
    restart_from_checkpoint: bool = True,
    weighted_labels: bool = False,
    learning_rate: float = 0.00001,
    masks_labels: Optional[list[str]] = None,
    epochs: int = 100,
    batch_size: int = 1000,
    num_workers: int = 1,
    dataloader_mode: str = "stream",
    enable_early_stop: bool = False,
    epoch_to_trigger: int = 5,
    early_stop_patience: int = 10,
    early_stop_metric: str = "val_loss",
    early_stop_tol: float = 0.01,
    adaptive_lr: dict = {},
    additional_statistics_percentage: bool = False,
    working_dir: Optional[str] = None,
    logger=LOGGER,
):
    """Train a neural network thanks to pytorch

    The resulting model is serialized and save thanks to pickle.

    Parameters
    ----------
    dataset_path
        input data base (netcdf format)
    features_labels
        column's name into the data base to consider in order to learn the model
    model_path
        output model path
    model_parameters_file
        file to save pytorch inputs arguments (to instanciate it)
    encoder_convert_file
        file to save LabelEncoder dictionary converter
    data_field
        label's data field in data set
    dl_name
        neural network name
    checkpoints_interval
        save the model at every checkpoints interval
    restart_from_checkpoint
        if a model checkpoint exixsts, then restart learning from
        the last valid epoch
    dl_parameters
        parameters as **dict to instaciate neural network object
    learning_rate
        nn learning rate
    masks_labels
        mask column's name into the data base
    epochs
        number of epochs
    batch_size
        batch size
    num_workers
        number of parallel learning tasks
    dataloader_mode
        'full' : load the full database into memory
        'stream' : load the database into memory by batch
    enable_early_stop
        flag to enable early stop
    epoch_to_trigger
        epoch to start monitoring metric evolution
    early_stop_patience
        number of checks with no improvement after which training will be stopped
    early_stop_metric
        metric to be monitored ['train_loss', 'val_loss', 'fscore', 'oa', 'kappa']
        default is val_loss
    early_stop_tol
        minimum change in the monitored quantity to qualify as an improvement.
        if metric is 'train_loss' or 'valid_loss' then tol must be in dB.
    adaptive_lr
        is kwargs of torch.optim.lr_scheduler.ReduceLROnPlateau.
        Adaptive learning rate is triggered if different from {}.
    working_dir
       if set copy the input dataset to the working directory
       expecting the working directory is a efficient access
       memory
    logger
        root logger
    """
    if ".txt" not in model_path:
        raise ValueError("models as to be save under '.txt'")
    checkpoint_model_file = model_path.replace(".txt", "_checkpoint.txt")

    if working_dir:
        _, dataset_name = os.path.split(dataset_path)
        shutil.copy(dataset_path, working_dir)
        dataset_path = os.path.join(working_dir, dataset_name)

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    logger.info("Building the data loaders")
    logger.debug(f"Features use to build model : {features_labels}")

    doy_sensors_dic = doy_by_sensors(features_labels, logger=logger)
    nb_class = len(set(get_netcdf_df_column_values(dataset_path, data_field)))

    logger.info(f"Found {nb_class} classes in the dataset")
    logger.info(f"There are {len(features_labels)} input features.")

    # learn nn model
    dl_parameters["doy_sensors_dic"] = doy_sensors_dic
    nb_features = len(features_labels)
    criterion = F.cross_entropy
    (
        model,
        stats,
        first_epoch,
        early_stop_obj,
        scheduler,
        train_loader,
        valid_loader,
        label_convert_dic,
        dataset_sensor_pos,
        dataset_sensor_pos,
        optimizer,
        train_loss,
        valid_loss,
        valid_kappa,
        valid_fscore,
        valid_oa,
        state_dict_best_coeff,
        batch_prov_train,
        batch_prov_valid,
    ) = learning_state_initialization(
        dl_name,
        dl_parameters,
        nb_features,
        nb_class,
        model_parameters_file,
        dl_module,
        learning_rate,
        enable_early_stop,
        epoch_to_trigger,
        early_stop_patience,
        early_stop_tol,
        early_stop_metric,
        adaptive_lr,
        checkpoint_model_file,
        restart_from_checkpoint,
        device,
        dataloader_mode,
        dataset_path,
        data_field,
        features_labels,
        masks_labels,
        batch_size,
        epochs,
        num_workers,
        additional_statistics_percentage,
    )

    print(f"START EPOCH : {dataset_path}")
    model.set_stats(stats)
    learning_state_dict = {"stats": stats}
    start_learn = time.time()
    checkpoints_interval_cpt = 0
    for i in np.arange(first_epoch, epochs):
        checkpoints_interval_cpt += 1
        start_epochs = time.time()
        train_loss_loader = []
        if dataloader_mode == "stream":
            train_loader.dataset.load_batch_series(i)
            valid_loader.dataset.load_batch_series(i)

        model.to(device)
        model.train()
        for _, train_data_batch in enumerate(train_loader):
            # print(f"training batch number : {j_train}")
            # TODO : is squeeze still needed if batch_size=None ?
            if dataloader_mode == "stream":
                targets = np.squeeze(
                    train_data_batch[dataset_sensor_pos["target"]], axis=0
                ).to(device, non_blocking=False)
                weights = np.squeeze(
                    train_data_batch[dataset_sensor_pos["weight"]], axis=0
                ).to(device, non_blocking=False)
                train_model_tensors_kwargs = tensors_to_kwargs(
                    train_data_batch, dataset_sensor_pos
                )
            else:
                targets = train_data_batch[dataset_sensor_pos["target"]].to(
                    device, non_blocking=False
                )
                weights = train_data_batch[dataset_sensor_pos["weight"]].to(
                    device, non_blocking=False
                )
                train_model_tensors_kwargs = tensors_to_kwargs_full(
                    train_data_batch, dataset_sensor_pos
                )

            y_hat = model(**train_model_tensors_kwargs)
            loss = criterion(
                y_hat, targets, weight=weights if weighted_labels else None
            )
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            train_loss_loader.append(loss.item())

        train_loss_mean = sum(train_loss_loader) / len(train_loss_loader)
        train_loss.append(train_loss_mean)
        logger.info(f"Epoch {i} train loss mean : {math.log(train_loss_mean)} dB")
        print(f"Epoch {i} train loss mean : {math.log(train_loss_mean)} dB")
        model.eval()
        with torch.no_grad():
            valid_loss_loader = []
            conf_matrix = torch.zeros(nb_class, nb_class, dtype=torch.int32)
            all_preds = torch.zeros(0)
            all_targets = torch.zeros(0)

            for _, valid_data_batch in enumerate(valid_loader):
                if dataloader_mode == "stream":
                    targets_val = np.squeeze(
                        valid_data_batch[dataset_sensor_pos["target"]], axis=0
                    ).to(device, non_blocking=False)
                    weights_val = np.squeeze(
                        valid_data_batch[dataset_sensor_pos["weight"]], axis=0
                    ).to(device, non_blocking=False)
                    valid_model_tensors_kwargs = tensors_to_kwargs(
                        valid_data_batch, dataset_sensor_pos
                    )
                else:
                    targets_val = valid_data_batch[dataset_sensor_pos["target"]].to(
                        device, non_blocking=False
                    )
                    weights_val = valid_data_batch[dataset_sensor_pos["weight"]].to(
                        device, non_blocking=False
                    )
                    valid_model_tensors_kwargs = tensors_to_kwargs_full(
                        valid_data_batch, dataset_sensor_pos
                    )
                pred = model(**valid_model_tensors_kwargs)
                loss_pred = criterion(
                    pred, targets_val, weight=weights_val if weighted_labels else None
                )
                valid_loss_loader.append(loss_pred.item())
                conf_matrix, all_preds, all_targets = update_confusion_matrix(
                    pred.cpu(), targets_val.cpu(), conf_matrix, all_preds, all_targets
                )
            valid_loss_mean = sum(valid_loss_loader) / len(valid_loss_loader)
            valid_loss.append(valid_loss_mean)
            logger.info(f"Epoch {i} valid loss mean : {math.log(valid_loss_mean)} dB")
            print(f"Epoch {i} valid loss mean : {math.log(valid_loss_mean)} dB")
            logger.info(f"\n{tabulate(conf_matrix.numpy())}\n")
            # print(f"\n{tabulate(conf_matrix.numpy())}\n")
            weighted_f1_score = f1_score(
                all_targets.numpy(), all_preds.numpy(), average="macro"
            )
            o_acc = accuracy_score(all_targets.numpy(), all_preds.numpy())
            logger.info(f"F1-score : {weighted_f1_score}")
            # print(f"F1-score : {weighted_f1_score}")
            kappa = cohen_kappa_score(all_targets.numpy(), all_preds.numpy())
            valid_kappa.append(kappa)
            valid_oa.append(o_acc)
            valid_fscore.append(weighted_f1_score)
            print(f"ep {i} F1-score {weighted_f1_score} - OA {o_acc} - Kappa {kappa}")
            model.to("cpu")
            state_dict_best_coeff = update_state_dict(
                o_acc,
                kappa,
                weighted_f1_score,
                valid_loss_mean,
                state_dict_best_coeff,
                model,
                i,
                model_parameters_file,
                encoder_convert_file,
                learning_rate,
                batch_size,
            )

        end_epochs = time.time()
        print(f"epoch duration : {end_epochs - start_epochs} [sec]")
        if enable_early_stop:
            case = {
                "oa": o_acc,
                "kappa": kappa,
                "fscore": weighted_f1_score,
                "train_loss": math.log(train_loss_mean),
                "val_loss": math.log(valid_loss_mean),
            }
            early_stop_flag = early_stop_obj.step(score=case[early_stop_metric])
            if early_stop_flag:
                print("EARLY STOP")
                break
        if adaptive_lr:
            scheduler.step(valid_loss_mean)
        if checkpoints_interval_cpt - checkpoints_interval == 0:
            checkpoints_interval_cpt = 0
            with open(checkpoint_model_file, "wb") as model_to_save:
                learning_state_dict.update(
                    {
                        "scheduler": scheduler,
                        "early_stop": early_stop_obj,
                        "batch_provider": (batch_prov_train, batch_prov_valid),
                        "epoch_checkpoint": i,
                        "current_model_state": model.state_dict(),
                        "current_optimizer_state": optimizer.state_dict(),
                        "best_coeff": state_dict_best_coeff,
                        "coeff_history": {
                            "kappa": valid_kappa,
                            "fscore": valid_fscore,
                            "oa": valid_oa,
                            "loss_val": valid_loss,
                            "loss_train": train_loss,
                        },
                    }
                )
                pickle.dump(
                    learning_state_dict, model_to_save, protocol=pickle.HIGHEST_PROTOCOL
                )
    end_learn = time.time()
    print(f"TOTAL LEARNING TIME : {end_learn - start_learn} [sec]")
    for coeff_name, coeff_dict in state_dict_best_coeff.items():
        msg = (
            f"Coeff : {coeff_name}, best_value : {coeff_dict['score']} "
            f"at epoch : {coeff_dict['epoch']}, with hyperparameters : "
            f"batch_size = {batch_size}, learning_rate = {learning_rate}\n"
        )
        print(msg)
        logger.info(msg)

    model.save(model_path, state_dict_best_coeff, stats, features_labels, device="cpu")

    # save metrics
    output_loss_figure = model_path.replace(".txt", "_loss.png")
    output_conf_metrics_figure = model_path.replace(".txt", "_confusion_metrics.png")
    plot_metrics(
        valid_kappa,
        valid_fscore,
        valid_oa,
        output_conf_metrics_figure,
        learning_rate,
        batch_size,
    )
    plot_loss(train_loss, valid_loss, output_loss_figure, learning_rate, batch_size)

    with open(encoder_convert_file, "wb") as enc_file:
        pickle.dump(label_convert_dic, enc_file, protocol=pickle.HIGHEST_PROTOCOL)
