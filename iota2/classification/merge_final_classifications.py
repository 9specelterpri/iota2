#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Module containing function to merge classification using voting methods"""
import logging
import os
from typing import Optional

import rasterio
from osgeo import gdal

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fut
from iota2.common import utils
from iota2.common.raster_utils import re_encode_raster
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")

I2_CONST = i2_const.Iota2Constants()


def compute_fusion_options(
    iota2_dir_final,
    final_classifications,
    method,
    undecidedlabel,
    dempstershafer_mob,
    pix_type,
    fusion_path,
):
    """use to determine fusion parameters"""

    if method == "majorityvoting":
        options = {
            "il": final_classifications,
            "method": method,
            "nodatalabel": "0",
            "undecidedlabel": str(undecidedlabel),
            "pixType": pix_type,
            "out": fusion_path,
        }
    else:
        confusion_seed = [
            fut.file_search_and(
                os.path.join(iota2_dir_final, "TMP"),
                True,
                f"Classif_Seed_{run}.csv",
            )[0]
            for run in range(len(final_classifications))
        ]
        confusion_seed.sort()
        final_classifications.sort()
        options = {
            "il": final_classifications,
            "method": "dempstershafer",
            "nodatalabel": "0",
            "undecidedlabel": str(undecidedlabel),
            "method.dempstershafer.mob": dempstershafer_mob,
            "method.dempstershafer.cmfl": confusion_seed,
            "pixType": pix_type,
            "out": fusion_path,
        }
    return options


def merge_final_classifications(
    iota2_dir: str,
    data_field: str,
    nom_path: str,
    color_file: str,
    user_labels_original,
    runs: int = 1,
    method: str = "majorityvoting",
    undecidedlabel: int = 255,
    dempstershafer_mob: str = "precision",
    keep_runs_results: bool = True,
    validation_shape: Optional[str] = None,
    labels_raster_table: Optional[dict] = None,
    working_directory: Optional[str] = None,
    logger=LOGGER,
) -> None:
    """
    Use to merge classifications by majorityvoting
    or dempstershafer's method and evaluate it.
    get all classifications Classif_Seed_*.tif in the /final directory
    and fusion them under the raster call Classifications_fusion.tif.
    Then compute statistics using the results_utils library.

    Parameters
    ----------
    iota2_dir : string
        path to the iota2's output path
    dataField : string
        data's field name
    nom_path : string
        path to the nomenclature file
    colorFile : string
        path to the color file description
    runs : int
        number of iota2 runs (random learning splits)
    method : string
        fusion's method (majorityvoting/dempstershafer)
    undecidedlabel : int
        label for label for un-decisions
    dempstershafer_mob : string
        mass of belief measurement (precision/recall/accuracy/kappa)
    keep_runs_results : bool
        flag to inform if seeds results could be overwritten
    validation_shape : string
        path to a shape dedicated to validate fusion of classifications
    working_directory : string
        path to a working directory
    labels_raster_table : dictionary
        labels conversion dictionary : dict[old_label] = new_label

    See Also
    --------
    results_utils.gen_confusion_matrix_fig
    results_utils.stats_report
    """
    import shutil

    from iota2.common import create_indexed_color_image as color
    from iota2.common import otb_app_bank as otbApp
    from iota2.validation import results_utils as ru

    fusion_name = "Classifications_fusion.tif"
    new_results_seed_file = "RESULTS_seeds.txt"
    fusion_vec_name = "fusion_validation"  # without extension
    confusion_matrix_name = "fusionConfusion.png"
    re_encode_labels = True
    if labels_raster_table:
        labels_raster_table[undecidedlabel] = undecidedlabel
        # all_castable = []
        # for _, user_label in labels_raster_table.items():
        #     try:
        #         __ = int(user_label)
        #         all_castable.append(True)
        #     except ValueError:
        #         all_castable.append(False)
        re_encode_labels = utils.is_nomenclature_castable_to_int(labels_raster_table)

    if method not in ["majorityvoting", "dempstershafer"]:
        err_msg = (
            "the fusion method must be "
            " 'majorityvoting' or 'dempstershafer'"
            f" and {method} readed"
        )
        logger.error(err_msg)
        raise Exception(err_msg)
    if dempstershafer_mob not in ["precision", "recall", "accuracy", "kappa"]:
        err_msg = (
            "the dempstershafer MoB must be 'precision'"
            " or 'recall' or 'accuracy' or 'kappa'"
            f" {dempstershafer_mob} readed"
        )
        logger.error(err_msg)
        raise Exception(err_msg)

    iota2_dir_final = os.path.join(iota2_dir, "final")
    work_dir = iota2_dir_final
    wd_merge = os.path.join(iota2_dir_final, "merge_final_classifications")
    if working_directory:
        work_dir = working_directory
        wd_merge = working_directory

    final_classifications = [
        fut.file_search_and(iota2_dir_final, True, f"Classif_Seed_{run}.tif")[0]
        for run in range(runs)
    ]
    fusion_path = os.path.join(work_dir, fusion_name)
    pix_format = [
        rasterio.open(raster_file).dtypes[0] for raster_file in final_classifications
    ]
    pix_type = "uint32" if "uint32" in pix_format else "uint8"

    fusion_options = compute_fusion_options(
        iota2_dir_final,
        final_classifications,
        method,
        undecidedlabel,
        dempstershafer_mob,
        pix_type,
        fusion_path,
    )
    logger.debug("fusion options:")
    logger.debug(fusion_options)
    fusion_app = otbApp.CreateFusionOfClassificationsApplication(fusion_options)
    logger.debug("START fusion of final classifications")
    fusion_app.ExecuteAndWriteOutput()
    logger.debug("END fusion of final classifications")

    encoded_raster_bool = False
    re_encoded_raster_path = fusion_path.replace(".tif", "_user_labels.tif")
    labels_conversion = None
    if labels_raster_table:
        reverse_labels = {v: k for k, v in labels_raster_table.items()}
        encoded_raster_bool, pix_type = re_encode_raster(
            fusion_path, re_encoded_raster_path, reverse_labels, logger=logger
        )
        labels_conversion = reverse_labels
    raster_to_color_path = fusion_path
    if encoded_raster_bool:
        raster_to_color_path = re_encoded_raster_path
    fusion_color_index = color.create_indexed_color_image(
        raster_to_color_path,
        color_file,
        co_option=["COMPRESS=LZW"],
        output_pix_type=gdal.GDT_Byte if pix_type == "uint8" else gdal.GDT_UInt16,
        labels_conversion=labels_conversion,
        logger=logger,
    )
    if encoded_raster_bool:
        os.remove(re_encoded_raster_path)
    confusion_matrix = os.path.join(
        iota2_dir_final, "merge_final_classifications", "confusion_mat_maj_vote.csv"
    )
    vector_val = fut.file_search_and(
        os.path.join(iota2_dir_final, "merge_final_classifications"),
        True,
        "majvote.sqlite",
    )

    if validation_shape:
        validation_vector = validation_shape
    else:
        vf.merge_sqlite(fusion_vec_name, wd_merge, vector_val)
        validation_vector = os.path.join(wd_merge, fusion_vec_name + ".sqlite")

    if re_encode_labels:
        data_field = user_labels_original
    confusion = otbApp.CreateComputeConfusionMatrixApplication(
        {
            "in": fusion_path,
            "out": confusion_matrix,
            "ref": "vector",
            "ref.vector.nodata": "0",
            "ref.vector.in": validation_vector,
            "ref.vector.field": data_field.lower(),
            "nodatalabel": "0",
            "ram": "5000",
        }
    )
    confusion.ExecuteAndWriteOutput()
    maj_vote_conf_mat = os.path.join(iota2_dir_final, confusion_matrix_name)
    ru.gen_confusion_matrix_fig(
        csv_in=confusion_matrix,
        out_png=maj_vote_conf_mat,
        nomenclature_path=nom_path,
        undecidedlabel=undecidedlabel,
        labels_table=labels_raster_table,
        dpi=900,
    )

    if keep_runs_results:
        seed_results = fut.file_search_and(iota2_dir_final, True, "RESULTS.txt")[0]
        shutil.copy(seed_results, os.path.join(iota2_dir_final, new_results_seed_file))

    maj_vote_report = os.path.join(iota2_dir_final, "RESULTS.txt")

    ru.stats_report(
        csv_in=[confusion_matrix],
        nomenclature_path=nom_path,
        out_report=maj_vote_report,
        labels_table=labels_raster_table,
        undecidedlabel=undecidedlabel,
    )

    if working_directory:
        shutil.copy(fusion_path, iota2_dir_final)
        shutil.copy(fusion_color_index, iota2_dir_final)
        os.remove(fusion_path)
