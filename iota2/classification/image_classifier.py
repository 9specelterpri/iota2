#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" This module contains pixel classification tools"""
import logging
import os
import sqlite3
from typing import Any, Optional, Union

import pandas as pd

from iota2.common import file_utils as fu
from iota2.common import raster_utils as ru
from iota2.vector_tools.vector_functions import get_layer_name

SensorsParamsType = dict[str, Union[str, list[str], int]]
FunctionNameWithParams = tuple[str, dict[str, Any]]

LOGGER = logging.getLogger("distributed.worker")


def autocontext_launch_classif(
    parameters_dict: dict[str, Union[str, list[str]]],
    classifier_type: str,
    tile: str,
    proba_map_expected: bool,
    dimred: bool,
    data_field: str,
    write_features: bool,
    reduction_mode: str,
    iota2_run_dir: str,
    sar_optical_post_fusion: bool,
    sensors_parameters: SensorsParamsType,
    ram: int,
    working_directory: str,
) -> None:
    """
    Launch the classification in autocontext mode.

    Parameters
    ----------
    parameter_dict:
        A dictionnary containing the parameters for autocontext
    classifier_type:
        the name of the classifier
    tile:
        the tile to be processed
    proba_map_expected:
        enable the creation of probability maps
    dimred:
        enable the dimensionality reduction
    data_field:
        the id class name field
    write_features:
        enable the save of temporary data
    reduction_mode:
        the mode for dimensionality reduction
    iota2_run_dir:
        the output path
    sar_optical_post_fusion:
        enable the fusion between SAR and optical
    nomenclature_path:
        the nomenclature file
    sensors_parameters:
        the dictionary containing all information about sensors
    ram:
        the available ram
    working_directory:
        temporary folder to save data
    """
    from iota2.common.file_utils import file_search_and
    from iota2.vector_tools.vector_functions import get_field_element

    pixtype = "uint8"

    models = parameters_dict["model_list"]
    classif_mask = parameters_dict["tile_mask"]

    class_labels_in_model = []
    sample_sel_directory = os.path.join(iota2_run_dir, "samplesSelection")
    model_sample_sel = file_search_and(
        sample_sel_directory,
        True,
        f"samples_region_{parameters_dict['model_name']}"
        f"_seed_{parameters_dict['seed_num']}.shp",
    )[0]
    class_labels_in_model = get_field_element(
        model_sample_sel, driver_name="ESRI Shapefile", field=data_field, mode="unique"
    )

    stats = os.path.join(
        iota2_run_dir,
        "stats",
        f"Model_{parameters_dict['model_name']}"
        f"_seed_{parameters_dict['seed_num']}.xml",
    )

    output_classif = (
        f"Classif_{parameters_dict['tile']}"
        f"_model_{parameters_dict['model_name']}"
        f"_seed_{parameters_dict['seed_num']}.tif"
    )
    launch_classification(
        classif_mask,
        models,
        stats,
        output_classif,
        working_directory,
        classifier_type,
        tile,
        proba_map_expected,
        dimred,
        sar_optical_post_fusion,
        iota2_run_dir,
        data_field,
        write_features,
        reduction_mode,
        sensors_parameters,
        pixtype,
        all_class=[],  # autocontext not need the list of classes
        chunk_config=None,
        ram=ram,
        auto_context={
            "labels_list": class_labels_in_model,
            "tile_segmentation": parameters_dict["tile_segmentation"],
        },
        multi_param_cust={
            "enable_raw": False,  # auto context cannot use raw data
            "enable_gap": True,
        },
        logger=LOGGER,
    )


class Iota2Classification:
    """Class for pixel classification using OTB"""

    def __init__(
        self,
        features_stack,
        classifier_type,
        model,
        tile,
        output_directory,
        models_class,
        all_class: list[int],
        proba_map: bool = False,
        classif_mask: Optional[str] = None,
        boundary_mask: Optional[str] = None,
        pixtype: str = "uint8",
        working_directory: Optional[str] = None,
        stat_norm: Optional[str] = None,
        ram: int = 128,
        auto_context: Optional[dict[str, Union[str, int]]] = None,
        external_features_flag: bool = False,
        targeted_chunk: Optional[int] = None,
        mode: str = "usually",
        logger=LOGGER,
    ) -> None:
        """
        Create a instance for classification

        PARAMETERS
        ----------
        features_stack:
            the time series to be classified
        classifier_type:
            the classifier name
        model:
            the model name
        tile:
            the tile name
        output_directory:
            the output path
        models_class:
            TODO
        proba_map:
            if enabled write the probability maps
        classif_mask:
            if provided the produced classification is masked
        pixtype:
            define the pixel type for classification
        working_directory:
            temporarly folder for writing data
        stat_norm:
            a statistics file name. if provided data are normalized
        ram:
            the ram available for OTB applications
        auto_context:
            a dictionary containing all the information for use autocontext
        # logger:
        #    File for logging
        external_features_flag:
            enable the use of external features
        targeted_chunk:
            indicate the part of image to be processed
        mode:
            the classificaton mode
        """
        self.models_class = models_class
        self.classif_mask = classif_mask
        self.boundary_mask = boundary_mask
        self.output_directory = output_directory
        self.ram = ram
        self.pixtype = pixtype
        self.stats = stat_norm
        self.classifier_model = model
        self.auto_context = auto_context if auto_context else {}
        self.tile = tile
        self.external_features = external_features_flag
        self.logger = logger
        self.all_class = all_class
        # use this to remove one parameter
        # self.external_features = targeted_chunk is not None

        if isinstance(model, list):
            self.model_name = self.get_model_name(os.path.split(model[0])[0])
            self.seed = self.get_model_seed(os.path.split(model[0])[0])
        else:
            self.model_name = self.get_model_name(model)
            self.seed = self.get_model_seed(model)
        self.features_stack = features_stack
        sub_name = "" if targeted_chunk is None else f"_SUBREGION_{targeted_chunk}"
        classification_name = (
            f"Classif_{tile}_model_{self.model_name}" f"_seed_{self.seed}{sub_name}.tif"
        )
        confidence_name = (
            f"{tile}_model_{self.model_name}_"
            f"confidence_seed_{self.seed}{sub_name}.tif"
        )
        proba_map_name = (
            f"PROBAMAP_{tile}_model_"
            f"{self.model_name}_seed_{self.seed}{sub_name}.tif"
        )
        if mode == "SAR":
            classification_name = classification_name.replace(".tif", "_SAR.tif")
            confidence_name = confidence_name.replace(".tif", "_SAR.tif")
            proba_map_name = proba_map_name.replace(".tif", "_SAR.tif")
        self.classification = os.path.join(output_directory, classification_name)
        self.confidence = os.path.join(output_directory, confidence_name)
        self.proba_map_path = self.get_proba_map(
            classifier_type, output_directory, proba_map, proba_map_name
        )
        self.working_directory = working_directory

    def get_proba_map(
        self,
        classifier_type: str,
        output_directory: str,
        gen_proba: bool,
        proba_map_name: str,
    ) -> str:
        """
        Get probability map absolute path

        Parameters
        ----------
        classifier_type :
            classifier's name (provided by OTB)
        output_directory :
            output directory
        gen_proba :
            indicate if proba map is asked
        proba_map_name :
            probability raster name

        """
        proba_map = ""
        classifier_avail = ["sharkrf"]
        if classifier_type in classifier_avail:
            proba_map = os.path.join(output_directory, proba_map_name)
        if gen_proba and proba_map == "":
            warn_mes = (
                f"classifier '{classifier_type}' not available to generate"
                f" a probability map, those available are {classifier_avail}"
            )
            self.logger.warning(warn_mes)
        return proba_map if gen_proba else ""

    def get_model_name(self, model: str):
        """
        from a full path return the basename of model

        Parameters
        ----------
        model:
            the model full path
        """
        return os.path.splitext(os.path.basename(model))[0].split("_")[1]

    def get_model_seed(self, model: str):
        """
        From model name return the seed number
        Parameters
        ----------
        model:
            the model name
        """
        return os.path.splitext(os.path.basename(model))[0].split("_")[3]

    def set_autocontext_options(self):
        """Set options for autocontext"""
        from iota2.common.file_utils import ensure_dir

        tmp_dir = os.path.join(
            self.output_directory,
            f"tmp_model_{self.model_name}_seed_{self.seed}_tile_{self.tile}",
        )
        if self.working_directory:
            tmp_dir = os.path.join(
                self.working_directory,
                f"tmp_model_{self.model_name}_seed_{self.seed}_tile_{self.tile}",
            )

        ensure_dir(tmp_dir)
        classifier_options = {
            "in": self.features_stack,
            "inseg": self.auto_context["tile_segmentation"],
            "models": self.classifier_model,
            "lablist": [str(lab) for lab in self.auto_context["labels_list"]],
            "confmap": f"{self.confidence}?&writegeom=false",
            "ram": str(0.4 * float(self.ram)),
            "pixType": self.pixtype,
            "tmpdir": tmp_dir,
            "out": f"{self.classification}?&writegeom=false",
        }

        return classifier_options, tmp_dir

    def apply_mask(self, nb_class_run: int) -> None:
        """
        If a mask is provided, apply it to all products

        Parameters
        ----------
        nb_class_run:
            the number of classes readed in the model file
        """
        from iota2.common.otb_app_bank import (
            CreateBandMathApplication,
            CreateBandMathXApplication,
        )

        if self.classif_mask:
            mask_filter = CreateBandMathApplication(
                {
                    "il": [self.classification, self.classif_mask],
                    "ram": str(self.ram),
                    "pixType": self.pixtype,
                    "out": self.classification,
                    "exp": "im2b1>=1?im1b1:0",
                }
            )
            mask_filter.ExecuteAndWriteOutput()
            mask_filter = CreateBandMathApplication(
                {
                    "il": [self.confidence, self.classif_mask],
                    "ram": str(self.ram),
                    "pixType": "float",
                    "out": self.confidence,
                    "exp": "im2b1>=1?im1b1:0",
                }
            )
            mask_filter.ExecuteAndWriteOutput()
            if self.proba_map_path:
                mask = (
                    self.classif_mask
                    if self.boundary_mask is None
                    else self.boundary_mask
                )
                expr = f"im2b1>=1?im1:{'{' + ';'.join(['0'] * nb_class_run) + '}'}"
                mask_filter = CreateBandMathXApplication(
                    {
                        "il": [self.proba_map_path, mask],
                        "ram": str(self.ram),
                        "pixType": "uint16",
                        "out": self.proba_map_path,
                        "exp": expr,
                    }
                )
                mask_filter.ExecuteAndWriteOutput()

    def clean_working_directory(self, tmp_dir: str):
        """
        Move classification from working directory to output_path
        Then remove all temporarly data
        Parameters
        ----------
        tmp_dir:
            temporary folder
        """
        import shutil

        if self.working_directory:
            shutil.copy(
                self.classification,
                os.path.join(
                    self.output_directory, os.path.split(self.classification)[-1]
                ),
            )
            # os.remove(self.classification)
            shutil.copy(
                self.confidence,
                os.path.join(self.output_directory, os.path.split(self.confidence)[-1]),
            )
            # os.remove(self.confidence)
            if self.proba_map_path:
                shutil.copy(
                    self.proba_map_path,
                    os.path.join(
                        self.output_directory, os.path.split(self.proba_map_path)[-1]
                    ),
                )
                # os.remove(self.proba_map_path)
            if self.auto_context:
                shutil.rmtree(tmp_dir)

    def generate(self):
        """
        Create and execute all applications for produce classification
        """

        from iota2.common.otb_app_bank import (
            CreateClassifyAutoContext,
            CreateImageClassifierApplication,
        )

        if self.working_directory:
            self.classification = os.path.join(
                self.working_directory, os.path.split(self.classification)[-1]
            )
            self.confidence = os.path.join(
                self.working_directory, os.path.split(self.confidence)[-1]
            )

        classifier_options = {
            "in": self.features_stack,
            "model": self.classifier_model,
            "confmap": f"{self.confidence}?&writegeom=false",
            "ram": str(0.2 * float(self.ram)),
            "pixType": self.pixtype,
            "out": f"{self.classification}?&writegeom=false",
        }

        tmp_dir = None
        if self.auto_context:
            classifier_options, tmp_dir = self.set_autocontext_options()
        # Used only if probamap is asked then 0 is a good default value
        nb_class_run = 0
        if self.proba_map_path:
            # all_class = []
            # # change models_class par un autre objet qui connait toutes les classes
            # # for _, dico_seed in list(self.models_class.items()):
            # #     for _, avail_class in list(dico_seed.items()):
            # #         all_class += avail_class
            # all_class = sorted(list(set(all_class)))
            nb_class_run = len(self.all_class)
            if self.working_directory:
                self.proba_map_path = os.path.join(
                    self.working_directory, os.path.split(self.proba_map_path)[-1]
                )
            classifier_options["probamap"] = f"{self.proba_map_path}?&writegeom=false"
            classifier_options["nbclasses"] = str(nb_class_run)

        if self.stats:
            classifier_options["imstat"] = self.stats
        if self.auto_context:
            classifier = CreateClassifyAutoContext(classifier_options)
        else:
            classifier = CreateImageClassifierApplication(classifier_options)
            if self.external_features:
                classifier.ImportVectorImage("in", self.features_stack)

        self.logger.info(f"Compute Classification : {self.classification}")
        self.logger.info(
            "ram before classification : " f"{fu.memory_usage_psutil()} MB"
        )
        classifier.ExecuteAndWriteOutput()
        self.logger.info("ram after classification : " f"{fu.memory_usage_psutil()} MB")
        self.logger.info(f"Classification : {self.classification} done")

        self.apply_mask(nb_class_run)
        if self.proba_map_path:
            class_model = self.models_class[self.model_name][int(self.seed)]
            if len(class_model) != len(self.all_class):
                self.logger.info(
                    f"reordering the probability map : {self.proba_map_path}"
                )

                ru.reorder_proba_map(
                    self.proba_map_path,
                    self.proba_map_path,
                    class_model,
                    self.all_class,
                    pix_type="uint16",
                )
        self.clean_working_directory(tmp_dir)


def get_model_dictionnary(model: str) -> list[int]:
    """
    From a model file, get the class dictionary
    Parameters
    ----------
    model:
        the model file
    """
    classes: list[int] = []
    with open(model) as modelfile:
        line = next(modelfile)
        if "#" in line and "with_dictionary" in line:
            _classes = next(modelfile).split(" ")[1:-1]
            classes = [int(x) for x in _classes]
    return classes


def get_class_by_models_from_i2_learn(
    iota2_learning_database_dir: str, label_field: str, region_field: str
) -> dict:
    """
    inform which class will be used to by models

    Parameters
    ----------
    iota2_learning_database_dir:
        folder containing learning samples vector files
    label_field:
        label name column in shapefile
    region_field:
        region name column in shapefile
    """
    # check only seed 0
    learning_files = fu.file_search_and(
        iota2_learning_database_dir, True, "seed_0_learn.sqlite"
    )
    class_model = {}
    for learning_file in learning_files:
        layer_name = get_layer_name(learning_file, "SQLite")
        conn = sqlite3.connect(learning_file)
        df_train = pd.read_sql_query(
            f"select {region_field},{label_field} from {layer_name}", conn
        )
        region_labels = fu.sort_by_first_elem(list(map(list, list(df_train.values))))
        for region, labels in region_labels:
            if region not in class_model:
                class_model[region] = set()
            class_model[region] = class_model[region].union(set(labels))
    class_model_out = {}
    for region_name, region_labels in class_model.items():
        class_model_out[region_name] = sorted(list(region_labels))
    return class_model_out


def get_class_by_models(
    iota2_samples_dir: str, data_field: str, model: Optional[str] = None
) -> dict[str, int]:
    """inform which class will be used to by models

    Parameters
    ----------
    iota2_samples_dir :
        path to the directory containing samples dedicated to learn models

    data_field :
        field which contains labels in vector file
    model :
        the model file
    Return
    ------
    dic[model][seed]

    Example
    -------
    >>> dico_models = get_class_by_models("/somewhere/learningSamples", "code")
    >>> print dico_models["1"][0]
    >>> [11, 12, 31]
    """
    from iota2.common.file_utils import file_search_and
    from iota2.vector_tools.vector_functions import get_field_element

    class_models = {}
    if model is not None:
        modelpath = os.path.dirname(model)
        models_files = file_search_and(modelpath, True, "model", "seed", ".txt")

        for model_file in models_files:
            model_name = os.path.splitext(os.path.basename(model_file))[0].split("_")[1]
            class_models[model_name] = {}

        for model_file in models_files:
            model_name = os.path.splitext(os.path.basename(model_file))[0].split("_")[1]
            seed_number = int(
                os.path.splitext(os.path.basename(model_file))[0]
                .split("_")[3]
                .replace(".txt", "")
            )
            classes = get_model_dictionnary(model_file)
            class_models[model_name][seed_number] = classes
    else:
        samples_files = file_search_and(
            iota2_samples_dir, True, "Samples_region_", "_seed", "_learn.sqlite"
        )

        for samples_file in samples_files:
            model_name = os.path.splitext(os.path.basename(samples_file))[0].split("_")[
                2
            ]
            class_models[model_name] = {}
        for samples_file in samples_files:
            model_name = os.path.splitext(os.path.basename(samples_file))[0].split("_")[
                2
            ]
            seed_number = int(
                os.path.splitext(os.path.basename(samples_file))[0]
                .split("_")[3]
                .replace("seed", "")
            )
            class_models[model_name][seed_number] = sorted(
                get_field_element(
                    samples_file,
                    driver_name="SQLite",
                    field=data_field.lower(),
                    mode="unique",
                    elem_type="int",
                )
            )
    return class_models


def launch_classification(
    classifmask: str,
    model: str,
    stats: str,
    output_classif: str,
    path_wd: str,
    classifier_type: str,
    tile: str,
    proba_map_expected: bool,
    dimred: bool,
    sar_optical_post_fusion: bool,
    output_path: str,
    data_field: str,
    write_features: bool,
    reduction_mode: str,
    sensors_parameters: SensorsParamsType,
    pixtype: str,
    all_class: list[int],
    boundary_mask: Optional[str] = None,
    ram: int = 500,
    auto_context: Optional[dict[str, str]] = None,
    force_standard_labels: Optional[bool] = None,
    external_features: bool = False,
    module_path: Optional[str] = None,
    list_functions: Optional[list[FunctionNameWithParams]] = None,
    chunk_config: ru.ChunkConfig = None,
    targeted_chunk: Optional[int] = None,
    concat_mode: bool = True,
    multi_param_cust=None,
    logger=LOGGER,
) -> None:
    """
    Launch the classification

    Parameters
    ----------
    classifmask:
        the classification mask
    model:
        the model name
    stats:
        the statistics file name
    output_classif:
        the output classification name
    path_wd:
        working directory path for temporarly data
    classifier_type:
        the classifier name
    tile:
        the tile name to be classified
    proba_map_expected:
        enable the writting of probability map (sharkrf only)
    dimred:
        enable the dimensionality reduction
    sar_optical_post_fusion:
        enable fusion between SAR and optical data
    output_path:
        the path where final classification must be stored
    data_field:
        the column name for class labels
    write_features:
        write the features stack
    reduction_mode:
        parameters for reduction mode
    sensors_parameters:
        dictionary with all sensors information
    pixtype:
        the output pixel type
    ram:
        the ram available for OTB applications
    auto_context:
        a dictionnary of all parameters for autocontext
    force_standard_labels:
        use generic label (value_x) instead of named label (sensor_band_date)
    external_features:
        enable external features mode
    module_path:
        the path to external features code
    list_functions:
        the list of function and parameters used to compute external features
    number_of_chunks:
        the number of chunks required
    chunk_size_mode:
        the image split mode
    chunk_size_x:
        the number of pixel in column for extracted chunk
    chunk_size_y:
        the number of pixel in row for extracted chunk
    targeted_chunk:
        indicate which chunk must be processed
    concat_mode:
        activate the concatenation mode
    logger:
        the logging instance

    Notes
    -----

    - Several parameter are activated only if external_features is set to true:
        - module_path
        - list_functions
        - number_of_chunks
        - chunk_size_mode
        - chunk_size_x
        - chunk_size_y
        - targeted_chunk
        - concat_mode

    - chunk_size_mode has two value and requiere different parameters for each

    +-------------------+-----------------------+
    | Parameter         | chunk_size_mode value |
    +-------------------+-----------------------+
    | chunk_size_x      | user_fixed            |
    +-------------------+-----------------------+
    | chunk_size_y      | user_fixed            |
    +-------------------+-----------------------+
    | number_of_chunks  | split_number          |
    +-------------------+-----------------------+
    | targeted_chunk    | both                  |
    +-------------------+-----------------------+

    - concat_mode:
        If enabled the external features are concatenated with all iota2
        features (reflectance, NDVI, NDWI,...)
        If disabled only the external features are used
    """
    from iota2.common import generate_features as genFeatures
    from iota2.common.custom_numpy_features import compute_custom_features
    from iota2.common.otb_app_bank import (
        CreateExtractROIApplication,
        getInputParameterOutput,
    )
    from iota2.sampling import dimensionality_reduction as DR

    auto_context = auto_context if auto_context else {}
    output_directory = os.path.join(output_path, "classif")

    features_path = os.path.join(output_path, "features")

    working_dir = path_wd
    if not path_wd:
        working_dir = features_path

    working_dir = os.path.join(features_path, tile)

    if path_wd:
        working_dir = os.path.join(path_wd, tile)
        if not os.path.exists(working_dir):
            try:
                os.mkdir(working_dir)
            except OSError:
                logger.warning(f"{working_dir} already exists")

    mode = "usually"
    if "SAR.tif" in output_classif:
        mode = "SAR"

    (
        apps_dict,
        labs_dict,  # pylint: disable=W0612
        dep_features,  # pylint: disable=W0612
    ) = genFeatures.generate_features(
        pathWd=working_dir,
        tile=tile,
        sar_optical_post_fusion=sar_optical_post_fusion,
        output_path=output_path,
        sensors_parameters=sensors_parameters,
        mode=mode,
        force_standard_labels=force_standard_labels,
        logger=logger,
    )
    apps_dict["enable_interp"] = multi_param_cust["enable_gap"]
    apps_dict["enable_raw"] = multi_param_cust["enable_raw"]
    apps_dict["enable_masks"] = multi_param_cust["enable_raw"]
    remove_flag = False
    if external_features:
        (
            otbimage,
            _,
            _,
            _,
            _,
            deps_pipeline,  # pylint: disable=W0612
        ) = compute_custom_features(
            tile=tile,
            output_path=output_path,
            sensors_parameters=sensors_parameters,
            module_path=module_path,
            list_functions=list_functions,
            otb_pipelines=apps_dict,
            feat_labels=[],
            path_wd=working_dir,
            chunk_config=chunk_config,
            targeted_chunk=targeted_chunk,
            concat_mode=concat_mode,
            exogeneous_data=multi_param_cust["exogeneous_data"],
            enabled_raw=multi_param_cust["enable_raw"],
            enabled_gap=multi_param_cust["enable_gap"],
            fill_missing_dates=multi_param_cust["fill_missing_dates"],
            all_dates_dict=multi_param_cust["all_dates_dict"],
            mask_valid_data=None,
            mask_value=0,
            logger=logger,
        )
        classif_input = otbimage
        if classifmask:
            chunked_mask = os.path.join(
                working_dir, f"Chunk_{targeted_chunk}_classif_mask.tif"
            )
            roi_param = {
                "in": classifmask,
                "out": chunked_mask,
                "mode": "fit",
                "mode.fit.im": otbimage,
                "ram": ram,
            }
            roi_mask = CreateExtractROIApplication(roi_param)
            roi_mask.ExecuteAndWriteOutput()
            roi_mask = None
            classifmask = chunked_mask
            remove_flag = True
    else:
        all_features = apps_dict["interp"]
        feature_raster = all_features.GetParameterValue(
            getInputParameterOutput(all_features)
        )
        if write_features:
            if not os.path.exists(feature_raster):
                all_features.ExecuteAndWriteOutput()
            all_features = feature_raster
        else:
            all_features.Execute()
        classif_input = all_features

    if dimred:
        logger.debug(f"Classification model : {model}")
        dim_red_model_list = DR.get_dim_red_models_from_classification_model(model)
        logger.debug(f"Dim red models : {dim_red_model_list}")
        [
            classif_input,
            other_dep,  # pylint: disable=W0612
        ] = DR.apply_dimensionality_reduction_to_feature_stack(
            reduction_mode, output_path, all_features, dim_red_model_list
        )
        if write_features:
            classif_input.ExecuteAndWriteOutput()
        else:
            classif_input.Execute()

    iota2_samples_dir = os.path.join(output_path, "learningSamples")
    models_class = get_class_by_models(
        iota2_samples_dir, data_field, model=model if proba_map_expected else None
    )
    classif = Iota2Classification(
        features_stack=classif_input,
        classifier_type=classifier_type,
        model=model,
        tile=tile,
        output_directory=output_directory,
        models_class=models_class,
        all_class=all_class,
        proba_map=proba_map_expected,
        working_directory=path_wd,
        classif_mask=classifmask,
        pixtype=pixtype,
        stat_norm=stats,
        ram=ram,
        mode=mode,
        external_features_flag=external_features,
        targeted_chunk=targeted_chunk,
        auto_context=auto_context,
        boundary_mask=boundary_mask,
        logger=logger,
    )
    classif.generate()
    if remove_flag:
        os.remove(chunked_mask)


# if __name__ == "__main__":

#     from iota2.configuration_files import read_config_file as rcf
#     PARSER = argparse.ArgumentParser(
#         description=("Performs a classification of the input image "
#                      "(compute in ram) according to a model file, "))
#     PARSER.add_argument(
#         "-in",
#         dest="tempFolderSerie",
#         help="path to the folder which contains temporal series",
#         default=None,
#         required=True)
#     PARSER.add_argument("-mask",
#                         dest="Classifmask",
#                         help="path to classification's mask",
#                         default=None,
#                         required=True)
#     PARSER.add_argument("-classifier_type",
#                         dest="classifier_type",
#                         help="classifier name",
#                         required=True)
#     PARSER.add_argument("-tile",
#                         dest="tile",
#                         help="tile's name",
#                         required=True)
#     PARSER.add_argument("-proba_map_expected",
#                         dest="proba_map_expected",
#                         help="is probality maps were generated",
#                         type=str2bool,
#                         default=False,
#                         required=False)
#     PARSER.add_argument("-dimred",
#                         dest="dimred",
#                         help="flag to use dimensionality reduction",
#                         type=str2bool,
#                         default=False,
#                         required=False)
#     PARSER.add_argument("-sar_optical_post_fusion",
#                         dest="sar_optical_post_fusion",
#                         help=("flag to enable sar and optical "
#                               "post-classification fusion"),
#                         type=str2bool,
#                         default=False,
#                         required=False)
#     PARSER.add_argument("-reduction_mode",
#                         dest="reduction_mode",
#                         help="reduction mode",
#                         default=None,
#                         required=True)
#     PARSER.add_argument(
#         "-data_field",
#         dest="data_field",
#         help="field containing labels in the groundtruth database",
#         default=None,
#         required=True)
#     PARSER.add_argument("-pixType",
#                         dest="pixType",
#                         help="pixel format",
#                         default=None,
#                         required=True)
#     PARSER.add_argument("-model",
#                         dest="model",
#                         help="path to the model",
#                         default=None,
#                         required=True)
#     PARSER.add_argument("-imstat",
#                         dest="stats",
#                         help="path to statistics",
#                         default=None,
#                         required=False)
#     PARSER.add_argument("-out",
#                         dest="output_classif",
#                         help="output classification's path",
#                         default=None,
#                         required=True)
#     PARSER.add_argument("-confmap",
#                         dest="confmap",
#                         help="output classification confidence map",
#                         default=None,
#                         required=True)
#     PARSER.add_argument("-output_path",
#                         dest="output_path",
#                         help="iota2 output path",
#                         required=True)
#     PARSER.add_argument("-ram",
#                         dest="ram",
#                         help="pipeline's size",
#                         default=128,
#                         required=False)
#     PARSER.add_argument("--wd",
#                         dest="pathWd",
#                         help="path to the working directory",
#                         default=None,
#                         required=False)
#     PARSER.add_argument("-conf",
#                         help="path to the configuration file (mandatory)",
#                         dest="pathConf",
#                         required=True)
#     PARSER.add_argument("-maxCPU",
#                         help="True : Class all the image and after apply mask",
#                         dest="MaximizeCPU",
#                         default="False",
#                         choices=["True", "False"],
#                         required=False)
#     ARGS = PARSER.parse_args()
#     CFG = rcf.read_config_file(ARGS.pathConf)
#     I2_PARAMS = rcf.iota2_parameters(ARGS.pathConf)
#     SENSORS_PARAMETERS = I2_PARAMS.get_sensors_parameters(ARGS.tile)
#     AUTO_CONTEXT_PARAMS = {}
#     if CFG.getParam("chain", "enable_autocontext"):
#         AUTO_CONTEXT_PARAMS = autocontext_classification_param(
#             ARGS.output_path, ARGS.data_field)
#     launch_classification(
#         ARGS.Classifmask, ARGS.model, ARGS.stats, ARGS.output_classif,
#         ARGS.pathWd, ARGS.classifier_type, ARGS.tile, ARGS.proba_map_expected,
#         ARGS.dimred, ARGS.sar_optical_post_fusion, ARGS.output_path,
#         ARGS.data_field, ARGS.write_features, ARGS.reduction_mode,
#         SENSORS_PARAMETERS, ARGS.pixType, AUTO_CONTEXT_PARAMS)
