#!/usr/bin/env python3

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
from itertools import product

from iota2.classification.py_classifiers import choose_best_model
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class ModelChoice(iota2_step.Step):
    """class dedicated to be used only with the use of pytorch workflow"""

    resources_block_name = "model_choice"

    def __init__(self, cfg, cfg_resources_file):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.runs = rcf.read_config_file(self.cfg).getParam("arg_train", "runs")
        self.suffix_list = ["usually"]
        if (
            rcf.read_config_file(self.cfg).getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
            is True
        ):
            self.suffix_list.append("SAR")

        self.neural_network = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "deep_learning_parameters"
        )

        batch_size_list = self.neural_network["hyperparameters_solver"]["batch_size"]
        learning_rate_list = self.neural_network["hyperparameters_solver"][
            "learning_rate"
        ]
        hyperparameters_couples = list(product(batch_size_list, learning_rate_list))
        selection_criterion = self.neural_network["model_selection_criterion"]

        # dep
        for suffix in self.suffix_list:
            for model_name, _ in self.spatial_models_distribution.items():
                for seed in range(self.runs):
                    target_model = f"model_{model_name}_seed_{seed}_{suffix}"
                    output_model = os.path.join(
                        self.output_path, "model", f"model_{model_name}_seed_{seed}.txt"
                    )
                    if suffix == "SAR":
                        output_model = output_model.replace(".txt", "_SAR.txt")
                    dep_tasks = []
                    hyperparameters_models = []
                    for hyper_num in range(len(hyperparameters_couples)):
                        dep_tasks.append(f"{target_model}_hyp_{hyper_num}")
                        hyperparameters_models.append(
                            f"{output_model.replace('.txt','')}_hyp_{hyper_num}.txt"
                        )
                    task = self.i2_task(
                        task_name=f"choose_{target_model}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f": choose_best_model,
                            "input_pytorch_models": hyperparameters_models,
                            "selection_criterion": selection_criterion,
                            "output_model_file": output_model,
                            "model_parameters_file": output_model.replace(
                                ".txt", "_parameters.pickle"
                            ),
                            "encoder_convert_file": output_model.replace(
                                ".txt", "_encoder.pickle"
                            ),
                            "hyperparameters_file": output_model.replace(
                                ".txt", "_hyper.pickle"
                            ),
                        },
                        task_resources=self.get_resources(),
                    )
                    dep_key = "region_tasks"
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="region_tasks",
                        task_sub_group=f"{target_model}",
                        task_dep_dico={dep_key: dep_tasks},
                    )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = (
            "from pytorch models generated for each"
            " hyperparameters couples, choose the best one"
        )
        return description
