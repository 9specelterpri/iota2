#!/usr/bin/env python3

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

from iota2.common.custom_numpy_features import exogenous_data_tile
from iota2.common.raster_utils import ChunkConfig
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.sampling import vector_sampler as vs
from iota2.steps import iota2_step
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class SamplesExtraction(iota2_step.Step):
    resources_block_name = "vectorSampler"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.working_directory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.ram_extraction = 1024.0 * get_ram(self.get_resources()["ram"])
        self.features_from_raw_dates = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "features_from_raw_dates"
        )
        self.custom_features = rcf.read_config_file(self.cfg).getParam(
            "external_features", "external_features_flag"
        )
        if self.custom_features:
            self.chunk_config = ChunkConfig(
                chunk_size_mode=rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "chunk_size_mode"
                ),
                number_of_chunks=rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "number_of_chunks"
                ),
                chunk_size_x=rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "chunk_size_x"
                ),
                chunk_size_y=rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "chunk_size_y"
                ),
                padding_size_x=rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "padding_size_x"
                ),
                padding_size_y=rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "padding_size_y"
                ),
            )

        # implement tests for check if custom features are well provided
        # so the chain failed during step init
        output_path_annual = None
        annual_config_file = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "prev_features"
        )
        if annual_config_file:
            output_path_annual = rcf.read_config_file(annual_config_file).getParam(
                "chain", "output_path"
            )
        sampling_validation = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "sampling_validation"
        )
        user_ground_truth = rcf.read_config_file(self.cfg).getParam(
            "chain", "ground_truth"
        )
        user_data_field = rcf.read_config_file(self.cfg).getParam("chain", "data_field")
        self.user_labels_to_i2_labels = get_re_encoding_labels_dic(
            user_ground_truth, user_data_field
        )
        enable_gap, enable_raw = rcf.read_config_file(self.cfg).getParam(
            "python_data_managing", "data_mode_access"
        )

        suffix_list = ["usually"]
        if (
            rcf.read_config_file(self.cfg).getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
            is True
        ):
            suffix_list.append("SAR")

        self.sensors_dates = rcf.iota2_parameters(
            rcf.read_config_file(self.cfg)
        ).get_available_sensors_dates()
        if "Sentinel1" in self.sensors_dates:
            s1_dates = rcf.iota2_parameters(
                rcf.read_config_file(self.cfg)
            ).get_sentinel1_input_dates()
            self.sensors_dates = {**self.sensors_dates, **s1_dates}
            self.sensors_dates.pop("Sentinel1", None)

        for suffix in suffix_list:
            for tile in self.tiles:
                task_name = f"extraction_{tile}"
                if suffix == "SAR":
                    task_name += f"_{suffix}"

                parameters_dic = {
                    "f": vs.generate_samples,
                    "train_shape_dic": {
                        f"{suffix}": os.path.join(
                            self.output_path, "formattingVectors", f"{tile}.shp"
                        )
                    },
                    "path_wd": self.working_directory,
                    "data_field": self.i2_const.re_encoding_label_name,
                    "output_path": rcf.read_config_file(self.cfg).getParam(
                        "chain", "output_path"
                    ),
                    "annual_crop": rcf.read_config_file(self.cfg).getParam(
                        "arg_train", "annual_crop"
                    ),
                    "crop_mix": rcf.read_config_file(self.cfg).getParam(
                        "arg_train", "crop_mix"
                    ),
                    "auto_context_enable": rcf.read_config_file(self.cfg).getParam(
                        "arg_train", "enable_autocontext"
                    ),
                    "region_field": rcf.read_config_file(self.cfg).getParam(
                        "chain", "region_field"
                    ),
                    "proj": rcf.read_config_file(self.cfg).getParam("chain", "proj"),
                    "runs": rcf.read_config_file(self.cfg).getParam(
                        "arg_train", "runs"
                    ),
                    "sensors_parameters": rcf.iota2_parameters(
                        rcf.read_config_file(self.cfg)
                    ).get_sensors_parameters(tile),
                    "sar_optical_post_fusion": rcf.read_config_file(self.cfg).getParam(
                        "arg_train", "dempster_shafer_sar_opt_fusion"
                    ),
                    "samples_classif_mix": rcf.read_config_file(self.cfg).getParam(
                        "arg_train", "samples_classif_mix"
                    ),
                    "output_path_annual": output_path_annual,
                    "ram": self.ram_extraction,
                    "w_mode": rcf.read_config_file(self.cfg).getParam(
                        "sensors_data_interpolation", "write_outputs"
                    ),
                    "folder_annual_features": rcf.read_config_file(self.cfg).getParam(
                        "arg_train", "output_prev_features"
                    ),
                    "previous_classif_path": rcf.read_config_file(self.cfg).getParam(
                        "arg_train", "annual_classes_extraction_source"
                    ),
                    "validity_threshold": rcf.read_config_file(self.cfg).getParam(
                        "arg_train", "validity_threshold"
                    ),
                    "labels_table": self.user_labels_to_i2_labels,
                    "sampling_validation": sampling_validation,
                    "features_from_raw_dates": self.features_from_raw_dates,
                    "multi_param_cust": {
                        "enable_raw": enable_raw,
                        "enable_gap": enable_gap,
                        "fill_missing_dates": rcf.read_config_file(self.cfg).getParam(
                            "python_data_managing", "fill_missing_dates"
                        ),
                        "all_dates_dict": self.sensors_dates,
                        "mask_valid_data": None,
                        "mask_value": 0,
                        "exogeneous_data": exogenous_data_tile(
                            rcf.read_config_file(self.cfg).getParam(
                                "external_features", "exogeneous_data"
                            ),
                            tile,
                        ),
                    },
                }
                if self.custom_features:
                    parameters_dic["custom_features"] = True
                    parameters_dic["module_path"] = rcf.read_config_file(
                        self.cfg
                    ).getParam("external_features", "module")
                    parameters_dic["list_functions"] = rcf.read_config_file(
                        self.cfg
                    ).getParam("external_features", "functions")
                    parameters_dic["force_standard_labels"] = rcf.read_config_file(
                        self.cfg
                    ).getParam("arg_train", "force_standard_labels")
                    parameters_dic["chunk_config"] = self.chunk_config
                    parameters_dic["concat_mode"] = rcf.read_config_file(
                        self.cfg
                    ).getParam("external_features", "concat_mode")
                    for target_chunk in range(self.chunk_config.number_of_chunks):

                        parameters_dic_chunk = parameters_dic.copy()
                        parameters_dic_chunk["targeted_chunk"] = target_chunk
                        task_name_chunk = f"{task_name}_chunk_{target_chunk}"
                        task = self.i2_task(
                            task_name=task_name_chunk,
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters=parameters_dic_chunk,
                            task_resources=self.get_resources(),
                        )
                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks",
                            task_sub_group=f"{tile}_chunk_{target_chunk}_{suffix}",
                            task_dep_dico={"tile_tasks": [tile]},
                        )
                else:
                    task = self.i2_task(
                        task_name=task_name,
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters=parameters_dic,
                        task_resources=self.get_resources(),
                    )
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="tile_tasks",
                        task_sub_group=f"{tile}_{suffix}",
                        task_dep_dico={"tile_tasks": [tile]},
                    )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Extract pixels values by tiles"
        return description
