#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation import classification_shaping as CS
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class Mosaic(iota2_step.Step):
    """Step to merge all classifications in one."""

    resources_block_name = "classifShaping"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')

        self.color_table = rcf.read_config_file(self.cfg).getParam(
            'chain', 'color_table')
        ds_sar_opt = rcf.read_config_file(cfg).getParam(
            'arg_train', 'dempster_shafer_sar_opt_fusion')
        reference_data = rcf.read_config_file(self.cfg).getParam(
            "chain", "ground_truth")
        reference_data_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "data_field")
        old_label_to_new = get_re_encoding_labels_dic(reference_data,
                                                      reference_data_field)
        self.enable_boundary_fusion = rcf.read_config_file(self.cfg).getParam(
            "arg_classification", "enable_boundary_fusion")
        self.comparison_mode = rcf.read_config_file(self.cfg).getParam(
            "arg_classification", "boundary_comparison_mode")

        self.tiles = rcf.read_config_file(self.cfg).getParam(
            "chain", "list_tile").split(" ")
        if self.comparison_mode:
            classif_path_list = [
                os.path.join(self.output_path, "classif"),
                os.path.join(self.output_path, "boundary")
            ]
            final_folder_list = [
                os.path.join(self.output_path, "final", "standard"),
                os.path.join(self.output_path, "final", "boundary")
            ]
            final_tmp_folder_list = [
                os.path.join(self.output_path, "final", "standard", "TMP"),
                os.path.join(self.output_path, "final", "boundary", "TMP")
            ]
        elif self.enable_boundary_fusion:
            classif_path_list = [os.path.join(self.output_path, "boundary")]
            final_folder_list = [os.path.join(self.output_path, "final")]
            final_tmp_folder_list = [
                os.path.join(self.output_path, "final", "TMP")
            ]
        else:
            classif_path_list = [os.path.join(self.output_path, "classif")]
            final_folder_list = [os.path.join(self.output_path, "final")]
            final_tmp_folder_list = [
                os.path.join(self.output_path, "final", "TMP")
            ]
        proba_map_flag = bool(
            rcf.read_config_file(self.cfg).getParam("arg_classification",
                                                    "enable_probability_map")
            and rcf.read_config_file(self.cfg).getParam(
                "arg_classification", "generate_final_probability_map"))
        task = self.i2_task(task_name="mosaic",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters={
                                "f":
                                CS.provide_input_to_mosaic,
                                "classif_path_list":
                                classif_path_list,
                                "runs":
                                self.runs,
                                "final_folder_list":
                                final_folder_list,
                                "final_tmp_folder_list":
                                final_tmp_folder_list,
                                "nb_view_folder":
                                os.path.join(self.output_path, "features"),
                                "validation_input_folder":
                                os.path.join(self.output_path, "dataAppVal"),
                                "working_directory":
                                self.workingDirectory,
                                "classif_mode":
                                rcf.read_config_file(self.cfg).getParam(
                                    "arg_classification", "classif_mode"),
                                "ds_sar_opt":
                                ds_sar_opt,
                                "output_statistics":
                                rcf.read_config_file(self.cfg).getParam(
                                    'chain', 'output_statistics'),
                                "spatial_resolution":
                                rcf.read_config_file(self.cfg).getParam(
                                    "chain", "spatial_resolution"),
                                "proba_map_flag":
                                proba_map_flag,
                                "region_shape":
                                rcf.read_config_file(self.cfg).getParam(
                                    'chain', 'region_path'),
                                "color_path":
                                self.color_table,
                                "data_field":
                                self.i2_const.re_encoding_label_name,
                                "tiles_from_cfg":
                                rcf.read_config_file(self.cfg).getParam(
                                    'chain', 'list_tile'),
                                "labels_conversion":
                                old_label_to_new,
                            },
                            task_resources=self.get_resources())

        dependencies = {}

        for model_name, model_meta in (
                self.spatial_models_distribution_no_sub_splits_classify.items(
                )):
            for seed in range(self.runs):
                model_subdivisions = model_meta["nb_sub_model"]
                dep_group = "tile_tasks_model"
                if model_subdivisions == 1:
                    dep_group = "tile_tasks_model_mode"
                if ds_sar_opt:
                    dep_group = "tile_tasks_model"
                if dep_group not in dependencies:
                    dependencies[dep_group] = []
                for tile in model_meta["tiles"]:
                    if dep_group == "tile_tasks_model_mode":
                        dependencies[dep_group].append(
                            f"{tile}_{model_name}_{seed}_usually")
                    else:
                        dependencies[dep_group].append(
                            f"{tile}_{model_name}_{seed}")
        if self.enable_boundary_fusion:
            dep = []
            for seed in range(self.runs):
                for tile in self.tiles:
                    dep.append(f"{tile}_{seed}")
            dependencies = {"tile_tasks_model": dep}
        self.add_task_to_i2_processing_graph(task,
                                             task_group="mosaic",
                                             task_sub_group="mosaic",
                                             task_dep_dico=dependencies)

    @classmethod
    def step_description(cls):
        """Print a short description of the step's purpose."""
        description = ("Mosaic")
        return description
