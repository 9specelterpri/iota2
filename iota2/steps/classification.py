#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Launch the classification task"""
import logging
import os
from typing import Optional

import iota2.common.raster_utils as ru
from iota2.classification import image_classifier as imageClassifier
from iota2.classification import py_classifiers
from iota2.classification.image_classifier import (
    autocontext_launch_classif,
    get_class_by_models_from_i2_learn,
)
from iota2.common.custom_numpy_features import exogenous_data_tile
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class Classification(iota2_step.Step):
    """Class for launching classification step"""

    resources_block_name = "classifications"

    def __init__(self, cfg, cfg_resources_file, working_directory=None):
        """Initialize the classification class

        Parameters
        ----------
        cfg:
            configuration file
        cfg_resources_file:
            configuration resource file
        working_directory:
            folder for temporary data
        """
        # heritage init

        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.external_features_flag = rcf.read_config_file(self.cfg).getParam(
            "external_features", "external_features_flag"
        )
        self.number_of_chunks = rcf.read_config_file(self.cfg).getParam(
            "python_data_managing", "number_of_chunks"
        )
        self.working_directory = working_directory
        self.autocontext_iterations = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "autocontext_iterations"
        )
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.nomenclature_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "nomenclature_path"
        )
        self.data_field = self.i2_const.re_encoding_label_name
        self.enable_autocontext = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "enable_autocontext"
        )
        self.ram = 1024.0 * get_ram(self.get_resources()["ram"])
        self.use_scikitlearn = (
            rcf.read_config_file(self.cfg).getParam(
                "scikit_models_parameters", "model_type"
            )
            is not None
        )
        self.runs = rcf.read_config_file(self.cfg).getParam("arg_train", "runs")
        self.classifier = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "classifier"
        )
        self.available_ram = 1024.0 * get_ram(self.get_resources()["ram"])
        self.enable_proba_map = rcf.read_config_file(self.cfg).getParam(
            "arg_classification", "enable_probability_map"
        )

        self.pixel_type = "uint8"
        self.suffix_list = ["usually"]
        if (
            rcf.read_config_file(self.cfg).getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
            is True
        ):
            self.suffix_list.append("SAR")
        self.features_from_raw_dates = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "features_from_raw_dates"
        )

        # about custom features
        if self.external_features_flag:
            self.number_of_chunks = rcf.read_config_file(self.cfg).getParam(
                "python_data_managing", "number_of_chunks"
            )
        self.concat_mode = rcf.read_config_file(self.cfg).getParam(
            "external_features", "concat_mode"
        )
        self.enable_gap, self.enable_raw = rcf.read_config_file(self.cfg).getParam(
            "python_data_managing", "data_mode_access"
        )

        self.pytorch_params = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "deep_learning_parameters"
        )

        region_field = (
            rcf.read_config_file(self.cfg).getParam("chain", "region_field")
        ).lower()
        self.class_by_models = get_class_by_models_from_i2_learn(
            os.path.join(self.output_path, "dataAppVal"), self.data_field, region_field
        )
        all_seeds_class = []
        for _, region_class in self.class_by_models.items():
            all_seeds_class += region_class
        self.all_seeds_class = sorted(list(set(all_seeds_class)))
        if self.enable_proba_map and not self.class_by_models:
            raise ValueError(
                "Iota2 cannot generate probability map,"
                " database is missing in 'dataAppVal' directory"
            )
        self.nn_name = None
        self.deep_learning_enabled = "dl_name" in self.pytorch_params
        if self.deep_learning_enabled:
            self.nn_name = self.pytorch_params["dl_name"]

        for suffix in self.suffix_list:
            for (
                model_name,
                model_meta,
            ) in self.spatial_models_distribution_classify.items():
                self.sensors_dates = rcf.iota2_parameters(
                    rcf.read_config_file(self.cfg)
                ).get_available_sensors_dates(model_meta["tiles"])
                if "Sentinel1" in self.sensors_dates:
                    s1_dates = rcf.iota2_parameters(
                        rcf.read_config_file(self.cfg)
                    ).get_sentinel1_input_dates(model_meta["tiles"])
                    self.sensors_dates = {**self.sensors_dates, **s1_dates}
                    self.sensors_dates.pop("Sentinel1", None)
                for seed in range(self.runs):
                    for tile in model_meta["tiles"]:
                        task_name = (
                            f"classification_{tile}_model_{model_name}_seed_{seed}"
                        )
                        if suffix == "SAR":
                            task_name += "_SAR"
                        target_model = f"model_{model_name}_seed_{seed}_{suffix}"
                        task_params = self.get_classification_params(
                            model_name, tile, seed, suffix
                        )
                        if self.enable_autocontext is False and (
                            self.use_scikitlearn is True
                            or self.external_features_flag
                            or self.deep_learning_enabled
                        ):
                            chunk_number = self.number_of_chunks
                            for chunk in range(chunk_number):
                                task_params = self.get_classification_params(
                                    model_name, tile, seed, suffix, chunk
                                )
                                task = self.i2_task(
                                    task_name=f"{task_name}_{chunk}",
                                    log_dir=self.log_step_dir,
                                    execution_mode=self.execution_mode,
                                    task_parameters=task_params,
                                    task_resources=self.get_resources(),
                                )
                                self.add_task_to_i2_processing_graph(
                                    task,
                                    task_group="tile_tasks_model_mode",
                                    task_sub_group=f"{tile}_{model_name}_{seed}_{suffix}_{chunk}",
                                    task_dep_dico={"region_tasks": [target_model]},
                                )
                        else:
                            task = self.i2_task(
                                task_name=task_name,
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters=task_params,
                                task_resources=self.get_resources(),
                            )
                            self.add_task_to_i2_processing_graph(
                                task,
                                task_group="tile_tasks_model_mode",
                                task_sub_group=f"{tile}_{model_name}_{seed}_{suffix}",
                                task_dep_dico={"region_tasks": [target_model]},
                            )

    def get_classification_params(
        self,
        region_name: str,
        tile_name: str,
        seed: int,
        suffix: str,
        target_chunk: Optional[int] = None,
    ):
        """Generate the classification parameters

        Parameters
        ----------
        region_name:
            the region to be classified
        tile_name:
            the tile to be classified
        seed:
            the run targeted
        suffix:
            use SAR suffix if enabled
        target_chunk:
            the chunk targeted
        """
        param = None
        region_mask_name = region_name.split("f")[0]
        target_model_name = f"model_{region_name}_seed_{seed}.txt"
        classif_file = os.path.join(
            self.output_path,
            "classif",
            f"Classif_{tile_name}_model_{region_name}_seed_{seed}.tif",
        )
        confidence_file = os.path.join(
            self.output_path,
            "classif",
            f"{tile_name}_model_{region_name}_confidence_seed_{seed}.tif",
        )
        if suffix == "SAR":
            target_model_name = target_model_name.replace(".txt", "_SAR.txt")
            classif_file = classif_file.replace(".tif", "_SAR.tif")
            confidence_file = confidence_file.replace(".tif", "_SAR.tif")
        classif_mask_file = os.path.join(
            self.output_path,
            "classif",
            "MASK",
            f"MASK_region_{region_mask_name}_{tile_name}.tif",
        )
        model_file = os.path.join(self.output_path, "model", target_model_name)
        stats_file = None
        enable_gap, enable_raw = rcf.read_config_file(self.cfg).getParam(
            "python_data_managing", "data_mode_access"
        )
        if self.classifier and "svm" in self.classifier:
            stats_file = os.path.join(
                self.output_path, "stats", f"Model_{region_name}_seed_{seed}.xml"
            )
        self.boundary_fusion_enabled = rcf.read_config_file(self.cfg).getParam(
            "arg_classification", "enable_boundary_fusion"
        )
        self.boundary_mask = None
        if self.boundary_fusion_enabled:
            self.boundary_mask = os.path.join(
                self.output_path,
                "classif",
                "MASK",
                f"Boundary_MASK_region_{region_mask_name}_{tile_name}.tif",
            )

        if (
            not self.enable_autocontext
            and not self.use_scikitlearn
            and not self.deep_learning_enabled
        ):
            param = {
                "f": imageClassifier.launch_classification,
                "classifmask": classif_mask_file,
                "model": model_file,
                "stats": stats_file,
                "output_classif": classif_file,
                "path_wd": self.working_directory,
                "classifier_type": self.classifier,
                "tile": tile_name,
                "proba_map_expected": self.enable_proba_map,
                "dimred": rcf.read_config_file(self.cfg).getParam("dim_red", "dim_red"),
                "sar_optical_post_fusion": rcf.read_config_file(self.cfg).getParam(
                    "arg_train", "dempster_shafer_sar_opt_fusion"
                ),
                "output_path": self.output_path,
                "data_field": self.data_field,
                "write_features": rcf.read_config_file(self.cfg).getParam(
                    "sensors_data_interpolation", "write_outputs"
                ),
                "reduction_mode": rcf.read_config_file(self.cfg).getParam(
                    "dim_red", "reduction_mode"
                ),
                "sensors_parameters": rcf.iota2_parameters(
                    rcf.read_config_file(self.cfg)
                ).get_sensors_parameters(tile_name),
                "pixtype": self.pixel_type,
                "ram": self.available_ram,
                "all_class": self.all_seeds_class,
                "auto_context": False,
                "boundary_mask": self.boundary_mask,
                "multi_param_cust": {
                    "enable_raw": enable_raw,
                    "enable_gap": enable_gap,
                    "fill_missing_dates": rcf.read_config_file(self.cfg).getParam(
                        "python_data_managing", "fill_missing_dates"
                    ),
                    "all_dates_dict": self.sensors_dates,
                    "exogeneous_data": exogenous_data_tile(
                        rcf.read_config_file(self.cfg).getParam(
                            "external_features", "exogeneous_data"
                        ),
                        tile_name,
                    ),
                },
            }
            if self.external_features_flag:
                param["external_features"] = True
                param["module_path"] = rcf.read_config_file(self.cfg).getParam(
                    "external_features", "module"
                )
                param["chunk_config"] = ru.ChunkConfig(
                    chunk_size_mode=rcf.read_config_file(self.cfg).getParam(
                        "python_data_managing", "chunk_size_mode"
                    ),
                    number_of_chunks=self.number_of_chunks,
                    chunk_size_x=rcf.read_config_file(self.cfg).getParam(
                        "python_data_managing", "chunk_size_x"
                    ),
                    chunk_size_y=rcf.read_config_file(self.cfg).getParam(
                        "python_data_managing", "chunk_size_y"
                    ),
                    padding_size_x=rcf.read_config_file(self.cfg).getParam(
                        "python_data_managing", "padding_size_x"
                    ),
                    padding_size_y=rcf.read_config_file(self.cfg).getParam(
                        "python_data_managing", "padding_size_y"
                    ),
                )
                param["targeted_chunk"] = target_chunk
                param["list_functions"] = rcf.read_config_file(self.cfg).getParam(
                    "external_features", "functions"
                )
                param["force_standard_labels"] = rcf.read_config_file(
                    self.cfg
                ).getParam("arg_train", "force_standard_labels")
                param["concat_mode"] = rcf.read_config_file(self.cfg).getParam(
                    "external_features", "concat_mode"
                )

        elif (
            self.enable_autocontext is True
            and self.use_scikitlearn is False
            and self.deep_learning_enabled is False
        ):
            param = {
                "f": autocontext_launch_classif,
                "parameters_dict": {
                    "model_name": region_name,
                    "seed_num": seed,
                    "tile": tile_name,
                    "tile_segmentation": os.path.join(
                        self.output_path,
                        "features",
                        tile_name,
                        "tmp",
                        f"SLIC_{tile_name}.tif",
                    ),
                    "tile_mask": classif_mask_file,
                    "model_list": [
                        os.path.join(
                            self.output_path,
                            "model",
                            f"model_{region_name}_seed_{seed}",
                            f"model_it_{auto_it}.rf",
                        )
                        for auto_it in range(self.autocontext_iterations)
                    ],
                },
                "classifier_type": self.classifier,
                "tile": tile_name,
                "proba_map_expected": self.enable_proba_map,
                "dimred": rcf.read_config_file(self.cfg).getParam("dim_red", "dim_red"),
                "data_field": self.data_field,
                "write_features": rcf.read_config_file(self.cfg).getParam(
                    "sensors_data_interpolation", "write_outputs"
                ),
                "reduction_mode": rcf.read_config_file(self.cfg).getParam(
                    "dim_red", "reduction_mode"
                ),
                "iota2_run_dir": self.output_path,
                "sar_optical_post_fusion": rcf.read_config_file(self.cfg).getParam(
                    "arg_train", "dempster_shafer_sar_opt_fusion"
                ),
                "sensors_parameters": rcf.iota2_parameters(
                    rcf.read_config_file(self.cfg)
                ).get_sensors_parameters(tile_name),
                "ram": self.available_ram,
                "working_directory": self.working_directory,
            }
        elif not self.enable_autocontext and (
            self.use_scikitlearn
            or self.deep_learning_enabled
            or self.features_from_raw_dates
        ):
            out_proba_file = None
            all_seeds_class = None
            if self.enable_proba_map:
                all_seeds_class = []
                for _, region_class in self.class_by_models.items():
                    all_seeds_class += region_class
                all_seeds_class = sorted(list(set(all_seeds_class)))
                if self.use_scikitlearn:
                    sub_name = (
                        "" if target_chunk is None else f"_SUBREGION_{target_chunk}"
                    )
                    out_proba_file = (
                        f"PROBAMAP_{tile_name}_model_{region_name}"
                        f"_seed_{seed}{sub_name}.tif"
                    )
                    # TODO : manage scikit probability map re-ordering
                    out_proba_file = None

                elif self.deep_learning_enabled:
                    out_proba_file = (
                        f"PROBAMAP_{tile_name}_model" f"_{region_name}_seed_{seed}.tif"
                    )
                out_proba_file = os.path.join(
                    self.output_path, "classif", out_proba_file
                )

            param = {
                "f": py_classifiers.scikit_learn_predict,
                "mask": classif_mask_file,
                "model": model_file,
                "out_classif": classif_file,
                "out_confidence": confidence_file,
                "out_proba": out_proba_file,
                "working_dir": self.working_directory,
                "tile_name": tile_name,
                "sar_optical_post_fusion": rcf.read_config_file(self.cfg).getParam(
                    "arg_train", "dempster_shafer_sar_opt_fusion"
                ),
                "output_path": rcf.read_config_file(self.cfg).getParam(
                    "chain", "output_path"
                ),
                "sensors_parameters": rcf.iota2_parameters(
                    rcf.read_config_file(self.cfg)
                ).get_sensors_parameters(tile_name),
                "number_of_chunks": self.number_of_chunks,
                "targeted_chunk": target_chunk,
            }

            if self.deep_learning_enabled or self.features_from_raw_dates:
                param.pop("pixel_type", None)
                param.pop("stat", None)
                param["f"] = py_classifiers.pytorch_predict
                param["external_nn_module"] = self.pytorch_params["dl_module"]
                param["all_class_labels"] = all_seeds_class
                param["features_from_raw_dates"] = self.features_from_raw_dates
                param["model_parameters_file"] = model_file.replace(
                    ".txt", "_parameters.pickle"
                )
                param["encoder_convert_file"] = model_file.replace(
                    ".txt", "_encoder.pickle"
                )
                param["sensors_dates"] = self.sensors_dates
                param["nn_name"] = self.nn_name
                param["hyperparam_file"] = model_file.replace(".txt", "_hyper.pickle")
                param["max_inference_size"] = rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "max_nn_inference_size"
                )
                if self.features_from_raw_dates:
                    param["enabled_raw"] = True
                if self.external_features_flag:
                    param["concat_mode"] = self.concat_mode
                    param["custom_features_enable"] = self.external_features_flag
                    param["custom_features_module"] = rcf.read_config_file(
                        self.cfg
                    ).getParam("external_features", "module")
                    param["custom_features_functions"] = rcf.read_config_file(
                        self.cfg
                    ).getParam("external_features", "functions")
                    param["enabled_raw"] = self.enable_raw
                    param["enabled_gap"] = self.enable_gap

        return param

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate classifications"
        return description
