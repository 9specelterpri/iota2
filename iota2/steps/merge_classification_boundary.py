#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Merge boundary classifications."""
import logging
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation.boundary_tile_fusion import merge_map_and_boundary

LOGGER = logging.getLogger("distributed.worker")


class MergeClassificationBoundary(iota2_step.Step):
    """Step to compute the fusion at boundary using probabilities."""

    resources_block_name = "compute_boundary_fusion"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        """Initialize the step."""
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        self.working_directory = workingDirectory

        # parameters
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path")
        self.number_of_chunks = rcf.read_config_file(self.cfg).getParam(
            'python_data_managing', "number_of_chunks")
        # dict_tile_model = {}
        self.epsg = int(
            rcf.read_config_file(self.cfg).getParam("chain",
                                                    "proj").split(":")[-1])
        self.number_of_chunks = rcf.read_config_file(self.cfg).getParam(
            'python_data_managing', "number_of_chunks")

        for seed in range(self.runs):
            for tile in self.tiles:
                task = self.i2_task(task_name=f"merge_proba_{tile}_{seed}",
                                    log_dir=self.log_step_dir,
                                    execution_mode=self.execution_mode,
                                    task_parameters={
                                        "f":
                                        merge_map_and_boundary,
                                        "tile":
                                        tile,
                                        "path_to_chunks":
                                        os.path.join(output_path, "boundary"),
                                        "path_to_env":
                                        os.path.join(output_path, "envelope"),
                                        "seed":
                                        seed,
                                        "output_path":
                                        os.path.join(output_path, "boundary"),
                                        "epsg_code":
                                        self.epsg,
                                        "working_directory":
                                        self.working_directory
                                    },
                                    task_resources=self.get_resources())

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks_model",
                    task_sub_group=f"{tile}_{seed}",
                    task_dep_dico={
                        "tile_tasks_model": [
                            f"{tile}_{seed}_{targeted_chunk}"
                            for targeted_chunk in range(self.number_of_chunks)
                        ]
                    })

    @classmethod
    def step_description(cls):
        """Print a short description of the step's purpose."""
        description = ("Merge all classification with boundary area")
        return description
