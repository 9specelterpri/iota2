#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging
from typing import Optional

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import samples_merge
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class SamplesMerge(iota2_step.Step):
    resources_block_name = "samplesMerge"

    def __init__(
        self, cfg: str, cfg_resources_file: str, workingDirectory: Optional[str] = None
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.working_directory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.field_region = rcf.read_config_file(self.cfg).getParam(
            "chain", "region_field"
        )
        self.nb_runs = rcf.read_config_file(self.cfg).getParam("arg_train", "runs")
        sampling_validation = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "sampling_validation"
        )
        for model_name, model_meta in self.spatial_models_distribution.items():
            for seed in range(self.nb_runs):
                target_model = f"model_{model_name}_seed_{seed}"
                task = self.i2_task(
                    task_name=f"merge_{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": samples_merge.samples_merge,
                        "region_tiles_seed": (model_name, model_meta["tiles"], seed),
                        "output_path": self.output_path,
                        "region_field": self.field_region,
                        "ds_sar_opt_flag": rcf.read_config_file(self.cfg).getParam(
                            "arg_train", "dempster_shafer_sar_opt_fusion"
                        ),
                        "working_directory": self.working_directory,
                        "sampling_validation": sampling_validation,
                    },
                    task_resources=self.get_resources(),
                )

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=target_model,
                    task_dep_dico={},
                )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "merge samples by models"
        return description
