#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Produce the boundary fusion."""
import logging
import os

from iota2.classification.image_classifier import \
    get_class_by_models_from_i2_learn
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation.boundary_tile_fusion import proba_fusion_at_boundaries

LOGGER = logging.getLogger("distributed.worker")


class ProbBoundaryFusion(iota2_step.Step):
    """Step to compute the fusion at boundary using probabilities."""

    resources_block_name = "compute_boundary_fusion"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        """Initialize the step."""
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        self.working_directory = workingDirectory

        # parameters
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path")
        self.number_of_chunks = rcf.read_config_file(self.cfg).getParam(
            'python_data_managing', "number_of_chunks")
        dict_tile_model = {}
        region_vec = rcf.read_config_file(self.cfg).getParam(
            'chain', 'region_path')
        region_field = (rcf.read_config_file(self.cfg).getParam(
            'chain', 'region_field')).lower()
        self.data_field = self.i2_const.re_encoding_label_name
        self.class_by_models = get_class_by_models_from_i2_learn(
            os.path.join(self.output_path, "dataAppVal"), self.data_field,
            region_field)
        all_seeds_class = []
        for _, region_class in self.class_by_models.items():
            all_seeds_class += region_class
        self.all_seeds_class = sorted(list(set(all_seeds_class)))
        for model_name, model_meta in (
                self.spatial_models_distribution_no_sub_splits_classify.items(
                )):
            for tile in model_meta["tiles"]:
                if tile in dict_tile_model:
                    dict_tile_model[tile].append(model_name)
                else:
                    dict_tile_model[tile] = [model_name]
        for seed in range(self.runs):
            for tile in self.tiles:

                for targeted_chunk in range(self.number_of_chunks):

                    task = self.i2_task(
                        task_name=f"fuse_proba_{tile}_{targeted_chunk}_{seed}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f":
                            proba_fusion_at_boundaries,
                            "tile":
                            tile,
                            "distance_map_folder":
                            os.path.join(self.output_path, "features", tile,
                                         "tmp"),
                            "probabilities_map_folder":
                            os.path.join(self.output_path, "classif"),
                            "seed":
                            seed,
                            "boundary_folder_out":
                            os.path.join(self.output_path, "boundary"),
                            "target_chunk":
                            targeted_chunk,
                            "number_of_chunks":
                            self.number_of_chunks,
                            "working_directory":
                            self.working_directory,
                            "original_region_file":
                            region_vec,
                            "region_field":
                            region_field,
                            "nb_class":
                            len(self.all_seeds_class),
                            "generate_final_probability_map":
                            rcf.read_config_file(self.cfg).getParam(
                                "arg_classification",
                                "generate_final_probability_map")
                        },
                        task_resources=self.get_resources())

                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="tile_tasks_model",
                        task_sub_group=f"{tile}_{seed}_{targeted_chunk}",
                        task_dep_dico={
                            "tile_tasks_model_mode": [
                                f"{tile}_{model_name}_{seed}_usually"
                                for model_name in dict_tile_model[tile]
                            ]
                        })

    @classmethod
    def step_description(cls):
        """Print a short description of the step's purpose."""
        description = ("Fusion into boundary area")
        return description
