#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
The coregistration step
"""
import logging

from iota2.common.tools import co_register
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class Coregistration(iota2_step.Step):
    resources_block_name = "coregistration"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.RAM = 1024.0 * get_ram(self.get_resources()["ram"])
        self.workingDirectory = workingDirectory

        l5_path = None
        l8_path = None
        s2_path = None
        s2_s2c_path = None
        correg_pattern = None
        if cfg.getParam('chain', 'l5_path_old'):
            l5_path = cfg.getParam('chain', 'l5_path_old')
        if cfg.getParam('chain', 'l8path_old'):
            # l8 or l8_old ?
            l8_path = cfg.getParam('chain', 'l8path_old')
        if cfg.getParam('chain', 's2_path'):
            s2_path = cfg.getParam('chain', 's2_path')
        if cfg.getParam('chain', 's2_s2c_path'):
            s2_s2c_path = cfg.getParam('chain', 's2_s2c_path')
        if cfg.getParam('coregistration', 'pattern'):
            correg_pattern = cfg.getParam('coregistration', 'pattern')
        sensors_parameters = rcf.iota2_parameters(
            rcf.read_config_file(self.cfg))

        for tile in self.tiles:
            task = self.i2_task(
                task_name=f"coreg_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f":
                    co_register.launch_coregister,
                    "working_directory":
                    self.workingDirectory,
                    "inref":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'vhr_path'),
                    "bandsrc":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'bandSrc'),
                    "bandref":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'bandRef'),
                    "resample":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'resample'),
                    "step":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'step'),
                    "minstep":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'minstep'),
                    "minsiftpoints":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'minshiftpoints'),
                    "iterate":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'iterate'),
                    "prec":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'prec'),
                    "mode":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'mode'),
                    "output_path":
                    rcf.read_config_file(self.cfg).getParam(
                        'chain', 'output_path'),
                    "date_vhr":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'date_vhr'),
                    "dates_src":
                    rcf.read_config_file(self.cfg).getParam(
                        'coregistration', 'date_src'),
                    "list_tiles":
                    rcf.read_config_file(self.cfg).getParam(
                        'chain', 'list_tile'),
                    "ipath_l5":
                    l5_path,
                    "ipath_l8":
                    l8_path,
                    "ipath_s2":
                    s2_path,
                    "ipath_s2_s2c":
                    s2_s2c_path,
                    "corregistration_pattern":
                    correg_pattern,
                    "launch_mask":
                    True,
                    "sensors_parameters":
                    sensors_parameters.get_sensors_parameters(tile)
                },
                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(task,
                                                 task_group="tile_tasks",
                                                 task_sub_group=tile,
                                                 task_dep_dico={})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Time series coregistration on a VHR reference")
        return description
