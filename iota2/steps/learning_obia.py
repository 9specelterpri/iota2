#!/usr/bin/python
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging

from iota2.configuration_files import read_config_file as rcf
from iota2.segmentation import prepare_segmentation_obia as pso
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class ObiaLearning(iota2_step.Step):
    resources_block_name = "obiaLearning"

    def __init__(self, cfg, cfg_resources_file, working_directory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.workingDirectory = working_directory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.field_region = rcf.read_config_file(self.cfg).getParam(
            'chain', 'region_field')
        self.nb_runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.classifier = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'classifier')
        self.options = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'otb_classifier_options')
        self.classifier = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "classifier")
        for model_name, model_meta in self.spatial_models_distribution.items():
            for seed in range(self.nb_runs):

                target_model = f"model_{model_name}_seed_{seed}"

                task = self.i2_task(task_name=f"{target_model}",
                                    log_dir=self.log_step_dir,
                                    execution_mode=self.execution_mode,
                                    task_parameters={
                                        "f": pso.learning_models_by_region,
                                        "iota2_directory": self.output_path,
                                        "region": model_name,
                                        "seed": seed,
                                        "data_field":
                                        self.i2_const.re_encoding_label_name,
                                        "classifier_name": self.classifier,
                                        "classifier_options": self.options,
                                        "enable_stats": False
                                    },
                                    task_resources=self.get_resources())
                dep = []
                for tile in model_meta["tiles"]:
                    dep.append(f"zonal_stats_learn_{tile}_seed_{seed}")
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=target_model,
                    task_dep_dico={"region_tasks": dep})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("learn model for obia")
        return description
