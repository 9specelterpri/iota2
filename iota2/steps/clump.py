#!/usr/bin/env python3

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.simplification import clump_classif as clump
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class Clump(iota2_step.Step):
    resources_block_name = "clump"

    def __init__(
        self,
        cfg,
        cfg_resources_file,
        file_to_clump,
        workingDirectory=None,
        from_regul=False,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.ram = 1024.0 * get_ram(self.get_resources()["ram"])
        self.workingdirectory = workingDirectory
        self.outputpath = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.lib64bit = rcf.read_config_file(self.cfg).getParam(
            "simplification", "lib64bit"
        )

        if from_regul:
            file_to_clump = os.path.join(
                self.outputpath, "simplification", "classif_regul.tif"
            )
        task = self.i2_task(
            task_name="clump",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": clump.clump_and_stack_classif,
                "path": os.path.join(self.outputpath, "simplification", "tmp"),
                "raster": file_to_clump,
                "outpath": os.path.join(
                    self.outputpath, "simplification", "classif_regul_clump.tif"
                ),
                "ram": str(self.ram),
                "float64": True if self.lib64bit is not None else False,
                "exe64": self.lib64bit,
            },
            task_resources=self.get_resources(),
        )

        dep_dico = {"merge_regul": ["merge_regul"]}
        self.add_task_to_i2_processing_graph(
            task, task_group="clump", task_sub_group="clump", task_dep_dico=dep_dico
        )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Clump of regularized classification raster"
        return description
