#!/usr/bin/python

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.segmentation import prepare_segmentation_obia as pso
from iota2.steps import iota2_step
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class MergeFinalMetrics(iota2_step.Step):
    resources_block_name = "merge_final_metrics"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = workingDirectory
        self.cfg = cfg
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.data_field = rcf.read_config_file(self.cfg).getParam("chain", "data_field")
        self.runs = rcf.read_config_file(self.cfg).getParam("arg_train", "runs")
        self.enable_stats = False
        self.field_region = rcf.read_config_file(self.cfg).getParam(
            "chain", "region_field"
        )
        self.res = rcf.read_config_file(self.cfg).getParam(
            "chain", "spatial_resolution"
        )[0]

        reference_data = rcf.read_config_file(self.cfg).getParam(
            "chain", "ground_truth"
        )
        reference_data_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "data_field"
        )
        old_label_to_new = get_re_encoding_labels_dic(
            reference_data, reference_data_field
        )
        for seed in range(self.runs):

            task = self.i2_task(
                task_name=f"merge_final_metrics{seed}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": pso.cumulate_confusion_matrix,
                    "out_directory": os.path.join(self.output_path, "final"),
                    "seed": seed,
                    "nomenclature_file": rcf.read_config_file(self.cfg).getParam(
                        "chain", "nomenclature_path"
                    ),
                    "data_field": self.i2_const.re_encoding_label_name,
                    "label_vector_table": old_label_to_new,
                    "tmp_dir": os.path.join(self.output_path, "final", "TMP"),
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="seed_tasks",
                task_sub_group=f"seed_{seed}",
                task_dep_dico={"seed_tasks": [f"seed_{seed}"]},
            )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge all metrics files"
        return description
