#!/usr/bin/env python3

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.simplification import manage_regularization as mr
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class MergeRegularization(iota2_step.Step):
    resources_block_name = "mergeregularisation"

    def __init__(
        self,
        cfg,
        cfg_resources_file,
        stepname=None,
        output=None,
        umc=None,
        resample=None,
        water=None,
        workingDirectory=None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        if stepname:
            self.step_name = stepname

        self.ram = 1024.0 * get_ram(self.get_resources()["ram"])
        self.cpu = self.get_resources()["cpu"]
        self.workingdirectory = workingDirectory
        self.outputpath = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.resample = resample
        self.water = water
        self.umc = umc
        self.output = output
        self.tmpdir = os.path.join(self.outputpath, "simplification", "tmp")
        nomenclature = rcf.read_config_file(self.cfg).getParam(
            "simplification", "nomenclature"
        )
        nb_reg_rules = len(mr.get_mask_regularisation(nomenclature))
        output, tmpdir = self.get_output_paths()

        expected_rasters = [
            os.path.join(self.tmpdir, f"mask_{rule_num}.tif")
            for rule_num in range(nb_reg_rules)
        ]

        task = self.i2_task(
            task_name=stepname,
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": mr.merge_regularization,
                "path": tmpdir,
                "rasters": expected_rasters,
                "threshold": self.umc,
                "output": output,
                "ram": self.ram,
                "resample": self.resample,
                "water": self.water,
                "working_dir": self.workingdirectory,
            },
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(
            task,
            task_group="merge_regul",
            task_sub_group="merge_regul",
            task_dep_dico={
                "regul": [f"regul_{rule_num}" for rule_num in range(nb_reg_rules)]
            },
        )

    def get_output_paths(self) -> tuple[str, str]:
        """ """
        tmpdir = os.path.join(self.outputpath, "simplification", "tmp")

        if not self.output:
            output = os.path.join(tmpdir, "regul1.tif")
        else:
            output = self.output
        return output, tmpdir

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Majority regularisation of adaptive-regularized raster"
        return description
