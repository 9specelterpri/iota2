#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" The CommonMasks step"""
import logging

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.sensors import process_launcher
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class CommonMasks(iota2_step.Step):
    resources_block_name = "get_common_mask"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(CommonMasks, self).__init__(
            cfg, cfg_resources_file, self.resources_block_name
        )

        # step variables
        self.workingDirectory = workingDirectory
        self.RAM = 1024.0 * get_ram(self.get_resources()["ram"])
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        coregistration_enable = bool(
            rcf.read_config_file(self.cfg).getParam("coregistration", "vhr_path")
        )
        sensors_params = rcf.iota2_parameters(rcf.read_config_file(self.cfg))
        for tile in self.tiles:
            task = self.i2_task(
                task_name=f"common_mask_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": process_launcher.common_mask,
                    "tile_name": tile,
                    "output_path": self.output_path,
                    "sensors_parameters": sensors_params.get_sensors_parameters(tile),
                    "working_directory": self.workingDirectory,
                    "ram": self.RAM,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"tile_tasks": [tile]} if coregistration_enable else {},
            )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate a common masks for each sensors"
        return description
