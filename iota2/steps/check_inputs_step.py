#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

from iota2.common.verify_inputs import check_iota2_inputs
from iota2.configuration_files import read_config_file as rcf
from iota2.sensors.sensors_container import sensors_container
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class CheckInputsClassifWorkflow(iota2_step.Step):

    resources_block_name = "check_inputs"

    def __init__(self, cfg, cfg_resources_file):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path")
        reference_db = os.path.join(output_path,
                                    self.i2_const.re_encoding_label_file)
        region_shape = rcf.read_config_file(self.cfg).getParam(
            "chain", "region_path")
        region_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "region_field")
        tiles = rcf.read_config_file(self.cfg).getParam("chain",
                                                        "list_tile").split(" ")
        epsg = int(
            rcf.read_config_file(self.cfg).getParam("chain",
                                                    "proj").split(":")[-1])
        running_parameters = rcf.iota2_parameters(
            rcf.read_config_file(self.cfg))
        sensors_parameters = running_parameters.get_sensors_parameters(
            tiles[0])
        sensor_tile_container = sensors_container(tiles[0], None, output_path,
                                                  **sensors_parameters)
        sensor_path = sensor_tile_container.get_enabled_sensors_path()[0]
        check_inputs_task = self.i2_task(
            task_name="check_inputs",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": check_iota2_inputs,
                "i2_output_path": output_path,
                "ground_truth": reference_db,
                "region_shape": region_shape,
                "data_field": self.i2_const.re_encoding_label_name,
                "region_field": region_field,
                "epsg": epsg,
                "sensor_path": sensor_path,
                "tiles": tiles
            },
            task_resources=self.get_resources())

        self.add_task_to_i2_processing_graph(check_inputs_task, "first_task")

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("check inputs")
        return description
