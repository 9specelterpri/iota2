#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

from iota2.classification import fusion as FUS
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class ClassificationsFusion(iota2_step.Step):
    resources_block_name = "fusion"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')

        self.dempster_shafer_sar_opt_fusion = rcf.read_config_file(
            self.cfg).getParam('arg_train', 'dempster_shafer_sar_opt_fusion')
        pix_type = "uint8"
        for model_name, model_meta in self.spatial_models_distribution_no_sub_splits_classify.items(
        ):
            for seed in range(self.runs):
                for tile in model_meta["tiles"]:
                    classif_to_merge = [
                        os.path.join(
                            self.output_path, "classif",
                            f"Classif_{tile}_model_{model_name}f{submodel_num + 1}_seed_{seed}.tif"
                        ) for submodel_num in range(model_meta["nb_sub_model"])
                    ]

                    if self.dempster_shafer_sar_opt_fusion:
                        classif_to_merge = [
                            elem.replace(".tif", "_DS.tif")
                            for elem in classif_to_merge
                        ]
                    task = self.i2_task(
                        task_name=
                        f"classification_fusion_{tile}_model_{model_name}_seed_{seed}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f":
                            FUS.fusion,
                            "in_classif":
                            classif_to_merge,
                            "fusion_options":
                            rcf.read_config_file(self.cfg).getParam(
                                'arg_classification', 'fusion_options'),
                            "out_classif":
                            os.path.join(
                                self.output_path, "classif",
                                f"{tile}_FUSION_model_{model_name}_seed_{seed}.tif"
                            ),
                            "out_pix_type":
                            pix_type
                        },
                        task_resources=self.get_resources())
                    if model_meta["nb_sub_model"] > 1:
                        task_dep_group = "tile_tasks_model_mode"
                        task_dep_sub_group = [
                            f"{tile}_{model_name}f{sub_model+1}_{seed}_usually"
                            for sub_model in range(model_meta["nb_sub_model"])
                        ]

                        if self.dempster_shafer_sar_opt_fusion:
                            task_dep_group = "tile_tasks_model"
                            task_dep_sub_group = [
                                f"{tile}_{model_name}f{sub_model+1}_{seed}" for
                                sub_model in range(model_meta["nb_sub_model"])
                            ]
                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks_model",
                            task_sub_group=f"{tile}_{model_name}_{seed}",
                            task_dep_dico={task_dep_group: task_dep_sub_group})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Fusion of classifications")
        return description
