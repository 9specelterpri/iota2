#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging

import iota2.common.raster_utils as ru
import iota2.common.write_features_map as wfm
from iota2.common.custom_numpy_features import exogenous_data_tile
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class WriteFeaturesMap(iota2_step.Step):

    resources_block_name = "writeMaps"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        # read config file to init custom features and check validity
        self.external_features_flag = rcf.read_config_file(self.cfg).getParam(
            "external_features", "external_features_flag"
        )
        self.sensors_dates = rcf.iota2_parameters(
            rcf.read_config_file(self.cfg)
        ).get_available_sensors_dates()
        if "Sentinel1" in self.sensors_dates:
            s1_dates = rcf.iota2_parameters(
                rcf.read_config_file(self.cfg)
            ).get_sentinel1_input_dates()
            self.sensors_dates = {**self.sensors_dates, **s1_dates}
            self.sensors_dates.pop("Sentinel1", None)

        if self.external_features_flag:
            self.chunk_config = ru.ChunkConfig(
                chunk_size_mode="split_number",
                number_of_chunks=rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "number_of_chunks"
                ),
                chunk_size_x=0,
                chunk_size_y=0,
                padding_size_x=rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "padding_size_x"
                ),
                padding_size_y=rcf.read_config_file(self.cfg).getParam(
                    "python_data_managing", "padding_size_y"
                ),
            )

        enabled_gap, enabled_raw = rcf.read_config_file(self.cfg).getParam(
            "python_data_managing", "data_mode_access"
        )
        for tile in self.tiles:
            sensors_params = rcf.iota2_parameters(
                rcf.read_config_file(self.cfg)
            ).get_sensors_parameters(tile)
            for i in range(self.chunk_config.number_of_chunks):
                task = self.i2_task(
                    task_name=f"write_features_{tile}_chunk_{i}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": wfm.write_features_by_chunk,
                        "working_directory": self.workingDirectory,
                        "output_path": rcf.read_config_file(self.cfg).getParam(
                            "chain", "output_path"
                        ),
                        "sar_optical_post_fusion": rcf.read_config_file(
                            self.cfg
                        ).getParam("arg_train", "dempster_shafer_sar_opt_fusion"),
                        "module_path": rcf.read_config_file(self.cfg).getParam(
                            "external_features", "module"
                        ),
                        "list_functions": rcf.read_config_file(self.cfg).getParam(
                            "external_features", "functions"
                        ),
                        "force_standard_labels": rcf.read_config_file(
                            self.cfg
                        ).getParam("arg_train", "force_standard_labels"),
                        "chunk_config": self.chunk_config,
                        "concat_mode": rcf.read_config_file(self.cfg).getParam(
                            "external_features", "concat_mode"
                        ),
                        "sensors_parameters": sensors_params,
                        "tile": tile,
                        "targeted_chunk": i,
                        "multi_param_cust": {
                            "enable_raw": enabled_raw,
                            "enable_gap": enabled_gap,
                            "fill_missing_dates": rcf.read_config_file(
                                self.cfg
                            ).getParam("python_data_managing", "fill_missing_dates"),
                            "all_dates_dict": self.sensors_dates,
                            "mask_valid_data": None,  # if region use it
                            "mask_value": 0,
                            "exogeneous_data": exogenous_data_tile(
                                rcf.read_config_file(self.cfg).getParam(
                                    "external_features", "exogeneous_data"
                                ),
                                tile,
                            ),
                        },
                    },
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="writing maps",
                    task_sub_group=f"writing maps {tile} {i}",
                    task_dep_dico={"tile_tasks": [tile]},
                )
        # implement tests for check if custom features are well provided
        # so the chain failed during step init

    @classmethod
    def step_description(self):
        """
        function use to print a short description of the step's purpose
        """
        description = "Write features by chunk"
        return description
