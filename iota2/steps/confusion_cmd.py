#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Produce confusion matrix."""
import logging
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.steps import iota2_step
from iota2.validation import gen_confusion_matrix as GCM
from iota2.vector_tools.vector_functions import get_reverse_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class ConfusionCmd(iota2_step.Step):
    """Step for compute confusion matrix."""

    resources_block_name = "gen_confusion_matrix"

    def __init__(self, cfg, cfg_resources_file, working_directory=None):
        """Initialize the step."""
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = working_directory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        # if the nomenclature is not int compatible i2label
        # is used in the function
        self.data_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "data_field")
        ground_truth = rcf.read_config_file(self.cfg).getParam(
            'chain', 'ground_truth')
        user_data_field = rcf.read_config_file(self.cfg).getParam(
            'chain', 'data_field')
        i2_labels_to_user_labels = get_reverse_encoding_labels_dic(
            ground_truth, user_data_field)
        self.enable_boundary_fusion = rcf.read_config_file(self.cfg).getParam(
            "arg_classification", "enable_boundary_fusion")
        self.comparison_mode = rcf.read_config_file(self.cfg).getParam(
            "arg_classification", "boundary_comparison_mode")

        for seed in range(self.runs):
            for tile in self.tiles:
                classif_list, out_csv_list = self.entry_list(tile, seed)
                task = self.i2_task(
                    task_name=f"confusion_{tile}_seed_{seed}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f":
                        GCM.run_gen_conf_matrix,
                        "classif_list":
                        classif_list,
                        "out_csv_list":
                        out_csv_list,
                        "data_field":
                        self.data_field,
                        "ref_vector":
                        os.path.join(self.output_path, "dataAppVal",
                                     f"{tile}_seed_{seed}_val.sqlite"),
                        "ram":
                        1024.0 * get_ram(self.get_resources()["ram"]),
                        "labels_table":
                        i2_labels_to_user_labels
                    },
                    task_resources=self.get_resources())
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks_seed",
                    task_sub_group=f"{tile}_{seed}",
                    task_dep_dico={"mosaic": ["mosaic"]})

    @classmethod
    def step_description(cls):
        """Print a short description of the step's purpose."""
        description = ("generate confusion matrix by tiles")
        return description

    def entry_list(self, tile: str, seed: int):
        """Provide list of file according to step parameters."""
        if self.comparison_mode:
            classif_list = [
                os.path.join(self.output_path, "final", "standard",
                             f"Classif_Seed_{seed}.tif"),
                os.path.join(self.output_path, "final", "boundary",
                             f"Classif_Seed_{seed}.tif")
            ]
            out_csv_list = [
                os.path.join(self.output_path, "final", "standard", "TMP",
                             f"Classif_{tile}_seed_{seed}.csv"),
                os.path.join(self.output_path, "final", "boundary", "TMP",
                             f"Classif_{tile}_seed_{seed}.csv")
            ]
        else:
            classif_list = [
                os.path.join(self.output_path, "final",
                             f"Classif_Seed_{seed}.tif")
            ]
            out_csv_list = [
                os.path.join(self.output_path, "final", "TMP",
                             f"Classif_{tile}_seed_{seed}.csv")
            ]
        return classif_list, out_csv_list
