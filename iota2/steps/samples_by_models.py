#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import logging
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
from typing import Dict, List

from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.read_config_file import iota2_parameters
from iota2.sampling import vector_samples_merge as VSM
from iota2.sensors.sensors_container import sensors_container
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class SamplesByModels(iota2_step.Step):
    resources_block_name = "mergeSample"

    def __init__(self, cfg, cfg_resources_file):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.data_field = rcf.read_config_file(self.cfg).getParam(
            'chain', 'data_field')
        self.nb_runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.suffix_list = ["usually"]
        if rcf.read_config_file(self.cfg).getParam(
                'arg_train', 'dempster_shafer_sar_opt_fusion') is True:
            self.suffix_list.append("SAR")

        self.custom_features = rcf.read_config_file(self.cfg).getParam(
            "external_features", "external_features_flag")
        if self.custom_features:
            self.number_of_chunks = rcf.read_config_file(self.cfg).getParam(
                'python_data_managing', "number_of_chunks")

        self.neural_network = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "deep_learning_parameters")
        dl_flag = "dl_name" in self.neural_network

        # get iota2 sensors order
        running_parameters = iota2_parameters(rcf.read_config_file(self.cfg))
        sensors_parameters = running_parameters.get_sensors_parameters(
            self.tiles[0])
        sensor_tile_container = sensors_container(self.tiles[0], None,
                                                  self.output_path,
                                                  **sensors_parameters)
        sensors = sensor_tile_container.get_enabled_sensors()
        sensors_order = [sensor.__class__.name.lower() for sensor in sensors]

        dico_model_samples_files = self.expected_files_to_merge()
        for suffix in self.suffix_list:
            for model_name, model_meta in self.spatial_models_distribution.items(
            ):
                for seed in range(self.nb_runs):
                    target_model = f"model_{model_name}_seed_{seed}_{suffix}"
                    task = self.i2_task(
                        task_name=f"merge_{target_model}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f": VSM.vector_samples_merge,
                            "vector_list":
                            dico_model_samples_files[target_model],
                            "output_path": self.output_path,
                            "user_label_field": self.data_field.lower(),
                            "deep_learning_db": dl_flag,
                            "sensors_order": sensors_order
                        },
                        task_resources=self.get_resources())
                    dependencies = [
                        f"{tile}_{suffix}" for tile in model_meta["tiles"]
                    ]
                    if self.custom_features:
                        dependencies = [
                            f"{tile}_chunk_{chunk_num}_{suffix}"
                            for tile in model_meta["tiles"]
                            for chunk_num in range(self.number_of_chunks)
                        ]
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="region_tasks",
                        task_sub_group=f"{target_model}",
                        task_dep_dico={"tile_tasks": dependencies})

    def expected_files_to_merge(self) -> Dict[str, List[str]]:
        """
        """
        files_to_merge = {}
        for suffix in self.suffix_list:
            for seed in range(self.nb_runs):
                for model_name, model_meta in self.spatial_models_distribution.items(
                ):
                    file_list = []
                    for tile in model_meta["tiles"]:
                        file_name = f"{tile}_region_{model_name}_seed{seed}_Samples_learn.sqlite"
                        if suffix == "SAR":
                            file_name = f"{tile}_region_{model_name}_seed{seed}_Samples_SAR_learn.sqlite"
                        if self.custom_features:
                            for chunk in range(self.number_of_chunks):
                                file_name = f"{tile}_region_{model_name}_seed{seed}_{chunk}_Samples_learn.sqlite"
                                file_list.append(
                                    os.path.join(self.output_path,
                                                 "learningSamples", file_name))
                        else:
                            file_list.append(
                                os.path.join(self.output_path,
                                             "learningSamples", file_name))
                    files_to_merge[
                        f"model_{model_name}_seed_{seed}_{suffix}"] = file_list
        return files_to_merge

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Merge samples dedicated to the same model")
        return description
