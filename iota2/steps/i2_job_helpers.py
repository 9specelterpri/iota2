# !/usr/bin/env python3

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import re
import time
from abc import ABC, abstractmethod
from pathlib import Path
from subprocess import PIPE, STDOUT, Popen
from typing import Callable, Optional, Union

import dill
import pandas as pd

from config import Config

LOGGER = logging.getLogger("distributed.worker")


class JobFileHelper(ABC):
    """Abstract class to use differents scheduling soft."""

    @property
    @abstractmethod
    def submit_cmd(self) -> str:
        """Define how to submit jobs."""

    @property
    @abstractmethod
    def extension(self) -> str:
        """Define jobfile extension."""

    def task_env(self, cpu: int) -> str:
        """Set environment for tasks."""
        return (
            f"\nmodule purge\n"
            f"export PYTHONPATH={os.environ.get('PYTHONPATH')}\n"
            f"export PATH={os.environ.get('PATH')}\n"
            f"export LD_LIBRARY_PATH={os.environ.get('LD_LIBRARY_PATH')}\n"
            f"export OTB_APPLICATION_PATH={os.environ.get('OTB_APPLICATION_PATH')}\n"
            f"export GDAL_DATA={os.environ.get('GDAL_DATA')}\n"
            f"export GEOTIFF_CSV={os.environ.get('GEOTIFF_CSV')}\n"
            f"export GRASSDIR={os.environ.get('GRASSDIR')}\n"
            "export GDAL_CACHEMAX=128\n"
            f"export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS={cpu}\n\n"
        )

    @staticmethod
    @abstractmethod
    def get_task_status(
        log_err: str, log_out: str, cmd_stdout: str
    ) -> tuple[str, bool]:
        """Guess tasks status by checking log files or thanks to the job id."""

    @abstractmethod
    def write(
        self,
        job_file: Path,
        name: str,
        cpu: int,
        ram: str,
        walltime: str,
        log_err: str,
        log_out: str,
        func: Callable,
        func_kwargs: dict,
        logger_lvl: str,
        gpu: Optional[int] = None,
        queue: Optional[str] = None,
        force_executable: Optional[str] = None,
    ):
        """Write job file on disk."""

    @staticmethod
    def get_executable_task(
        log_out: str,
        func: Callable,
        func_kwargs: dict,
        logger_lvl: str,
    ) -> str:
        """Build executable cmd and serialise object."""
        serialized_file = log_out.replace(".out", ".dill")
        dill.dump((func, func_kwargs, logger_lvl), open(serialized_file, "wb"))
        cmd = f"task_launcher.py -dill_file {serialized_file}"
        return cmd


class PBSJobHelper(JobFileHelper):
    """Class defining some helper functions with PBS scheduler."""

    submit_cmd = "qsub -W block=true "
    extension = "pbs"

    @staticmethod
    def get_task_status(
        log_err: str, log_out: str, cmd_stdout: str
    ) -> tuple[str, bool]:
        """
        Guess tasks status by checking log files or thanks to the job id.

        Parameters
        ----------
        log_err
            error log file.
        log_out
            standard log file.
        cmd_stdout
            not used in this context.

        Notes
        -----
        the returned tuple is formated as the following
        (task_status, killed_worker)
        where task_status is in ("done", "failed", "unlaunchable")
        and killed_worker inform if the PBS scheduler kill the task due
        to resources consumption.
        """
        killed_worker = False
        task_status = "failed"

        done_pattern = {"JOB EXIT CODE": 0}
        walltime_killed_pattern = "PBS: job killed: walltime"
        time.sleep(10)

        # it has been observed that is some conditions, the log_err file is not
        # generated by PBS (nothing to put in ?)
        if os.path.exists(log_out):
            with open(log_out) as log_file:
                for line in log_file:
                    if list(done_pattern.keys())[0] in line:
                        code = list(map(int, re.findall(r"\b\d+\b", line)))[0]
                        break
            if code == done_pattern[list(done_pattern.keys())[0]]:
                task_status = "done"
                killed_worker = False
            if os.path.exists(log_err):
                with open(log_err) as log_file:
                    for line in log_file:
                        if walltime_killed_pattern in line:
                            killed_worker = True
                            break
            if code in [137, 271]:
                killed_worker = True
            elif code == 1:
                task_status = "failed"
        else:
            print(f"logs not found : {log_out}")
            task_status = "failed"
            killed_worker = True
        return task_status, killed_worker

    def write(
        self,
        job_file: Path,
        name: str,
        cpu: int,
        ram: str,
        walltime: str,
        log_err: str,
        log_out: str,
        func: Callable,
        func_kwargs: dict,
        logger_lvl: str,
        gpu: Optional[int] = None,
        queue: Optional[str] = None,
        force_executable: Optional[str] = None,
    ):
        """Write PBS job file.

        Parameters
        ----------
        job_file
            output job file
        name
            task/job's name
        cpu
            required number of cpu
        ram
            required amout of RAM
        walltime
            time treshold
        log_err
            error log file
        log_out
            standard log file
        func
            function to launch
        func_kwargs
            function parameters
        gpu
            required number of gpu
        queue
            queue to launch calculations
        force_executable
            force executable to launch
        """
        nb_gpu = f":ngpus={gpu}" if gpu else ""
        header = (
            "#!/bin/bash\n"
            f"#PBS -N {name}\n"
            f"#PBS -l select=1:ncpus={cpu}:mem={ram}{nb_gpu}\n"
            f"#PBS -l walltime={walltime}\n"
            f"#PBS -e {log_err}\n"
            f"#PBS -o {log_out}\n"
        )
        if queue:
            header = f"{header}#PBS -q {queue}"

        execution = force_executable
        if force_executable is None:
            serialized_file = log_out.replace(".out", ".dill")
            dill.dump((func, func_kwargs, logger_lvl), open(serialized_file, "wb"))
            execution = self.get_executable_task(log_out, func, func_kwargs, logger_lvl)

        pbs_content = header + self.task_env(cpu) + execution

        with open(job_file, "w") as pbs_file:
            pbs_file.write(pbs_content)


class SlurmJobHelper(JobFileHelper):
    """Class defining some helper functions with Slurm scheduler."""

    submit_cmd = "sbatch -W "
    extension = "slurm"

    @staticmethod
    def get_task_status(
        log_err: str, log_out: str, cmd_stdout: str
    ) -> tuple[str, bool]:
        """Guess tasks status by checking log files or thanks to the job id.

        Parameters
        ----------
        log_err
            error log file.
        log_out
            standard log file.
        cmd_stdout
            string containing the job id.

        Notes
        -----
        the returned tuple is formated as the following
        (task_status, killed_worker)
        where task_status is in ("done", "failed", "unlaunchable")
        and killed_worker inform if the Slurm scheduler kill the task.
        """
        killed_worker = False
        task_status = "done"
        job_id = list(map(int, re.findall(r"\b\d+\b", cmd_stdout)))[0]
        job_summary_cmd = f"sacct -j {job_id} --format=All -p --delimiter=|".split(" ")
        process = Popen(job_summary_cmd, shell=False, stdout=PIPE, stderr=STDOUT)
        process.wait()
        stdout, _ = process.communicate()

        lines = stdout.decode("utf-8").split("\n")
        columns = lines[0].split("|")

        job_summary_df = pd.DataFrame(
            [row.split("|") for row in lines[1:-1]], columns=columns
        )
        job_status = job_summary_df[["JobName", "State"]]
        for val in job_status["State"].values:
            if val != "COMPLETED":
                task_status, killed_worker = ("failed", True)
                break
        return task_status, killed_worker

    def write(
        self,
        job_file: Path,
        name: str,
        cpu: int,
        ram: str,
        walltime: str,
        log_err: str,
        log_out: str,
        func: Callable,
        func_kwargs: dict,
        logger_lvl: str,
        gpu: Optional[int] = None,
        queue: Optional[str] = None,
        force_executable: bool = True,
    ) -> None:
        """Write Slurm job file.

        Parameters
        ----------
        job_file
            output job file
        name
            task/job's name
        cpu
            required number of cpu
        ram
            required amout of RAM
        walltime
            time treshold
        log_err
            error log file
        log_out
            standard log file
        func
            function to launch
        func_kwargs
            function parameters
        gpu
            required number of gpu
        queue
            queue to launch calculations
        force_executable
            force executable to launch
        """
        gpu_req = f"#SBATCH --gres=gpu:{gpu}" if gpu else ""
        header = (
            "#!/bin/bash\n"
            f"#SBATCH --job-name {name}\n"
            "#SBATCH -N 1\n"
            f"#SBATCH -n {cpu}\n"
            f"#SBATCH --mem={ram}\n"
            f"{gpu_req}"
            f"#SBATCH --time={walltime}\n"
            f"#SBATCH --error={log_err}\n"
            f"#SBATCH --output={log_out}\n"
        )
        if queue:
            header = f"{header}#SBATCH -p {queue}"

        execution = force_executable
        if force_executable is None:
            serialized_file = log_out.replace(".out", ".dill")
            dill.dump((func, func_kwargs, logger_lvl), open(serialized_file, "wb"))
            execution = self.get_executable_task(log_out, func, func_kwargs, logger_lvl)
        slurm_content = header + self.task_env(cpu) + execution

        with open(job_file, "w") as slurm_file:
            slurm_file.write(slurm_content)


class JobHelper:
    """Writer class dedicated to write job file regarding scheduler tech."""

    def __init__(self, cfg_resources_file: Path, cfg_resources_name: str):
        """
        Parameters
        ----------
        cfg_resources_file
            file containing resources
        cfg_resources_name
            resource block name in resources file
        """
        self.cfg_resources_file = cfg_resources_file
        self.cfg_resources_name = cfg_resources_name
        self.inventory_jobfile_helper = {
            "pbs": PBSJobHelper(),
            "slurm": SlurmJobHelper(),
        }
        self.avail_schedulers = list(self.inventory_jobfile_helper.keys())
        if self.cfg_resources_file and isinstance(self.cfg_resources_file, str):
            self.cfg_resources_file = Path(cfg_resources_file)
        if self.cfg_resources_file:
            if not Path.exists(self.cfg_resources_file):
                raise FileNotFoundError(
                    f"file {cfg_resources_file}" " does not exists."
                )
            assert cfg_resources_name, (
                "'cfg_resources_file' then " "'cfg_resources_name' is mandatory"
            )
        self.resources = self.parse_resource_file(
            cfg_resources_name, cfg_resources_file
        )

    def force_resource(self, resource_name: str, value: Union[str, int]):
        """Force resource."""
        if resource_name in list(self.resources.keys()):
            self.resources[resource_name] = value
        else:
            raise ValueError(
                f"{resource_name} not in available resources :"
                " '{','.join(list(self.resources.keys()))}'"
            )

    def check_scheduler(self, scheduler_name: str) -> None:
        """Check if the scheduler is available."""
        if scheduler_name not in self.avail_schedulers:
            raise ValueError(
                f"scheduler {scheduler_name} not in : "
                f"{(',').join(self.avail_schedulers)}"
            )

    def write(
        self,
        scheduler: str,
        job_name: str,
        job_file: Path,
        log_out: Path,
        log_err: Path,
        func: Optional[Callable] = None,
        f_kwargs: Optional[dict] = None,
        logger_lvl: Optional[str] = None,
        force_executable: Optional[str] = None,
    ) -> None:
        """Write job file.

        Parameters
        ----------
        job_name
            name of the job
        job_file
            output jobfile to write
        log_out
            standard output file
        log_err
            standard error file
        func
            python function
        f_kwargs
            func's kwargs
        logger_lvl
            logging level
        force_executable
            force cmd line to launch
        """
        self.check_scheduler(scheduler)
        scheduler = self.inventory_jobfile_helper[scheduler]
        scheduler.write(
            job_file,
            job_name,
            self.resources["cpu"],
            self.resources["ram"],
            self.resources["walltime"],
            log_err,
            log_out,
            func,
            f_kwargs,
            logger_lvl,
            self.resources["gpu"],
            self.resources["queue"],
            force_executable,
        )

    def get_task_status(
        self,
        scheduler_name: str,
        log_err: str,
        log_out: str,
        job_id: Optional[str] = None,
    ) -> tuple[str, bool]:
        """Guess tasks status by checking log files or thanks to the job id.

        Parameters
        ----------
        scheduler
            scheduler name (ie: 'pbs' or 'slurm').
        log_err
            error log file.
        log_out
            standard log file.
        job_id
            string containing the job id.

        Notes
        -----
        the returned tuple is formated as the following
        (task_status, killed_worker)
        where task_status is in ("done", "failed", "unlaunchable")
        and killed_worker inform if the PBS scheduler kill the task due
        to resources consumption.
        """
        self.check_scheduler(scheduler_name)
        scheduler = self.inventory_jobfile_helper[scheduler_name]
        return scheduler.get_task_status(log_err, log_out, job_id)

    @classmethod
    def parse_resource_file(cls, step_name, cfg_resources_file):
        """Parse a configuration file dedicated to reserve resources to HPC.
        Notes
        -----
        the returned dictionary is formatted as :
        {"cpu": 1, "ram": "5gb",
         "walltime", "HH:MM:SS", "resource_block_name": "block_name",
         "resource_block_found": True}
        """
        default_ram = "5gb"
        default_cpu = 1
        default_gpu = None
        default_queue = None
        default_walltime = "01:00:00"

        cfg_resources = None
        if cfg_resources_file and os.path.exists(cfg_resources_file):
            cfg_resources = Config(cfg_resources_file).as_dict()

        cfg_resources = None
        if cfg_resources_file and os.path.exists(cfg_resources_file):
            cfg_resources = Config(cfg_resources_file).as_dict()

        resource = {
            "cpu": default_cpu,
            "ram": default_ram,
            "walltime": default_walltime,
            "gpu": default_gpu,
            "queue": default_queue,
            "resource_block_name": step_name,
            "resource_block_found": False,
        }

        if cfg_resources:
            cfg_step_resources = cfg_resources.get(str(step_name), {})
            resource["cpu"] = cfg_step_resources.get("nb_cpu", default_cpu)
            resource["ram"] = cfg_step_resources.get("ram", default_ram)
            resource["walltime"] = cfg_step_resources.get("walltime", default_walltime)
            resource["gpu"] = cfg_step_resources.get("nb_gpu", default_gpu)
            resource["queue"] = cfg_step_resources.get("queue", default_queue)
            resource["resource_block_name"] = step_name
            resource["resource_block_found"] = False
        if cfg_resources:
            resource["resource_block_found"] = step_name in cfg_resources
        return resource
